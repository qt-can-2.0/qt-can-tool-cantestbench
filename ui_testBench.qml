import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;
import "components";

Item {
    id: root;
    onNetworkChanged: {
        Shared.currentNode       = null;
        Shared.highlightIO       = null;
        Shared.highlightSensor   = null;
        Shared.highlightActuator = null;
        Shared.highlightPhyVal   = null;
        if (network) {
            if (network.hideSensorsPanel) {
                panelSensors.collapse ();
            }
            else {
                panelSensors.expand ();
            }
            if (network.hideActuatorsPanel) {
                panelActuators.collapse ();
            }
            else {
                panelActuators.expand ();
            }
            if (network.hideMessagesPanel) {
                panelLogs.collapse ();
            }
            else {
                panelLogs.expand ();
            }
            switch (network.startTab) {
            case Tabs.NODES_IO:
                tabsView.currentTab = tabElecNodes;
                break;
            case Tabs.DASHBOARD:
                tabsView.currentTab = tabDashboard;
                break;
            case Tabs.LOGIC:
                tabsView.currentTab = tabAppLogic;
                break;
            case Tabs.DATASHEET:
                tabsView.currentTab = tabDatasheet;
                break;
            case Tabs.PHYSIC_3D:
                tabsView.currentTab = tabPhysMachine;
                break;
            }
        }
    }

    readonly property NetworkDefinition  network        : (Shared.manager.ready && Shared.manager.hasValidNetwork ? Shared.manager.currentNetwork : null);
    readonly property PhysicalWorld      world          : (Shared.manager.ready && network ? network.world : null);
    readonly property ObjectRefListModel nodesList      : (Shared.manager.ready ? Shared.manager.nodes       : null);
    readonly property ObjectRefListModel sensorsList    : (Shared.manager.ready ? Shared.manager.sensors     : null);
    readonly property ObjectRefListModel actuatorsList  : (Shared.manager.ready ? Shared.manager.actuators   : null);
    readonly property ObjectRefListModel dashboardsList : (Shared.manager.ready ? Shared.manager.dashboards  : null);
    readonly property ObjectRefListModel canBusList     : (Shared.manager.ready ? Shared.manager.canBuses    : null);
    readonly property ObjectRefListModel serialBusList  : (Shared.manager.ready ? Shared.manager.serialBuses : null);

    function singleShot (delay, callback) {
        Components.singleShot.createObject (root, {
                                                "interval" : delay,
                                                "callback" : callback,
                                            });
    }

    QtObject {
        property Window subWinCodeEditor : WindowCodeEditor { id: windowEditor; }
    }
    Connections {
        target: Shared;
        onHighlightIOChanged: {
            if (Shared.highlightIO !== null) {
                tabsView.currentTab = tabElecNodes;
                areaNodes.contentZoom = 2.0; // zoom in before
                singleShot (50, function () {
                    areaNodes.ensureVisible (Shared.manager.dict.getDelegate (Shared.highlightIO.path));
                });
            }
        }
        onHighlightSensorChanged: {
            if (Shared.highlightSensor !== null) {
                panelSensors.expand ();
                singleShot (50, function () {
                    scrollerSensors.ensureVisible (Shared.manager.dict.getDelegate (Shared.highlightSensor.path));
                });
            }
        }
        onHighlightActuatorChanged: {
            if (Shared.highlightActuator !== null) {
                panelActuators.expand ();
                singleShot (50, function () {
                    scrollerActuators.ensureVisible (Shared.manager.dict.getDelegate (Shared.highlightActuator.path));
                });
            }
        }
        onHighlightPhyValChanged: {
            if (Shared.highlightPhyVal !== null) {
                tabsView.currentTab = tabDatasheet;
                comboTable.selectByKey (ObjectFamily.VALUE);
                singleShot (50, function () {
                    scrollerTables.ensureVisible (Shared.manager.dict.getDelegate (Shared.highlightPhyVal.path));
                });
            }
        }
    }
    ToolBar {
        id: toolBar;

        TextButton {
            text: qsTr ("Load definition");
            icon: SvgIconLoader {
                icon: "actions/open";
                color: Style.colorForeground;
            }
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { Components.dlgOpen.createObject (root, { }); }
        }
        SvgIconLoader {
            icon: "actions/info";
            size: Style.iconSize (1.5);
            color: Style.colorForeground;
            visible: Shared.manager.hasValidNetwork;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Column {
            visible: Shared.manager.hasValidNetwork;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            ClickableTextLabel {
                text: (network ? '"' + network.title + '"' : "");
                color: Style.colorForeground;
                font.pixelSize: Style.fontSizeNormal;
                onClicked: { windowEditor.showLineColumnInFile (Shared.manager.currentFileUrl); }
            }
            TextLabel {
                text: {
                    var ret = [];
                    if (Shared.manager.ready) {
                        if (nodesList) {
                            ret.push (qsTr ("%1 nodes").arg (nodesList.count));
                        }
                        if (sensorsList) {
                            ret.push (qsTr ("%1 sensors").arg (sensorsList.count));
                        }
                        if (actuatorsList) {
                            ret.push (qsTr ("%1 actuators").arg (actuatorsList.count));
                        }
                    }
                    return ("(" + ret.join (", ") + ")");
                }
                color: Style.colorBorder;
                font.pixelSize: Style.fontSizeSmall;
            }
        }
        Stretcher { }
        GridContainer {
            cols: capacity;
            visible: Shared.manager.hasValidNetwork;
            capacity: 2;
            colSpacing: Style.spacingNormal;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            TextButton {
                text: (Shared.manager.running ? qsTr ("Pause") : qsTr ("Start"));
                icon: SvgIconLoader {
                    icon: (Shared.manager.running ? "actions/pause" : "actions/start");
                }
                onClicked: {
                    if (Shared.manager.running) {
                        Shared.manager.pause ();
                    }
                    else {
                        Shared.manager.start ();
                    }
                }
            }
            TextButton {
                text: qsTr ("Reset");
                icon: SvgIconLoader {
                    icon: "actions/stop";
                }
                onClicked: {
                    Shared.manager.reset ();
                }
            }
        }
        Stretcher { }
        GridContainer {
            cols: capacity;
            visible: Shared.manager.hasValidNetwork;
            capacity: 3;
            colSpacing: Style.spacingNormal;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            TextButton {
                text: qsTr ("Import state");
                icon: SvgIconLoader {
                    icon: "actions/import";
                    color: Style.colorForeground;
                }
                onClicked: { Components.dlgImport.createObject (root, { }); }
            }
            TextButton {
                text: qsTr ("Export state");
                icon: SvgIconLoader {
                    icon: "actions/export";
                    color: Style.colorForeground;
                }
                onClicked: { Components.dlgExport.createObject (root, { }); }
            }
            TextButton {
                text: qsTr ("Print as HTML");
                icon: SvgIconLoader { icon: "devices/printer"; }
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                onClicked: { Components.dlgPrintLocation.createObject (root, { }); }
            }
        }
    }
    StatusBar {
        id: statusBar;

        TextLabel {
            text: qsTr ("Show :");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextButton {
            id: btnToggleLogs;
            text: qsTr ("Logs panel");
            checked: panelLogs.expanded;
            padding: Style.spacingNormal;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: {
                if (panelLogs.expanded) {
                    panelLogs.collapse ();
                }
                else {
                    panelLogs.expand ();
                }
            }
        }
        TextButton {
            id: btnToggleDescriptions;
            text: qsTr ("Descriptions");
            padding: Style.spacingNormal;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { Shared.showDescriptions = !checked; }

            Binding on checked { value: Shared.showDescriptions; }
        }
        Line {
            implicitHeight: Style.fontSizeNormal;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: qsTr ("Bus :");
            visible: Shared.manager.hasValidNetwork;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Repeater {
            model: canBusList;
            delegate: DelegateCanBus {
                canBus: modelData;
                onClicked: {
                    Components.dlgConfCanBus.createObject (root, {
                                                               "canBus" : canBus,
                                                           });
                }
            }
        }
        Repeater {
            model: serialBusList;
            delegate: DelegateSerialBus {
                serialBus: modelData;
                onClicked: {
                    Components.dlgConfSerialBus.createObject (root, {
                                                                  "serialBus" : serialBus,
                                                              });
                }
            }
        }
        Stretcher { }
        TextLabel {
            text: qsTr ("Theme :");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextButton {
            id: btnToggleTheme;
            backColor: (Style.useDarkTheme ? "darkblue" : "skyblue");
            textColor: (Style.useDarkTheme ? "white"    : "yellow");
            icon: SvgIconLoader {
                icon: (Style.useDarkTheme ? "others/moon" : "others/sun");
            }
            padding: (Style.lineSize * 2);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { Style.useDarkTheme = !Style.useDarkTheme; }
        }
    }
    Item {
        id: workspace;
        anchors {
            top: toolBar.bottom;
            bottom: (panelLogs.visible ? panelLogs.top : statusBar.top);
        }
        ExtraAnchors.horizontalFill: parent;

        Item {
            id: centralArea;
            anchors.fill: parent;

            PanelContainer {
                id: panelSensors;
                size: Style.realPixels (340);
                title: qsTr ("Sensors");
                icon: SvgIconLoader {
                    icon: "qrc:///icons/sensor.svg";
                    color: Style.colorForeground;
                }
                resizable: false;
                detachable: false;
                collapsable: true;
                borderSide: Item.Right;
                ExtraAnchors.leftDock: parent;

                ToolBar {
                    id: stripSensors;
                    gradient: Style.gradientEditable (Style.colorWindow);

                    TextBox {
                        id: inputFilterSensors;
                        hasClear: true;
                        textHolder: qsTr ("Filter...");
                        implicitWidth: -1;

                        function contains (str) {
                            return (str.trim ().toLowerCase ().indexOf (text.trim ().toLowerCase ()) >= 0);
                        }
                    }
                }
                ScrollContainer {
                    id: scrollerSensors;
                    showBorder: false;
                    anchors.fill: parent;
                    anchors.topMargin: stripSensors.height;

                    Flickable {
                        id: flickerSensors;
                        contentHeight: layoutSensors.height;
                        flickableDirection: Flickable.VerticalFlick;

                        StretchColumnContainer {
                            id: layoutSensors;
                            ExtraAnchors.topDock: parent;

                            Repeater {
                                model: sensorsList;
                                delegate: Item {
                                    id: delegateSensor;
                                    visible: (inputFilterSensors.isEmpty
                                              || inputFilterSensors.contains (delegateSensor.sensor.uid)
                                              || inputFilterSensors.contains (delegateSensor.sensor.title));
                                    implicitHeight: (layoutSensorInfo.height + layoutSensorInfo.anchors.margins * 2);
                                    Component.onCompleted: { Shared.manager.setDelegateForPath (sensor.path, delegateSensor); }
                                    onHeightChanged: {
                                        if (current) {
                                            scrollerSensors.ensureVisible (delegateSensor);
                                        }
                                    }
                                    onCurrentChanged: {
                                        if (current) {
                                            if (!layoutSensorValControl.visible) {
                                                layoutSensorValControl.visible = true;
                                            }
                                        }
                                    }

                                    readonly property bool current : (sensor === Shared.highlightSensor);

                                    readonly property AbstractSensor sensor : model.object;

                                    Rectangle {
                                        color: (modelData === Shared.highlightSensor
                                                ? Style.colorHighlight
                                                : (model.index % 2
                                                   ? Style.colorWindow
                                                   : Style.colorSecondary));
                                        opacity: 0.35;
                                        anchors.fill: parent;
                                    }
                                    Line { ExtraAnchors.bottomDock: parent; }
                                    Column {
                                        id: layoutSensorInfo;
                                        spacing: Style.spacingSmall;
                                        anchors {
                                            margins: Style.spacingNormal;
                                            verticalCenter: (parent ? parent.verticalCenter : undefined);
                                        }
                                        ExtraAnchors.horizontalFill: parent;

                                        MouseArea {
                                            id: clickerSensor;
                                            height: implicitHeight;
                                            hoverEnabled: true;
                                            implicitHeight: layoutSensorHeader.implicitHeight;
                                            ExtraAnchors.horizontalFill: parent;
                                            onClicked: {
                                                if (layoutSensorValControl.visible) {
                                                    layoutSensorValControl.visible = false;
                                                    Shared.highlightSensor = null;
                                                }
                                                else {
                                                    Shared.highlightSensor = sensor;
                                                    layoutSensorValControl.visible = true;
                                                }
                                            }

                                            StretchRowContainer {
                                                id: layoutSensorHeader;
                                                spacing: Style.spacingSmall;
                                                ExtraAnchors.horizontalFill: parent;

                                                Column {
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                                    TextLabel {
                                                        id: lblSensorName;
                                                        text: delegateSensor.sensor.uid;
                                                        visible: !lblSensorTitle.visible;
                                                    }
                                                    TextLabel {
                                                        id: lblSensorTitle;
                                                        text: delegateSensor.sensor.title;
                                                        visible: (text !== "" && clickerSensor.containsMouse);
                                                    }
                                                }
                                                Stretcher { }
                                                InstanceCreator {
                                                    visible: !layoutSensorValControl.visible;
                                                    component: {
                                                        switch (delegateSensor.sensor.type) {
                                                        case ObjectType.ANALOG:
                                                            return Components.delegateAnalogSensorMini;
                                                        case ObjectType.DIGITAL:
                                                            return Components.delegateDigitalSensorMini;
                                                        case ObjectType.HYBRID:
                                                            return Components.delegateHybridSensorMini;
                                                        default:
                                                            return null;
                                                        }
                                                    }
                                                    properties: ({ "sensor" : delegateSensor.sensor });
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                            }
                                        }
                                        InstanceCreator {
                                            visible: !layoutSensorValControl.visible;
                                            component: {
                                                switch (delegateSensor.sensor.type) {
                                                case ObjectType.ANALOG:
                                                    return Components.delegateAnalogSensorWide;
                                                case ObjectType.DIGITAL:
                                                case ObjectType.HYBRID:
                                                default:
                                                    return null;
                                                }
                                            }
                                            properties: ({ "sensor" : delegateSensor.sensor });
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                        Balloon {
                                            visible: (Shared.showDescriptions && content !== "");
                                            content: (delegateSensor.sensor ? delegateSensor.sensor.description : "");
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                        InstanceCreator {
                                            id: layoutSensorValControl;
                                            visible: false;
                                            component: {
                                                switch (delegateSensor.sensor.type) {
                                                case ObjectType.ANALOG:
                                                    return Components.delegateAnalogSensor;
                                                case ObjectType.DIGITAL:
                                                    return Components.delegateDigitalSensor;
                                                case ObjectType.HYBRID:
                                                    return Components.delegateHybridSensor;
                                                default:
                                                    return null;
                                                }
                                            }
                                            properties: ({ "sensor" : delegateSensor.sensor });
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            PanelContainer {
                id: panelActuators;
                size: Style.realPixels (340);
                title: qsTr ("Actuators");
                icon: SvgIconLoader {
                    icon: "qrc:///icons/actuator.svg";
                    color: Style.colorForeground;
                }
                resizable: false;
                detachable: false;
                collapsable: true;
                borderSide: Item.Left;
                ExtraAnchors.rightDock: parent;

                ToolBar {
                    id: stripActuators;
                    gradient: Style.gradientEditable (Style.colorWindow);

                    TextBox {
                        id: inputFilterActuators;
                        hasClear: true;
                        textHolder: qsTr ("Filter...");
                        implicitWidth: -1;

                        function contains (str) {
                            return (str.trim ().toLowerCase ().indexOf (text.trim ().toLowerCase ()) >= 0);
                        }
                    }
                }
                ScrollContainer {
                    id: scrollerActuators;
                    showBorder: false;
                    anchors.fill: parent;
                    anchors.topMargin: stripActuators.height;

                    Flickable {
                        id: flickerActuators;
                        contentHeight: layoutActuators.height;
                        flickableDirection: Flickable.VerticalFlick;

                        StretchColumnContainer {
                            id: layoutActuators;
                            ExtraAnchors.topDock: parent;

                            Repeater {
                                model: actuatorsList;
                                delegate: Item {
                                    id: delegateActuator;
                                    visible: (inputFilterActuators.isEmpty
                                              || inputFilterActuators.contains (delegateActuator.actuator.uid)
                                              || inputFilterActuators.contains (delegateActuator.actuator.title));
                                    implicitHeight: (layoutActuatorsInfo.height + layoutActuatorsInfo.anchors.margins * 2);
                                    Component.onCompleted: { Shared.manager.setDelegateForPath (actuator.path, delegateActuator); }
                                    onHeightChanged: {
                                        if (current) {
                                            scrollerActuators.ensureVisible (delegateActuator);
                                        }
                                    }
                                    onCurrentChanged: {
                                        if (current) {
                                            if (!layoutActuatorValControl.visible) {
                                                layoutActuatorValControl.visible = true;
                                            }
                                        }
                                    }

                                    readonly property bool current : (actuator === Shared.highlightActuator);

                                    readonly property AbstractActuator actuator : model.object;

                                    Rectangle {
                                        color: (modelData === Shared.highlightActuator
                                                ? Style.colorHighlight
                                                : (model.index % 2
                                                   ? Style.colorWindow
                                                   : Style.colorSecondary));
                                        opacity: 0.35;
                                        anchors.fill: parent;
                                    }
                                    Line { ExtraAnchors.bottomDock: parent; }
                                    Column {
                                        id: layoutActuatorsInfo;
                                        spacing: Style.spacingSmall;
                                        anchors {
                                            margins: Style.spacingNormal;
                                            verticalCenter: (parent ? parent.verticalCenter : undefined);
                                        }
                                        ExtraAnchors.horizontalFill: parent;

                                        MouseArea {
                                            id: clickerActuator;
                                            height: implicitHeight;
                                            hoverEnabled: true;
                                            implicitHeight: layoutActuatorHeader.implicitHeight;
                                            ExtraAnchors.horizontalFill: parent;
                                            onClicked: {
                                                if (layoutActuatorValControl.visible) {
                                                    layoutActuatorValControl.visible = false;
                                                    Shared.highlightActuator = null;
                                                }
                                                else {
                                                    Shared.highlightActuator = actuator;
                                                    layoutActuatorValControl.visible = true;
                                                }
                                            }

                                            StretchRowContainer {
                                                id: layoutActuatorHeader;
                                                spacing: Style.spacingSmall;
                                                ExtraAnchors.horizontalFill: parent;

                                                Column {
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                                    TextLabel {
                                                        id: lblActuatorName;
                                                        text: delegateActuator.actuator.uid;
                                                        visible: !lblActuatorTitle.visible;
                                                    }
                                                    TextLabel {
                                                        id: lblActuatorTitle;
                                                        text: delegateActuator.actuator.title;
                                                        visible: (text !== "" && clickerActuator.containsMouse);
                                                    }
                                                }
                                                Stretcher { }
                                                InstanceCreator {
                                                    visible: !layoutActuatorValControl.visible;
                                                    component: {
                                                        switch (delegateActuator.actuator.type) {
                                                        case ObjectType.ANALOG:
                                                            return Components.delegateAnalogActuatorMini;
                                                        case ObjectType.DIGITAL:
                                                            return Components.delegateDigitalActuatorMini;
                                                        case ObjectType.HYBRID:
                                                            return Components.delegateHybridActuatorMini;
                                                        default:
                                                            return null;
                                                        }
                                                    }
                                                    properties: ({ "actuator" : delegateActuator.actuator });
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                            }
                                        }
                                        InstanceCreator {
                                            visible: !layoutActuatorValControl.visible;
                                            component: {
                                                switch (delegateActuator.actuator.type) {
                                                case ObjectType.ANALOG:
                                                    return Components.delegateAnalogActuatorWide;
                                                case ObjectType.DIGITAL:
                                                case ObjectType.HYBRID:
                                                default:
                                                    return null;
                                                }
                                            }
                                            properties: ({ "actuator" : delegateActuator.actuator });
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                        Balloon {
                                            visible: (Shared.showDescriptions && content !== "");
                                            content: (delegateActuator.actuator ? delegateActuator.actuator.description : "");
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                        InstanceCreator {
                                            id: layoutActuatorValControl;
                                            visible: false;
                                            component: {
                                                switch (delegateActuator.actuator.type) {
                                                case ObjectType.ANALOG:
                                                    return Components.delegateAnalogActuator;
                                                case ObjectType.DIGITAL:
                                                    return Components.delegateDigitalActuator;
                                                case ObjectType.HYBRID:
                                                    return Components.delegateHybridActuator;
                                                default:
                                                    return null;
                                                }
                                            }
                                            properties: ({ "actuator" : delegateActuator.actuator });
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            TabBar {
                id: tabsView;
                tabsSize: (Style.iconSize (1) + Style.spacingNormal * 2);
                currentTab: tabElecNodes;
                anchors {
                    left: panelSensors.right;
                    right: panelActuators.left;
                }
                ExtraAnchors.verticalFill: parent;

                Group {
                    id: tabDatasheet;
                    title: qsTr ("Tables");
                    icon: SvgIconLoader {
                        icon: "actions/view-list";
                        color: Style.colorForeground;
                    }

                    ToolBar {
                        id: stripBarTables;
                        gradient: Style.gradientEditable (Style.colorWindow);

                        TextLabel {
                            text: qsTr ("View :");
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                        ComboList {
                            id: comboTable;
                            model: [
                                { "key" : ObjectFamily.VALUE, "value" : qsTr ("Physical values") },
                                { "key" : ObjectFamily.IO,    "value" : qsTr ("I/O values")      },
                            ];
                            delegate: ComboListDelegateForModelDataAttributes {
                                attributeKey: "key";
                                attributeValue: "value";
                            }
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                            Component.onCompleted: { selectByKey (ObjectFamily.VALUE); }
                        }
                        Stretcher { }
                        TextButton {
                            id: toggleShowOnlyEditable;
                            text: qsTr ("Show only editable");
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                            onClicked: { Shared.showOnlyEditable = !checked; }

                            Binding on checked { value: Shared.showOnlyEditable; }
                        }
                    }
                    ScrollContainer {
                        id: scrollerTables;
                        showBorder: false;
                        anchors.fill: parent;
                        anchors.topMargin: stripBarTables.height;

                        Flickable {
                            contentHeight: (layoutTable.height + layoutTable.anchors.margins * 2);
                            flickableDirection: Flickable.VerticalFlick;

                            StretchColumnContainer {
                                id: layoutTable;
                                spacing: Style.spacingNormal;
                                anchors.margins: Style.spacingNormal;
                                ExtraAnchors.topDock: parent;

                                StretchColumnContainer {
                                    spacing: parent.spacing;
                                    visible: (comboTable.currentKey === ObjectFamily.IO);

                                    Repeater {
                                        model: nodesList;
                                        delegate: InstanceCreator {
                                            component: Components.delegateRawNodeTable;
                                            properties: ({ "node" : model.object });
                                        }
                                    }
                                }
                                StretchColumnContainer {
                                    spacing: parent.spacing;
                                    visible: (comboTable.currentKey === ObjectFamily.VALUE);

                                    Repeater {
                                        model: (Shared.manager.ready && network ? network.subObjects : 0);
                                        delegate: InstanceCreator {
                                            id: creator;
                                            component: {
                                                switch (modelData ["family"]) {
                                                case ObjectFamily.VALUE:  return Components.delegatePhyVal;
                                                case ObjectFamily.GROUP:  return Components.delegatePhyGroup;
                                                case ObjectFamily.BLOCK:  return Components.delegatePhyBlock;
                                                case ObjectFamily.MARKER: return Components.delegatePhyMarker;
                                                default:                  return null;
                                                }
                                            }
                                            properties: {
                                                switch (modelData ["family"]) {
                                                case ObjectFamily.VALUE:  return ({ "phyVal" : modelData });
                                                case ObjectFamily.GROUP:  return ({ "group"  : modelData });
                                                case ObjectFamily.BLOCK:  return ({ "block"  : modelData });
                                                case ObjectFamily.MARKER: return ({ "marker" : modelData });
                                                default:                  return ({});
                                                }
                                            }

                                            Connections {
                                                target: creator.instance;
                                                onNeedDetails: {
                                                    Components.dlgPhyValDetails.createObject (tabDatasheet, {
                                                                                                  "object" : phyVal,
                                                                                              });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Group {
                    id: tabElecNodes;
                    title: qsTr ("I/O Nodes");
                    icon: SvgIconLoader {
                        icon: "qrc:///icons/circuit.svg";
                        color: Style.colorForeground;
                    }

                    ToolBar {
                        id: stripBarElecNodes;
                        gradient: Style.gradientEditable (Style.colorWindow);

                        TextLabel {
                            text: qsTr ("Node :");
                            visible: nodesList;
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                        Stretcher {
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                            Row {
                                id: layoutNodesButtons;
                                spacing: Style.spacingSmall;
                                opacity: (width <= parent.width ? 1 : 0);
                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                Repeater {
                                    model: nodesList;
                                    delegate: TextButton {
                                        text: (node ? node.uid : "");
                                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                        onClicked: { areaNodes.fit (Shared.manager.dict.getDelegate (node.path)); }

                                        readonly property Node node : model.object;
                                    }
                                }
                            }
                            Row {
                                id: layoutNodesCombo;
                                spacing: Style.spacingNormal;
                                visible: (1 - layoutNodesButtons.opacity);
                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                ComboList {
                                    id: comboJumpToNode;
                                    model: nodesList;
                                    visible: nodesList;
                                    delegate: ComboListDelegate {
                                        key: (node ? node.path : "");
                                        value: (node ? node.uid : "");
                                        width: implicitWidth;
                                        height: implicitHeight;
                                        implicitWidth: lbl.contentWidth;
                                        implicitHeight: lbl.contentHeight;

                                        readonly property Node node : (model ? model.object : null);

                                        readonly property alias label : lbl;

                                        TextLabel {
                                            id: lbl;
                                            text: parent.value;
                                            emphasis: parent.active;
                                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                        }
                                    }
                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                }
                                TextButton {
                                    text: qsTr ("Show");
                                    visible: nodesList;
                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    onClicked: { areaNodes.fit (Shared.manager.dict.getDelegate (comboJumpToNode.currentKey)); }
                                }
                            }
                        }
                        TextButton {
                            id: btnShowAll;
                            text: qsTr ("Show all");
                            visible: nodesList;
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                            onClicked: { areaNodes.fit (layoutNetwork); }
                        }
                    }
                    Rectangle {
                        id: headerNetwork;
                        height: implicitHeight;
                        gradient: Style.gradientShaded (Style.colorWindow, Style.colorEditable);
                        implicitHeight: (Style.iconSize (1) + Style.spacingNormal * 2);
                        ExtraAnchors.topDock: areaNodes;
                    }
                    Rectangle {
                        color: Style.colorEditable;
                        anchors.top: headerNetwork.bottom;
                        ExtraAnchors.bottomDock: areaNodes;
                    }
                    ZoomAndMoveArea {
                        id: areaNodes;
                        contentPadding: (Style.spacingBig * 10);
                        contentZoomMin: 0.5;
                        contentZoomMax: 3;
                        anchors.fill: parent;
                        anchors.topMargin: stripBarElecNodes.height;

                        function fit (item) {
                            if (item && item.width > 0 && item.height > 0) {
                                var ratioX = ((areaNodes.width  - Style.spacingBig * 2) / item.width);
                                var ratioY = ((areaNodes.height - Style.spacingBig * 2) / item.height);
                                contentZoom = Math.min (ratioX, ratioY);
                                ensureVisible (item);
                            }
                        }

                        Column {
                            id: layoutNetwork;
                            spacing: Style.spacingBig;

                            Repeater {
                                model: nodesList;
                                delegate: Item {
                                    id: delegateNode;
                                    implicitWidth:  (layoutNode.width  + layoutNode.anchors.margins * 2);
                                    implicitHeight: (layoutNode.height + layoutNode.anchors.margins * 2);
                                    Component.onCompleted: { Shared.manager.setDelegateForPath (node.path, delegateNode); }

                                    readonly property Node node : model.object;

                                    Rectangle {
                                        color: Style.opacify (Style.colorSecondary, (delegateNode.node.mode === Modes.INACTIVE ? 0.35 : 1.0));
                                        radius: Style.roundness;
                                        antialiasing: radius;
                                        border {
                                            width: Style.lineSize;
                                            color: Style.colorBorder;
                                        }
                                        anchors.fill: parent;
                                    }
                                    StretchColumnContainer {
                                        id: layoutNode;
                                        spacing: Style.spacingNormal;
                                        anchors.margins: Style.spacingNormal;
                                        ExtraAnchors.topLeftCorner: parent;

                                        StretchRowContainer {
                                            spacing: Style.spacingNormal;

                                            ClickableTextLabel {
                                                text: delegateNode.node.uid;
                                                color: (delegateNode.node.mode === Modes.INACTIVE ? Style.colorBorder : Style.colorLink);
                                                emphasis: true;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                onClicked: { areaNodes.fit (delegateNode); }
                                            }
                                            TextLabel {
                                                text: ("(" + delegateNode.node.title + ")");
                                                color: (delegateNode.node.mode === Modes.INACTIVE ? Style.colorBorder : Style.colorForeground);
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            Stretcher { }
                                            ComboList {
                                                model: [
                                                    { "key" : Modes.ACTIVE,   "value" : qsTr ("Active") },
                                                    { "key" : Modes.PASSIVE,  "value" : qsTr ("Passive") },
                                                    { "key" : Modes.INACTIVE, "value" : qsTr ("Inactive") },
                                                ];
                                                delegate: ComboListDelegateForModelDataAttributes {
                                                    attributeKey: "key";
                                                    attributeValue: "value";
                                                }
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                onCurrentKeyChanged: {
                                                    if (currentKey !== undefined) {
                                                        delegateNode.node.mode = currentKey;
                                                    }
                                                }
                                                Component.onCompleted: { selectByKey (delegateNode.node.mode); }
                                            }
                                        }
                                        Balloon {
                                            visible: (Shared.showDescriptions && content !== "");
                                            content: (delegateNode.node ? delegateNode.node.description : "");
                                            ExtraAnchors.horizontalFill: parent;
                                        }
                                        Line { }
                                        Row {
                                            spacing: Style.spacingNormal;

                                            Repeater {
                                                model: (delegateNode.node ? delegateNode.node.boards : 0);
                                                delegate: Item {
                                                    id: delegateBoard;
                                                    implicitWidth: (layoutBoard.width + layoutBoard.anchors.margins * 2);
                                                    implicitHeight: (layoutBoard.height + layoutBoard.anchors.margins * 2);
                                                    Component.onCompleted: { Shared.manager.setDelegateForPath (board.path, delegateBoard); }

                                                    readonly property Board board : model.object;

                                                    Rectangle {
                                                        color: Style.opacify (Style.colorClickable, (delegateNode.node.mode === Modes.INACTIVE ? 0.35 : 1.0));
                                                        border {
                                                            width: Style.lineSize;
                                                            color: Style.colorBorder;
                                                        }
                                                        anchors.fill: parent;
                                                    }
                                                    StretchColumnContainer {
                                                        id: layoutBoard;
                                                        spacing: Style.spacingSmall;
                                                        anchors.margins: Style.spacingSmall;
                                                        ExtraAnchors.topLeftCorner: parent;

                                                        StretchRowContainer {
                                                            spacing: Style.spacingNormal;

                                                            TextLabel {
                                                                text: delegateBoard.board.uid;
                                                                color: Style.colorBorder;
                                                                font.italic: true;
                                                                font.pixelSize: Style.fontSizeSmall;
                                                            }
                                                            TextLabel {
                                                                text: ("(" + delegateBoard.board.title + ")");
                                                                color: Style.colorBorder;
                                                                font.pixelSize: Style.fontSizeSmall;
                                                            }
                                                        }
                                                        Repeater {
                                                            model: (delegateBoard.board ? delegateBoard.board.ios : 0);
                                                            delegate: StretchRowContainer {
                                                                id: delegateIo;
                                                                spacing: Style.spacingSmall;
                                                                Component.onCompleted: { Shared.manager.setDelegateForPath (io.path, delegateIo); }

                                                                readonly property AbstractIO io : model.object;

                                                                TextLabel {
                                                                    text: delegateIo.io.uid;
                                                                    color: (delegateNode.node.mode === Modes.INACTIVE
                                                                            ? Style.colorBorder
                                                                            : Style.colorForeground);
                                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                                                    MouseArea {
                                                                        anchors.fill: parent;
                                                                        onClicked: {
                                                                            var ret = null;
                                                                            switch (delegateIo.io.type) {
                                                                            case ObjectType.ANALOG:
                                                                                switch (delegateIo.io.direction) {
                                                                                case ObjectDirection.INPUT:
                                                                                    ret = Components.dlgAinDetails;
                                                                                    break;
                                                                                case ObjectDirection.OUTPUT:
                                                                                    ret = Components.dlgAoutDetails;
                                                                                    break;
                                                                                }
                                                                                break;
                                                                            case ObjectType.DIGITAL:
                                                                                switch (delegateIo.io.direction) {
                                                                                case ObjectDirection.INPUT:
                                                                                    ret = Components.dlgDinDetails;
                                                                                    break;
                                                                                case ObjectDirection.OUTPUT:
                                                                                    ret = Components.dlgDoutDetails;
                                                                                    break;
                                                                                }
                                                                                break;
                                                                            }
                                                                            ret.createObject (tabElecNodes, {
                                                                                                  "object" : delegateIo.io
                                                                                              });
                                                                        }
                                                                    }
                                                                    Rectangle {
                                                                        z: -1;
                                                                        color: Style.colorHighlight;
                                                                        radius: Style.roundness;
                                                                        visible: (Shared.highlightIO === modelData);
                                                                        antialiasing: radius;
                                                                        anchors {
                                                                            fill: parent;
                                                                            margins: -Style.lineSize;
                                                                        }
                                                                    }
                                                                }
                                                                Stretcher { }
                                                                InstanceCreator {
                                                                    id: loaderIo;
                                                                    properties: ({ "io" : delegateIo.io });
                                                                    component: {
                                                                        switch (delegateIo.io.type) {
                                                                        case ObjectType.ANALOG:
                                                                            switch (delegateIo.io.direction) {
                                                                            case ObjectDirection.INPUT:
                                                                                return Components.delegateAin;
                                                                            case ObjectDirection.OUTPUT:
                                                                                return Components.delegateAout;
                                                                            }
                                                                            return null;
                                                                        case ObjectType.DIGITAL:
                                                                            switch (delegateIo.io.direction) {
                                                                            case ObjectDirection.INPUT:
                                                                                return Components.delegateDin;
                                                                            case ObjectDirection.OUTPUT:
                                                                                return Components.delegateDout;
                                                                            }
                                                                            return null;
                                                                        }
                                                                        return null;
                                                                    }
                                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Group {
                    id: tabAppLogic;
                    title: qsTr ("Logic");
                    icon: SvgIconLoader {
                        icon: "qrc:///icons/logic.svg";
                        color: Style.colorForeground;
                    }

                    GridContainer {
                        cols: 1;
                        capacity: 2;
                        anchors.fill: parent;

                        ScrollContainer {
                            id: scrollerRoutines;
                            showBorder: false;
                            headerItem: Item {
                                height: implicitHeight;
                                implicitHeight: (Style.iconSize (1) + Style.spacingNormal * 2);
                                ExtraAnchors.horizontalFill: parent;

                                Row {
                                    spacing: Style.spacingNormal;
                                    anchors.centerIn: parent;

                                    SvgIconLoader {
                                        icon: "qrc:///icons/routine.svg";
                                        color: Style.colorForeground;
                                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                    TextLabel {
                                        text: qsTr ("Routines");
                                        font.pixelSize: Style.fontSizeBig;
                                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                }
                            }

                            Flickable {
                                id: flickerRoutine;
                                contentHeight: (layoutRoutine.height + layoutRoutine.anchors.margins * 2);
                                flickableDirection: Flickable.VerticalFlick;

                                Column {
                                    id: layoutRoutine;
                                    spacing: Style.spacingNormal;
                                    anchors.margins: Style.spacingNormal;
                                    ExtraAnchors.topDock: parent;

                                    Repeater {
                                        model: nodesList;
                                        delegate: ExpandableGroup {
                                            id: delegateGroup;
                                            visible: (repeaterRoutines.count > 0);
                                            uidLabel {
                                                text: (delegateGroup.node ? delegateGroup.node.uid : "");
                                                emphasis: true;
                                            }
                                            titleLabel {
                                                text: (delegateGroup.node ? "(" + delegateGroup.node.title + ")" : "");
                                            }
                                            ExtraAnchors.horizontalFill: parent;

                                            readonly property Node node : model.object;

                                            Repeater {
                                                id: repeaterRoutines;
                                                model: delegateGroup.node.routines;
                                                delegate: MouseArea {
                                                    id: delegateRoutine;
                                                    implicitHeight: (loaderRoutineDelegate.height + loaderRoutineDelegate.anchors.margins * 2);
                                                    ExtraAnchors.horizontalFill: parent;
                                                    onClicked: { Shared.currentNode = delegateGroup.node; }
                                                    Component.onCompleted: { Shared.manager.setDelegateForPath (routine.path, delegateRoutine); }

                                                    readonly property AbstractRoutine routine : model.object;

                                                    BorderedBackground {
                                                        alternate: !delegateGroup.alternate;
                                                        anchors.fill: parent;
                                                    }
                                                    InstanceCreator {
                                                        id: loaderRoutineDelegate;
                                                        properties: ({ "routine" : delegateRoutine.routine });
                                                        component: {
                                                            switch (delegateRoutine.routine.triggerType) {
                                                            case Triggers.TIMER_TICK:
                                                                return Components.delegateRoutineOnTimer;
                                                            case Triggers.EVENT_SIGNAL:
                                                                return Components.delegateRoutineOnEvent;
                                                            case Triggers.CAN_FRAME_RECV:
                                                                return Components.delegateRoutineOnCanFrame;
                                                            case Triggers.SERIAL_FRAME_RECV:
                                                                return Components.delegateRoutineOnSerialFrame;
                                                            case Triggers.CANOPEN_OBD_VAL_CHANGE:
                                                                return Components.delegateRoutineOnCanOpenObdValChange;
                                                            case Triggers.CANOPEN_STATE_CHANGE:
                                                                return Components.delegateRoutineOnCanOpenNmtStateChange;
                                                            case Triggers.CANOPEN_SDO_READ_REPLY:
                                                                return Components.delegateRoutineOnCanOpenSdoReadReply;
                                                            case Triggers.CANOPEN_SDO_WRITE_REQUEST:
                                                                return Components.delegateRoutineOnCanOpenSdoWriteRequest;
                                                            case Triggers.CANOPEN_BOOTUP:
                                                                return Components.delegateRoutineOnCanOpenBootUp;
                                                            case Triggers.CANOPEN_HB_CONSUMER:
                                                                return Components.delegateRoutineOnCanOpenHeartbeatConsumer;
                                                            }
                                                            return null;
                                                        }
                                                        anchors {
                                                            margins: Style.spacingNormal;
                                                            verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                        }
                                                        ExtraAnchors.horizontalFill: parent;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ScrollContainer {
                            id: scrollerMemory;
                            placeholder: (Shared.currentNode === null
                                          ? qsTr ("Click a node to see its memory...")
                                          : (repeaterVars.count === 0
                                             ? qsTr ("This node has no memory.")
                                             : ""));
                            showBorder: false;
                            headerItem: Item {
                                height: implicitHeight;
                                implicitHeight: (Style.iconSize (1) + Style.spacingNormal * 2);
                                ExtraAnchors.horizontalFill: parent;

                                Row {
                                    spacing: Style.spacingNormal;
                                    anchors.centerIn: parent;

                                    SvgIconLoader {
                                        icon: "qrc:///icons/memory.svg";
                                        color: Style.colorForeground;
                                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                    TextLabel {
                                        text: qsTr ("Memory of node %1").arg (Shared.currentNode ? Shared.currentNode.uid : "");
                                        font.pixelSize: Style.fontSizeBig;
                                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                }
                            }

                            Flickable {
                                id: flickerMemory;
                                contentHeight: (layoutMemory.height + layoutMemory.anchors.margins * 2);
                                flickableDirection: Flickable.VerticalFlick;

                                StretchColumnContainer {
                                    id: layoutMemory;
                                    ExtraAnchors.topDock: parent;

                                    Repeater {
                                        id: repeaterVars;
                                        model: (Shared.manager.ready && Shared.currentNode ? Shared.currentNode.memory.varsModel : 0);
                                        delegate: Item {
                                            id: delegateVar;
                                            implicitHeight: (layoutVar.height + layoutVar.anchors.margins * 2);

                                            Rectangle {
                                                color: (model.index % 2 ? Style.colorWindow : Style.colorEditable);
                                                opacity: 0.35;
                                                anchors.fill: parent;
                                            }
                                            StretchRowContainer {
                                                id: layoutVar;
                                                spacing: Style.spacingBig;
                                                anchors.margins: Style.spacingNormal;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                ExtraAnchors.horizontalFill: parent;

                                                TextLabel {
                                                    text: model.path;
                                                    emphasis: true;
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                                TextLabel {
                                                    text: ("(" + model.type + ")");
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                                Stretcher { }
                                                TextLabel {
                                                    text: model.value;
                                                    font.pixelSize: Style.fontSizeBig;
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                                TextLabel {
                                                    text: "=";
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                }
                                                TextLabel {
                                                    text: model.hex;
                                                    font.family: Style.fontFixedName;
                                                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                                                    Rectangle {
                                                        z: -1;
                                                        color: Style.colorNone;
                                                        radius: Style.roundness;
                                                        antialiasing: radius;
                                                        border {
                                                            width: Style.lineSize;
                                                            color: Style.colorBorder;
                                                        }
                                                        anchors {
                                                            fill: parent;
                                                            margins: (Style.lineSize * -2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Line {
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        ExtraAnchors.horizontalFill: parent;
                    }
                }
                Group {
                    id: tabPhysMachine;
                    title: qsTr ("Physics");
                    icon: SvgIconLoader {
                        icon: "qrc:///icons/3d.svg";
                        color: Style.colorForeground;
                    }

                    GridContainer {
                        id: gridViews;
                        cols: (((width > height) && (width / 2 >= 450)) ? capacity : 1);
                        capacity: 2;
                        anchors.fill: parent;

                        Repeater {
                            id: repeaterViews;
                            model: [Sides.RIGHT, Sides.TOP];
                            delegate: Item {
                                id: delegate3dView;

                                function detach () {
                                    if (!detachedWindow) {
                                        detachedWindow = compoDetachedWindow.createObject (null, {
                                                                                               "width"  : delegate3dView.width,
                                                                                               "height" : delegate3dView.height,
                                                                                           });
                                        layout3dView.parent = detachedWindow.contentItem;
                                    }
                                }

                                function attach () {
                                    layout3dView.parent = delegate3dView;
                                    if (detachedWindow) {
                                        detachedWindow.destroy ();
                                        detachedWindow = null;
                                    }
                                }

                                property Window detachedWindow : null;

                                Component {
                                    id: compoDetachedWindow;

                                    Window {
                                        color: root.color;
                                        title: (root.title + " (detached 3D view)");
                                        visible: true;
                                        onClosing: { delegate3dView.attach (); }

                                        WindowIconHelper {
                                            iconPath: ":/vcan_testbench_logo.svg";
                                        }
                                    }
                                }
                                Column {
                                    spacing: Style.spacingBig;
                                    visible: delegate3dView.detachedWindow;
                                    anchors.centerIn: parent;

                                    TextLabel {
                                        text: qsTr ("Detached in separate window");
                                        color: Style.colorBorder;
                                        font.pixelSize: Style.fontSizeBig;
                                        anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                                    }
                                    TextButton {
                                        flat: true;
                                        icon: SvgIconLoader { icon: "actions/restore"; }
                                        anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                                        onClicked: { delegate3dView.attach (); }
                                    }
                                }
                                StretchColumnContainer {
                                    id: layout3dView;
                                    spacing: 0;
                                    anchors.fill: parent;

                                    Rectangle {
                                        color: Style.colorWindow;
                                        implicitHeight: (layoutSideAndZoom.height + layoutSideAndZoom.anchors.margins * 2);

                                        StretchRowContainer {
                                            id: layoutSideAndZoom;
                                            spacing: Style.spacingNormal;
                                            anchors.margins: Style.spacingNormal;
                                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            ExtraAnchors.horizontalFill: parent;

                                            TextLabel {
                                                text: qsTr ("Side :");
                                                font.pixelSize: Style.fontSizeBig;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            ComboList {
                                                id: comboSide;
                                                model: [
                                                    { "key" : Sides.FRONT,  "value" : qsTr ("Front")  },
                                                    { "key" : Sides.RIGHT,  "value" : qsTr ("Right")  },
                                                    { "key" : Sides.LEFT,   "value" : qsTr ("Left")   },
                                                    { "key" : Sides.BACK,   "value" : qsTr ("Back")   },
                                                    { "key" : Sides.TOP,    "value" : qsTr ("Top")    },
                                                    { "key" : Sides.BOTTOM, "value" : qsTr ("Bottom") },
                                                ];
                                                delegate: ComboListDelegateForModelDataAttributes { }
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                Component.onCompleted: { selectByKey (modelData); }
                                            }
                                            Line {
                                                implicitHeight: (Style.fontSizeNormal * 2);
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            TextLabel {
                                                text: qsTr ("Perspective :");
                                                font.pixelSize: Style.fontSizeBig;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            CheckableBox {
                                                id: togglePerspective;
                                                value: true;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            Line {
                                                implicitHeight: (Style.fontSizeNormal * 2);
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            SvgIconLoader {
                                                icon: "actions/zoom-out";
                                                color: Style.colorForeground;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            SliderBar {
                                                id: sliderDistance;
                                                handleSize: (Style.spacingNormal * 3);
                                                showTooltipWhenMoved: false;
                                                value: 50;
                                                minValue: 100;
                                                maxValue: 0;
                                                decimals: 2;
                                                implicitWidth: -1;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            SvgIconLoader {
                                                icon: "actions/zoom-in";
                                                color: Style.colorForeground;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            Line {
                                                implicitHeight: (Style.fontSizeNormal * 2);
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                            TextButton {
                                                flat: true;
                                                icon: SvgIconLoader { icon: "actions/fullscreen"; }
                                                visible: !delegate3dView.detachedWindow;
                                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                                onClicked: { delegate3dView.detach (); }
                                            }
                                        }
                                    }
                                    Line { }
                                    Rectangle {
                                        color: Style.colorEditable;
                                        implicitHeight: -1;

                                        RendererItem {
                                            id: renderer;
                                            world: root.world;
                                            viewingSide: comboSide.currentKey;
                                            viewingZoom: sliderDistance.value;
                                            usePerspective: togglePerspective.value;
                                            transformOrigin: Item.Center;
                                            transform: Scale { // Y-up
                                                xScale: +1;
                                                yScale: -1;
                                                origin {
                                                    x: (renderer.width  / 2);
                                                    y: (renderer.height / 2);
                                                }
                                            }
                                            anchors.fill: parent;

                                            MouseArea {
                                                anchors.fill: parent;
                                                onWheel: {
                                                    var tmp = (sliderDistance.value + (wheel.angleDelta.y / 10));
                                                    sliderDistance.value = (tmp < 100 ? (tmp > 0 ? tmp : 0) : 100);
                                                }
                                            }
                                        }
                                        TextLabel {
                                            id: lblAxisTop;
                                            style: Text.Outline;
                                            styleColor: Style.colorInverted;
                                            emphasis: true;
                                            text: {
                                                switch (renderer.viewingSide) {
                                                case Sides.FRONT:
                                                case Sides.BACK:
                                                case Sides.LEFT:
                                                case Sides.RIGHT:
                                                    return qsTr ("Top");
                                                case Sides.TOP:
                                                case Sides.BOTTOM:
                                                    return qsTr ("Front");
                                                }
                                            }
                                            anchors {
                                                top: parent.top;
                                                margins: Style.spacingSmall;
                                                horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                                            }
                                        }
                                        TextLabel {
                                            id: lblAxisBottom;
                                            style: Text.Outline;
                                            styleColor: Style.colorInverted;
                                            emphasis: true;
                                            text: {
                                                switch (renderer.viewingSide) {
                                                case Sides.FRONT:
                                                case Sides.BACK:
                                                case Sides.LEFT:
                                                case Sides.RIGHT:
                                                    return qsTr ("Bottom");
                                                case Sides.TOP:
                                                case Sides.BOTTOM:
                                                    return qsTr ("Back");
                                                }
                                            }
                                            anchors {
                                                bottom: parent.bottom;
                                                margins: Style.spacingSmall;
                                                horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                                            }
                                        }
                                        TextLabel {
                                            id: lblAxisLeft;
                                            style: Text.Outline;
                                            styleColor: Style.colorInverted;
                                            emphasis: true;
                                            text: {
                                                switch (renderer.viewingSide) {
                                                case Sides.FRONT:
                                                    return qsTr ("Right");
                                                case Sides.BACK:
                                                    return qsTr ("Left");
                                                case Sides.LEFT:
                                                    return qsTr ("Front");
                                                case Sides.RIGHT:
                                                    return qsTr ("Back");
                                                case Sides.TOP:
                                                    return qsTr ("Left");
                                                case Sides.BOTTOM:
                                                    return qsTr ("Right");
                                                }
                                            }
                                            anchors {
                                                left: parent.left;
                                                margins: Style.spacingSmall;
                                                verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                        }
                                        TextLabel {
                                            id: lblAxisRight;
                                            style: Text.Outline;
                                            styleColor: Style.colorInverted;
                                            emphasis: true;
                                            text: {
                                                switch (renderer.viewingSide) {
                                                case Sides.FRONT:
                                                    return qsTr ("Left");
                                                case Sides.BACK:
                                                    return qsTr ("Right");
                                                case Sides.LEFT:
                                                    return qsTr ("Back");
                                                case Sides.RIGHT:
                                                    return qsTr ("Front");
                                                case Sides.TOP:
                                                    return qsTr ("Right");
                                                case Sides.BOTTOM:
                                                    return qsTr ("Left");
                                                }
                                            }
                                            anchors {
                                                right: parent.right;
                                                margins: Style.spacingSmall;
                                                verticalCenter: (parent ? parent.verticalCenter : undefined);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Line {
                        visible: (gridViews.cols === 1);
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        ExtraAnchors.horizontalFill: parent;
                    }
                    Line {
                        visible: (gridViews.cols === 2);
                        anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                        ExtraAnchors.verticalFill: parent;
                    }
                }
                Group {
                    id: tabDashboard;
                    title: qsTr ("Dashboard");
                    icon: SvgIconLoader {
                        icon: "devices/tablet";
                        color: Style.colorForeground;
                    }

                    Rectangle {
                        color: Style.colorEditable;
                        anchors.fill: parent;
                    }
                    Binding {
                        target: Shared.manager;
                        property: "dashboardZoom";
                        value: zoomerDashboards.contentZoom;
                    }
                    ZoomAndMoveArea {
                        id: zoomerDashboards;
                        contentItem: layoutDashboards;
                        contentPadding: (Style.spacingBig * 5);
                        contentZoomMin: 0.20;
                        contentZoomMax: 4.00;
                        anchors.fill: parent;
                        anchors.topMargin: stripDashboards.height;
                        onWidthChanged: { zoomerDashboards.fit (layoutDashboards); }
                        onHeightChanged: { zoomerDashboards.fit (layoutDashboards); }

                        function fit (item) {
                            if (item && item.width > 0 && item.height > 0) {
                                var ratioX = ((zoomerDashboards.width  - Style.spacingBig * 2) / item.width);
                                var ratioY = ((zoomerDashboards.height - Style.spacingBig * 2) / item.height);
                                contentZoom = Math.min (ratioX, ratioY);
                                ensureVisible (item);
                            }
                        }

                        Item {
                            id: layoutDashboards;
                            width: (Shared.currentDashboard ? Shared.currentDashboard.width : 0);
                            height: (Shared.currentDashboard ? Shared.currentDashboard.height : 0);
                            onWidthChanged:  { zoomerDashboards.fit (layoutDashboards); }
                            onHeightChanged: { zoomerDashboards.fit (layoutDashboards); }

                            Repeater {
                                id: repeaterDashboards;
                                model: dashboardsList;
                                delegate: Rectangle {
                                    id: holder;
                                    color: Style.opacify (Style.colorWindow, 0.35);
                                    width: (dashboard ? dashboard.width : 0);
                                    height: (dashboard ? dashboard.height : 0);
                                    radius: (Style.roundness * 4);
                                    opacity: (dashboard === Shared.currentDashboard ? 1 : 0);
                                    enabled: (dashboard === Shared.currentDashboard);
                                    antialiasing: true;
                                    border {
                                        color: Style.colorBorder;
                                        width: (Style.lineSize * 2 / Shared.manager.dashboardZoom);
                                    }
                                    anchors.centerIn: parent;
                                    Component.onCompleted: { dashboard.parent = holder; }

                                    readonly property Dashboard dashboard : modelData;
                                }
                                onModelChanged: {
                                    if (count > 0) {
                                        Shared.currentDashboard = dashboardsList.get (0);
                                    }
                                }
                            }
                        }
                    }
                    ToolBar {
                        id: stripDashboards;
                        gradient: Style.gradientEditable (Style.colorWindow);

                        TextLabel {
                            text: qsTr ("Show dashboard :");
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                        Repeater {
                            model: dashboardsList;
                            delegate: TextButton {
                                id: delegateDashboard;
                                text: (dashboard
                                       ? (containsMouse && (dashboard.title !== "")
                                          ? dashboard.title
                                          : ((dashboard.uid !== "")
                                             ? dashboard.uid
                                             : fallbackName))
                                       : "");
                                checked: (dashboard === Shared.currentDashboard);
                                implicitWidth: (Math.max (metricsDashboardUid.width,
                                                          metricsDashboardTitle.width,
                                                          metricsDashboardNum.width) + (padding * 2));
                                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                                onClicked: { Shared.currentDashboard = dashboard; }

                                readonly property Dashboard dashboard : modelData;

                                readonly property string fallbackName : qsTr ("Dashboard %1").arg (model.index +1);

                                TextLabel {
                                    id: metricsDashboardNum;
                                    text: delegateDashboard.fallbackName;
                                    color: Style.colorNone;
                                }
                                TextLabel {
                                    id: metricsDashboardUid;
                                    text: delegateDashboard.dashboard.uid;
                                    color: Style.colorNone;
                                }
                                TextLabel {
                                    id: metricsDashboardTitle;
                                    text: delegateDashboard.dashboard.title;
                                    color: Style.colorNone;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    PanelContainer {
        id: panelLogs;
        size: 120;
        minSize: 120;
        maxSize: 400;
        visible: expanded;
        resizable: true;
        detachable: false;
        collapsable: false;
        borderSide: Item.Top;
        anchors.bottom: statusBar.top;
        ExtraAnchors.horizontalFill: parent;

        ScrollContainer {
            anchors.fill: parent;
            showBorder: false;
            headerItem: Item {
                height: implicitHeight;
                implicitHeight: (btnClearLogs.height + btnClearLogs.anchors.margins * 2);

                TextLabel {
                    text: qsTr ("Messages output");
                    font.pixelSize: Style.fontSizeTitle;
                    font.capitalization: Font.SmallCaps;
                    anchors {
                        left: parent.left;
                        margins: Style.spacingNormal;
                        verticalCenter: (parent ? parent.verticalCenter : undefined);
                    }
                }
                TextButton {
                    id: btnClearLogs;
                    text: qsTr ("Clear logs");
                    anchors {
                        right: parent.right;
                        margins: Style.spacingNormal;
                        verticalCenter: (parent ? parent.verticalCenter : undefined);
                    }
                    onClicked: { Shared.manager.logsModel.clear (); }
                }
            }

            ListView {
                model: Shared.manager.logsModel;
                delegate: Stretcher {
                    height: implicitHeight;
                    implicitHeight: (layoutLog.height + layoutLog.anchors.margins * 2);
                    ExtraAnchors.horizontalFill: parent;

                    StretchRowContainer {
                        id: layoutLog;
                        spacing: Style.spacingNormal;
                        anchors.margins: Style.spacingSmall;
                        ExtraAnchors.horizontalFill: parent;

                        TextLabel {
                            id: lblLogDateTime;
                            text: Qt.formatTime (modelData ["when"], "[hh:mm:ss]");
                            color: Style.colorBorder;
                        }
                        StretchColumnContainer {
                            spacing: Style.lineSize;

                            StretchRowContainer {
                                spacing: Style.spacingNormal;

                                TextLabel {
                                    id: lblLogType;
                                    text: modelData ["type"];
                                    color: lblLogDetails.color;
                                    emphasis: true;
                                }
                                TextLabel {
                                    id: lblLogModule;
                                    text: (modelData ["module"] + " :");
                                    color: lblLogDetails.color;
                                }
                                Stretcher {
                                    implicitHeight: lblLogDetails.height;

                                    TextLabel {
                                        id: lblLogDetails;
                                        text: modelData ["msg"];
                                        color:  {
                                            switch (modelData ["type"]) {
                                            case "ERR":     return Style.colorError;
                                            case "ERROR":   return Style.colorError;
                                            case "WARNING": return Style.colorError;
                                            case "WARN":    return Style.colorError;
                                            }
                                            return Style.colorForeground;
                                        }
                                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                        ExtraAnchors.horizontalFill: parent;
                                    }
                                }
                            }
                            ClickableTextLabel {
                                id: lblLogObjectLocation;
                                text: modelData ["object"];
                                visible: (text !== "");
                                wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                font.pixelSize: Style.fontSizeSmall;
                                onClicked: {
                                    var tmp = modelData ["object"].split (" @ ");
                                    var locUrl = tmp [tmp.length -1]
                                    var result = locUrl.match (/(file:\/\/\/.*):(\d+):(\d+)/);
                                    if (result) {
                                        windowEditor.showLineColumnInFile (FileSystem.pathFromUrl (result [1]),
                                                                           parseInt (result [2]),
                                                                           parseInt (result [3]));
                                    }
                                    else {
                                        windowEditor.showLineColumnInFile (FileSystem.pathFromUrl (locUrl), 0, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
