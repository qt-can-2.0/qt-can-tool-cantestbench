#ifndef TRANSFORMER_H
#define TRANSFORMER_H

#include "IdentifiableObject.h"

#include <QJSValue>
#include <QJSValueList>

class AbstractTransformer : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (bool, isReversible)

public:
    explicit AbstractTransformer (const bool isReversible = false, QObject * parent = Q_NULLPTR);
    virtual ~AbstractTransformer (void);

    virtual void onComponentCompleted (void) Q_DECL_OVERRIDE;

    virtual int   targetFromSource (const int   source);
    virtual int   sourceFromTarget (const int   target);

    virtual float targetFromSource (const float source);
    virtual float sourceFromTarget (const float target);
};

class AffineTransformer : public AbstractTransformer {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (float, factor)
    QML_WRITABLE_VAR_PROPERTY (float, offset)

public:
    explicit AffineTransformer (QObject * parent = Q_NULLPTR);
    virtual ~AffineTransformer (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    int   targetFromSource (const int   source) Q_DECL_FINAL;
    int   sourceFromTarget (const int   target) Q_DECL_FINAL;

    float targetFromSource (const float source) Q_DECL_FINAL;
    float sourceFromTarget (const float target) Q_DECL_FINAL;
};

class CustomTransformer : public AbstractTransformer {
    Q_OBJECT
    QML_WRITABLE_CSTREF_PROPERTY (QQmlScriptString, fromSourceToTarget)
    QML_WRITABLE_CSTREF_PROPERTY (QQmlScriptString, fromTargetToSource)

public:
    explicit CustomTransformer (QObject * parent = Q_NULLPTR);
    virtual ~CustomTransformer (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    int   targetFromSource (const int   source) Q_DECL_FINAL;
    int   sourceFromTarget (const int   target) Q_DECL_FINAL;

    float targetFromSource (const float source) Q_DECL_FINAL;
    float sourceFromTarget (const float target) Q_DECL_FINAL;

private:
    template<typename T> T callQmlFunction (const QQmlScriptString & script, const T arg) {
        QQmlExpression expr (script);
        QJSValue func = expr.evaluate ().value<QJSValue> ();
        const QJSValue val = func.call (QJSValueList () << arg);
        return T (val.toNumber ());
    }
};

#endif // TRANSFORMER_H
