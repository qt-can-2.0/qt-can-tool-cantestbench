
#include "Help.h"

static ObjectHelp basicObject {
    "BasicObject", "object", { }, { "Node", "Board", "AbstractIO", "AbstractSensor", "AbstractActuator", "AbstractRoutine", "AbstractLink", "AbstractTransformer", "PhysicalValue", "PhysicalPoint", "PhysicalSize", "PhysicalAngle", "PhysicalBlock", "PhysicalMarker", "PhysicalWorld", "SerialBus", "CanBus", "CanOpen", "ObjectsGroup" }, "abstract class",
    {
        new PropertyHelp {
            "family", "enum (ObjectFamily)", "constant", false,
            "The category of the object (node, I/O, sensor, actuator, physical value/block/marker...)."
        },
        new PropertyHelp {
            "uid", "string", "writable", false,
            "The user-defined unique identifier for the object (in fact, must be unique inside a given parent if the parent itself is a BasicObject). Should be quite short."
        },
        new PropertyHelp {
            "path", "string", "read-only", false,
            "The really unique identifier of an object, generated automatically by concatenating the object's UID with its ancestors ones."
        },
        new PropertyHelp {
            "title", "string", "writable", false,
            "The human readable name for an object displayed in UI when hovering the object UID."
        },
    },
    { },
    { },
    { },
    "The root class for every identifiable object (has a type enum, an UID...)."
};

static ObjectHelp objectsGroup {
    "ObjectsGroup", "object", { }, { "object" }, "instantiable",
    {
        new PropertyHelp {
            "subObjects", "object", "writable", true,
            "The list of children objects. This is the default property."
        }
    },
    { },
    { },
    { },
    "A generic component to group relative objects together, giving them a common 'folder' in their UID path. It allows creating macro-components (e.g: physical blocks + sensors + actuators)."
};

static ObjectHelp abstractSensor {
    "AbstractSensor", "BasicObject", { "AnalogSensor", "DigitalSensor" }, { }, "abstract class",
    {
        new PropertyHelp {
            "type", "enum (ObjectType)", "constant", false,
            "The type of sensor (analog or digital)."
        },
        new PropertyHelp {
            "description", "string", "writable", false,
            "A text description for UI help mode and HTML export."
        },
    },
    { },
    { },
    { },
    "The base class for all sensor objects."
};

static ObjectHelp analogSensor {
    "AnalogSensor", "AbstractSensor", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sourceLink", "LinkPhysicalValueToAnalogSensor", "writable", false,
            "The link to read the physical value from. If null, sensor value will be directly modifiable by user in UI (e.g: joystick)."
        },
        new PropertyHelp {
            "targetLinks", "LinkAnalogSensorToAnalogInput", "writable", true,
            "The link(s) to write the electrical value to."
        },
        new PropertyHelp {
            "minPhy", "int32", "writable", false,
            "The minimum boundary for source physical value."
        },
        new PropertyHelp {
            "maxPhy", "int32", "writable", false,
            "The maximum boundary for source physical value."
        },
        new PropertyHelp {
            "valPhy", "int32", "writable", false,
            "The current physical value read by the sensor."
        },
        new PropertyHelp {
            "minRaw", "int32", "writable", false,
            "The minimum boundary for target electrical value."
        },
        new PropertyHelp {
            "maxRaw", "int32", "writable", false,
            "The maximum boundary for target electrical value."
        },
        new PropertyHelp {
            "valRaw", "int32", "readonly", false,
            "The current electrical level computed by the sensor."
        },
        new PropertyHelp {
            "percent", "int32", "readonly", false,
            "The current value of the sensor as a percentage (takes in account the split point and margins/dead-areas if any)."
        },
        new PropertyHelp {
            "inverted", "bool", "readonly", false,
            "Whether the sensor flow is inverted, i.e: plugged on an input from a passive node (false by default))."
        },
        new PropertyHelp {
            "bypassScale", "bool", "writable", false,
            "Whether the sensor should not convert value internally and pass untransformed value to its links (false by default)."
        },
        new PropertyHelp {
            "decimals", "int32", "writable", false,
            "The amount of decimals that should be used when reading physical (floating) value and convert it to integer."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the physical value read by the sensor (only for display)."
        },
        new PropertyHelp {
            "useSplitPoint", "bool", "writable", false,
            "The range mode for sensor's percentage computation : if true, sensor is -100 to +100 %, else it is 0 to 100 % (default). Usefull for bidirectional joysticks."
        },
        new PropertyHelp {
            "upperMargin", "int32", "writable", false,
            "The margin in mV that is used for maximum percentage (all values above this limit are used as maximum too)."
        },
        new PropertyHelp {
            "lowerMargin", "int32", "writable", false,
            "The margin in mV that is used for minimum percentage (all values under this limit are used as minimum too)."
        },
        new PropertyHelp {
            "middleMargin", "int32", "writable", false,
            "The margin in mV that is used for around 0% percentage (all values under this limit are used as 0% too). Also called dead-area. Only used if useSplitPoint is enabled."
        },
    },
    { },
    { },
    { },
    "An item reading a physical value and converting it to electrical value (using min/max for proportionality between both values)."
};

static ObjectHelp digitalSensor {
    "DigitalSensor", "AbstractSensor", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "bool", "writable", false,
            "The current binary state of the sensor."
        },
        new PropertyHelp {
            "trueLabel", "string", "readonly", false,
            "The label used in UI when state is true ('ON' by default)."
        },
        new PropertyHelp {
            "falseLabel", "string", "readonly", false,
            "The label used in UI when state is false ('OFF' by default)."
        },
    },
    { },
    { },
    { },
    "An item mapping a world state to binary value (e.g: switcher)."
};

static ObjectHelp hybridSensor {
    "HybridSensor", "AbstractSensor", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sourceLink", "LinkPhysicalValueToHybridSensor", "writable", false,
            "The link to read the physical value from. If null, sensor value will be directly modifiable by user in UI."
        },
        new PropertyHelp {
            "targetLinks", "LinkHybridSensorToDigitalInput", "writable", true,
            "The link(s) to write the electrical state to."
        },
        new PropertyHelp {
            "disabled", "bool", "readonly", false,
            "Whether the manager have disabled the sensor because it can't be reversed."
        },
        new PropertyHelp {
            "valPhy", "int32", "writable", false,
            "The physical value used as input (usually updated through the link)."
        },
        new PropertyHelp {
            "lowThresholdPhy", "int32", "writable", false,
            "The low threshold of hysterisis."
        },
        new PropertyHelp {
            "highThresholdPhy", "int32", "writable", false,
            "The high threshold of hysterisis."
        },
        new PropertyHelp {
            "invertState", "bool", "writable", false,
            "Whether the state must be inverted, true when under low threshold, and false when above high threshold (false by default)."
        },
        new PropertyHelp {
            "valState", "bool", "readonly", false,
            "The output binary value of the sensor."
        },
        new PropertyHelp {
            "decimals", "int32", "writable", false,
            "The amount of decimals that should be used when reading physical (floating) value and convert it to integer."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the physical value read by the sensor (only for display)."
        },
    },
    { },
    { },
    { },
    "An item mapping a world physical value to a binary state, using hysterisis thresholds."
};

static ObjectHelp abstractActuator {
    "AbstractActuator", "BasicObject", { "AnalogActuator", "DigitalActuator" }, { }, "abstract class",
    {
        new PropertyHelp {
            "type", "enum (ObjectType)", "constant", false,
            "The type of actuator (analog or digital)."
        },
        new PropertyHelp {
            "description", "string", "writable", false,
            "A text description for UI help mode and HTML export."
        },
    },
    { },
    { },
    { },
    "The base class for all actuator objects."
};

static ObjectHelp analogActuator {
    "AnalogActuator", "AbstractActuator", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sourceLink", "LinkAnalogOutputToAnalogActuator", "writable", false,
            "The link to take the electrical level of a power output and use it to regulate speed of the actuator."
        },
        new PropertyHelp {
            "targetLink", "LinkAnalogActuatorToPhysicalValue", "writable", false,
            "The link to apply the generated speed to a physical value."
        },
        new PropertyHelp {
            "minRaw", "int32", "writable", false,
            "The minimum boundary for source electrical value."
        },
        new PropertyHelp {
            "maxRaw", "int32", "writable", false,
            "The maximum boundary for source electrical value."
        },
        new PropertyHelp {
            "valRaw", "int32", "writable", false,
            "The current electrical level feeding the actuator."
        },
        new PropertyHelp {
            "minSpeed", "int32", "writable", false,
            "The minimum boundary for target physical speed."
        },
        new PropertyHelp {
            "maxSpeed", "int32", "writable", false,
            "The maximum boundary for target physical speed."
        },
        new PropertyHelp {
            "valSpeed", "int32", "readonly", false,
            "The current speed generated by the actuator."
        },
        new PropertyHelp {
            "percent", "int32", "readonly", false,
            "The current value of the actuator as a percentage (takes in account the split point if any)."
        },
        new PropertyHelp {
            "useSplitPoint", "bool", "writable", false,
            "The range mode for actuator's percentage computation : if true, actuator is -100 to +100 %, else it is 0 to 100 % (default). Usefull for bidirectional motors."
        },
        new PropertyHelp {
            "decimals", "int32", "writable", false,
            "The amount of decimals that should be used when setting physical (floating) speed and convert it from integer."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the physical speed applied by the actuator (only for display)."
        },
    },
    { },
    { },
    { },
    "An item taking an electrical value and converting it physical speed applied to a physical value (using min/max for proportionality between both values)."
};

static ObjectHelp digitalActuator {
    "DigitalActuator", "AbstractActuator", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "bool", "writable", false,
            "The current binary state of the actuator."
        },
        new PropertyHelp {
            "trueLabel", "string", "readonly", false,
            "The label used in UI when state is true ('ON' by default)."
        },
        new PropertyHelp {
            "falseLabel", "string", "readonly", false,
            "The label used in UI when state is false ('OFF' by default)."
        },
    },
    { },
    { },
    { },
    "An item mapping binary state to real world (e.g: LED, buzzer, etc...)."
};

static ObjectHelp hybridActuator {
    "HybridActuator", "AbstractActuator", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sourceLink", "LinkDigitalOutputToHybridActuator", "writable", false,
            "The link to take the electrical state of a digital output and use it to set speed of the actuator."
        },
        new PropertyHelp {
            "targetLink", "LinkHybridActuatorToPhysicalValue", "writable", false,
            "The link to apply the generated speed to a physical value."
        },
        new PropertyHelp {
            "value", "bool", "writable", false,
            "The current electrical state activating the actuator."
        },
        new PropertyHelp {
            "ratedSpeed", "int32", "writable", false,
            "The rated physical speed."
        },
        new PropertyHelp {
            "valSpeed", "int32", "readonly", false,
            "The current speed generated by the actuator."
        },
        new PropertyHelp {
            "decimals", "int32", "writable", false,
            "The amount of decimals that should be used when setting physical (floating) speed and convert it from integer."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the physical speed applied by the actuator (only for display)."
        },
    },
    { },
    { },
    { },
    "An item taking an electrical state and using to activate a rated physical speed applied to a physical value."
};

static ObjectHelp abstractIO {
    "AbstractIO", "BasicObject", { "AbstractAnalogIO", "AbstractDigitalIO" }, { }, "abstract class",
    {
        new PropertyHelp {
            "type", "enum (ObjectType)", "constant", false,
            "The type of I/O (analog or digital)."
        },
        new PropertyHelp {
            "direction", "enum (ObjectDirection)", "constant", false,
            "The direction of I/O (input or output)."
        },
    },
    { },
    { },
    { },
    "The base class for all I/O objects."
};

static ObjectHelp abstractAnalogIO {
    "AbstractAnalogIO", "AbstractIO", { "AnalogInput", "AnalogOutput" }, { }, "abstract class",
    {
        new PropertyHelp {
            "minRaw", "int32", "writable", false,
            "The minimum boundary for electrical value."
        },
        new PropertyHelp {
            "maxRaw", "int32", "writable", false,
            "The maximum boundary for electrical value."
        },
        new PropertyHelp {
            "valRaw", "int32", "writable", false,
            "The current electrical value (0 mV by default)."
        },
        new PropertyHelp {
            "resolutionInPoints", "int32", "writable", false,
            "The resolution used when sampling electrical value to logical value (13 bits by default = 8192)."
        },
    },
    { },
    { },
    { },
    "The base class for analog I/O objects, holding mV values."
};

static ObjectHelp analogInput {
    "AnalogInput", "AbstractAnalogIO", { }, { }, "instantiable",
    { },
    {
        new MethodHelp {
            "getValueInPoints", "int32",
            { },
            "Returns the electrical value converted to logical points value (based on resolution)."
        },
        new MethodHelp {
            "getValueInPercents", "int32",
            { },
            "Returns the electrical value converted to percentage (based on min/max range)."
        },
    },
    { },
    { },
    "The object representing an analog input, accepting an electrical level."
};

static ObjectHelp analogOutput {
    "AnalogOutput", "AbstractAnalogIO", { }, { }, "instantiable",
    { },
    {
        new MethodHelp {
            "setValueInPoints", "int32",
            {
                new ArgumentHelp {
                    "points", "int32", "", false,
                    "The logical points value to convert."
                },
            },
            "Changes the electrical value converted from logical points value (based on resolution)."
        },
        new MethodHelp {
            "setValueInPercents", "int32",
            {
                new ArgumentHelp {
                    "percents", "int32", "", false,
                    "The percentage value to convert."
                },
            },
            "Changes the electrical value converted from percentage (based on min/max range)."
        },
    },
    { },
    { },
    "The object representing an analog output, producing an electrical level."
};

static ObjectHelp abstractDigitalIO {
    "AbstractDigitalIO", "AbstractIO", { "DigitalInput", "DigitalOutput" }, { }, "abstract class",
    {
        new PropertyHelp {
            "value", "bool", "writable", false,
            "The current binary value (false by default)."
        },
    },
    { },
    { },
    { },
    "The base class for digital I/O objects, holding binary values."
};

static ObjectHelp digitalInput {
    "DigitalInput", "AbstractDigitalIO", { }, { }, "instantiable",
    { },
    { },
    { },
    { },
    "The object representing a digital input, accepting an electrical binary state."
};

static ObjectHelp digitalOutput {
    "DigitalOutput", "AbstractDigitalIO", { }, { }, "instantiable",
    { },
    { },
    { },
    { },
    "The object representing a digital output, producing an electrical binary state."
};

static ObjectHelp abstractRoutine {
    "AbstractRoutine", "BasicObject", { "RoutineOnTimer", "RoutineOnEvent", "RoutineOnCanFrame", "RoutineOnSerialFrame", "AbstractCanOpenRoutine" }, { }, "abstract class",
    {
        new PropertyHelp {
            "triggerType", "enum (Triggers)", "constant", false,
            "The type of event that triggers the routine (timer, signal; CAN/serial frame, CANopen event...)."
        },
        new PropertyHelp {
            "benchmarkExecutionTime", "bool", "writable", false,
            "Enabled it if you want to have debug messages with elasped time after execution of the routine (false by default)."
        },
        new PropertyHelp {
            "dumpMemoryAfterExecution", "bool", "writable", false,
            "Enabled it if you want to have debug messages with memory bytes content after execution of the routine (false by default)."
        },
        new PropertyHelp {
            "node", "Node", "readonly", false,
            "The node which the routine is attached on. Set automatically from parent node."
        },
        new PropertyHelp {
            "memory", "Memory", "readonly", false,
            "The memory storage of the node which the routine is attached on. Set automatically from parent node."
        },
    },
    {
        new MethodHelp {
            "execute", "void",
            { },
            "Trigger the routine. Normally you don't have to call it manually, since all routines have dedicated configurable trigger events."
        },
    },
    { },
    { },
    "The base class for all routine objects (executing specific code on particular triggers)."
};

static ObjectHelp routineOnTimer {
    "RoutineOnTimer", "AbstractRoutine", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "interval", "uint32", "writable", false,
            "The timer period for triggering the routine, in milliseconds (0 by default, inactive)."
        },
    },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "loops", "uint32", "", false,
                    "The counter of iterations of the routine."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code on a timer interval (in ms)."
};

static ObjectHelp routineOnEvent {
    "RoutineOnEvent", "AbstractRoutine", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "emiter", "object", "writable", false,
            "The object to watch for signals."
        },
        new PropertyHelp {
            "signalName", "string", "writable", false,
            "The name of the signal to catch."
        },
    },
    { },
    {
        new SignalHelp {
            "triggered",
            { },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code on a particular signal of another object."
};

static ObjectHelp routineOnCanFrame {
    "RoutineOnCanFrame", "AbstractRoutine", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "canBus", "CanBus", "writable", false,
            "The CAN bus object which must be watched by the routine."
        },
        new PropertyHelp {
            "canId", "uint16", "writable", false,
            "The specific CAN frame ID that the routine can catch."
        },
    },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "frameRx", "CanDataWrapperRx", "", false,
                    "The received frame wrapper (to access bytes)."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code on reception of a given CAN frame ID."
};

static ObjectHelp routineOnSerialFrame {
    "RoutineOnSerialFrame", "AbstractRoutine", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "serialBus", "SerialBus", "writable", false,
            "The serial bus to watch for bytes reception."
        },
    },
    { },
    {
        new SignalHelp {
            "triggered",
            { },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code on reception of bytes on the serial bus."
};

static ObjectHelp abstractCanOpenRoutine {
    "AbstractCanOpenRoutine", "AbstractRoutine", { "RoutineOnCanOpenStateChange", "RoutineOnCanOpenObdValChange", "RoutineOnCanOpenSdoWriteRequest", "RoutineOnCanOpenSdoReadReply" }, { }, "abstract class",
    {
        new PropertyHelp {
            "canOpen", "CanOpen", "writable", false,
            "The CANopen manager object on which the routine operates."
        }
    },
    { },
    { },
    { },
    "A routine object executing code CANopen protocol-specific events."
};

static ObjectHelp routineOnCanOpenStateChange {
    "RoutineOnCanOpenStateChange", "AbstractCanOpenRoutine", { }, { }, "instantiable",
    { },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "state", "enum (CanOpenNmtState)", "", false,
                    "The current NMT state of the CANopen node."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code when CANopen node NMT state changes."
};

static ObjectHelp routineOnCanOpenObdValChange {
    "RoutineOnCanOpenObdValChange", "AbstractCanOpenRoutine", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "index", "uint16", "writable", false,
            "The CANopen OBD index of the entry to watch."
        },
        new PropertyHelp {
            "subIndex", "uint8", "writable", false,
            "The CANopen OBD sub-index of the sub-entry to watch."
        },
    },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "value", "variant", "", false,
                    "The new value of the OBD entry."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code when CANopen OBD entry value changes."
};

static ObjectHelp routineOnCanOpenSdoWriteRequest {
    "RoutineOnCanOpenSdoWriteRequest", "AbstractCanOpenRoutine", { }, { }, "instantiable",
    { },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The target node ID of the write request."
                },
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The target ODB entry index ID of the write request."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The target ODB sub-entry sub-index ID of the write request."
                },
                new ArgumentHelp {
                    "buffer", "ByteArrayWrapper", "", false,
                    "The binary content of the write request. Use methods to parse/store it."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code when CANopen SDO write request is received."
};

static ObjectHelp routineOnCanOpenSdoReadReply {
    "RoutineOnCanOpenSdoReadReply", "AbstractCanOpenRoutine", { }, { }, "instantiable",
    { },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The target node ID of the read reply."
                },
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The target ODB entry index ID of the read reply."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The target ODB sub-entry sub-index ID of the read reply."
                },
                new ArgumentHelp {
                    "buffer", "ByteArrayWrapper", "", false,
                    "The binary content of the read reply. Use methods to parse/store it."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code when CANopen SDO read reply is received."
};

static ObjectHelp routineOnCanOpenBootUp {
    "RoutineOnCanOpenBootUp", "AbstractCanOpenRoutine", { }, { }, "instantiable",
    { },
    { },
    {
        new SignalHelp {
            "triggered",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The node ID of the boot-up frame."
                },
            },
            "Emitted each time the routine is executed, put logic code inside the QML signal-handler."
        },
    },
    { },
    "A routine object executing code when CANopen boot-up frame is received."
};

static ObjectHelp abstractLink {
    "AbstractLink", "BasicObject", { "LinkPhysicalValueToAnalogSensor", "LinkAnalogSensorToAnalogInput", "LinkDigitalSensorToDigitalInput", "LinkAnalogOutputToAnalogActuator", "LinkDigitalOutputToDigitalActuator", "LinkAnalogActuatorToPhysicalValue", "LinkAnalogOutputToAnalogInput", "LinkDigitalOutputToDigitalInput" }, { }, "abstract class",
    {
        new PropertyHelp {
            "valid", "bool", "readonly", false,
            "Whether link has matching source and target."
        },
        new PropertyHelp {
            "enabled", "bool", "readonly", false,
            "Whether link can be used (i.e: not part of an inactive node flow)."
        },
        new PropertyHelp {
            "reversed", "bool", "readonly", false,
            "Whether link is reversed (i.e: not part of a passive node flow)."
        },
        new PropertyHelp {
            "detached", "bool", "writable", false,
            "Whether the user have manually broken the link (e.g: to manipulate value directly and/or create errors)."
        },
        new PropertyHelp {
            "source", "BasicObject", "writable", false,
            "The source of the link (origin of data in normal flow)."
        },
        new PropertyHelp {
            "target", "BasicObject", "writable", false,
            "The target of the link (destination of data in normal flow)."
        },
        new PropertyHelp {
            "transformer", "AbstractTransformer", "writable", false,
            "The optional transformer object between source and target (null by default)."
        },
    },
    { },
    { },
    { },
    "The base class for all data-flow links objects."
};

static ObjectHelp linkPhysicalValueToAnalogSensor {
    "LinkPhysicalValueToAnalogSensor", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "PhysicalValue", "readonly", false,
            "Set by the link if the source object is a physical value."
        },
        new PropertyHelp {
            "sensor", "AnalogSensor", "readonly", false,
            "Set by the link if the target object is an analog sensor."
        },
    },
    { },
    { },
    { },
    "A link measuring a physical value and providing it to an analog sensor."
};

static ObjectHelp linkAnalogSensorToAnalogInput {
    "LinkAnalogSensorToAnalogInput", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sensor", "AnalogSensor", "readonly", false,
            "Set by the link if the source object is an analog sensor."
        },
        new PropertyHelp {
            "input", "AnalogInput", "readonly", false,
            "Set by the link if the target object is an analog input."
        },
    },
    { },
    { },
    { },
    "A link taking the electrical level of an analog sensor and injecting it in an analog input."
};

static ObjectHelp linkDigitalSensorToDigitalInput {
    "LinkDigitalSensorToDigitalInput", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sensor", "DigitalSensor", "readonly", false,
            "Set by the link if the source object is a digital sensor."
        },
        new PropertyHelp {
            "input", "DigitalInput", "readonly", false,
            "Set by the link if the target object is a digital input."
        },
    },
    { },
    { },
    { },
    "A link taking the state of a digital sensor and injecting it in a digital input."
};

static ObjectHelp linkAnalogOutputToAnalogActuator {
    "LinkAnalogOutputToAnalogActuator", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "AnalogOutput", "readonly", false,
            "Set by the link if the source object is an analog output."
        },
        new PropertyHelp {
            "actuator", "AnalogActuator", "readonly", false,
            "Set by the link if the target object is an analog actuator."
        },
    },
    { },
    { },
    { },
    "A link taking the level of an analog output using it to regulate speed of an analog actuator."
};

static ObjectHelp linkDigitalOutputToDigitalActuator {
    "LinkDigitalOutputToDigitalActuator", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "DigitalOutput", "readonly", false,
            "Set by the link if the source object is a digital output."
        },
        new PropertyHelp {
            "actuator", "DigitalActuator", "readonly", false,
            "Set by the link if the target object is a digital actuator."
        },
    },
    { },
    { },
    { },
    "A link taking the binary value of a digital output and using it to toggle the state of a digital actuator."
};

static ObjectHelp linkAnalogActuatorToPhysicalValue {
    "LinkAnalogActuatorToPhysicalValue", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "AnalogActuator", "readonly", false,
            "Set by the link if the source object is an analog actuator."
        },
        new PropertyHelp {
            "actuator", "PhysicalValue", "readonly", false,
            "Set by the link if the target object is a physical value."
        },
    },
    { },
    { },
    { },
    "A link applying the speed induced by an analog sensor to a physical world value."
};

static ObjectHelp linkDigitalOutputToDigitalInput {
    "LinkDigitalOutputToDigitalInput", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "DigitalOutput", "readonly", false,
            "Set by the link if the source object is a digital output."
        },
        new PropertyHelp {
            "input", "DigitalInput", "readonly", false,
            "Set by the link if the target object is a digital input."
        },
    },
    { },
    { },
    { },
    "A link mapping a digital input state from a digital output state."
};

static ObjectHelp linkAnalogOutputToAnalogInput {
    "LinkAnalogOutputToAnalogInput", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "AnalogOutput", "readonly", false,
            "Set by the link if the source object is an analog output."
        },
        new PropertyHelp {
            "input", "AnalogInput", "readonly", false,
            "Set by the link if the target object is an analog input."
        },
    },
    { },
    { },
    { },
    "A link mapping an analog input value from an analog output level."
};

static ObjectHelp linkPhysicalValueToHybridSensor {
    "LinkPhysicalValueToHybridSensor", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "PhysicalValue", "readonly", false,
            "Set by the link if the source object is a physical value."
        },
        new PropertyHelp {
            "sensor", "HybridSensor", "readonly", false,
            "Set by the link if the target object is an hybrid sensor."
        },
    },
    { },
    { },
    { },
    "A link mapping a physical value to an hybrid sensor."
};

static ObjectHelp linkHybridSensorToDigitalInput {
    "LinkHybridSensorToDigitalInput", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "sensor", "HybridSensor", "readonly", false,
            "Set by the link if the source object is an hybrid sensor."
        },
        new PropertyHelp {
            "input", "DigitalInput", "readonly", false,
            "Set by the link if the target object is a digital input."
        },
    },
    { },
    { },
    { },
    "A link mapping an hybrid sensor to a digital input."
};

static ObjectHelp linkDigitalOutputToHybridActuator {
    "LinkDigitalOutputToHybridActuator", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "output", "DigitalOutput", "readonly", false,
            "Set by the link if the source object is a digital output."
        },
        new PropertyHelp {
            "actuator", "HybridActuator", "readonly", false,
            "Set by the link if the target object is an hybrid actuator."
        },
    },
    { },
    { },
    { },
    "A link mapping a digital output to an hybrid actuator."
};

static ObjectHelp linkHybridActuatorToPhysicalValue {
    "LinkHybridActuatorToPhysicalValue", "AbstractLink", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "actuator", "HybridActuator", "readonly", false,
            "Set by the link if the source object is an hybrid actuator."
        },
        new PropertyHelp {
            "value", "PhysicalValue", "readonly", false,
            "Set by the link if the target object is a physical value."
        },
    },
    { },
    { },
    { },
    "A link mapping an hybrid actuator to a physical value."
};

static ObjectHelp physicalValue {
    "PhysicalValue", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "val", "float", "writable", false,
            "The current physical value, in arbitrary unit (0.0 by default)."
        },
        new PropertyHelp {
            "min", "float", "writable", false,
            "The minimum physical value, in arbitrary unit (0.0 by default)."
        },
        new PropertyHelp {
            "max", "float", "writable", false,
            "The maximum physical value, in arbitrary unit (0.0 by default)."
        },
        new PropertyHelp {
            "loop", "bool", "writable", false,
            "Enable this if you need to go back to min value when reaching the max value (useful for rotation angle)."
        },
        new PropertyHelp {
            "accelLimit", "float", "writable", false,
            "The maximum acceleration, used to create speed-up ramps, in arbitrary unit/s/s (0.0 by default, no limit)."
        },
        new PropertyHelp {
            "decelLimit", "float", "writable", false,
            "The maximum deceleration, used to create slow-down ramps, in arbitrary unit/s/s (0.0 by default, no limit)."
        },
        new PropertyHelp {
            "currentSpeed", "float", "readonly", false,
            "The current speed, in arbitrary unit/s (0.0 by default). Derivated from assigned speed and taking in account previous motion and ramps."
        },
        new PropertyHelp {
            "assignedSpeed", "float", "readonly", false,
            "The assigned target speed, in arbitrary unit/s (0.0 by default). Set by link from analog actuators."
        },
    },
    { },
    { },
    { },
    "An object holding a physical world value (such as size or position) along with its min/max boundaries and its current speed and acceleration/deceleration ramps limits."
};

static ObjectHelp physicalPoint {
    "PhysicalPoint", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "onLeftToRight", "PhysicalValue", "grouped property", false,
            "The position on left-to-right axis."
        },
        new PropertyHelp {
            "onBackToFront", "PhysicalValue", "grouped property", false,
            "The position on back-to-front axis."
        },
        new PropertyHelp {
            "onBottomToTop", "PhysicalValue", "grouped property", false,
            "The position on bottom-to-top axis."
        },
    },
    { },
    { },
    { },
    "An object representing a point in space (on 3 axis)."
};

static ObjectHelp physicalSize {
    "PhysicalSize", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "toLeft", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward left."
        },
        new PropertyHelp {
            "toRight", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward right."
        },
        new PropertyHelp {
            "toBottom", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward bottom."
        },
        new PropertyHelp {
            "toTop", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward top."
        },
        new PropertyHelp {
            "toBack", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward back."
        },
        new PropertyHelp {
            "toFront", "PhysicalValue", "grouped property", false,
            "The extension between reference point toward front."
        },
    },
    { },
    { },
    { },
    "An object representing a size in space (2 directions for each 3 axis)."
};

static ObjectHelp physicalAngle {
    "PhysicalAngle", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "yaw", "PhysicalValue", "grouped property", false,
            "The rotation around bottom-to-top axis (front goes : -left / +right)."
        },
        new PropertyHelp {
            "pitch", "PhysicalValue", "grouped property", false,
            "The rotation around left-to-right axis (front goes : -bottom / +top)."
        },
        new PropertyHelp {
            "roll", "PhysicalValue", "grouped property", false,
            "The rotation around back-to-front axis (right goes : -up / +down)."
        },
    },
    { },
    { },
    { },
    "An object representing an in space (using yaw-pitch-roll convention)."
};

static ObjectHelp physicalBlock {
    "PhysicalBlock", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "visible", "bool", "writable", false,
            "Whether the block should be rendered."
        },
        new PropertyHelp {
            "color", "color", "writable", false,
            "The base color for block cell-shading rendering."
        },
        new PropertyHelp {
            "referenceBlock", "PhysicalBlock", "writable", false,
            "The reference block which current block is anchored on (null by default, block is anchored on world)."
        },
        new PropertyHelp {
            "anchorSide", "enum (Sides)", "writable", false,
            "The side of the reference block on which the anchor point is setup."
        },
        new PropertyHelp {
            "roundedAxis", "enum (Axis)", "writable", false,
            "The axis around which the block must be rounded instead of square to create cylinder (none by default)."
        },
        new PropertyHelp {
            "pivotPos", "PhysicalPoint", "grouped property", false,
            "The 3-axis translation of the block relatively to its anchor point position."
        },
        new PropertyHelp {
            "size", "PhysicalSize", "grouped property", false,
            "The 6-direction dimension of the block relatively to its pivot position."
        },
        new PropertyHelp {
            "angle", "PhysicalAngle", "grouped property", false,
            "The yaw/pitch/roll rotation of the block relatively to its pivot position."
        },
        new PropertyHelp {
            "absoluteAngle", "PhysicalAngle", "grouped property", false,
            "The absolute yaw/pitch/roll orientation of the block on the world, taking all ancestors transformations in account. Not modifiable."
        },
    },
    { },
    { },
    { },
    "An object for graphical rendering of a physical element, having a position, size and orientation, and additional visual properties like color and rounding."
};

static ObjectHelp physicalMarker {
    "PhysicalMarker", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "referenceBlock", "PhysicalBlock", "writable", false,
            "The reference block which the marker is anchored on."
        },
        new PropertyHelp {
            "anchorSide", "enum (Sides)", "writable", false,
            "The side of the reference block on which the anchor point is setup."
        },
        new PropertyHelp {
            "relativePos", "PhysicalPoint", "grouped property", false,
            "The 3-axis translation of the marker relatively to its anchor point position."
        },
        new PropertyHelp {
            "absolutePos", "PhysicalPoint", "grouped property", false,
            "The 3-axis position of the marker on the world. Not modifiable."
        },
    },
    { },
    { },
    { },
    "An object without size and orientation, just to watch a fixed position on a block."
};

static ObjectHelp physicalWorld {
    "PhysicalWorld", "BasicObject", { }, { }, "grouped-property",
    {
        new PropertyHelp {
            "bounds", "PhysicalSize", "grouped property", false,
            "The 6-direction world boundaries around the [0,0,0] origin. Used to limit the zoom in rendering."
        },
    },
    { },
    { },
    { },
    "The referential and boundary of physical world. It's used as the 'world' property of the NetworkDefinition."
};

static ObjectHelp board {
    "Board", "BasicObject", { }, { "AbstractIO" }, "instantiable",
    {
        new PropertyHelp {
            "ios", "AbstractIO", "readonly", true,
            "This model is automatically filled with I/O objects from children. Use it to access them from routines if needed."
        },
    },
    { },
    { },
    { },
    "The object representing a set of I/Os in a Node."
};

static ObjectHelp node {
    "Node", "BasicObject", { }, { "Board", "AbstractRoutine"}, "instantiable",
    {
        new PropertyHelp {
            "boards", "Board", "readonly", true,
            "This model is automatically filled with board objects from children. Use it to access them from routines if needed."
        },
        new PropertyHelp {
            "routines", "AbstractRoutine", "readonly", true,
            "This model is automatically filled with routine objects from children. Use it to access them from routines if needed."
        },
        new PropertyHelp {
            "memory", "Memory", "grouped property", false,
            "The memory storage of the node, that can be used to keep persitent data from routines."
        },
        new PropertyHelp {
            "mode", "enum (Modes)", "writable", false,
            "The mode of the node, between active, passive and inactive (default active)."
        },
        new PropertyHelp {
            "description", "string", "writable", false,
            "A text description for UI help mode and HTML export."
        },
    },
    { },
    {
        new SignalHelp {
            "started",
            { },
            "Emited when node is started (i.e: when testbench is simulator is started)."
        },
        new SignalHelp {
            "stopped",
            { },
            "Emited when node is stopped (i.e: when testbench is simulator is stopped)."
        },
    },
    { },
    "The object representing a logic node on the network (with I/Os)."
};

static ObjectHelp networkDefinition {
    "NetworkDefinition", "object", { }, { "BasicObject", "object" }, "instantiable",
    {
        new PropertyHelp {
            "uid", "string", "writable", false,
            "The user-defined unique identifier for the network (used to check that snapshot exports/import are related to the right project)."
        },
        new PropertyHelp {
            "title", "string", "writable", false,
            "The human readable name for the network displayed in UI when loaded."
        },
        new PropertyHelp {
            "clock", "uint32", "writable", false,
            "The simulator clock cycle, in milliseconds (50 by default)."
        },
        new PropertyHelp {
            "dumpObjectsOnInit", "bool", "writable", false,
            "Enabled this to have debug message with objects and UIDs after project is loaded."
        },
        new PropertyHelp {
            "benchmarkSimulatorTick", "bool", "writable", false,
            "Enabled this to have debug message with elapsed time after each simulator cycle (to know the time spent in data-flow updates)."
        },
        new PropertyHelp {
            "benchmarkCanFramesTiming", "bool", "writable", false,
            "Enabled this to have debug message with elapsed time after each all routines for a CAN frame ID have been executed (to know the time spent parsing frame and optionally answering it)."
        },
        new PropertyHelp {
            "world", "PhysicalWorld", "grouped-property", false,
            "The physical world and its blocks and values this network."
        },
    },
    { },
    { },
    { },
    "The root object for the simulation network definition, holding all the other objects."
};

static ObjectHelp canBus {
    "CanBus", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "frameTx", "CanDataWrapperTx", "writable", false,
            "The wrapper for sending CAN frames in this bus, from routines."
        },
    },
    { },
    { },
    { },
    "An object representing a configurable CAN bus, to use with routines."
};

static ObjectHelp serialBus {
    "SerialBus", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "portLoaded", "bool", "readonly", false,
            "Activated when the port is succesfully up and running. You don't have to use it directly."
        },
        new PropertyHelp {
            "portName", "string", "readonly", false,
            "Set when the port selected and configured. You don't have to use it directly."
        },
        new PropertyHelp {
            "portsList", "variantmap", "readonly", true,
            "The list of available ports, regularly updated internally. You don't have to use it directly."
        },
    },
    {
        new MethodHelp {
            "init", "bool",
            {
                new ArgumentHelp {
                    "portName", "string", "", false,
                    "The system name of the serial port (for the portsList property)."
                },
                new ArgumentHelp {
                    "baudrate", "uint32", "", false,
                    "The speed the serial port in bauds (1200, 2400, 4800, 9600, 19200, 38400, 57600, or 115200)."
                },
                new ArgumentHelp {
                    "dataBits", "uint32", "", false,
                    "The amount of bits used per character (8, 7, 6, or 5)."
                },
                new ArgumentHelp {
                    "stopBits", "uint32", "", false,
                    "The amount of bits used for stop (1, 2, or 3 which means 1.5)."
                },
                new ArgumentHelp {
                    "parity", "uint32", "", false,
                    "The parity mode (0=none, 2=even, 3=odd, 4=space, 5=mark)."
                },
            },
            "Try to configure and activate the given port with parameters. Returns true if succeded. Normally you don't call this directly, use UI dialogs."
        },
        new MethodHelp {
            "stop", "bool",
            { },
            "Try to stop the bus. Returns true if succeded. Normally you don't call this directly, use UI dialogs."
        },
        new MethodHelp {
            "count", "uint32",
            { },
            "Returns the amount of bytes to read in buffer."
        },
        new MethodHelp {
            "writeBool", "void",
            {
                new ArgumentHelp {
                    "value", "bool", "", false,
                    "The value to write as a single byte (0x00 or 0x01)."
                },
            },
            "Write a boolean value to serial port."
        },
        new MethodHelp {
            "writeInt8", "void",
            {
                new ArgumentHelp {
                    "value", "int8", "", false,
                    "The value to write as a single byte (0x00 to 0xFF, with sign bit)."
                },
            },
            "Write a 8 bit signed integer value to serial port."
        },
        new MethodHelp {
            "writeInt16", "void",
            {
                new ArgumentHelp {
                    "value", "int16", "", false,
                    "The value to write as two bytes (0x0000 to 0xFFFF, with sign bit)."
                },
            },
            "Write a 16 bit signed integer value to serial port."
        },
        new MethodHelp {
            "writeInt32", "void",
            {
                new ArgumentHelp {
                    "value", "int32", "", false,
                    "The value to write as four bytes (0x00000000 to 0xFFFFFFFF, with sign bit)."
                },
            },
            "Write a 32 bit signed integer value to serial port."
        },
        new MethodHelp {
            "writeUInt8", "void",
            {
                new ArgumentHelp {
                    "value", "uint8", "", false,
                    "The value to write as a single byte (0x00 to 0xFF, without sign bit)."
                },
            },
            "Write a 8 bit unsigned integer value to serial port."
        },
        new MethodHelp {
            "writeUInt16", "void",
            {
                new ArgumentHelp {
                    "value", "uint16", "", false,
                    "The value to write as two bytes (0x0000 to 0xFFFF, without sign bit)."
                },
            },
            "Write a 16 bit unsigned integer value to serial port."
        },
        new MethodHelp {
            "writeUInt32", "void",
            {
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to write as four bytes (0x00000000 to 0xFFFFFFFF, without sign bit)."
                },
            },
            "Write a 32 bit unsigned integer value to serial port."
        },
        new MethodHelp {
            "writeString", "void",
            {
                new ArgumentHelp {
                    "value", "string", "", false,
                    "The value to write as string."
                },
                new ArgumentHelp {
                    "len", "uint32", "", false,
                    "The length of string (if value is bigger, it will be cut down; and if smaller it will be filled with null chars '\0')."
                },
            },
            "Write an ASCII/Latin1 string to serial port."
        },
        new MethodHelp {
            "readBool", "bool",
            { },
            "Read a single byte from serial port, and convert to boolean."
        },
        new MethodHelp {
            "readInt8", "int8",
            { },
            "Read a single byte from serial port, and convert to 8 bit signed integer."
        },
        new MethodHelp {
            "readInt16", "int16",
            { },
            "Read two bytes from serial port, and convert to 16 bit signed integer."
        },
        new MethodHelp {
            "readInt32", "int32",
            { },
            "Read four bytes from serial port, and convert to 32 bit signed integer."
        },
        new MethodHelp {
            "readUInt8", "uint8",
            { },
            "Read a single byte from serial port, and convert to 8 bit unsigned integer."
        },
        new MethodHelp {
            "readUInt16", "uint16",
            { },
            "Read two bytes from serial port, and convert to 16 bit unsigned integer."
        },
        new MethodHelp {
            "readUInt32", "uint32",
            { },
            "Read four bytes from serial port, and convert to 32 bit unsigned integer."
        },
        new MethodHelp {
            "readString", "string",
            {
                new ArgumentHelp {
                    "len", "uint32", "", false,
                    "The length of string to read."
                },
            },
            "Read a given amount of bytes from serial port, and convert to ASCII/Latin1 string, stopping to first null char ('\0')."
        },
    },
    {
        new SignalHelp {
            "started",
            { },
            "Emited when serial bus is started (i.e: after is has been sucessfully configured by user)."
        },
        new SignalHelp {
            "stopped",
            { },
            "Emited when serial bus is stopped (i.e: after is has stopped configured by user)."
        },
        new SignalHelp {
            "error",
            {
                new ArgumentHelp {
                    "msg", "string", "", false,
                    "The error message produced by internal implementation."
                },
            },
            "Emited when serial bus has an error (generally during configuration)."
        },
    },
    { },
    "An object representing a configurable serial bus, with send/recv API."
};

static ObjectHelp abstractTransformer {
    "AbstractTransformer", "BasicObject", { "AffineTransformer", "CustomTransformer" }, { }, "abstract class",
    {
        new PropertyHelp {
            "isReversible", "bool", "constant", false,
            "Whether the transformer can be used on a reversed link."
        },
    },
    { },
    { },
    { },
    "The base class for all transformer objects."
};

static ObjectHelp affineTransformer {
    "AffineTransformer", "AbstractTransformer", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "factor", "float", "writable", false,
            "The factor used to multiply the source value before settings to target (or divide in reversed mode)."
        },
        new PropertyHelp {
            "offset", "float", "writable", false,
            "The offset used to add the source value before settings to target (or substract in reversed mode)."
        },
    },
    { },
    { },
    { },
    "A transformer that applies an affine (y=ax+b) on the link value. It is reversible."
};

static ObjectHelp customTransformer {
    "CustomTransformer", "AbstractTransformer", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "fromSourceToTarget", "function float(float)", "writable", false,
            "The JS function used to convert source value to target value, in normal mode. Takes a float as param and returns a float."
        },
        new PropertyHelp {
            "fromTargetToSource", "function float(float)", "writable", false,
            "The JS function used to convert target value to source value, in reversed mode. Takes a float as param and returns a float."
        },
    },
    { },
    { },
    { },
    "A transformer that a custom script to modify the link value. It is reversible if both scripts are provided."
};

static ObjectHelp memory {
    "Memory", "object", { }, { }, "sub-object",
    {
        new PropertyHelp {
            "dumpTreeAfterInit", "bool", "writable", false,
            "Enabled this if you want to have a tree of declared types and vars after memory initialization."
        },
    },
    {
        new MethodHelp {
            "declConst", "bool",
            {
                new ArgumentHelp {
                    "name", "string", "", false,
                    "The name for the constant (must be unique in memory)."
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value for the constant."
                },
            },
            "Creates a integer constant inside memory. Returns true if succeded. Throws a QML error at runtime if name is conflicting."
        },
        new MethodHelp {
            "declType", "bool",
            {
                new ArgumentHelp {
                    "name", "string", "", false,
                    "The name for the custom type (must be unique in memory)."
                },
                new ArgumentHelp {
                    "structure", "map<string,string>", "", false,
                    "The members of the structure : keys are the names, and values are the types. Type can be a base type, a custom type, or an fixed size array of any type (use form 'T[size]')."
                },
            },
            "Creates a integer constant inside memory. Returns true if succeded. Throws a QML error at runtime if name is conflicting or if the structure is incorrect."
        },
        new MethodHelp {
            "declVar", "bool",
            {
                new ArgumentHelp {
                    "name", "string", "", false,
                    "The name for the variable (must be unique in memory)."
                },
                new ArgumentHelp {
                    "type", "string", "", false,
                    "The type for the variable. Can be a base type, a custom type, or an fixed size array of any type (use form 'T[size]')."
                },
            },
            "Creates a integer constant inside memory. Returns true if succeded. Throws a QML error at runtime if name is conflicting or if type specification is invalid."
        },
        new MethodHelp {
            "getVar", "uint32",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable : for a simple var, the path is 'name', for array it's 'name[index]', and for structure members it's 'parent_name.member_name'. Obviously arrays and structs can be nested. In fact the path looks exactly as it would in C or C++."
                },
            },
            "Get the value of a variable, specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "setVar", "void",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to assign to the variable."
                },
            },
            "Set the value of a variable, specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "addVar", "void",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to add to the variable."
                },
            },
            "Add a value to a variable, specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "mulVar", "void",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to multiply with the variable."
                },
            },
            "Multiply a value with a variable, specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "divVar", "void",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to divide the variable with."
                },
            },
            "Divide a variable with a value, specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "isEqualTo", "bool",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to compare the variable with."
                },
            },
            "Compare a variable with a value, specified by its path. Returns true if given value is equal to var. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "isLesserThan", "bool",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to compare the variable with."
                },
            },
            "Compare a variable with a value, specified by its path. Returns true if given value is smaller than var. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "isGreaterThan", "bool",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to compare the variable with."
                },
            },
            "Compare a variable with a value, specified by its path. Returns true if given value is bigger than var. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "isDifferentFrom", "bool",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to compare the variable with."
                },
            },
            "Compare a variable with a value, specified by its path. Returns true if given value is different from var. Throws a QML error at runtime if variable doesn't exists."
        },
        new MethodHelp {
            "absDelta", "uint32",
            {
                new ArgumentHelp {
                    "path", "string", "", false,
                    "The complete path for the variable (see getVar for rules)"
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The value to compare the variable with."
                },
            },
            "Compute the absolute delta between a given value and variable specified by its path. Throws a QML error at runtime if variable doesn't exists."
        },
    },
    {
        new SignalHelp {
            "init",
            { },
            "Emited at parent startup, to let you declare custom types and variable. Do it inside the QML signal-handler."
        }
    },
    { },
    "An object holding a set of persistent strongly-typed variables that can be manipulated from routines and displayed in UI. Base type are int8, int16, int32, uint8, uint16, uint32. Arrays and custom structure types can also be created."
};

static ObjectHelp canDataWrapperRx {
    "CanDataWrapperRx", "object", { }, { }, "helper",
    { },
    {
        new MethodHelp {
            "getBitsAs", "uint32",
            {
                new ArgumentHelp {
                    "type", "enum (VarType.Enum)", "", false,
                    "The type of the value."
                },
                new ArgumentHelp {
                    "pos", "uint32", "", false,
                    "The position of the value in the frame (from 0 to 63 bits)."
                },
                new ArgumentHelp {
                    "count", "uint32", "", false,
                    "The size of the value in the frame (from 1 to 64 bits)."
                },
            },
            "Maps an arbitrary amount of bits at specific position in frame to an integer."
        },
    },
    { },
    { },
    "A helper to get integer values from a binary CAN frame."
};

static ObjectHelp canDataWrapperTx {
    "CanDataWrapperTx", "object", { }, { }, "helper",
    { },
    {
        new MethodHelp {
            "prepare", "void",
            {
                new ArgumentHelp {
                    "length", "uint32", "", false,
                    "The length of frame, between 0 and 8."
                },
            },
            "Prepare a new frame, with zeroed bytes."
        },
        new MethodHelp {
            "setBitsAs", "void",
            {
                new ArgumentHelp {
                    "type", "enum (VarType.Enum)", "", false,
                    "The type of the value."
                },
                new ArgumentHelp {
                    "pos", "uint32", "", false,
                    "The position of the value in the frame (from 0 to 63 bits)."
                },
                new ArgumentHelp {
                    "count", "uint32", "", false,
                    "The size of the value in the frame (from 1 to 64 bits)."
                },
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The integer value."
                },
            },
            "Maps an arbitrary amount of bits from an integer value to a specific position in frame."
        },
        new MethodHelp {
            "send", "void",
            {
                new ArgumentHelp {
                    "canId", "uint16", "", false,
                    "The CAN ID for the frame (11bits format, between 0x000 and 07FF)."
                },
            },
            "Send the frame on the CAN bus, with a CAN ID."
        },
    },
    { },
    { },
    "A helper to set integer values into a binary CAN frame, and send it with a CAN ID."
};

static ObjectHelp canOpen {
    "CanOpen", "BasicObject", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "nodeId", "uint8", "writable", false,
            "The CANopen node ID for this CANopen handler."
        },
        new PropertyHelp {
            "isMaster", "bool", "writable", false,
            "Whether this CANopen handler acts as master (false by default)."
        },
        new PropertyHelp {
            "canBus", "CanBus", "writable", false,
            "The CAN bus object this CANopen handler operates on."
        },
        new PropertyHelp {
            "obdUrl", "string", "writable", false,
            "The QML file containing the Qt CAN v2 OBD definition."
        },
    },
    {
        new MethodHelp {
            "readValue", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The index of the OBD entry."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The sub-index of the OBD sub-entry."
                },
            },
            "Reads an entry from OBD and returns it as binary buffer. Returns null if not exists."
        },
        new MethodHelp {
            "changeValue", "void",
            {
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The index of the OBD entry."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The sub-index of the OBD sub-entry."
                },
                new ArgumentHelp {
                    "buffer", "ByteArrayWrapper", "", false,
                    "The binary buffer to write in entry."
                }
            },
            "Write an entry of OBD and with data from a binary buffer."
        },
        new MethodHelp {
            "sendNmtChangeRequest", "void",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The ID of the target node (use 0 to target all slave nodes)."
                },
                new ArgumentHelp {
                    "state", "enum (CanOpenNmtCmd)", "", false,
                    "The new state the target(s) node(s) must switch to."
                },
            },
            "Sends an NMT state change request to one or all nodes."
        },
        new MethodHelp {
            "sendSdoReadRequest", "void",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The ID of the target node to read from."
                },
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The index of the OBD entry."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The sub-index of the OBD sub-entry."
                },
                new ArgumentHelp {
                    "blockMode", "bool", "false", false,
                    "Whether to read using block-transfer or regular expedited/segmented mode."
                }
            },
            "Sends an SDO read request to a node. Use RoutineOnSdoReadReply object to handle the reply."
        },
        new MethodHelp {
            "sendSdoWriteRequest", "void",
            {
                new ArgumentHelp {
                    "nodeId", "uint8", "", false,
                    "The ID of the target node to write to."
                },
                new ArgumentHelp {
                    "index", "uint16", "", false,
                    "The index of the OBD entry."
                },
                new ArgumentHelp {
                    "subIndex", "uint8", "", false,
                    "The sub-index of the OBD sub-entry."
                },
                new ArgumentHelp {
                    "buffer", "ByteArrayWrapper", "", false,
                    "The binary buffer containing the data to write."
                }
            },
            "Sends an SDO write request to a node, with data."
        },
        new MethodHelp {
            "createBufferFromInt8", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "int8", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from a signed 8 bit integer value."
        },
        new MethodHelp {
            "createBufferFromInt16", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "int16", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from a signed 16 bit integer value."
        },
        new MethodHelp {
            "createBufferFromInt32", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "int32", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from a signed 32 bit integer value."
        },
        new MethodHelp {
            "createBufferFromUInt8", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "uint8", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from an unsigned 8 bit integer value."
        },
        new MethodHelp {
            "createBufferFromUInt16", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "uint16", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from an unsigned 16 bit integer value."
        },
        new MethodHelp {
            "createBufferFromUInt32", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "value", "uint32", "", false,
                    "The integer value."
                },
            },
            "Create a binary buffer from an unsigned 32 bit integer value."
        },
        new MethodHelp {
            "createBufferFromString", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "str", "string", "", false,
                    "The text string."
                },
            },
            "Create a binary buffer from an ASCII/Latin1 string."
        },
        new MethodHelp {
            "createBufferFromHex", "ByteArrayWrapper",
            {
                new ArgumentHelp {
                    "hex", "string", "", false,
                    "The hexadecimal string."
                },
            },
            "Create a binary buffer from an hexadecimal string."
        },
    },
    {
        new SignalHelp {
            "initialized",
            { },
            "Emited when CANopen handler initializes (i.e: loading OBD and creating protocol manager on CAN bus)."
        },
        new SignalHelp {
            "started",
            { },
            "Emited after the CANopen handler has been started (i.e: when parent node is started)."
        },
        new SignalHelp {
            "stopped",
            { },
            "Emited after the CANopen handler has been stopped (i.e: when parent node is stopped)."
        },
        new SignalHelp {
            "resetting",
            { },
            "Emited when CANopen resets (i.e: when it needs to reload due to hot param changing)."
        },
    },
    { },
    "This object represents a CANopen handler (operates on a CAN bus, with an OBD and some params)."
};

static ObjectHelp byteArrayWrapper {
    "ByteArrayWrapper", "object", { }, { }, "helper",
    { },
    {
        new MethodHelp {
            "toBool", "bool",
            {  },
            "Convert bytes to bool."
        },
        new MethodHelp {
            "toInt8", "int8",
            {  },
            "Convert bytes to signed 8 bit integer."
        },
        new MethodHelp {
            "toInt16", "int16",
            {  },
            "Convert bytes to signed 16 bit integer."
        },
        new MethodHelp {
            "toInt32", "int32",
            {  },
            "Convert bytes to signed 32 bit integer."
        },
        new MethodHelp {
            "toUInt8", "uint8",
            {  },
            "Convert bytes to unsigned 8 bit integer."
        },
        new MethodHelp {
            "toUInt16", "uint16",
            {  },
            "Convert bytes to unsigned 16 bit integer."
        },
        new MethodHelp {
            "toUInt32", "uint32",
            {  },
            "Convert bytes to unsigned 32 bit integer."
        },
        new MethodHelp {
            "toString", "string",
            {  },
            "Convert bytes to ASCII/Latin1 string."
        },
        new MethodHelp {
            "toHex", "string",
            {  },
            "Convert bytes to hexadecimal string."
        },
    },
    { },
    { },
    "This helpers contains binary buffer and allows manipulate it from QML."
};

// TODO : Dictionary (convenience)

static ObjectHelp objectRefListModel {
    "ObjectRefListModel", "model", { }, { }, "helper",
    {
        new PropertyHelp {
            "count", "uint32", "readonly", false,
            "The number of objects in the list."
        },
    },
    {
        new MethodHelp {
            "clear", "void",
            { },
            "Clear the model (be carefull, it doesn't destroy the objects, you have to handle them separately)."
        },
        new MethodHelp {
            "append", "void",
            {
                new ArgumentHelp {
                    "object", "BasicObject", "", false,
                    "The object reference."
                },
            },
            "Append given object to model."
        },
        new MethodHelp {
            "remove", "void",
            {
                new ArgumentHelp {
                    "object", "BasicObject", "", false,
                    "The object reference."
                },
            },
            "Remove given object from model."
        },
        new MethodHelp {
            "get", "BasicObject",
            {
                new ArgumentHelp {
                    "idx", "uint32", "", false,
                    "The index in model (between '0' and 'count -1')."
                },
            },
            "Returns object at given position in model."
        }
    },
    {
        new SignalHelp {
            "itemAdded",
            {
                new ArgumentHelp {
                    "item", "BasicObject", "", false,
                    "The object that have been added."
                },
            },
            "Emited when an object is added to the model."
        },
        new SignalHelp {
            "itemRemoved",
            {
                new ArgumentHelp {
                    "item", "BasicObject", "", false,
                    "The object that have been removed."
                },
            },
            "Emited when an object is removed from the model."
        },
    },
    { },
    "This class is a basic list model of Qt objects, allowing to manager dynamic list of objects from C++ and expose them to QML."
};

static ObjectHelp dashboard {
    "Dashboard", "item", { }, { "CircleButton", "LedLight", "CircularGauge", "TicksJauge", "LinearGauge", "Oscilloscope", "OnOffSwitcher", "NumberValueDisplay", "NumberValueInput", "RotativeKnob", "MultiChoiceList" }, "instantiable",
    {
        new PropertyHelp {
            "uid", "string", "writable", false,
            "The UID for the dashboard. Must be unique."
        },
        new PropertyHelp {
            "title", "string", "writable", false,
            "The human readable name for the dashboard, displayed in UI when hovering the button."
        },
        new PropertyHelp {
            "width", "uint32", "writable", false,
            "The width for the dashboard (e.g: 800)."
        },
        new PropertyHelp {
            "height", "uint32", "writable", false,
            "The height for the dashboard (e.g: 480)."
        },
    },
    { },
    { },
    { },
    "The base container for dashboard UIs. Put visual components inside."
};

static ObjectHelp oscilloscope {
    "Oscilloscope", "item", { }, { "PlotParams" }, "instantiable",
    {
        new PropertyHelp {
            "timeFrame", "uint32", "writable", false,
            "The time window to keep values and draw them (5000 ms by default). You can change it in UI."
        },
        new PropertyHelp {
            "samplingInterval", "uint32", "writable", false,
            "The period used to sample the source value and add it to the plot (20 ms by default). You can change it in UI."
        },
        new PropertyHelp {
            "lineSize", "uint32", "writable", false,
            "The line width for the plots curves (2 by default)."
        },
        new PropertyHelp {
            "running", "bool", "writable", false,
            "Whether the values are being sampled right now. Normally you don't change this in code, use the buttons in UI."
        },
        new PropertyHelp {
            "plotsParams", "PlotParams", "writable", true,
            "The list of plot parameters objects that define 'what' and 'how' is plotted. This is the default property."
        },
        new PropertyHelp {
            "width", "uint32", "writable", false,
            "The width for the dashboard (e.g: 800)."
        },
        new PropertyHelp {
            "height", "uint32", "writable", false,
            "The height for the dashboard (e.g: 480)."
        },
    },
    { },
    { },
    { },
    "This object can draw 1 or more plots tracing values in time, with various scales."
};

static ObjectHelp plotParams {
    "PlotParams", "object", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "currentValue", "float", "writable", false,
            "Bind the value to trace, it will be tracked by the sampler."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "Bind the minimum boundary for the scale."
        },
        new PropertyHelp {
            "maxValue", "float", "writable", false,
            "Bind the maximum boundary for the scale."
        },
        new PropertyHelp {
            "markValues", "float", "writable", true,
            "The special values you want displayed on the scale. By default the scale shows only min and max."
        },
        new PropertyHelp {
            "decimals", "uint32", "writable", false,
            "The number of decimals to keep on floating point values (0 by default)."
        },
        new PropertyHelp {
            "lineColor", "color", "writable", false,
            "The drawing color for the plot line (by default, the foreground color of UI)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The description to be displayed beside the scale."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit displayed beside values."
        },
    },
    { },
    { },
    { },
    "The object containing the params for sampling and drawing a curve on the Oscilloscope."
};

static ObjectHelp circularGauge {
    "CircularGauge", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The diameter of the gauge (200 by default)."
        },
        new PropertyHelp {
            "step", "uint32", "writable", false,
            "The step between small ticks (5 by default)."
        },
        new PropertyHelp {
            "marks", "uint32", "writable", false,
            "The step between big ticks with labels (20 by default)."
        },
        new PropertyHelp {
            "value", "float", "writable", false,
            "Bind a something on this property to have it updated on change."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "Bind the minimum boundary for the gauge (0 by default)."
        },
        new PropertyHelp {
            "maxValue", "float", "writable", false,
            "Bind the maximum boundary for the gauge (100 by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend displayed on the gauge bottom center (empty by default)."
        },
    },
    { },
    { },
    { },
    "This item displays a circular gauge (like a speedometer), with customizable range and steps."
};

static ObjectHelp linearGauge {
    "LinearGauge", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "float", "writable", false,
            "Bind a something on this property to have it updated on change."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "Bind the minimum boundary for the gauge (0 by default)."
        },
        new PropertyHelp {
            "maxValue", "float", "writable", false,
            "Bind the maximum boundary for the gauge (100 by default)."
        },
        new PropertyHelp {
            "colorMin", "color", "writable", false,
            "The gradient stop color for minimum value (blue by default)."
        },
        new PropertyHelp {
            "colorMax", "color", "writable", false,
            "The gradient stop color for maximum value (red by default)."
        },
        new PropertyHelp {
            "divisions", "uint32", "writable", false,
            "The number of divisions of the gauge (10 by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend displayed on the gauge (empty by default)."
        },
        new PropertyHelp {
            "side", "enum (Borders)", "writable", false,
            "The border side for the legend (right by default)."
        },
        new PropertyHelp {
            "width", "uint32", "writable", false,
            "The width for the gauge, not including the legend (20 by default)."
        },
        new PropertyHelp {
            "height", "uint32", "writable", false,
            "The height for the gauge, not including the legend (200 by default)."
        },
    },
    { },
    { },
    { },
    "This item displays a circular gauge (like a thermometer), with customizable range and gradient."
};

static ObjectHelp ticksJauge {
    "TicksJauge", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "value", "float", "writable", false,
            "Bind a something on this property to have it updated on change."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "Bind the minimum boundary for the gauge (0 by default)."
        },
        new PropertyHelp {
            "maxValue", "float", "writable", false,
            "Bind the maximum boundary for the gauge (100 by default)."
        },
        new PropertyHelp {
            "divisions", "uint32", "writable", false,
            "The number of ticks (20 by default)."
        },
        new PropertyHelp {
            "baseColor", "color", "writable", false,
            "The color for the ticks (blue by default)."
        },
        new PropertyHelp {
            "tickWidth", "color", "writable", false,
            "The width for the ticks (8 by default)."
        },
        new PropertyHelp {
            "tickHeight", "color", "writable", false,
            "The height for the ticks (30 by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend displayed on the gauge (empty by default)."
        },
    },
    { },
    { },
    { },
    "This item shows ticks that change color when value is in range."
};

static ObjectHelp rotativeKnob {
    "RotativeKnob", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The diameter of the knob (80 by default)."
        },
        new PropertyHelp {
            "value", "float", "writable", false,
            "Bind a something on this property to have it updated on change."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "Bind the minimum boundary for the knob (0 by default)."
        },
        new PropertyHelp {
            "maxValue", "float", "writable", false,
            "Bind the maximum boundary for the knob (100 by default)."
        },
        new PropertyHelp {
            "decimals", "uint32", "writable", false,
            "The number of decimals to keep on floating point values (0 by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend displayed on the knob (empty by default)."
        },
        new PropertyHelp {
            "side", "enum (Borders)", "writable", false,
            "The border side for the legend (bottom by default)."
        },
    },
    { },
    { },
    { },
    "This item allows changing a numeric value using a rotative knob (e.g: potentiometer). It loops back to min value when reaching max value."
};

static ObjectHelp onOffSwitcher {
    "OnOffSwitcher", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The height of the switcher, width is double (40 by default)."
        },
        new PropertyHelp {
            "value", "bool", "writable", false,
            "The state of the switcher (off by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend text for the switcher (empty by default)."
        },
        new PropertyHelp {
            "side", "enum (Borders)", "writable", false,
            "The border side for the legend (bottom by default)."
        },
    },
    { },
    { },
    { },
    "An interactive ON/OFF switcher that let user change a boolean value from UI."
};

static ObjectHelp ledLight {
    "LedLight", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The diameter of the LED's circle (20 by default)."
        },
        new PropertyHelp {
            "baseColor", "color", "writable", false,
            "The base light color for button (cyan by default)."
        },
        new PropertyHelp {
            "active", "bool", "writable", false,
            "The state of the LED (off by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend text for the LED (empty by default)."
        },
        new PropertyHelp {
            "side", "enum (Borders)", "writable", false,
            "The border side for the legend (left by default)."
        },
    },
    { },
    { },
    { },
    "A simple color LED that can be lit on or off."
};

static ObjectHelp numberValueDisplay {
    "NumberValueDisplay", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The font size of the value, all the other sizes are derived from this."
        },
        new PropertyHelp {
            "decimals", "uint32", "writable", false,
            "The number of decimals displayed (0 by default)."
        },
        new PropertyHelp {
            "digits", "uint32", "writable", false,
            "The number of digits displayed (3 by default)."
        },
        new PropertyHelp {
            "sign", "bool", "writable", false,
            "Whether the value can be negative (true by default)."
        },
        new PropertyHelp {
            "value", "float", "writable", false,
            "Bind a value on this property to have something displayed and updated on change."
        },
        new PropertyHelp {
            "background", "color", "writable", false,
            "The base color for background gradient (gray by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend text displayed under the value (empty by default)."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the value (empty by default)."
        },
    },
    { },
    { },
    { },
    "This item displays a floating number formatted value along with legend and unit."
};

static ObjectHelp numberValueInput {
    "NumberValueInput", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The font size of the value, all the other sizes are derived from this."
        },
        new PropertyHelp {
            "decimals", "uint32", "writable", false,
            "The number of decimals displayed (0 by default)."
        },
        new PropertyHelp {
            "digits", "uint32", "writable", false,
            "The number of digits displayed (3 by default)."
        },
        new PropertyHelp {
            "sign", "bool", "writable", false,
            "Whether the value can be negative (true by default)."
        },
        new PropertyHelp {
            "value", "float", "writable", false,
            "The value edited in the component."
        },
        new PropertyHelp {
            "minValue", "float", "writable", false,
            "The minimum boundary for the value edited in the component."
        },
        new PropertyHelp {
            "maxnValue", "float", "writable", false,
            "The maximum boundary for the value edited in the component."
        },
        new PropertyHelp {
            "background", "color", "writable", false,
            "The base color for background gradient (gray by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend text displayed under the value (empty by default)."
        },
        new PropertyHelp {
            "unit", "string", "writable", false,
            "The unit of the value (empty by default)."
        },
    },
    { },
    { },
    { },
    "This item displays a floating number formatted value along with legend and unit."
};

static ObjectHelp multiChoiceList {
    "MultiChoiceList", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The font size of the value, all the other sizes are derived from this (20 by default)."
        },
        new PropertyHelp {
            "side", "enum (Borders)", "writable", false,
            "The border side for the legend (top by default)."
        },
        new PropertyHelp {
            "legend", "string", "writable", false,
            "The legend text displayed for the list (empty by default)."
        },
        new PropertyHelp {
            "model", "map<string,variant>", "writable", true,
            "The model for the list. Must be a JSON Array of Object, with the fields 'key' and 'value' for each item."
        },
        new PropertyHelp {
            "currentIdx", "uint32", "writable", false,
            "The index of the current selection in the list (0 by default)."
        },
        new PropertyHelp {
            "currentKey", "variant", "readonly", false,
            "The key of the current selection in the list (depends on the model and the currentIdx)."
        },
        new PropertyHelp {
            "currentValue", "variant", "readonly", false,
            "The value of the current selection in the list (depends on the model and the currentIdx)."
        },
    },
    { },
    { },
    { },
    "This item displays a selectable list of values associated with internal values."
};

static ObjectHelp circleButton {
    "CircleButton", "item", { }, { }, "instantiable",
    {
        new PropertyHelp {
            "size", "uint32", "writable", false,
            "The diameter of the button's circle (50 by default)."
        },
        new PropertyHelp {
            "background", "color", "writable", false,
            "The background color for button (gray by default)."
        },
        new PropertyHelp {
            "label", "string", "writable", false,
            "The text for button (empty by default)."
        },
    },
    { },
    {
        new SignalHelp {
            "clicked",
            { },
            "Emited when user pushes the button."
        }
    },
    { },
    "A circular push button with a text label and customizable background color. Use it to let user trigger events from UI manually."
};

// TODO : Shared (singleton)

static ObjectHelp varTypes {
    "VarTypes", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "Int8",   "Signed 8 bit integer"    },
        new EnumKeyHelp { "UInt8",  "Unsigned 8 bit integer"  },
        new EnumKeyHelp { "Int16",  "Signed 16 bit integer"   },
        new EnumKeyHelp { "UInt16", "Unsigned 16 bit integer" },
        new EnumKeyHelp { "Int32",  "Signed 32 bit integer"   },
        new EnumKeyHelp { "UInt32", "Unsigned 32 bit integer" },
    },
    "This enum contains base types supported in CanDataWrapper RX/TX."
};

static ObjectHelp sides {
    "Sides", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "LEFT",   "The left face"   },
        new EnumKeyHelp { "RIGHT",  "The right face"  },
        new EnumKeyHelp { "TOP",    "The top face"    },
        new EnumKeyHelp { "BOTTOM", "The bottom face" },
        new EnumKeyHelp { "FRONT",  "The front face"  },
        new EnumKeyHelp { "BACK",   "The back face"   },
    },
    "This enum contains the 6 faces of a 3D object."
};

static ObjectHelp axis {
    "Axis", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "LEFT_TO_RIGHT", "The left-to-right horizontal axis" },
        new EnumKeyHelp { "BOTTOM_TO_TOP", "The bottom-to-top vertical axis"   },
        new EnumKeyHelp { "BACK_TO_FRONT", "The back-to-front horizontal axis" },
    },
    "This enum contains the 3 axis of a 3D object."
};

static ObjectHelp corners {
    "Corners", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "FRONT_TOP_RIGHT",    "The top-right corner of the front face"    },
        new EnumKeyHelp { "FRONT_TOP_LEFT",     "The top-left corner of the front face"     },
        new EnumKeyHelp { "FRONT_BOTTOM_RIGHT", "The bottom-right corner of the front face" },
        new EnumKeyHelp { "FRONT_BOTTOM_LEFT",  "The bottom-left corner of the front face"  },
        new EnumKeyHelp { "BACK_TOP_RIGHT",     "The top-right corner of the back face"     },
        new EnumKeyHelp { "BACK_TOP_LEFT",      "The top-left corner of the back face"      },
        new EnumKeyHelp { "BACK_BOTTOM_RIGHT",  "The bottom-right corner of the back face"  },
        new EnumKeyHelp { "BACK_BOTTOM_LEFT",   "The bottom-left corner of the back face"   },
    },
    "This enum contains the 8 corners of a 3D object."
};

static ObjectHelp borders {
    "Borders", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "TOP",    "The top border"    },
        new EnumKeyHelp { "LEFT",   "The left border"   },
        new EnumKeyHelp { "RIGHT",  "The right border"  },
        new EnumKeyHelp { "BOTTOM", "The bottom border" },
    },
    "This enum contains the 4 borders of a 2D object."
};

static ObjectHelp modes {
    "Modes", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "INACTIVE", "The node does nothing. Its routines are disabled, and linked objects too."    },
        new EnumKeyHelp { "ACTIVE",   "The node operates on 'normal' data-flow, with sensors and actuators working." },
        new EnumKeyHelp { "PASSIVE",  "The node operates on 'spy' data-flow, mirroring a physical node."             },
    },
    "This enum contains the different operating modes for node objects."
};

static ObjectHelp objectFamily {
    "ObjectFamily", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "NODE",        "A node (the base unit of network)."                               },
        new EnumKeyHelp { "BOARD",       "A board (the sub-unit of the node, that contains I/Os)."          },
        new EnumKeyHelp { "IO",          "An I/O (either analog or digital)."                               },
        new EnumKeyHelp { "SENSOR",      "A sensor or manipulator."                                         },
        new EnumKeyHelp { "ACTUATOR",    "An actuator."                                                     },
        new EnumKeyHelp { "LINK",        "A link for data-flow."                                            },
        new EnumKeyHelp { "TRANSFORMER", "A transformer for links."                                         },
        new EnumKeyHelp { "ROUTINE",     "A logic tied to a particular trigger."                            },
        new EnumKeyHelp { "VALUE",       "A physical value, with boundaries and speed/acceleration."        },
        new EnumKeyHelp { "POSITION",    "A 3D position."                                                   },
        new EnumKeyHelp { "SIZE",        "A 3D size."                                                       },
        new EnumKeyHelp { "ANGLE",       "A 3D angle."                                                      },
        new EnumKeyHelp { "BLOCK",       "A physical block with position, size, and angle."                 },
        new EnumKeyHelp { "MARKER",      "A virtual marker on a physical block to track absolute location." },
        new EnumKeyHelp { "WORLD",       "The physical world limits."                                       },
        new EnumKeyHelp { "CANOPEN",     "A CANopen handler."                                               },
        new EnumKeyHelp { "CANBUS",      "A CAN bus for raw message send/receive handling."                 },
        new EnumKeyHelp { "SERIALBUS",   "A serial bus, for byte-based types read/write handling."          },
    },
    "This enum contains the different kinds of objects that can be used in the network definition."
};

static ObjectHelp objectType {
    "ObjectType", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "ANALOG",  "An analog object (working with integer value between min and max)." },
        new EnumKeyHelp { "DIGITAL", "A digital object (working with boolean state)."                     },
    },
    "This enum contains the different types of data-flow objects that can be used in the network definition."
};

static ObjectHelp objectDirection {
    "ObjectDirection", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "INPUT",  "An input object."  },
        new EnumKeyHelp { "OUTPUT", "An output object." },
    },
    "This enum contains the different directions of data-flow objects that can be used in the network definition."
};

static ObjectHelp triggers {
    "Triggers", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "TIMER_TICK",                "Routine is triggered on timer tick."                           },
        new EnumKeyHelp { "EVENT_SIGNAL",              "Routine is triggered on signal emited."                        },
        new EnumKeyHelp { "CAN_FRAME_RECV",            "Routine is triggered when specific CAN frame ID is received."  },
        new EnumKeyHelp { "CANOPEN_STATE_CHANGE",      "Routine is triggered when CANopen node NMT state changes."     },
        new EnumKeyHelp { "CANOPEN_OBD_VAL_CHANGE",    "Routine is triggered when CANopen specific OBD entry changes." },
        new EnumKeyHelp { "CANOPEN_SDO_READ_REPLY",    "Routine is triggered when an SDO read reply is received."      },
        new EnumKeyHelp { "CANOPEN_SDO_WRITE_REQUEST", "Routine is triggered when an SDO write request is received."   },
        new EnumKeyHelp { "SERIAL_FRAME_RECV",         "Routine is triggered when a serial byte or more is received."  },
    },
    "This enum contains the different triggers that can execute routines."
};

static ObjectHelp canOpenNmtCmd {
    "CanOpenNmtCmd", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "StartNode", "Ask remote node to start operational mode  (0x01)." },
        new EnumKeyHelp { "StopNode",  "Ask remote node to stop                    (0x02)." },
        new EnumKeyHelp { "SetPreOp",  "Ask remote node to go pre-operational mode (0x80)." },
        new EnumKeyHelp { "ResetNode", "Ask remote node to reset its application   (0x81)." },
        new EnumKeyHelp { "ResetComm", "Ask remote node to reset its communication (0x82)." },
    },
    "This enum contains the CANopen NMT state changed commands that can be sent by a master node."
};

static ObjectHelp canOpenNmtState {
    "CanOpenNmtState", "", { }, { }, "enum",
    { },
    { },
    { },
    {
        new EnumKeyHelp { "INITIALIZING",    "Node is initializing/booting-up (0x00)."        },
        new EnumKeyHelp { "STOPPED",         "Node have been stopped (0x04)."                 },
        new EnumKeyHelp { "OPERATIONAL",     "Node is operational, PDOs activated (0x05)."    },
        new EnumKeyHelp { "PRE_OPERATIONAL", "Node is pre-operational, SDOs accepted (0x7F)." },
    },
    "This enum contains the CANopen NMT state values for local node."
};

ObjectHelp * Help::getHelpForTypeByName (const QString & name) {
    ObjectHelp * ret = m_objectsByName.value (name, Q_NULLPTR);
    QQmlEngine::setObjectOwnership (ret, QQmlEngine::CppOwnership);
    return ret;
}

void Help::initMap (void) {
    registerHelp (basicObject);
    registerHelp (objectsGroup);
    registerHelp (objectRefListModel);
    registerHelp (abstractSensor);
    registerHelp (analogSensor);
    registerHelp (digitalSensor);
    registerHelp (hybridSensor);
    registerHelp (abstractActuator);
    registerHelp (analogActuator);
    registerHelp (digitalActuator);
    registerHelp (hybridActuator);
    registerHelp (abstractIO);
    registerHelp (abstractAnalogIO);
    registerHelp (analogInput);
    registerHelp (analogOutput);
    registerHelp (abstractDigitalIO);
    registerHelp (digitalInput);
    registerHelp (digitalOutput);
    registerHelp (abstractRoutine);
    registerHelp (routineOnTimer);
    registerHelp (routineOnCanFrame);
    registerHelp (routineOnEvent);
    registerHelp (routineOnSerialFrame);
    registerHelp (abstractCanOpenRoutine);
    registerHelp (routineOnCanOpenStateChange);
    registerHelp (routineOnCanOpenObdValChange);
    registerHelp (routineOnCanOpenSdoWriteRequest);
    registerHelp (routineOnCanOpenSdoReadReply);
    registerHelp (routineOnCanOpenBootUp);
    registerHelp (abstractLink);
    registerHelp (linkPhysicalValueToAnalogSensor);
    registerHelp (linkAnalogSensorToAnalogInput);
    registerHelp (linkDigitalSensorToDigitalInput);
    registerHelp (linkAnalogOutputToAnalogActuator);
    registerHelp (linkDigitalOutputToDigitalActuator);
    registerHelp (linkAnalogActuatorToPhysicalValue);
    registerHelp (linkAnalogOutputToAnalogInput);
    registerHelp (linkDigitalOutputToDigitalInput);
    registerHelp (linkPhysicalValueToHybridSensor);
    registerHelp (linkHybridSensorToDigitalInput);
    registerHelp (linkDigitalOutputToHybridActuator);
    registerHelp (linkHybridActuatorToPhysicalValue);
    registerHelp (physicalValue);
    registerHelp (physicalPoint);
    registerHelp (physicalSize);
    registerHelp (physicalAngle);
    registerHelp (physicalBlock);
    registerHelp (physicalMarker);
    registerHelp (physicalWorld);
    registerHelp (node);
    registerHelp (board);
    registerHelp (networkDefinition);
    registerHelp (canBus);
    registerHelp (serialBus);
    registerHelp (abstractTransformer);
    registerHelp (affineTransformer);
    registerHelp (customTransformer);
    registerHelp (memory);
    registerHelp (canOpen);
    registerHelp (canDataWrapperRx);
    registerHelp (canDataWrapperTx);
    registerHelp (byteArrayWrapper);
    registerHelp (sides);
    registerHelp (axis);
    registerHelp (corners);
    registerHelp (borders);
    registerHelp (modes);
    registerHelp (objectFamily);
    registerHelp (objectType);
    registerHelp (objectDirection);
    registerHelp (triggers);
    registerHelp (varTypes);
    registerHelp (canOpenNmtCmd);
    registerHelp (canOpenNmtState);
    registerHelp (dashboard);
    registerHelp (oscilloscope);
    registerHelp (plotParams);
    registerHelp (circleButton);
    registerHelp (ledLight);
    registerHelp (onOffSwitcher);
    registerHelp (numberValueDisplay);
    registerHelp (numberValueInput);
    registerHelp (circularGauge);
    registerHelp (linearGauge);
    registerHelp (ticksJauge);
    registerHelp (rotativeKnob);
    registerHelp (multiChoiceList);
}

void Help::initIndex (void) {
    const QList<HelpSection *> tmp {
        new HelpSection {
            QObject::tr ("Base"),
            {
                new HelpPage { 1, "NetworkDefinition"  },
                new HelpPage { 1, "BasicObject"        },
                new HelpPage { 1, "ObjectsGroup"       },
                new HelpPage { 1, "ObjectRefListModel" },
            }
        },
        new HelpSection {
            QObject::tr ("Enumerations"),
            {
                new HelpPage { 1, "Axis"            },
                new HelpPage { 1, "Sides"           },
                new HelpPage { 1, "Corners"         },
                new HelpPage { 1, "Modes"           },
                new HelpPage { 1, "Triggers"        },
                new HelpPage { 1, "ObjectFamily"    },
                new HelpPage { 1, "ObjectType"      },
                new HelpPage { 1, "ObjectDirection" },
                new HelpPage { 1, "CanOpenNmtCmd"   },
                new HelpPage { 1, "CanOpenNmtState" },
            }
        },
        new HelpSection {
            QObject::tr ("Electronic"),
            {
                new HelpPage { 1, "Node"                               },
                new HelpPage { 1, "Board"                              },
                new HelpPage { 1, "AbstractIO"                         },
                new HelpPage { 2, "AnalogInput"                        },
                new HelpPage { 2, "AnalogOutput"                       },
                new HelpPage { 2, "DigitalInput"                       },
                new HelpPage { 2, "DigitalOutput"                      },
                new HelpPage { 1, "AbstractSensor"                     },
                new HelpPage { 2, "AnalogSensor"                       },
                new HelpPage { 2, "DigitalSensor"                      },
                new HelpPage { 2, "HybridSensor"                       },
                new HelpPage { 1, "AbstractActuator"                   },
                new HelpPage { 2, "AnalogActuator"                     },
                new HelpPage { 2, "DigitalActuator"                    },
                new HelpPage { 2, "HybridActuator"                     },
                new HelpPage { 1, "AbstractLink"                       },
                new HelpPage { 2, "LinkPhysicalValueToAnalogSensor"    },
                new HelpPage { 2, "LinkPhysicalValueToHybridSensor"    },
                new HelpPage { 2, "LinkAnalogSensorToAnalogInput"      },
                new HelpPage { 2, "LinkDigitalSensorToDigitalInput"    },
                new HelpPage { 2, "LinkHybridSensorToDigitalInput"     },
                new HelpPage { 2, "LinkAnalogOutputToAnalogActuator"   },
                new HelpPage { 2, "LinkDigitalOutputToDigitalActuator" },
                new HelpPage { 2, "LinkDigitalOutputToHybridActuator"  },
                new HelpPage { 2, "LinkHybridActuatorToPhysicalValue"  },
                new HelpPage { 2, "LinkAnalogActuatorToPhysicalValue"  },
                new HelpPage { 2, "LinkAnalogOutputToAnalogInput"      },
                new HelpPage { 2, "LinkDigitalOutputToDigitalInput"    },
                new HelpPage { 1, "AbstractTransformer"                },
                new HelpPage { 2, "AffineTransformer"                  },
                new HelpPage { 2, "CustomTransformer"                  },
            }
        },
        new HelpSection {
            QObject::tr ("Logic"),
            {
                new HelpPage { 1, "Memory"                          },
                new HelpPage { 1, "CanBus"                          },
                new HelpPage { 1, "CanOpen"                         },
                new HelpPage { 1, "SerialBus"                       },
                new HelpPage { 1, "ByteArrayWrapper"                },
                new HelpPage { 1, "CanDataWrapperRx"                },
                new HelpPage { 1, "CanDataWrapperTx"                },
                new HelpPage { 1, "AbstractRoutine"                 },
                new HelpPage { 2, "RoutineOnTimer"                  },
                new HelpPage { 2, "RoutineOnEvent"                  },
                new HelpPage { 2, "RoutineOnCanFrame"               },
                new HelpPage { 2, "RoutineOnSerialFrame"            },
                new HelpPage { 2, "AbstractCanOpenRoutine"          },
                new HelpPage { 3, "RoutineOnCanOpenStateChange"     },
                new HelpPage { 3, "RoutineOnCanOpenObdValChange"    },
                new HelpPage { 3, "RoutineOnCanOpenSdoWriteRequest" },
                new HelpPage { 3, "RoutineOnCanOpenSdoReadReply"    },
                new HelpPage { 3, "RoutineOnCanOpenBootUp"          },
            }
        },
        new HelpSection {
            QObject::tr ("Physics"),
            {
                new HelpPage { 1, "PhysicalValue"   },
                new HelpPage { 1, "PhysicalPoint"   },
                new HelpPage { 1, "PhysicalSize"    },
                new HelpPage { 1, "PhysicalAngle"   },
                new HelpPage { 1, "PhysicalBlock"   },
                new HelpPage { 1, "PhysicalMarker"  },
                new HelpPage { 1, "PhysicalWorld"   },
            }
        },
        new HelpSection {
            QObject::tr ("User-frontend"),
            {
                new HelpPage { 1, "Dashboard"          },
                new HelpPage { 1, "Oscilloscope"       },
                new HelpPage { 1, "PlotParams"         },
                new HelpPage { 1, "CircularGauge"      },
                new HelpPage { 1, "LinearGauge"        },
                new HelpPage { 1, "TicksJauge"         },
                new HelpPage { 1, "RotativeKnob"       },
                new HelpPage { 1, "OnOffSwitcher"      },
                new HelpPage { 1, "LedLight"           },
                new HelpPage { 1, "NumberValueDisplay" },
                new HelpPage { 1, "NumberValueInput"   },
                new HelpPage { 1, "CircleButton"       },
                new HelpPage { 1, "MultiChoiceList"    },
            }
        },
    };
    m_sectionsList->append (tmp);
}

Help::Help (QObject * parent)
    : QObject (parent)
{
    m_sectionsList = new QQmlObjectListModel<HelpSection> (this, "name", "name");
    initMap ();
    initIndex ();
}

void Help::registerHelp (ObjectHelp & help) {
    m_objectsByName.insert (help.get_name (), &help);
}
