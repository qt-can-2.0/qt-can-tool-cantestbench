#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <QtGlobal>
#include <QtMath>

class MathUtils {
public:
    static float intToFloat (const int val, const int decimals) {
        return float (qreal (val) / qPow (10, decimals));
    }

    static int floatToInt (const float val, const int decimals) {
        return int (qreal (val) * qPow (10, decimals));
    }

    template<typename T> static T convert (const T srcVal, const T srcMin, const T srcMax, const T dstMin, const T dstMax) {
        const qreal srcRatio (qreal (srcVal - srcMin) / qreal (srcMax - srcMin));
        const T dstRange (dstMax - dstMin);
        return T (dstMin + dstRange * srcRatio);
    }

    template<typename T> static T clamp (const T val, const T min, const T max) {
        return T (val > max ? max : (val < min ? min : val));
    }
};

#endif // MATHUTILS_H
