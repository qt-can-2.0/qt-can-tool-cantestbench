#ifndef DASHBOARD_H
#define DASHBOARD_H

#include <QQuickItem>
#include <QQmlParserStatus>

#include "IdentifiableObject.h"

QML_ENUM_CLASS (Borders,
                TOP,
                LEFT,
                RIGHT,
                BOTTOM,
                NB_BORDERS)

class Dashboard : public QQuickItem {
    Q_OBJECT
    QML_WRITABLE_CSTREF_PROPERTY (QString, uid)
    QML_WRITABLE_CSTREF_PROPERTY (QString, title)

public:
    explicit Dashboard (QQuickItem * parent = Q_NULLPTR);
    virtual ~Dashboard (void);

    void classBegin        (void) Q_DECL_FINAL;
    void componentComplete (void) Q_DECL_FINAL;
};

#endif // DASHBOARD_H
