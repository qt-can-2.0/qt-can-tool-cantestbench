#ifndef ABSTRACTROUTINE_H
#define ABSTRACTROUTINE_H

#include <QObject>
#include <QMultiHash>
#include <QTimer>
#include <QPointer>
#include <QElapsedTimer>
#include <QQmlParserStatus>

#include "CanMessage.h"

#include "CanOpenDefs.h"

#include "IdentifiableObject.h"

QML_ENUM_CLASS (Triggers,
                UNKNOWN_TRIGGER,
                TIMER_TICK,
                EVENT_SIGNAL,
                CAN_FRAME_RECV,
                CANOPEN_STATE_CHANGE,
                CANOPEN_OBD_VAL_CHANGE,
                CANOPEN_SDO_READ_REPLY,
                CANOPEN_SDO_WRITE_REQUEST,
                CANOPEN_BOOTUP,
                CANOPEN_HB_CONSUMER,
                SERIAL_FRAME_RECV,
                NB_TRIGGERS)

QML_ENUM_CLASS (CanOpenNmtState,
                INITIALIZING    = 0x00,
                STOPPED         = 0x04,
                OPERATIONAL     = 0x05,
                PRE_OPERATIONAL = 0x7F,
                NB_STATE)

class Node;
class Memory;
class SerialBus;
class CanBus;
class CanOpen;
class CanOpenSubEntry;
class CanOpenProtocolManager;
class ByteArrayWrapper;
class CanDataWrapperRx;
class CanDataWrapperTx;

class AbstractRoutine : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (Triggers::Type, triggerType)
    QML_WRITABLE_VAR_PROPERTY (bool, benchmarkExecutionTime)
    QML_WRITABLE_VAR_PROPERTY (bool, dumpMemoryAfterExecution)
    QML_READONLY_PTR_PROPERTY (Node, node)
    QML_READONLY_PTR_PROPERTY (Memory, memory)

public:
    explicit AbstractRoutine (const Triggers::Type triggerType = Triggers::UNKNOWN_TRIGGER, QObject * parent = Q_NULLPTR);
    virtual ~AbstractRoutine (void);

    void onComponentCompleted (void) Q_DECL_OVERRIDE;

public slots:
    void execute (void);

protected:
    virtual void trigger (void);

protected:
    bool m_ready;

private:
    QElapsedTimer m_benchmark;
};

class RoutineOnTimer : public AbstractRoutine {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, interval)

public:
    explicit RoutineOnTimer (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnTimer (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void start (void);
    void stop  (void);

signals:
    void triggered (const int loops);

protected:
    void trigger (void) Q_DECL_FINAL;

protected slots:
    void updateTimer (void);

private:
    int m_loops;
    QTimer * m_timer;
};

class RoutineOnCanFrame : public AbstractRoutine {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, canId)
    QML_WRITABLE_PTR_PROPERTY (CanBus, canBus)

public:
    explicit RoutineOnCanFrame (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanFrame (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    CanDataWrapperRx * getFrameRx (void) const;

signals:
    void triggered (CanDataWrapperRx * frameRx);

protected:
    void trigger (void) Q_DECL_FINAL;

protected slots:
    void updateSubscription (void);

private:
    CanDataWrapperRx * m_canDataWrapperRx;
};

class RoutineOnEvent : public AbstractRoutine {
    Q_OBJECT
    QML_WRITABLE_PTR_PROPERTY (QObject, emiter)
    QML_WRITABLE_CSTREF_PROPERTY (QString, signalName)

public:
    explicit RoutineOnEvent (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnEvent (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (void);
};

class RoutineOnSerialFrame : public AbstractRoutine {
    Q_OBJECT
    QML_WRITABLE_PTR_PROPERTY (SerialBus, serialBus)

public:
    explicit RoutineOnSerialFrame (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnSerialFrame (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void updateSubscription (void);

signals:
    void triggered ();

protected:
    void trigger (void) Q_DECL_FINAL;
};

class AbstractCanOpenRoutine : public AbstractRoutine {
    Q_OBJECT
    QML_WRITABLE_PTR_PROPERTY (CanOpen, canOpen)

public:
    explicit AbstractCanOpenRoutine (const Triggers::Type triggerType = Triggers::UNKNOWN_TRIGGER, QObject * parent = Q_NULLPTR);
    virtual ~AbstractCanOpenRoutine (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

protected slots:
    virtual void prepare (void) = 0;
    virtual void cleanup (void) = 0;
};

class RoutineOnCanOpenStateChange : public AbstractCanOpenRoutine {
    Q_OBJECT

public:
    explicit RoutineOnCanOpenStateChange (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenStateChange (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const CanOpenNmtState::Type state);

private:
    QPointer<CanOpenProtocolManager> m_canMgr;
};

class RoutineOnCanOpenObdValChange : public AbstractCanOpenRoutine {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, index)
    QML_WRITABLE_VAR_PROPERTY (int, subIndex)

public:
    explicit RoutineOnCanOpenObdValChange (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenObdValChange (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const QVariant & value);

private:
    QPointer<CanOpenSubEntry> m_subEntry;
};

class RoutineOnCanOpenSdoWriteRequest : public AbstractCanOpenRoutine {
    Q_OBJECT

public:
    explicit RoutineOnCanOpenSdoWriteRequest (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenSdoWriteRequest (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const int nodeId, const int index, const int subIndex, ByteArrayWrapper * buffer);

protected slots:
    void onRecvSdoWriteRequest (const CanOpenNodeId nodeId,
                                const CanOpenIndex idx,
                                const CanOpenSubIndex subIdx,
                                const CanOpenSdoAbortCode statusCode,
                                const QByteArray & buffer);

private:
    QPointer<CanOpenProtocolManager> m_canMgr;
    CanOpenNodeId m_currNodeId;
    CanOpenIndex m_currIndex;
    CanOpenSubIndex m_currSubIndex;
    CanOpenSdoAbortCode m_currStatusCode;
    ByteArrayWrapper * m_currBuffer;
};

class RoutineOnCanOpenSdoReadReply : public AbstractCanOpenRoutine {
    Q_OBJECT

public:
    explicit RoutineOnCanOpenSdoReadReply (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenSdoReadReply (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const int nodeId, const int index, const int subIndex, ByteArrayWrapper * buffer);

protected slots:
    void onRecvSdoReadReply (const CanOpenNodeId nodeId,
                             const CanOpenIndex idx,
                             const CanOpenSubIndex subIdx,
                             const CanOpenSdoAbortCode statusCode,
                             const QByteArray & buffer);

private:
    QPointer<CanOpenProtocolManager> m_canMgr;
    CanOpenNodeId m_currNodeId;
    CanOpenIndex m_currIndex;
    CanOpenSubIndex m_currSubIndex;
    CanOpenSdoAbortCode m_currStatusCode;
    ByteArrayWrapper * m_currBuffer;
};

class RoutineOnCanOpenBootUp : public AbstractCanOpenRoutine { // DEPRECATED, use RoutineOnCanOpenHeartbeatConsumer
    Q_OBJECT

public:
    explicit RoutineOnCanOpenBootUp (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenBootUp (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const int nodeId);

protected slots:
    void onRecvHeartbeatState (const CanOpenNodeId nodeId,
                               const CanOpenHeartBeatState state);

private:
    QPointer<CanOpenProtocolManager> m_canMgr;
    CanOpenNodeId m_currNodeId;
};

class RoutineOnCanOpenHeartbeatConsumer : public AbstractCanOpenRoutine {
    Q_OBJECT

public:
    explicit RoutineOnCanOpenHeartbeatConsumer (QObject * parent = Q_NULLPTR);
    virtual ~RoutineOnCanOpenHeartbeatConsumer (void);

    void prepare (void) Q_DECL_FINAL;
    void cleanup (void) Q_DECL_FINAL;

protected:
    void trigger (void) Q_DECL_FINAL;

signals:
    void triggered (const int nodeId, const bool alive, const int state);

protected slots:
    void onRemoteNodeStateChanged (const CanOpenNodeId nodeId,
                                   const CanOpenHeartBeatState state);
    void onRemoteNodeAliveChanged (const CanOpenNodeId nodeId,
                                   const bool alive);

private:
    QPointer<CanOpenProtocolManager> m_canMgr;
    CanOpenNodeId m_currNodeId;
    CanOpenHeartBeatState m_currState;
    bool m_currAlive;
};

#endif // ABSTRACTROUTINE_H
