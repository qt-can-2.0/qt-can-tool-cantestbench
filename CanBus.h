#ifndef CANBUS_H
#define CANBUS_H

#include <QObject>
#include <QMultiHash>
#include <QQmlParserStatus>

#include "CanDriver.h"
#include "CanMessage.h"

#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"

#include "IdentifiableObject.h"

class CanDriverWrapper;
class CanDataWrapperTx;
class RoutineOnCanFrame;

class CanDriverProxy : public CanDriver {
    Q_OBJECT

public:
    explicit CanDriverProxy (CanDriverWrapper * wrapper = Q_NULLPTR, QObject * parent = Q_NULLPTR);
    virtual ~CanDriverProxy (void);

public slots:
    bool init (const QVariantMap & options) Q_DECL_FINAL;
    bool send (CanMessage * message) Q_DECL_FINAL;
    bool stop (void) Q_DECL_FINAL;

private:
    CanDriverWrapper * m_wrapper;
};

class CanBus : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (CanDataWrapperTx, frameTx)
    QML_CONSTANT_PTR_PROPERTY (CanDriverWrapper, driverWrapper)
    QML_CONSTANT_PTR_PROPERTY (CanDriverProxy, driverProxy)

public:
    explicit CanBus (QObject * parent = Q_NULLPTR);
    virtual ~CanBus (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void subscribeRoutineToCanFrameId (RoutineOnCanFrame * routine, int canId);

protected:
    void onRecv (CanMessage * msg);
    void onDiag (const QString & type, const QString & description);

private:
    QMultiHash<int, RoutineOnCanFrame *> m_routinesForCanId;
};

#endif // CANBUS_H
