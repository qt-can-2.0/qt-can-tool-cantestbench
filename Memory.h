#ifndef MEMORY_H
#define MEMORY_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QString>
#include <QVariant>
#include <QVariantMap>
#include <QQmlError>
#include <QQmlParserStatus>
#include <QElapsedTimer>
#include <QtMath>

#include "IdentifiableObject.h"

#include "TypesQmlWrapper.h"

typedef unsigned int quint;

typedef QMap<QByteArray, QByteArray> ByteArraysMap;

template<typename T> struct TypeDetector          { static const VarType varType = VarTypes::Unknown; };
template<>           struct TypeDetector<qint8>   { static const VarType varType = VarTypes::Int8;    };
template<>           struct TypeDetector<quint8>  { static const VarType varType = VarTypes::UInt8;   };
template<>           struct TypeDetector<qint16>  { static const VarType varType = VarTypes::Int16;   };
template<>           struct TypeDetector<quint16> { static const VarType varType = VarTypes::UInt16;  };
template<>           struct TypeDetector<qint32>  { static const VarType varType = VarTypes::Int32;   };
template<>           struct TypeDetector<quint32> { static const VarType varType = VarTypes::UInt32;  };
#ifndef NO_64_BIT_IN_QML
template<>           struct TypeDetector<qint64>  { static const VarType varType = VarTypes::Int64;   };
template<>           struct TypeDetector<quint64> { static const VarType varType = VarTypes::UInt64;  };
#endif

struct AbstractVarWrapper {
    const quint size;
    const VarType type;

    explicit AbstractVarWrapper (const quint size, const VarType type);
    virtual ~AbstractVarWrapper (void);

    virtual QmlBiggestInt get (void) const = 0;

    virtual bool set (const QmlBiggestInt value) = 0;

    virtual void add (const QmlBiggestInt value) = 0;
    virtual void mul (const QmlBiggestInt value) = 0;
    virtual void div (const QmlBiggestInt value) = 0;

    virtual bool isEqualTo       (const QmlBiggestInt value) = 0;
    virtual bool isLesserThan    (const QmlBiggestInt value) = 0;
    virtual bool isGreaterThan   (const QmlBiggestInt value) = 0;
    virtual bool isDifferentFrom (const QmlBiggestInt value) = 0;

    virtual QmlBiggestInt absoluteDelta (const QmlBiggestInt value) = 0;

    virtual QByteArray hex (void) const = 0;
};

template<typename BasicIntType> struct VarWrapper : public AbstractVarWrapper {
    BasicIntType data;

    explicit VarWrapper (void) : AbstractVarWrapper (sizeof (BasicIntType), TypeDetector<BasicIntType>::varType), data (0) { }

    ~VarWrapper (void) { }

    QmlBiggestInt get (void) const Q_DECL_FINAL { return QmlBiggestInt (data); }

    bool set (const QmlBiggestInt value) Q_DECL_FINAL {
        const BasicIntType tmp = BasicIntType (value);
        if (data != tmp) {
            data = tmp;
            return true;
        }
        return false;
    }

    void add (const QmlBiggestInt value) Q_DECL_FINAL { data += BasicIntType (value); }
    void mul (const QmlBiggestInt value) Q_DECL_FINAL { data *= BasicIntType (value); }
    void div (const QmlBiggestInt value) Q_DECL_FINAL { data /= BasicIntType (value); }

    bool isEqualTo       (const QmlBiggestInt value) Q_DECL_FINAL { return (data == BasicIntType (value)); }
    bool isDifferentFrom (const QmlBiggestInt value) Q_DECL_FINAL { return (data != BasicIntType (value)); }
    bool isLesserThan    (const QmlBiggestInt value) Q_DECL_FINAL { return (data  < BasicIntType (value)); }
    bool isGreaterThan   (const QmlBiggestInt value) Q_DECL_FINAL { return (data  > BasicIntType (value)); }

    QmlBiggestInt absoluteDelta (const QmlBiggestInt value) Q_DECL_FINAL { return qAbs (data - value); }

    QByteArray hex (void) const Q_DECL_FINAL {
        return QByteArray::number (data, 16).rightJustified (size * 2, '0', false).right (size * 2);
    }
};

class MemoryModelItem : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, path)
    QML_CONSTANT_CSTREF_PROPERTY (QString, type)
    Q_PROPERTY (QmlBiggestInt value READ getValue NOTIFY valueChanged)
    Q_PROPERTY (QByteArray    hex   READ getHex   NOTIFY hexChanged)

public:
    explicit MemoryModelItem (const QString & path = "",
                              const QString & type = "",
                              AbstractVarWrapper * wrapper = Q_NULLPTR,
                              QObject * parent = Q_NULLPTR);

    QmlBiggestInt getValue (void) const;
    QByteArray    getHex   (void) const;

signals:
    void valueChanged (void);
    void hexChanged   (void);

private:
    AbstractVarWrapper * m_varWrapper;
};

class Memory : public QObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (bool, dumpTreeAfterInit)
    QML_OBJMODEL_PROPERTY (MemoryModelItem, varsModel)

public:
    explicit Memory (QObject * parent = Q_NULLPTR);

    enum InternalState {
        Uninitialized,
        Initializing,
        Initialized,
        Deinitializing,
    };

    ~Memory (void);

public slots: // methods
    /// runtime control
    void initialize (void);
    void reset      (void);

    /// debug
    void dump (void) const;
    void tree (void) const;

    /// declarations
    bool declConst (const QByteArray & name, const QmlBiggestInt value);
    bool declType  (const QByteArray & type, const QVariantMap & structure);
    bool declVar   (const QByteArray & name, const QByteArray  & type);

    /// reader
    QmlBiggestInt getVar (const QByteArray & path);

    /// mutators
    void setVar (const QByteArray & path, const QmlBiggestInt value);
    void addVar (const QByteArray & path, const QmlBiggestInt value);
    void mulVar (const QByteArray & path, const QmlBiggestInt value);
    void divVar (const QByteArray & path, const QmlBiggestInt value);

    /// comparators
    bool isEqualTo       (const QByteArray & path, const QmlBiggestInt value);
    bool isLesserThan    (const QByteArray & path, const QmlBiggestInt value);
    bool isGreaterThan   (const QByteArray & path, const QmlBiggestInt value);
    bool isDifferentFrom (const QByteArray & path, const QmlBiggestInt value);

    QmlBiggestInt absDelta (const QByteArray & path, const QmlBiggestInt value);

signals:
    void init (Memory * memory);

protected slots: // internals
    void handleError (const QByteArray & message);

private: // members
    bool m_bug;
    InternalState m_init;
    QMap<QByteArray, int> m_constants;
    QVector<QByteArray> m_basicTypeNames;
    QMap<QByteArray, ByteArraysMap *> m_structDefs;
    QVector<AbstractVarWrapper *> m_varWrappers;
    QMap<QByteArray, AbstractVarWrapper *> m_varsByName;
    ByteArraysMap m_varsTypes;
};

#endif // MEMORY_H
