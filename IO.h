#ifndef ABSTRACTIO_H
#define ABSTRACTIO_H

#include <QObject>

#include "IdentifiableObject.h"

class AnalogSensor;
class DigitalSensor;
class AnalogActuator;
class DigitalActuator;
class AbstractLink;

class AbstractIO : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (ObjectType::Type, type)
    QML_CONSTANT_VAR_PROPERTY (ObjectDirection::Type, direction)

public:
    explicit AbstractIO (const ObjectType::Type type = ObjectType::UNKNOWN_TYPE, const ObjectDirection::Type direction = ObjectDirection::UNKNOWN_DIRECTION, QObject * parent = Q_NULLPTR);
    virtual ~AbstractIO (void);

    void onComponentCompleted (void) Q_DECL_OVERRIDE;
};

class AbstractAnalogIO : public AbstractIO {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, valRaw)
    QML_WRITABLE_VAR_PROPERTY (int, minRaw)
    QML_WRITABLE_VAR_PROPERTY (int, maxRaw)
    QML_WRITABLE_VAR_PROPERTY (int, resolutionInPoints)

public:
    explicit AbstractAnalogIO (const ObjectDirection::Type direction = ObjectDirection::UNKNOWN_DIRECTION, QObject * parent = Q_NULLPTR);

    QJsonObject exportState (void) const Q_DECL_FINAL;

    void onComponentCompleted (void) Q_DECL_OVERRIDE;

    Q_INVOKABLE int  getValueInPoints   (void);
    Q_INVOKABLE int  getValueInPercents (void);

    Q_INVOKABLE void setValueInPoints   (const int points);
    Q_INVOKABLE void setValueInPercents (const int percents);
};

class AbstractDigitalIO : public AbstractIO {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (bool, value)

public:
    explicit AbstractDigitalIO (const ObjectDirection::Type direction = ObjectDirection::UNKNOWN_DIRECTION, QObject * parent = Q_NULLPTR);

    QJsonObject exportState (void) const Q_DECL_FINAL;

    void onComponentCompleted (void) Q_DECL_OVERRIDE;
};

class AnalogInput : public AbstractAnalogIO {
    Q_OBJECT

public:
    explicit AnalogInput (QObject * parent = Q_NULLPTR);
    virtual ~AnalogInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

class AnalogOutput : public AbstractAnalogIO {
    Q_OBJECT

public:
    explicit AnalogOutput (QObject * parent = Q_NULLPTR);
    virtual ~AnalogOutput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

class DigitalInput : public AbstractDigitalIO {
    Q_OBJECT

public:
    explicit DigitalInput (QObject * parent = Q_NULLPTR);
    virtual ~DigitalInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

class DigitalOutput : public AbstractDigitalIO {
    Q_OBJECT

public:
    explicit DigitalOutput (QObject * parent = Q_NULLPTR);
    virtual ~DigitalOutput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

#endif // ABSTRACTIO_H
