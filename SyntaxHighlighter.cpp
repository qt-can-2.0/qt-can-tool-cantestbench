
#include "SyntaxHighlighter.h"

#include <QTextDocument>
#include <QTextBlock>
#include <QTextCursor>

#include "Help.h"
#include "QtCAN.h"

QmlSyntaxTokenId::QmlSyntaxTokenId (const int position, const QString & identifier, const QString & typeName, QObject * parent)
    : QObject (parent)
    , m_position (position)
    , m_identifier (identifier)
    , m_typeName (typeName)
{ }

QmlSyntaxHighlighter::QmlSyntaxHighlighter (QTextDocument * document)
    : QSyntaxHighlighter (document)
    , m_useDarkTheme (true)
    , keywords ({
                "if",
                "else",
                "return",
                "import",
                "signal",
                "property",
                "function",
                "readonly",
                "alias",
                "for",
                "in",
                "do",
                "while",
                "break",
                "continue",
                "switch",
                "case",
                "default",
                "var",
                "null",
                "undefined",
                "string",
                "bool",
                "int",
                "real",
                "date",
                "true",
                "false",
                })
    , highlightingRules ({
                         Rule { "\\b[a-z][a-zA-Z0-9_]*\\b",                &memberFormat     }, // member
                         Rule { "\\b[0-9]+\\b",                            &numberFormat     }, // numbers
                         Rule { "\\b0x[0-9A-Fa-f]+\\b",                    &numberFormat     }, // hexa
                         Rule { ("\\b(" % keywords.join ('|') % ")\\b"),   &keywordFormat    }, // keywords
                         Rule { "\\b[A-Z][a-zA-Z0-9_]*\\b",                &objectFormat     }, // objects
                         Rule { "\\bid\\s*:\\s*([a-z][a-zA-Z0-9_]*)\\s*;", &identifierFormat }, // id
                         Rule { "\".*\"",                                  &quotationFormat  }, // double quoted string
                         Rule { "'.*'",                                    &quotationFormat  }, // simple quoted string
                         Rule { "\\/\\/.*",                                &commentFormat    }, // double slash comment
                         })
    , highlightingRulesMultiline ({
                                  RuleMultiline { "/\\*", "\\*/", &commentFormat, Commented }, // block comment
                                  })
{
    identifierFormat.setFontItalic (true);
    connect (this, &QmlSyntaxHighlighter::useDarkThemeChanged, this, &QmlSyntaxHighlighter::refreshFormats);
    emit useDarkThemeChanged (m_useDarkTheme);
}

void QmlSyntaxHighlighter::highlightBlock (const QString & text) {
    for (const Rule & rule : highlightingRules) {
        QRegExp expression (rule.pattern);
        int index = expression.indexIn (text);
        int length = 0;
        while (index >= 0) {
            if (expression.captureCount () > 0) {
                const int idx = expression.captureCount ();
                index  = expression.pos (idx);
                length = expression.cap (idx).size ();
            }
            else {
                length = expression.matchedLength ();
            }
            setFormat (index, length, (* rule.format));
            index = expression.indexIn (text, index + length);
        }
    }
    setCurrentBlockState (Normal);
    for (const RuleMultiline & rule : highlightingRulesMultiline) {
        QRegExp startExpression (rule.patternStart);
        QRegExp endExpression   (rule.patternEnd);
        int startIndex = 0;
        if (previousBlockState () != rule.state) {
            startIndex = text.indexOf (startExpression);
        }
        while (startIndex >= 0) {
            int endIndex = text.indexOf (endExpression, startIndex);
            int chunkLength = 0;
            if (endIndex < 0) {
                setCurrentBlockState (rule.state);
                chunkLength = (text.length () - startIndex);
            }
            else {
                chunkLength = (endIndex - startIndex + endExpression.matchedLength ());
            }
            setFormat (startIndex, chunkLength, commentFormat);
            startIndex = text.indexOf (startExpression, (startIndex + chunkLength));
        }
    }
}

void QmlSyntaxHighlighter::refreshFormats (void) {
    identifierFormat.setForeground (QColor (m_useDarkTheme ? "#ff5500" : "#D14500"));
    memberFormat.setForeground     (QColor (m_useDarkTheme ? "#ff5500" : "#D14500"));
    numberFormat.setForeground     (QColor (m_useDarkTheme ? "#00aa7f" : "#2377FF"));
    keywordFormat.setForeground    (QColor (m_useDarkTheme ? "#ffff7f" : "#927F00"));
    objectFormat.setForeground     (QColor (m_useDarkTheme ? "#aa00ff" : "#A1009A"));
    quotationFormat.setForeground  (QColor (m_useDarkTheme ? "#00aa00" : "#346A00"));
    commentFormat.setForeground    (QColor (m_useDarkTheme ? "#808080" : "#808080"));
    rehighlight ();
}

SyntaxHighlighter::SyntaxHighlighter (QObject * parent)
    : QObject (parent)
    , m_useDarkTheme (true)
    , m_info     (Q_NULLPTR)
    , m_document (Q_NULLPTR)
    , m_dirty (false)
{
    m_info = new DocInfo (this);
    m_idsList = new QQmlObjectListModel<QmlSyntaxTokenId> (this);
    m_inhibitReparse = new QTimer (this);
    m_inhibitReparse->setInterval (2000);
    m_inhibitReparse->setSingleShot (true);
    connect (m_inhibitReparse, &QTimer::timeout, [this] () {
        refreshAst ();
        emit infoChanged (m_info);
    });
}

QVariantList SyntaxHighlighter::getAutoCompletionAtPos (const int position) {
    Q_UNUSED (position)
    QVariantList ret;
    return ret;
}

ObjectHelp * SyntaxHighlighter::getContextualHelpAtPos (const int position) {
    ObjectHelp * ret = Q_NULLPTR;
    if (m_document) {
        refreshAst ();
        for (SyntaxToken * token : m_ast) {
            if (token->category == SyntaxToken::Type) {
                if ((position >= token->position) &&
                    (position < (token->position + token->name.count ()))) {
                    ret = Help::instance ().getHelpForTypeByName (token->name);
                }
            }
        }
    }
    return ret;
}

void SyntaxHighlighter::classBegin (void) { }

void SyntaxHighlighter::componentComplete (void) {
    if (m_document) {
        QTextDocument * doc = m_document->textDocument ();
        m_info->update_document (doc);
        QmlSyntaxHighlighter * highlighter = new QmlSyntaxHighlighter (doc);
        highlighter->update_useDarkTheme (m_useDarkTheme);
        connect (this, &SyntaxHighlighter::useDarkThemeChanged, highlighter, &QmlSyntaxHighlighter::update_useDarkTheme);
        connect (doc, &QTextDocument::contentsChanged, [this] () {
            m_dirty = true;
            if (m_inhibitReparse->isActive ()) {
                m_inhibitReparse->stop ();
            }
            m_inhibitReparse->start ();
        });
    }
}

void SyntaxHighlighter::refreshAst (void) {
    static QRegExp regexpObject ("\\b([A-Z][a-zA-Z0-9_]*)(?=\\s*\\{)");
    static QRegExp regexpId     ("\\bid\\s*:\\s*([a-z][a-zA-Z0-9_]*)\\b");
    static const QString OPEN  = QStringLiteral ("{");
    static const QString CLOSE = QStringLiteral ("}");
    if (m_document && m_dirty) {
        m_dirty = false;
        const QString text = m_document->textDocument ()->toPlainText ();
        qDeleteAll (m_ast);
        m_ast.clear ();
        int index = 0;
        forever {
            const int nextObjectPos = regexpObject.indexIn (text, index);
            const int nextIdPos     = regexpId.indexIn     (text, index);
            const int nextOpenPos   = text.indexOf (OPEN,  index);
            const int nextClosePos  = text.indexOf (CLOSE, index);
            if ((nextIdPos >= 0) &&
                (nextIdPos < nextObjectPos || nextObjectPos < 0) &&
                (nextIdPos < nextOpenPos   || nextOpenPos   < 0) &&
                (nextIdPos < nextClosePos  || nextClosePos  < 0)) { // ID
                index = (nextIdPos + regexpId.matchedLength ());
                m_ast.append (new SyntaxToken { SyntaxToken::Id, regexpId.cap (1), regexpId.pos (1) });
            }
            else if ((nextObjectPos >= 0) &&
                     (nextObjectPos < nextIdPos    || nextIdPos    < 0) &&
                     (nextObjectPos < nextOpenPos  || nextOpenPos  < 0) &&
                     (nextObjectPos < nextClosePos || nextClosePos < 0)) { // Object
                index = (nextObjectPos + regexpObject.matchedLength ());
                m_ast.append (new SyntaxToken { SyntaxToken::Type, regexpObject.cap (1), regexpObject.pos (1) });
            }
            else if ((nextOpenPos >= 0) &&
                     (nextOpenPos < nextObjectPos || nextObjectPos < 0) &&
                     (nextOpenPos < nextIdPos     || nextIdPos     < 0) &&
                     (nextOpenPos < nextClosePos  || nextClosePos  < 0)) { // Open
                index = (nextOpenPos + OPEN.size ());
                m_ast.append (new SyntaxToken { SyntaxToken::OpenBrace, "", nextOpenPos });
            }
            else if ((nextClosePos >= 0) &&
                     (nextClosePos < nextObjectPos || nextObjectPos < 0) &&
                     (nextClosePos < nextIdPos     || nextIdPos     < 0) &&
                     (nextClosePos < nextOpenPos   || nextOpenPos   < 0)) { // Close
                index = (nextClosePos + CLOSE.size ());
                m_ast.append (new SyntaxToken { SyntaxToken::CloseBrace, "", nextClosePos });
            }
            else { break; }
        }
        QList<QmlSyntaxTokenId *> tokens;
        const int count = m_ast.count ();
        for (int idx = 0; idx < count; ++idx) {
            SyntaxToken * token = m_ast.at (idx);
            if (token->category == SyntaxToken::Id) {
                if (idx >= 2) { // the token before immediate previous brace is the typename
                    tokens.append (new QmlSyntaxTokenId (token->position, token->name, m_ast.at (idx -2)->name));
                }
            }
        }
        qSort (tokens);
        m_idsList->clear ();
        m_idsList->append (tokens);
    }
}

DocInfo::DocInfo (QObject * parent)
    : QObject (parent)
    , m_document (Q_NULLPTR)
{ }

int DocInfo::getLinesOfCode (void) const {
    int ret = -1;
    if (m_document) {
        ret = m_document->blockCount ();
    }
    return ret;
}

int DocInfo::getLine (const int position) const {
    int ret = -1;
    if (m_document && position >= 0) {
        QTextCursor cursor (m_document);
        cursor.setPosition (position, QTextCursor::MoveAnchor);
        ret = (cursor.block ().blockNumber () +1);
    }
    return ret;
}

int DocInfo::getColumn (const int position) const {
    int ret = -1;
    if (m_document && position >= 0) {
        QTextCursor cursor (m_document);
        cursor.setPosition (position, QTextCursor::MoveAnchor);
        ret = (cursor.positionInBlock () +1);
    }
    return ret;
}

int DocInfo::getPosition (const int line, const int column) const {
    int ret = -1;
    if (m_document && line > 0 && column > 0) {
        QTextCursor cursor (m_document);
        cursor.setPosition  (0,                          QTextCursor::MoveAnchor);
        cursor.movePosition (QTextCursor::NextBlock,     QTextCursor::MoveAnchor, (line   -1));
        cursor.movePosition (QTextCursor::NextCharacter, QTextCursor::MoveAnchor, (column -1));
        ret = cursor.position ();
    }
    return ret;
}
