
#include "CanBus.h"

#include "Manager.h"
#include "Routine.h"
#include "CppUtils.h"
#include "NetworkDefinition.h"
#include "IdentifiableObject.h"
#include "CanDataQmlWrapper.h"
#include "CanDriverQmlWrapper.h"

CanDriverProxy::CanDriverProxy(CanDriverWrapper * wrapper, QObject * parent)
    : CanDriver (parent)
    , m_wrapper (wrapper)
{ }

CanDriverProxy::~CanDriverProxy (void) { }

bool CanDriverProxy::init (const QVariantMap & options) {
    Q_UNUSED (options)
    return true;
}

bool CanDriverProxy::send (CanMessage * message) {
    if (message && m_wrapper && m_wrapper->getDriverObject ()) {
        m_wrapper->sendCanMsg (message);
    }
    return true;
}

bool CanDriverProxy::stop (void) {
    return true;
}

CanBus::CanBus (QObject * parent)
    : BasicObject (ObjectFamily::CANBUS, parent)
{
    m_driverWrapper = new CanDriverWrapper (this);
    connect (m_driverWrapper, &CanDriverWrapper::canMsgRecv,       this, &CanBus::onRecv);
    connect (m_driverWrapper, &CanDriverWrapper::diagLogRequested, this, &CanBus::onDiag);
    m_driverProxy = new CanDriverProxy (m_driverWrapper, this);
    m_frameTx = new CanDataWrapperTx (this);
    connect (m_frameTx, &CanDataWrapperTx::produced, m_driverProxy, &CanDriverProxy::send);
    Manager::instance ().registerObject (this);
}

CanBus::~CanBus (void) {
    Manager::instance ().unregisterObject (this);
}

void CanBus::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

void CanBus::subscribeRoutineToCanFrameId (RoutineOnCanFrame * routine, int canId) {
    for (int key : arrayRange (m_routinesForCanId.keys (routine))) {
        m_routinesForCanId.remove (key, routine);
    }
    if (canId >= 0) {
        m_routinesForCanId.insertMulti (canId, routine);
    }
}

void CanBus::onRecv (CanMessage * msg) {
    static QElapsedTimer bench;
    if (msg != Q_NULLPTR) {
        bench.restart ();
        if (Manager::instance ().get_running ()) {
            if (!msg->getCanId ().isERR ()) {
                const int canId = (msg->getCanId ().isEFF ()
                                   ? int (msg->getCanId ().canIdEFF ())
                                   : int (msg->getCanId ().canIdSFF ()));
                const QList<RoutineOnCanFrame *> list = m_routinesForCanId.values (canId);
                if (!list.isEmpty ()) {
                    for (RoutineOnCanFrame * routine : arrayRange (list)) {
                        routine->getFrameRx ()->setCanData (msg->getCanData ());
                        routine->execute ();
                    }
                    if (Manager::instance ().get_currentNetwork () &&
                        Manager::instance ().get_currentNetwork ()->get_benchmarkCanFramesTiming ()) {
                        INFO << "RECV" << qPrintable (QLatin1String (QByteArray::number (qreal (bench.nsecsElapsed ()) / 1000000.0, 'f', 6))) << "ms"
                             << "(" << "frame" << canId << "with timestamp" << qPrintable (msg->getTimeStamp ().toString ("hh:mm:ss.zzz")) << ")";
                    }
                }
                emit m_driverProxy->recv (msg);
            }
        }
        delete msg;
    }
}

void CanBus::onDiag (const QString & type, const QString & description) {
    static const QString info    = QStringLiteral ("INFO");
    static const QString warning = QStringLiteral ("WARNING");
    static const QString error   = QStringLiteral ("ERROR");
    const QString tmp = ("CANBUS '" % get_uid () % "'");
    if (type == info) {
        Manager::instance ().logInfo (tmp, Q_NULLPTR, description);
    }
    else if (type == warning) {
        Manager::instance ().logWarning (tmp, Q_NULLPTR, description);
    }
    else if (type == error) {
        Manager::instance ().logError (tmp, Q_NULLPTR, description);
    }
    else { }
}
