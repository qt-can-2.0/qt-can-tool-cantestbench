import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

ModalDialog {
    title: qsTr ("Select a QML network definition file");
    buttons: (buttonAccept | buttonCancel);
    minWidth: 650;
    maxWidth: 650;
    onButtonClicked: {
        switch (buttonType) {
        case buttonAccept:
            tryOpen (fileSelector.currentPath);
            break;
        case buttonCancel:
            hide ();
            break;
        }
    }

    function tryOpen (path) {
        if (FileSystem.exists (path)) {
            Shared.addMru (path);
            Shared.lastFolderUsed = FileSystem.parentDir (path);
            Shared.manager.load (FileSystem.urlFromPath (path));
            hide ();
        }
        else {
            shake ();
        }
    }

    TabBar {
        currentTab: tabOpenLastUsed;
        implicitWidth: 650;
        implicitHeight: 350;

        Group {
            id: tabOpenLastUsed;
            title: qsTr ("Last used");
            icon: SvgIconLoader {
                icon: "others/clock";
                color: Style.colorForeground;
                size: Style.fontSizeNormal;
            }

            Rectangle {
                color: Style.colorWindow;
                anchors.fill: parent;
            }
            Line { ExtraAnchors.leftDock: parent; }
            Line { ExtraAnchors.rightDock: parent; }
            Line { ExtraAnchors.bottomDock: parent; }
            ScrollContainer {
                placeholder: (repeaterMru.count === 0 ? qsTr ("No recent file") : "");
                anchors.fill: parent;
                anchors.margins: (Style.spacingNormal * 2);

                Flickable {
                    id: flickerMru;
                    flickableDirection: Flickable.VerticalFlick;

                    StretchColumnContainer {
                        id: layoutMru;
                        ExtraAnchors.horizontalFill: parent;

                        Repeater {
                            id: repeaterMru;
                            model: Shared.mruModel;
                            delegate: MouseArea {
                                id: delegateMru;
                                hoverEnabled: true;
                                implicitHeight: (linkLastUsed.height + linkLastUsed.anchors.margins * 2);

                                readonly property string mruPath : modelData;

                                Rectangle {
                                    color: Style.colorHighlight;
                                    opacity: 0.35;
                                    visible: (linkLastUsed.clickable && parent.containsMouse);
                                    anchors.fill: parent;
                                }
                                Line {
                                    opacity: 0.65;
                                    ExtraAnchors.bottomDock: parent;
                                }
                                ClickableTextLabel {
                                    id: linkLastUsed;
                                    text: "%1. %2".arg (model.index +1). arg (delegateMru.mruPath.replace (FileSystem.homePath, "~"));
                                    elide: Text.ElideMiddle;
                                    enabled: clickable;
                                    clickable: FileSystem.exists (delegateMru.mruPath);
                                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                    maximumLineCount: 3;
                                    anchors {
                                        left: parent.left;
                                        right: btnRemoveLastUsed.left;
                                        margins: Style.spacingNormal;
                                        verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                    onClicked: { tryOpen (delegateMru.mruPath); }
                                }
                                TextButton {
                                    id: btnRemoveLastUsed;
                                    flat: true;
                                    icon: SymbolLoader { symbol: Style.symbolCross; }
                                    padding: Style.spacingSmall;
                                    visible: parent.containsMouse;
                                    anchors {
                                        right: parent.right;
                                        margins: Style.spacingNormal;
                                        verticalCenter: (parent ? parent.verticalCenter : undefined);
                                    }
                                    onClicked: { Shared.removeMru (delegateMru.mruPath); }
                                }
                            }
                        }
                    }
                }
            }
        }
        Group {
            id: tabOpenBrowse;
            title: qsTr ("Browse");
            icon: SvgIconLoader {
                icon: "filetypes/folder-closed";
                color: Style.colorForeground;
                size: Style.fontSizeNormal;
            }

            Rectangle {
                color: Style.colorWindow;
                anchors.fill: parent;
            }
            Line { ExtraAnchors.leftDock: parent; }
            Line { ExtraAnchors.rightDock: parent; }
            Line { ExtraAnchors.bottomDock: parent; }
            FileSelector {
                id: fileSelector;
                folder: Shared.lastFolderUsed;
                nameFilters: ["*.qml"];
                anchors.fill: parent;
                anchors.margins: (Style.spacingNormal * 2);
                onFileDoubleClicked: { tryOpen (currentPath);}
            }
        }
    }
    Balloon {
        title: qsTr ("Be careful");
        content: qsTr ("Ensure that the loaded QML file is inside a directory, with the other project file with it. Never open a QML file that is in a folder shared with a lot of other unrelated files, else you will encounter freezes/crashes when the TestBench will try to find all the project files !");
    }
}
