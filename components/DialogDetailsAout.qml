import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDialogDetails {
    id: base;
    title: qsTr ("Details of analog output :");

    readonly property AnalogOutput aout : object;

    FormContainer {
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblVal;
                text: (aout ? aout.valRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblVal.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Minimum :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblMin;
                text: (aout ? aout.minRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblMin.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Maximum :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblMax;
                text: (aout ? aout.maxRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblMax.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Resolution :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblResolution;
                text: (aout ? aout.resolutionInPoints : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "points";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblResolution.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Linked as source to :");
            visible: repeaterLinksAsSource.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsSource.count;

            Repeater {
                id: repeaterLinksAsSource;
                model: Shared.manager.getLinksAsSource (aout);
                delegate: DelegateLinkTarget {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
    }
}
