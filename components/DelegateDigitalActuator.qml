import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (actuator !== null);

    property DigitalActuator actuator : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Applied value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        CheckableBox {
            size: Style.fontSizeBig;
            enabled: (actuator && actuator.output
                      ? actuator.output.breakLink
                      : false);
            onEdited: { actuator.value = value; }

            Binding on value { value: (actuator ? actuator.value : false); }
        }
        TextLabel {
            text: (actuator
                   ? (actuator.value
                      ? actuator.trueLabel
                      : actuator.falseLabel)
                   : "");
            font.family: Style.fontFixedName;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    Repeater {
        model: (actuator ? actuator.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
}
