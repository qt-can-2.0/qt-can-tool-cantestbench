import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchRowContainer {
    id: base;
    spacing: Style.spacingNormal;

    property ArgumentHelp argumentHelp : null;

    Stretcher {
        height: implicitHeight;
        implicitWidth: Style.spacingBig;
        implicitHeight: Style.spacingSmall;
    }
    TextLabel {
        text: base.argumentHelp.name;
        emphasis: true;
        font.family: Style.fontFixedName;
    }
    TextLabel { text: ":"; }
    Stretcher {
        height: implicitHeight;
        implicitHeight: lblDescription.height;

        TextLabel {
            id: lblDescription;
            text: base.argumentHelp.description + (base.argumentHelp.fallback !== "" ? qsTr ("(default %1)").arg (base.argumentHelp.fallback) : "");
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
            ExtraAnchors.horizontalFill: parent;
        }
    }
}
