import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

TextButton {
    text: (canBus ? (containsMouse && (canBus.title !== "") ? canBus.title : canBus.uid) : "");
    autoColorIcon: false;
    implicitWidth: (Math.max (metricsCanUid.width, metricsCanTitle.width) + Style.fontSizeNormal + (padding * 3));
    icon: Circle {
        size: Style.fontSizeNormal;
        color: (canBus
                ? (canBus.driverWrapper.driverLoaded
                   ? Style.colorButtonGreen
                   : Style.colorButtonRed)
                : Style.colorBorder);
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
    }

    property CanBus canBus : null;

    TextLabel { id: metricsCanUid;   text: canBus.uid;   color: Style.colorNone; }
    TextLabel { id: metricsCanTitle; text: canBus.title; color: Style.colorNone; }
}
