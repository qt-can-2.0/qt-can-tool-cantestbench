import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (sensor !== null);

    property DigitalSensor sensor : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        CheckableBox {
            size: Style.fontSizeBig;
            enabled: (sensor
                      && ((sensor.sourceLink
                           && (sensor.sourceLink.detached
                               || sensor.sourceLink.reversed))
                          || !sensor.sourceLink));
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { sensor.value = value; }

            Binding on value { value: (sensor ? sensor.value : false); }
        }
        TextLabel {
            text: (sensor
                   ? (sensor.value
                      ? sensor.trueLabel
                      : sensor.falseLabel)
                   : "");
            font.family: Style.fontFixedName;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    Repeater {
        model: (sensor ? sensor.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
    Repeater {
        model: (sensor ? sensor.targetLinks : 0);
        delegate: DelegateLinkTarget {
            link: modelData;
        }
    }
}
