import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

Window {
    id: windowEditor;
    flags: Qt.Window;
    title: "Virtual Testbench - Qt CAN 2.0 (Code Editor)";
    color: Style.colorWindow;
    width: 1024;
    height: 800;
    modality: Qt.NonModal;
    minimumWidth: 800;
    minimumHeight: 600;

    property string currentFilePath : "";

    readonly property var projectRoot : (network
                                         ? FileSystem.parentDir (FileSystem.pathFromUrl (Shared.manager.currentFileUrl))
                                         : "");

    readonly property var projectFiles : {
        if (projectRoot !== "") {
            return FileSystem.list (projectRoot, ["*.qml"], false, true, false, true);
        }
        else {
            return [];
        }
    }

    function showLineColumnInFile (filePath, lineNumber, columnNumber) {
        showMaximized ();
        raise ();
        requestActivate ();
        currentFilePath = filePath;
        if (lineNumber && columnNumber) {
            showLineColumnInFileRequested (filePath, lineNumber, columnNumber);
        }
    }

    signal showLineColumnInFileRequested (string filePath, int lineNumber, int columnNumber);

    WindowIconHelper {
        iconPath: ":/vcan_testbench_logo.svg";
    }
    PanelContainer {
        id: panelEditor;
        size: (minSize + maxSize) / 2;
        icon: SvgIconLoader {
            icon: "actions/view-list";
            color: Style.colorForeground;
        }
        title: qsTr ("Project files");
        minSize: 200;
        maxSize: 400;
        resizable: true;
        detachable: false;
        collapsable: true;
        ExtraAnchors.leftDock: parent;

        ScrollContainer {
            anchors.fill: parent;

            ListView {
                model: windowEditor.projectFiles;
                delegate: Stretcher {
                    implicitHeight: (linkEditor.height + linkEditor.anchors.margins * 2);
                    ExtraAnchors.horizontalFill: parent;

                    ClickableTextLabel {
                        id: linkEditor;
                        text: fsModelEntry.path.replace (windowEditor.projectRoot, "");
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                        emphasis: (windowEditor.currentFilePath === fsModelEntry.path);
                        anchors.margins: Style.spacingSmall;
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        ExtraAnchors.horizontalFill: parent;
                        onClicked: { windowEditor.currentFilePath = fsModelEntry.path; }

                        readonly property FileSystemModelEntry fsModelEntry : modelData;
                    }
                }
            }
        }
    }
    Repeater {
        model: windowEditor.projectFiles;
        delegate: Item {
            id: delegateEditor;
            z: (isCurrent ? 1000 : model.index);
            enabled: isCurrent;
            opacity: (isCurrent ? 1 : 0);
            anchors.left: panelEditor.right;
            ExtraAnchors.rightDock: parent;
            onIsCurrentChanged: {
                if (isCurrent) {
                    editorCode.forceActiveFocus ();
                }
            }
            Component.onCompleted: { load (); }

            readonly property bool                 isCurrent    : (filePath === windowEditor.currentFilePath);
            readonly property string               filePath     : fsModelEntry.path;
            readonly property FileSystemModelEntry fsModelEntry : modelData;

            property string reference : "";

            property var history : [];

            function load () {
                reference = FileSystem.readTextFile (filePath);
                editorCode.text = reference;
            }

            function save () {
                if (reference !== editorCode.text) {
                    reference = editorCode.text;
                    FileSystem.writeTextFile (filePath, reference);
                }
            }

            function jump (pos) {
                editorCode.forceActiveFocus ();
                editorCode.cursorPosition = pos;
                scrollerEditor.ensureVisible (cursorMetrics);
            }

            function insert (code) {
                editorCode.forceActiveFocus ();
                scrollerEditor.ensureVisible (cursorMetrics);
                editorCode.insert (editorCode.cursorPosition, code);
            }

            function home () {
                repeaterHelpNope.model = 0;
                repeaterHelpTypeAPI.model = 0;
                repeaterHelpHome.model = 1;
                scrollerHelp.flickableItem.contentY = 0;
                panelHelp.expand ();
            }

            function help (objectHelp) {
                if (repeaterHelpTypeAPI.model && (!history.length || repeaterHelpTypeAPI.model !== history [history.length -1])) {
                    history.push (repeaterHelpTypeAPI.model);
                    history = history;
                }
                repeaterHelpNope.model = 0;
                repeaterHelpHome.model = 0;
                repeaterHelpTypeAPI.model = objectHelp;
                scrollerHelp.flickableItem.contentY = 0;
                panelHelp.expand ();
            }

            function prev () {
                var tmp = history.pop ();
                if (tmp) {
                    history = history;
                    repeaterHelpNope.model = 0;
                    repeaterHelpHome.model = 0;
                    repeaterHelpTypeAPI.model = tmp;
                    scrollerHelp.flickableItem.contentY = 0;
                    panelHelp.expand ();
                }
            }

            function nope () {
                if (repeaterHelpTypeAPI.model && (!history.length || repeaterHelpTypeAPI.model !== history [history.length -1])) {
                    history.push (repeaterHelpTypeAPI.model);
                    history = history;
                }
                repeaterHelpHome.model = 0;
                repeaterHelpTypeAPI.model = 0;
                repeaterHelpNope.model = 1;
                scrollerHelp.flickableItem.contentY = 0;
                panelHelp.expand ();
            }

            Connections {
                target: windowEditor;
                onShowLineColumnInFileRequested: {
                    if (filePath === delegateEditor.filePath) {
                        var tmp = syntaxHighlighter.info.getPosition (lineNumber, columnNumber);
                        editorCode.select (tmp, tmp);
                        scrollerEditor.ensureVisible (cursorMetrics);
                        editorCode.forceActiveFocus ();
                    }
                }
            }
            ToolBar {
                id: toolBarEditor;

                Stretcher {
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                    TextLabel {
                        text: delegateEditor.filePath.replace (windowEditor.projectRoot, "");
                        elide: Text.ElideLeft;
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        ExtraAnchors.horizontalFill: parent;
                    }
                }
                GridContainer {
                    cols: 2;
                    colSpacing: Style.spacingSmall;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

                    TextButton {
                        text: qsTr ("Revert");
                        icon: SvgIconLoader { icon: "actions/refresh"; }
                        enabled: (delegateEditor.reference !== editorCode.text);
                        backColor: Style.colorButtonRed;
                        onClicked: { editorCode.text = delegateEditor.reference; }
                    }
                    TextButton {
                        text: qsTr ("Save");
                        icon: SvgIconLoader { icon: "actions/save"; }
                        enabled: (delegateEditor.reference !== editorCode.text);
                        backColor: Style.colorButtonGreen;
                        onClicked: { delegateEditor.save (); }
                    }
                }
            }
            StatusBar {
                id: statusBarEditor;

                TextLabel {
                    text: qsTr ("%1 lines of code").arg (syntaxHighlighter.info.getLinesOfCode ());
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
                Stretcher { }
                TextLabel {
                    text: qsTr ("line %1 : column %2").arg (syntaxHighlighter.info.getLine (editorCode.cursorPosition)).arg (syntaxHighlighter.info.getColumn (editorCode.cursorPosition));
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
            }
            PanelContainer {
                id: panelOutline;
                icon: SvgIconLoader {
                    icon: "actions/find";
                    color: Style.colorForeground;
                }
                title: qsTr ("Symbols Inspector");
                size: 250;
                minSize: 100;
                maxSize: 350;
                resizable: true;
                detachable: true;
                collapsable: true;
                borderSide: Item.Left;
                anchors.top: toolBarEditor.bottom;
                anchors.right: parent.right;
                anchors.bottom: (panelHelp.detached ? statusBarEditor.top : panelHelp.top);

                ScrollContainer {
                    id: scrollerOutline;
                    showBorder: false;
                    anchors.fill: parent;

                    Flickable {
                        contentHeight: (layoutOutline.height + layoutOutline.anchors.margins * 2);
                        flickableDirection: Flickable.VerticalFlick;

                        StretchColumnContainer {
                            id: layoutOutline;
                            spacing: Style.spacingSmall;
                            anchors.margins: Style.spacingNormal;
                            ExtraAnchors.topDock: parent;

                            TextLabel {
                                text: qsTr ("Objects in this file :");
                                font.pixelSize: Style.fontSizeTitle;
                            }
                            TextBox {
                                id: inputFilterId;
                                hasClear: true;
                                textHolder: qsTr ("Filter IDs...");
                            }
                            TextLabel {
                                text: qsTr ("(click on ID to show in code editor)");
                                visible: repeaterHelpIds.count;
                                wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                font.pixelSize: Style.fontSizeSmall;
                            }
                            Line { visible: repeaterHelpIds.count; }
                            TextLabel {
                                text: qsTr ("No ID found in this file");
                                color: Style.colorBorder;
                                visible: !repeaterHelpIds.count;
                                horizontalAlignment: Text.AlignHCenter;
                                font.pixelSize: Style.fontSizeBig;
                            }
                            Repeater {
                                id: repeaterHelpIds;
                                model: syntaxHighlighter.idsList;
                                delegate: DelegateTokenId {
                                    tokenId: model.qtObject;
                                    visible: (inputFilterId.isEmpty || tokenId.identifier.toLowerCase ().indexOf (inputFilterId.text.toLowerCase ()) >= 0);
                                    onShowLocationRequested: { delegateEditor.jump (pos); }
                                }
                            }
                        }
                    }
                }
            }
            PanelContainer {
                id: panelHelp;
                icon: SvgIconLoader {
                    icon: "actions/help";
                    color: Style.colorForeground;
                }
                title: qsTr ("API Help");
                size: 250;
                minSize: 150;
                maxSize: 450;
                resizable: true;
                detachable: true;
                collapsable: true;
                borderSide: Item.Top;
                anchors.bottom: statusBarEditor.top;
                ExtraAnchors.horizontalFill: parent;
                Component.onCompleted: { collapse (); }

                ToolBar {
                    id: toolbarHelp;

                    TextButton {
                        flat: true;
                        padding: Style.lineSize;
                        text: qsTr ("Show help home");
                        icon: SvgIconLoader { icon: "actions/home"; }
                        onClicked: { delegateEditor.home (); }
                    }
                    Stretcher { }
                    TextButton {
                        flat: true;
                        enabled: delegateEditor.history.length;
                        padding: Style.lineSize;
                        text: qsTr ("Previous help page");
                        icon: SvgIconLoader { icon: "actions/arrow-left"; }
                        onClicked: { delegateEditor.prev (); }
                    }
                }
                ScrollContainer {
                    id: scrollerHelp;
                    showBorder: false;
                    anchors.top: toolbarHelp.bottom;
                    ExtraAnchors.bottomDock: parent;

                    Flickable {
                        contentHeight: (layoutHelp.height + layoutHelp.anchors.margins * 2);
                        flickableDirection: Flickable.VerticalFlick;

                        StretchColumnContainer {
                            id: layoutHelp;
                            spacing: Style.spacingSmall;
                            anchors.margins: Style.spacingBig;
                            ExtraAnchors.topDock: parent;

                            Repeater {
                                id: repeaterHelpNope;
                                delegate: TextLabel {
                                    text: qsTr ("No documentation found for symbol under cursor !");
                                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                    font.pixelSize: Style.fontSizeTitle;
                                }
                            }
                            Repeater {
                                id: repeaterHelpHome;
                                model: 1;
                                delegate: StretchColumnContainer {
                                    spacing: Style.spacingBig;

                                    TextLabel {
                                        text: qsTr ("API reference");
                                        font.pixelSize: Style.fontSizeTitle;
                                    }
                                    Line { }
                                    TextLabel {
                                        text: qsTr ("Here is the complete list of QML objects that can be used in a network project.");
                                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
                                        font.pixelSize: Style.fontSizeSmall;
                                    }
                                    Repeater {
                                        model: Help.sectionsList;
                                        delegate: StretchColumnContainer {
                                            id: delegateHelpSection;
                                            spacing: Style.spacingNormal;

                                            readonly property HelpSection section : model.qtObject;

                                            StretchRowContainer {
                                                spacing: Style.spacingBig;

                                                TextLabel {
                                                    text: delegateHelpSection.section.name;
                                                    emphasis: true;
                                                    font.pixelSize: Style.fontSizeBig;
                                                }
                                                Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
                                            }
                                            StretchColumnContainer {
                                                spacing: Style.spacingSmall;

                                                Repeater {
                                                    model: delegateHelpSection.section.pagesList;
                                                    delegate: DelegateLinkObjectHelp {
                                                        x: Style.iconSize (page.lvl);
                                                        typeName: page.name;
                                                        onObjectHelpRequested: { delegateEditor.help (objectHelp); }

                                                        readonly property HelpPage page : model.qtObject;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Repeater {
                                id: repeaterHelpTypeAPI;
                                delegate: DelegateObjectHelp {
                                    objectHelp: modelData;
                                    onInsertCodeRequested: { delegateEditor.insert (code); }
                                    onObjectHelpRequested: { delegateEditor.help (objectHelp); }
                                }
                            }
                        }
                    }
                }
            }
            ScrollContainer {
                id: scrollerEditor;
                showBorder: false;
                background: Style.colorEditable;
                anchors {
                    top: toolBarEditor.bottom;
                    left: parent.left;
                    right: (panelOutline.detached ? parent.right : panelOutline.left);
                    bottom: (panelHelp.detached ? statusBarEditor.top : panelHelp.top);
                }

                Flickable {
                    interactive: false;
                    contentHeight: (editorCode.height + editorCode.anchors.margins * 2);
                    flickableDirection: Flickable.VerticalFlick;

                    TextLabel {
                        id: side;
                        text: {
                            var ret = [];
                            if (editorCode.width > 0 && editorCode.height > 0) {
                                var count = syntaxHighlighter.info.getLinesOfCode ();
                                for (var line = 1; line < count; ++line) {
                                    var pos = syntaxHighlighter.info.getPosition (line, 1);
                                    var rect = editorCode.positionToRectangle (pos);
                                    var block = (rect.y / rect.height);
                                    while (ret.length < block) {
                                        ret.push ("");
                                    }
                                    ret.push ("%1".arg (line));
                                }
                            }
                            return ret.join ("\n");
                        }
                        font: editorCode.font;
                        color: Style.colorSecondary;
                        width: metricsNum.width;
                        verticalAlignment: Text.AlignTop;
                        horizontalAlignment: Text.AlignRight;
                        anchors.top: editorCode.top;
                        anchors.left: parent.left;
                        anchors.bottom: editorCode.bottom;
                        anchors.leftMargin: Style.spacingSmall;

                        TextLabel {
                            id: metricsNum;
                            text: "0000";
                            font: editorCode.font;
                            color: Style.colorNone;
                        }
                    }
                    Line {
                        id: line;
                        anchors {
                            top: parent.top;
                            left: side.right;
                            bottom: editorCode.bottom;
                            leftMargin: Style.spacingSmall;
                        }
                    }
                    TextEdit {
                        id: editorCode;
                        color: Style.colorForeground;
                        height: Math.max (contentHeight, scrollerEditor.height);
                        wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere;
                        textFormat: TextEdit.PlainText;
                        selectByMouse: true;
                        selectionColor: Style.colorSelection;
                        selectByKeyboard: true;
                        selectedTextColor: Style.colorInverted;
                        activeFocusOnPress: true;
                        renderType: (Style.useNativeText
                                     ? TextEdit.NativeRendering
                                     : TextEdit.QtRendering);
                        font {
                            weight: (Style.useSlimFonts ? Font.Light : Font.Normal);
                            family: Style.fontFixedName;
                            pixelSize: Style.fontSizeNormal;
                        }
                        cursorDelegate: Rectangle {
                            width: Style.lineSize;
                            color: Style.colorForeground;

                            Rectangle {
                                x: (-parent.x - editorCode.anchors.margins);
                                width: (editorCode.width + editorCode.anchors.margins * 2);
                                color: Style.opacify (Style.colorHighlight, 0.15);
                                ExtraAnchors.verticalFill: parent;
                            }
                        }
                        anchors.top: parent.top;
                        anchors.left: line.right;
                        anchors.right: parent.right;
                        anchors.margins: Style.spacingNormal;
                        Keys.onBacktabPressed: {
                            var pos = editorCode.cursorPosition;
                            var rect = editorCode.cursorRectangle;
                            var linestart = editorCode.positionAt (0, (rect.y + rect.height / 2));
                            if (editorCode.getText (linestart, pos).trim () === "") {
                                editorCode.remove (Math.max (pos -4, linestart), pos);
                            }
                            event.accepted = true;
                        }
                        Keys.onTabPressed: {
                            delegateEditor.insert ("    ");
                            event.accepted = true;
                        }
                        Keys.onPressed: {
                            if (event.key === Qt.Key_S && event.modifiers & Qt.ControlModifier) {
                                delegateEditor.save ();
                                event.accepted = true;
                            }
                            else if (event.key === Qt.Key_Space && event.modifiers & Qt.ControlModifier) {
                                syntaxHighlighter.getAutoCompletionAtPos (editorCode.cursorPosition); // TODO : use results
                                event.accepted = true;
                            }
                            else if (event.key === Qt.Key_F1) {
                                var tmp = syntaxHighlighter.getContextualHelpAtPos (editorCode.cursorPosition);
                                if (tmp) {
                                    delegateEditor.help (tmp);
                                }
                                else {
                                    delegateEditor.nope ();
                                }
                                event.accepted = true;
                            }
                            else { }
                        }

                        Item {
                            id: cursorMetrics;
                            x: editorCode.cursorRectangle.x;
                            y: editorCode.cursorRectangle.y;
                            width: editorCode.cursorRectangle.width;
                            height: editorCode.cursorRectangle.height;
                        }
                        SyntaxHighlighter {
                            id: syntaxHighlighter;
                            document: editorCode.textDocument;
                            useDarkTheme: Style.useDarkTheme;
                        }
                    }
                }
            }
        }
    }
}
