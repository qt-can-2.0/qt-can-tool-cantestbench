import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

Repeater {
    id: base;
    model: physicalValuesTree;
    delegate: ExpandableGroup {
        id: delegateDimensionTable;
        alternate: base.alternate;
        visible: (modelData ["highlightCount"] > 0 ||
                  modelData ["editableCount"] > 0 ||
                  !Shared.showOnlyEditable);
        uidLabel {
            text: dimension.uid;
            font.italic: true;
        }
        ExtraAnchors.horizontalFill: parent;

        readonly property BasicObject dimension : modelData ["dimension"];

        Repeater {
            id: repeaterDimensionsPhyVal;
            model: modelData ["phyValList"];
            delegate: InstanceCreator {
                visible: (instance && instance ["shouldBeVisible"]);
                component: DelegatePhyValTable {
                    onNeedVisible: {
                        delegateDimensionTable.expanded = true;
                        base.needVisible (item);
                    }
                    onNeedDetails: {
                        base.needDetails (phyVal);
                    }
                    onValueEdited: { base.valueEdited (delegateDimensionTable.dimension, phyVal, value); }
                }
                properties: ({ "phyVal" : modelData, "alternate" : !delegateDimensionTable.alternate });
                ExtraAnchors.horizontalFill: parent;
            }
        }
    }

    property var dimensionsList : [];

    readonly property var physicalValuesTree : {
        var ret = [];
        dimensionsList.forEach (function (dimension) {
            var phyValList = [];
            switch (dimension.family) {
            case ObjectFamily.POSITION:
                phyValList.push (dimension ["onLeftToRight"]);
                phyValList.push (dimension ["onBackToFront"]);
                phyValList.push (dimension ["onBottomToTop"]);
                break;
            case ObjectFamily.SIZE:
                phyValList.push (dimension ["toLeft"]);
                phyValList.push (dimension ["toRight"]);
                phyValList.push (dimension ["toBack"]);
                phyValList.push (dimension ["toFront"]);
                phyValList.push (dimension ["toBottom"]);
                phyValList.push (dimension ["toTop"]);
                break;
            case ObjectFamily.ANGLE:
                phyValList.push (dimension ["yaw"]);
                phyValList.push (dimension ["pitch"]);
                phyValList.push (dimension ["roll"]);
                break;
            }
            var editableCount = 0;
            var highlightCount = 0;
            phyValList.forEach (function (phyVal) {
                if (phyVal.min !== phyVal.max) {
                    ++editableCount;
                }
                if (phyVal === Shared.highlightPhyVal) {
                    ++highlightCount;
                }
            });
            ret.push ({
                          "dimension" : dimension,
                          "phyValList" : phyValList,
                          "editableCount" : editableCount,
                          "highlightCount" : highlightCount,
                      });
        });
        return ret;
    }

    property bool alternate : false;

    readonly property int shouldBeVisible : {
        var ret = false;
        physicalValuesTree.forEach (function (entry) {
            ret |= (entry ["editableCount"]  > 0 ||
                    entry ["highlightCount"] > 0);
        });
        return ret;
    }

    signal needVisible (Item item);
    signal needDetails (PhysicalValue phyVal);
    signal valueEdited (BasicObject dimension, PhysicalValue phyVal, real value);
}
