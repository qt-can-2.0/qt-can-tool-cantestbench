import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

BorderedBackground {
    id: base;
    active: clicker.containsMouse;
    height: implicitHeight;
    visible: shouldBeVisible;
    implicitHeight: (layoutValPhyTable.height + layoutValPhyTable.anchors.margins * 2);
    ExtraAnchors.horizontalFill: parent;
    onCurrentChanged: {
        if (current) {
            editValPhy.forceActiveFocus ();
            needVisible (base);
        }
    }
    Component.onCompleted: {
        if (phyVal) {
            Shared.manager.setDelegateForPath (phyVal.path, base);
        }
    }

    property PhysicalValue phyVal : null;

    readonly property bool shouldBeVisible : (current || editable || !Shared.showOnlyEditable);

    readonly property bool current  : (phyVal ? phyVal === Shared.highlightPhyVal : false);
    readonly property bool editable : (phyVal ? phyVal.min !== phyVal.max : false);

    signal needVisible (Item item);
    signal needDetails (PhysicalValue phyVal);
    signal valueEdited (real value);

    MouseArea {
        id: clicker;
        hoverEnabled: true;
        anchors.fill: parent;
    }
    StretchRowContainer {
        id: layoutValPhyTable;
        spacing: Style.spacingNormal;
        anchors.margins: Style.spacingSmall;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            text: (phyVal ? phyVal.uid : "");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            Rectangle {
                z: -1;
                color: Style.colorHighlight;
                visible: current;
                anchors {
                    fill: parent;
                    margins: -Style.spacingSmall;
                }
            }
            MouseArea {
                anchors.fill: parent;
                onClicked: { base.needDetails (phyVal); }
            }
        }
        Stretcher { }
        TextLabel {
            text: (phyVal ? phyVal.val.toFixed (2) : "");
            visible: !editable;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        NumberBox {
            id: editValPhy;
            padding: Style.spacingSmall;
            visible: editable;
            decimals: 2;
            minValue: (phyVal ? phyVal.min : 0);
            maxValue: (phyVal ? phyVal.max : 1);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: {
                base.valueEdited (value);
                value = phyVal.val;
            }

            Binding on value { value: (phyVal ? phyVal.val : 0); }
        }
    }
}
