import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenSdoReadReply routineOnCanOpenSdoReadReply : null;

    property alias routine : base.routineOnCanOpenSdoReadReply;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenSdoReadReply ? routineOnCanOpenSdoReadReply.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenSdoReadReply ? "(" + routineOnCanOpenSdoReadReply.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on reply to a local CANopen 'READ' request");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
