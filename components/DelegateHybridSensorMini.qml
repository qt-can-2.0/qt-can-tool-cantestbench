import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Row {
    spacing: Style.spacingSmall;

    TextLabel {
        text: (sensor ? Shared.format (sensor.valPhy, sensor.decimals, sensor.unit) : "");
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }
    Circle {
        size: (Style.spacingNormal * 2.0);
        color: (sensor
                ? (sensor.valState
                   ? Style.colorSelection
                   : Style.colorEditable)
                : Style.colorNone);
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }

    property HybridSensor sensor : null;
}
