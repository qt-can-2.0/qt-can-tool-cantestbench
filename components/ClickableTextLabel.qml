import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;

TextLabel {
    id: base;
    color: (enabled ? (clickable ? (broken ? Style.colorError : Style.colorLink) : Style.colorForeground) : Style.colorBorder);
    font.underline: (clickable && clicker.containsMouse);

    property bool clickable : true;
    property bool broken    : false;

    signal clicked ();

    MouseArea {
        id: clicker;
        visible: base.clickable;
        hoverEnabled: true;
        cursorShape: Qt.PointingHandCursor;
        anchors {
            fill: parent
            margins: -Style.lineSize;
        }
        onClicked: { base.clicked (); }
    }
}
