import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

ExpandableGroup {
    id: base;
    uidLabel {
        text: (node ? node.uid : 0);
        emphasis: true;
        font.pixelSize: Style.fontSizeBig;
    }
    titleLabel {
        text: (node ? node.title : 0);
        font.pixelSize: Style.fontSizeBig;
    }
    ExtraAnchors.horizontalFill: parent;

    property Node node : null;

    signal needVisible (Item item);

    Repeater {
        model: (node ? node.boards : 0);
        delegate: InstanceCreator {
            component: Components.delegateRawBoardTable;
            properties: ({ "board" : modelData, "alternate" : !base.alternate });
            ExtraAnchors.horizontalFill: parent;
        }
    }
}
