import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

TextButton {
    text: (serialBus ? (containsMouse && (serialBus.title !== "") ? serialBus.title : serialBus.uid) : "");
    autoColorIcon: false;
    implicitWidth: (Math.max (metricsSerialUid.width, metricsSerialTitle.width) + Style.fontSizeNormal + (padding * 3));
    icon: Circle {
        size: Style.fontSizeNormal;
        color: (serialBus
                ? (serialBus.portLoaded
                   ? Style.colorButtonGreen
                   : Style.colorButtonRed)
                : Style.colorBorder);
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
    }

    property SerialBus serialBus : null;

    TextLabel { id: metricsSerialUid;   text: serialBus.uid;   color: Style.colorNone; }
    TextLabel { id: metricsSerialTitle; text: serialBus.title; color: Style.colorNone; }
}
