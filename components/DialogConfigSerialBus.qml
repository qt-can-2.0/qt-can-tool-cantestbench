import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

ModalDialog {
    title: qsTr ("Select and configure serial port");
    buttons: (!serialBus.portLoaded ? (buttonAccept | buttonCancel) : buttonCancel);
    onButtonClicked: {
        switch (buttonType) {
        case buttonAccept:
            if (serialBus.init (comboSerialPortName.currentKey,
                                comboSerialBaudrate.currentKey,
                                comboSerialDatabits.currentKey,
                                comboSerialStopbits.currentKey,
                                comboSerialParity.currentKey)) {
                hide ();
            }
            else {
                shake ();
            }
            break;
        case buttonCancel:
            hide ();
            break;
        default:
            break;
        }
    }

    property SerialBus serialBus : null;

    FormContainer {
        visible: !serialBus.portLoaded;
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Port :");
        }
        ComboList {
            id: comboSerialPortName;
            model: serialBus.portsList;
            delegate: ComboListDelegateForModelDataAttributes { }
        }
        TextLabel {
            text: qsTr ("Baudrate :");
        }
        ComboList {
            id: comboSerialBaudrate;
            model: [
                { "key" : 1200,   "value" : qsTr ("1200 bauds") },
                { "key" : 2400,   "value" : qsTr ("2400 bauds") },
                { "key" : 4800,   "value" : qsTr ("4800 bauds") },
                { "key" : 9600,   "value" : qsTr ("9600 bauds") },
                { "key" : 19200,  "value" : qsTr ("19200 bauds") },
                { "key" : 38400,  "value" : qsTr ("38400 bauds") },
                { "key" : 57600,  "value" : qsTr ("57600 bauds") },
                { "key" : 115200, "value" : qsTr ("115200 bauds") },
            ];
            delegate: ComboListDelegateForModelDataAttributes { }
            Component.onCompleted: { selectByKey (9600); }
        }
        TextLabel {
            text: qsTr ("Data bits :");
        }
        ComboList {
            id: comboSerialDatabits;
            model: [
                { "key" : 5, "value" : qsTr ("5 bits (Baudot)")       },
                { "key" : 6, "value" : qsTr ("6 bits (rarely used)")  },
                { "key" : 7, "value" : qsTr ("7 bits (compat ASCII)") },
                { "key" : 8, "value" : qsTr ("8 bits (full-byte)")    },
            ];
            delegate: ComboListDelegateForModelDataAttributes { }
            Component.onCompleted: { selectByKey (8); }
        }
        TextLabel {
            text: qsTr ("Stop bits :");
        }
        ComboList {
            id: comboSerialStopbits;
            model: [
                { "key" : 1, "value" : qsTr ("1 stop bit")                       },
                { "key" : 3, "value" : qsTr ("1.5 stop bits (only for Windows)") },
                { "key" : 2, "value" : qsTr ("2 stop bits")                      },
            ];
            delegate: ComboListDelegateForModelDataAttributes { }
            Component.onCompleted: { selectByKey (1); }
        }
        TextLabel {
            text: qsTr ("Parity :");
        }
        ComboList {
            id: comboSerialParity;
            model: [
                { "key" : 0, "value" : qsTr ("No parity (handled by protocol)")                   },
                { "key" : 2, "value" : qsTr ("Even parity (number of '1' bits always even)")      },
                { "key" : 3, "value" : qsTr ("Odd parity (number of '1' bits always odd)")        },
                { "key" : 4, "value" : qsTr ("Space parity (sent in the space signal condition)") },
                { "key" : 5, "value" : qsTr ("Mark parity (set to the mark signal condition)")    },
            ];
            delegate: ComboListDelegateForModelDataAttributes { }
            Component.onCompleted: { selectByKey (0); }
        }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: serialBus.portLoaded;

        TextLabel {
            text: qsTr ("Serial port '%1' loaded").arg (serialBus.portName);
            horizontalAlignment: Text.AlignHCenter;
            font {
                weight: Font.Light;
                pixelSize: Style.fontSizeTitle;
            }
        }
        Line { }
        TextLabel {
            text: qsTr ("If you have issues or need to change config,\npress this button :");
            horizontalAlignment: Text.AlignHCenter;
        }
        StretchRowContainer {
            Stretcher { }
            TextButton {
                text: qsTr ("Stop and reconfigure");
                icon: SvgIconLoader { icon: "actions/stop"; }
                textColor: Style.colorError;
                onClicked: { serialBus.stop (); }
            }
            Stretcher { }
        }
    }
}
