import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

ExpandableGroup {
    id: base;
    uidLabel {
        text: (board ? board.uid : "");
        emphasis: true;
        font.italic: true;
    }
    titleLabel {
        text: (board ? board.title : "");
        font.italic: true;
    }
    ExtraAnchors.horizontalFill: parent;

    property Board board : null;

    signal needVisible (Item item);

    Repeater {
        model: (board ? board.ios : 0);
        delegate: InstanceCreator {
            component: {
                switch (io.type) {
                case ObjectType.ANALOG:
                    switch (io.direction) {
                    case ObjectDirection.INPUT:  return Components.delegateRawAinTable;
                    case ObjectDirection.OUTPUT: return Components.delegateRawAoutTable;
                    }
                    break;
                case ObjectType.DIGITAL:
                    switch (io.direction) {
                    case ObjectDirection.INPUT:  return Components.delegateRawDinTable;
                    case ObjectDirection.OUTPUT: return Components.delegateRawDoutTable;
                    }
                    break;
                }
                return null;
            }
            properties: ({ "io" : io, "alternate" : !base.alternate });
            ExtraAnchors.horizontalFill: parent;

            readonly property AbstractIO io : modelData;
        }
    }
}
