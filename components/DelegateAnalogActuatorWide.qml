import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

SliderBar {
    barSize: Style.spacingNormal;
    useSplit: (actuator  ? actuator.useSplitPoint : false);
    handleSize: (barSize * 1.5);
    minValue: (actuator ? actuator.minRaw : 0);
    maxValue: (actuator ? actuator.maxRaw : 1);
    editable: (actuator && ((actuator.sourceLink && actuator.sourceLink.detached) || !actuator.sourceLink));
    ExtraAnchors.horizontalFill: parent;
    onEdited: { actuator.valRaw = value; }

    Binding on value { value: (actuator ? actuator.valRaw : 0); }

    property AnalogActuator actuator : null;
}
