import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

BorderedBackground {
    id: base;
    rounded: true;
    implicitHeight: (layoutMain.height + layoutMain.anchors.margins * 2);

    property alias iconItem   : icoLoader.component;
    property alias expanded   : layoutItems.visible;
    property alias expandable : chevron.visible;

    readonly property alias uidLabel   : lblUid;
    readonly property alias titleLabel : lblTitle;

    default property alias content : layoutItems.data;

    Column {
        id: layoutMain;
        spacing: Style.spacingNormal;
        anchors {
            margins: Style.spacingNormal;
            verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        ExtraAnchors.horizontalFill: parent;

        MouseArea {
            implicitHeight: layoutHeader.height;
            ExtraAnchors.horizontalFill: parent;
            onClicked: { expanded = !expanded; }

            StretchRowContainer {
                id: layoutHeader;
                spacing: Style.spacingNormal;
                ExtraAnchors.horizontalFill: parent;

                InstanceCreator {
                    id: icoLoader;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
                TextLabel {
                    id: lblUid;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
                TextLabel {
                    id: lblTitle;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
                Line {
                    opacity: 0.65;
                    implicitWidth: -1;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
                SvgIconLoader {
                    id: chevron;
                    icon: (layoutItems.visible
                           ? "actions/chevron-down"
                           : "actions/chevron-right");
                    color: Style.colorForeground;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
            }
        }
        Column {
            id: layoutItems;
            spacing: Style.spacingSmall;
            anchors.margins: Style.spacingBig;
            ExtraAnchors.horizontalFill: parent;

            // CONTENT HERE
        }
    }
}
