import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    id: base;

    property bool active    : false;
    property bool alternate : false;
    property bool rounded   : false;

    Rectangle {
        color: (active
                ? Style.colorHighlight
                : (alternate
                   ? Style.colorWindow
                   : Style.colorSecondary));
        radius: (rounded ? Style.roundness : 0);
        antialiasing: rounded;
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
        anchors.fill: parent;
    }
}
