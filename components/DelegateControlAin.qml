﻿import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

SliderBar {
    id: base;
    enabled: (io && ((io.link && (io.link.detached || io.link.reversed)) || !io.link));
    minValue: (io ? io.minRaw : 0);
    maxValue: (io ? io.maxRaw : 1);
    handleSize: Style.fontSizeNormal;
    implicitWidth: Style.realPixels (80);
    showTooltipWhenMoved: true;
    onEdited: { io.valRaw = value; }

    property AnalogInput io : null;

    Binding on value { value: (io ? io.valRaw : 0); }
}
