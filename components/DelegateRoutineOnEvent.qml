import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnEvent routineOnEvent : null;

    property alias routine : base.routineOnEvent;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnEvent ? routineOnEvent.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnEvent ? "(" + routineOnEvent.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on signal");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            text: (routineOnEvent ? routineOnEvent.signalName : "");
            enabled: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: qsTr ("from object");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            text: {
                var routine = routineOnEvent
                var obj = (routine ? routine.emiter : null);
                if (obj !== null) {
                    if ("path" in obj) {
                        return obj ["path"];
                    }
                    else {
                        return obj.toString ();
                    }
                }
                else {
                    return "NULL";
                }
            }
            enabled: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
