import QtQuick 2.1;

Item {
    id: base;
    implicitWidth:  (instance ? priv.instance.implicitWidth  : 0);
    implicitHeight: (instance ? priv.instance.implicitHeight : 0);
    onComponentChanged:    { priv.restart (); }
    onPropertiesChanged:   { priv.restart (); }
    Component.onCompleted: { priv.restart (); }

    property Component component  : null;
    property var       properties : ({});

    readonly property Item instance : priv.instance;

    Timer {
        id: priv;
        repeat: false;
        running: false;
        interval: 1;
        onTriggered: {
            if (instance) {
                instance.destroy ();
            }
            if (component) {
                instance = component.createObject (base, properties);
                instance.anchors.fill = base;
            }
            else {
                instance = null;
            }
        }

        property Item instance : null;
    }
}
