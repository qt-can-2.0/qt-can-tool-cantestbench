import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDialogDetails {
    id: base;
    title: qsTr ("Details of digital output :");

    readonly property DigitalOutput dout : object;

    FormContainer {
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (dout ? (dout.value ? "ON" : "OFF") : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Linked as source to :");
            visible: repeaterLinksAsSource.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsSource.count;

            Repeater {
                id: repeaterLinksAsSource;
                model: Shared.manager.getLinksAsSource (dout);
                delegate: DelegateLinkTarget {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
    }
}
