import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenObdValChange routineOnCanOpenObdValChange : null;

    property alias routine : base.routineOnCanOpenObdValChange;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenObdValChange ? routineOnCanOpenObdValChange.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenObdValChange ? "(" + routineOnCanOpenObdValChange.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on CANopen OBD change");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: qsTr ("on index");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            enabled: false;
            textAlign: TextInput.AlignHCenter;
            implicitWidth: (metricsCanOpenIndex.contentWidth + padding * 2);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            Binding on text {
                value: (routineOnCanOpenObdValChange
                        ? ("0x" + routineOnCanOpenObdValChange.index.toString (16))
                        : "");
            }
            TextLabel {
                id: metricsCanOpenIndex;
                text: "0x0000";
                color: Style.colorNone;
            }
        }
        TextLabel {
            text: qsTr ("and sub-index");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            enabled: false;
            textAlign: TextInput.AlignHCenter;
            implicitWidth: (metricsCanOpenSubIndex.contentWidth + padding * 2);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

            Binding on text {
                value: (routineOnCanOpenObdValChange
                        ? ("0x" + routineOnCanOpenObdValChange.subIndex.toString (16))
                        : "");
            }
            TextLabel {
                id: metricsCanOpenSubIndex;
                text: "0x00";
                color: Style.colorNone;
            }
        }
    }
}
