import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;

    property MethodHelp methodHelp : null;

    signal insertCodeRequested (string     code);
    signal objectHelpRequested (ObjectHelp objectHelp);

    Flow {
        spacing: Style.spacingNormal;

        ClickableTextLabel {
            text: methodHelp.name;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeBig;
            onClicked: { base.insertCodeRequested (methodHelp.name); }
        }
        TextLabel { text: ":"; }
        DelegateLinkObjectHelp {
            typeName: methodHelp.returnType;
            onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
        }
        TextLabel { text: "("; }
        Repeater {
            model: methodHelp.argumentsList;
            delegate: DelegateArgumentHelp {
                showComma: (model.index > 0);
                argumentHelp: model.qtObject;
                onInsertCodeRequested: { base.insertCodeRequested (code); }
                onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
            }
        }
        TextLabel { text: ")"; }
    }
    TextLabel {
        text: methodHelp.description;
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
        font.pixelSize: Style.fontSizeNormal;
    }
    Repeater {
        model: methodHelp.argumentsList;
        delegate: DelegateArgumentDetailsHelp {
            argumentHelp: model.qtObject;
        }
    }
}
