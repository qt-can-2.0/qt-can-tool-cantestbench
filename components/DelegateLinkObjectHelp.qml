import QtQuick 2.0;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

ClickableTextLabel {
    text: typeName;
    broken: (objectHelp === null);
    font.family: Style.fontFixedName;
    font.pixelSize: Style.fontSizeBig;
    onClicked: { objectHelpRequested (objectHelp); }

    property string typeName : "";

    readonly property ObjectHelp objectHelp : {
        var tmp = typeName.match (/enum\s+\((\w+)\)/);
        if (tmp) {
            return Help.getHelpForTypeByName (tmp [1]);
        }
        else {
            return Help.getHelpForTypeByName (typeName);
        }
    }

    signal objectHelpRequested (ObjectHelp objectHelp);
}
