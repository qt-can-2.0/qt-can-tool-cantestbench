import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Row {
    spacing: Style.spacingSmall;

    TextLabel {
        text: (actuator ? (actuator.value ? actuator.trueLabel : actuator.falseLabel) : "");
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }
    Circle {
        size: (Style.spacingNormal * 2.0);
        color: (actuator
                ? (actuator.value
                   ? Style.colorSelection
                   : Style.colorEditable)
                : Style.colorNone);
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }

    property DigitalActuator actuator : null;
}
