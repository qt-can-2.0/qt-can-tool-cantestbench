import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

TextLabel {
    text: (sensor ? Shared.format (sensor.valPhy, sensor.decimals, sensor.unit) : "");
    color: (sensor
            ? ((sensor.valPhy >= sensor.minPhy &&
                sensor.valPhy <= sensor.maxPhy)
               ? Style.colorForeground
               : Style.colorError)
            : Style.colorBorder);

    property AnalogSensor sensor : null;
}
