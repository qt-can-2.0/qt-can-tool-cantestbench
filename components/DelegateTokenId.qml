import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchRowContainer {
    id: base;
    spacing: Style.spacingSmall;

    property SyntaxTokenId tokenId : modelData;

    signal showLocationRequested (int pos);

    ClickableTextLabel {
        id: linkName;
        text: tokenId.identifier;
        font.italic: true;
        font.family: Style.fontFixedName;
        font.pixelSize: Style.fontSizeNormal;
        onClicked: { base.showLocationRequested (parent.tokenId.position); }
    }
    TextLabel { text: ":"; }
    TextLabel {
        text: tokenId.typeName;
        font.pixelSize: Style.fontSizeSmall;
        anchors.baseline: linkName.baseline;
    }
}
