import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenSdoWriteRequest routineOnCanOpenSdoWriteRequest : null;

    property alias routine : base.routineOnCanOpenSdoWriteRequest;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenSdoWriteRequest ? routineOnCanOpenSdoWriteRequest.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenSdoWriteRequest ? "(" + routineOnCanOpenSdoWriteRequest.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on an incoming CANopen 'WRITE' request");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
