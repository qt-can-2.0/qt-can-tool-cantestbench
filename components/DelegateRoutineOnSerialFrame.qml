import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnSerialFrame routineOnSerialFrame : null;

    property alias routine : base.routineOnSerialFrame;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnSerialFrame ? routineOnSerialFrame.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnSerialFrame ? "(" + routineOnSerialFrame.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on %1 frame").arg (routineOnSerialFrame && routineOnSerialFrame.serialBus ? routineOnSerialFrame.serialBus.uid : "serial");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
