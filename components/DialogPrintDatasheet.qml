import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

ModalDialog {
    title: qsTr ("Select a path for HTML print");
    buttons: (buttonAccept | buttonCancel);
    minWidth: 650;
    maxWidth: 650;
    onButtonClicked: {
        switch (buttonType) {
        case buttonAccept:
            var tmp = fileSelector.currentPath;
            if (tmp !== "") {
                Shared.manager.printToHtml (tmp,
                                            togglePrintIOs.value,
                                            togglePrintSensors.value,
                                            togglePrintActuators.value);
                hide ();
            }
            else {
                shake ();
            }
            break;
        case buttonCancel:
            hide ();
            break;
        }
    }

    FileSelector {
        id: fileSelector;
        folder: FileSystem.homePath;
        nameFilters: ["*.*htm*"];
        selectionType: (selectFile | selectAllowNew);
        implicitHeight: 300;
        onFileNameReturned: { buttonClicked (buttonAccept); }
    }
    Line { }
    TextLabel {
        text: qsTr ("Options");
        font.pixelSize: Style.fontSizeTitle;
    }
    StretchRowContainer {
        spacing: Style.spacingBig;

        TextLabel {
            text: qsTr ("Print I/O tables :");
        }
        CheckableBox {
            id: togglePrintIOs;
            value: true;
        }
        Stretcher { }
        TextLabel {
            text: qsTr ("Print sensors tables :");
        }
        CheckableBox {
            id: togglePrintSensors;
            value: true;
        }
        Stretcher { }
        TextLabel {
            text: qsTr ("Print actuators tables :");
        }
        CheckableBox {
            id: togglePrintActuators;
            value: true;
        }
    }
}
