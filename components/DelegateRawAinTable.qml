import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

BorderedBackground {
    id: base;
    height: implicitHeight;
    active: clicker.containsMouse;
    implicitHeight: (layoutIoTable.height + layoutIoTable.anchors.margins * 2);
    ExtraAnchors.horizontalFill: parent;

    property AnalogInput io : null;

    MouseArea {
        id: clicker;
        hoverEnabled: true;
        anchors.fill: parent;
    }
    StretchRowContainer {
        id: layoutIoTable;
        spacing: Style.spacingNormal;
        anchors.margins: Style.spacingSmall;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            text: (io ? io.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        SvgIconLoader {
            icon: (io && io.link && io.link.reversed
                   ? "qrc:///icons/reverse.svg"
                   : "qrc:///icons/normal.svg");
            size: Style.fontSizeSmall;
            color: Style.colorForeground;
            visible: linkSource.visible;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        ClickableTextLabel {
            id: linkSource;
            text: (io && io.link && io.link.source
                   ? io.link.source.uid
                   : "");
            visible: (text !== "");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: {
                Shared.highlightSensor = null;
                Shared.highlightSensor = io.link.source;
            }
        }
        Stretcher { }
        TextLabel {
            text: (io ? io.resolutionInPoints + " points" : "");
            color: Style.colorBorder;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { implicitWidth: (Style.spacingBig * 3); }
        NumberBox {
            padding: Style.spacingSmall;
            minValue: (io ? io.minRaw : 0);
            maxValue: (io ? io.maxRaw : 1);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { io.valRaw = value; }

            Binding on value { value: (io ? io.valRaw : 0); }
        }
    }
}
