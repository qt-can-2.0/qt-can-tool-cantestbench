import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

SliderBar {
    barSize: Style.spacingNormal;
    handleSize: (barSize * 1.5);
    useSplit: (sensor  ? sensor.useSplitPoint : false);
    minValue: (sensor ? sensor.minPhy : 0);
    maxValue: (sensor ? sensor.maxPhy : 1);
    editable: (sensor && ((sensor.sourceLink && sensor.sourceLink.detached) || !sensor.sourceLink));
    ExtraAnchors.horizontalFill: parent;
    onEdited: { sensor.valPhy = value; }

    Binding on value { value: (sensor ? sensor.valPhy : 0); }

    property AnalogSensor sensor : null;
}
