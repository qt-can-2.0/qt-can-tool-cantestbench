pragma Singleton;
import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

QtObject {
    id: base;

    readonly property Component symbolPlug                              : Component { SymbolPlug { } }
    readonly property Component delegateAin                             : Component { DelegateControlAin  { } }
    readonly property Component delegateAout                            : Component { DelegateControlAout { } }
    readonly property Component delegateDin                             : Component { DelegateControlDin  { } }
    readonly property Component delegateDout                            : Component { DelegateControlDout { } }
    readonly property Component delegateAnalogSensor                    : Component { DelegateAnalogSensor { } }
    readonly property Component delegateAnalogSensorMini                : Component { DelegateAnalogSensorMini { } }
    readonly property Component delegateAnalogSensorWide                : Component { DelegateAnalogSensorWide { } }
    readonly property Component delegateDigitalSensor                   : Component { DelegateDigitalSensor { } }
    readonly property Component delegateDigitalSensorMini               : Component { DelegateDigitalSensorMini { } }
    readonly property Component delegateHybridSensor                    : Component { DelegateHybridSensor { } }
    readonly property Component delegateHybridSensorMini                : Component { DelegateHybridSensorMini { } }
    readonly property Component delegateAnalogActuator                  : Component { DelegateAnalogActuator  { } }
    readonly property Component delegateAnalogActuatorMini              : Component { DelegateAnalogActuatorMini { } }
    readonly property Component delegateAnalogActuatorWide              : Component { DelegateAnalogActuatorWide { } }
    readonly property Component delegateDigitalActuator                 : Component { DelegateDigitalActuator { } }
    readonly property Component delegateDigitalActuatorMini             : Component { DelegateDigitalActuatorMini { } }
    readonly property Component delegateHybridActuator                  : Component { DelegateHybridActuator  { } }
    readonly property Component delegateHybridActuatorMini              : Component { DelegateHybridActuatorMini { } }
    readonly property Component delegateRoutineOnTimer                  : Component { DelegateRoutineOnTimer { } }
    readonly property Component delegateRoutineOnEvent                  : Component { DelegateRoutineOnEvent { } }
    readonly property Component delegateRoutineOnCanFrame               : Component { DelegateRoutineOnCanFrame { } }
    readonly property Component delegateRoutineOnSerialFrame            : Component { DelegateRoutineOnSerialFrame { } }
    readonly property Component delegateRoutineOnCanOpenBootUp          : Component { DelegateRoutineOnCanOpenBootUp { } }
    readonly property Component delegateRoutineOnCanOpenHeartbeatConsumer : Component { DelegateRoutineOnCanOpenHeartbeatConsumer { } }
    readonly property Component delegateRoutineOnCanOpenObdValChange    : Component { DelegateRoutineOnCanOpenObdValChange { } }
    readonly property Component delegateRoutineOnCanOpenNmtStateChange  : Component { DelegateRoutineOnCanOpenNmtStateChange { } }
    readonly property Component delegateRoutineOnCanOpenSdoReadReply    : Component { DelegateRoutineOnCanOpenSdoReadReply { } }
    readonly property Component delegateRoutineOnCanOpenSdoWriteRequest : Component { DelegateRoutineOnCanOpenSdoWriteRequest { } }
    readonly property Component delegatePhyVal                          : Component { DelegatePhyValTable   { } }
    readonly property Component delegatePhyGroup                        : Component { DelegatePhyGroupTable { } }
    readonly property Component delegatePhyBlock                        : Component { DelegatePhyBlockTable { } }
    readonly property Component delegatePhyMarker                       : Component { DelegatePhyMarkerTable { } }
    readonly property Component delegateRawNodeTable                    : Component { DelegateRawNodeTable { } }
    readonly property Component delegateRawBoardTable                   : Component { DelegateRawBoardTable { } }
    readonly property Component delegateRawAinTable                     : Component { DelegateRawAinTable { } }
    readonly property Component delegateRawDinTable                     : Component { DelegateRawDinTable { } }
    readonly property Component delegateRawDoutTable                    : Component { DelegateRawDoutTable { } }
    readonly property Component delegateRawAoutTable                    : Component { DelegateRawAoutTable { } }
    readonly property Component dlgAinDetails                           : Component { DialogDetailsAin { } }
    readonly property Component dlgAoutDetails                          : Component { DialogDetailsAout { } }
    readonly property Component dlgDinDetails                           : Component { DialogDetailsDin { } }
    readonly property Component dlgDoutDetails                          : Component { DialogDetailsDout { } }
    readonly property Component dlgPhyValDetails                        : Component { DialogDetailsPhy { } }
    readonly property Component dlgConfCanBus                           : Component { DialogConfigCanBus { } }
    readonly property Component dlgConfSerialBus                        : Component { DialogConfigSerialBus { } }
    readonly property Component dlgOpen                                 : Component { DialogOpenNetworkDefinition { } }
    readonly property Component dlgImport                               : Component { DialogImportSnapshot { } }
    readonly property Component dlgExport                               : Component { DialogExportSnapshot { } }
    readonly property Component dlgPrintLocation                        : Component { DialogPrintDatasheet { } }
    readonly property Component singleShot                              : Component {
        Timer {
            repeat: false;
            running: true;
            onTriggered: {
                callback ();
                destroy ();
            }

            property var callback : function () { }
        }
    }
}
