import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

Row {
    id: base;
    spacing: Style.spacingSmall;

    property bool showComma : false;

    property ArgumentHelp argumentHelp : null;

    signal insertCodeRequested (string     code);
    signal objectHelpRequested (ObjectHelp objectHelp);

    TextLabel { text: ","; visible: showComma; }
    ClickableTextLabel {
        text: argumentHelp.name;
        font.family: Style.fontFixedName;
        font.pixelSize: Style.fontSizeBig;
        onClicked: { insertCodeRequested (argumentHelp.name); }
    }
    TextLabel { text: ":"; }
    DelegateLinkObjectHelp {
        typeName: argumentHelp.type;
        onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
    }
    TextLabel {
        text: "[]";
        font.family: Style.fontFixedName;
        font.pixelSize: Style.fontSizeBig;
        visible: argumentHelp.isArray;
    }
}
