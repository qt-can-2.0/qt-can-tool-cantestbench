import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenStateChange routineOnCanOpenNmtStateChange : null;

    property alias routine : base.routineOnCanOpenNmtStateChange;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenNmtStateChange ? routineOnCanOpenNmtStateChange.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenNmtStateChange ? "(" + routineOnCanOpenNmtStateChange.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on CANopen NMT state change");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
