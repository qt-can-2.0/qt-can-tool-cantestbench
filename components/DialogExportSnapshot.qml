import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

ModalDialog {
    title: qsTr ("Select a path for snapshot export");
    buttons: (buttonAccept | buttonCancel);
    minWidth: 650;
    maxWidth: 650;
    onButtonClicked: {
        switch (buttonType) {
        case buttonAccept:
            var tmp = fileSelector.currentPath;
            if (tmp !== "") {
                Shared.manager.exportValues (tmp);
                hide ();
            }
            else {
                shake ();
            }
            break;
        case buttonCancel:
            hide ();
            break;
        }
    }

    FileSelector {
        id: fileSelector;
        folder: FileSystem.homePath;
        nameFilters: ["*.json"];
        selectionType: (selectFile | selectAllowNew);
        implicitHeight: 300;
    }
}
