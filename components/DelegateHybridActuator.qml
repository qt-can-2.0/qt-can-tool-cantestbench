import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (actuator !== null);

    property HybridActuator actuator : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("State :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        CheckableBox {
            size: Style.fontSizeBig;
            enabled: (actuator
                      && ((actuator.sourceLink
                           && (actuator.sourceLink.detached
                               || actuator.sourceLink.reversed))
                          || !actuator.sourceLink));
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { actuator.value = value; }

            Binding on value { value: (actuator ? actuator.value : false); }
        }
        TextLabel {
            text: (actuator
                   ? (actuator.value
                      ? "ON"
                      : "OFF")
                   : "");
            font.family: Style.fontFixedName;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblActuatorRatedSpeed;
            text: qsTr ("Rated speed :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorHighThreshold;
            text: (actuator ? Shared.format (actuator.ratedSpeed, actuator.decimals) : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblActuatorRatedSpeed.baseline;
        }
        TextLabel {
            text: (actuator ? actuator.unit : "");
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorRatedSpeed.baseline;
        }
    }
    Repeater {
        model: (actuator ? actuator.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
    Repeater {
        model: (actuator ? actuator.targetLink : 0);
        delegate: DelegateLinkTarget {
            link: modelData;
        }
    }
}
