import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDialogDetails {
    id: base;
    title: qsTr ("Details of physical block :");

    readonly property PhysicalValue phyVal : object;

    readonly property bool ranged : (phyVal && phyVal.max !== phyVal.min);

    FormContainer {
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.val.toFixed (2) + " u")
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Minimum :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.min.toFixed (2) + " u")
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Maximum :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.max.toFixed (2) + " u")
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Loop :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.loop
                          ? qsTr ("YES")
                          : qsTr ("NO"))
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Current speed :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.currentSpeed.toFixed (3) + " u/s")
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Acceleration limit :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.accelLimit !== 0.0
                          ? (phyVal.accelLimit.toFixed (3) + " u/s²")
                          : qsTr ("None"))
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Deceleration limit :");
            visible: ranged;
            emphasis: true;
        }
        StretchRowContainer {
            visible: ranged;
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (phyVal
                       ? (phyVal.decelLimit !== 0.0
                          ? (phyVal.decelLimit.toFixed (3) + " u/s²")
                          : qsTr ("None"))
                       : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Linked as source to :");
            visible: repeaterLinksAsSource.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsSource.count;

            Repeater {
                id: repeaterLinksAsSource;
                model: Shared.manager.getLinksAsSource (phyVal);
                delegate: DelegateLinkTarget {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
        TextLabel {
            text: qsTr ("Linked as target to :");
            visible: repeaterLinksAsTarget.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsTarget.count;

            Repeater {
                id: repeaterLinksAsTarget;
                model: Shared.manager.getLinksAsTarget (phyVal);
                delegate: DelegateLinkSource {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
    }
}
