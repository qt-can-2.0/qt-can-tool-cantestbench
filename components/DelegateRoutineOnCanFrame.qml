import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanFrame routineOnCanFrame : null;

    property alias routine : base.routineOnCanFrame;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanFrame ? routineOnCanFrame.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanFrame ? "(" + routineOnCanFrame.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on %1 frame").arg (routineOnCanFrame && routineOnCanFrame.canBus ? routineOnCanFrame.canBus.uid : "CAN");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: qsTr ("with ID");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            textAlign: TextInput.AlignHCenter;
            validator: RegExpValidator { regExp: /^[xABCDEFabcdef0123456789]*$/; }
            implicitWidth: (metricsCanIdDec.contentWidth + padding * 2);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onAccepted: {
                if (routineOnCanFrame) {
                    routineOnCanFrame.canId = parseInt (text.trim ());
                }
            }

            Binding on text {
                value: (routineOnCanFrame
                        ? routineOnCanFrame.canId.toString (10)
                        : "");
            }
            TextLabel {
                id: metricsCanIdDec;
                text: "0000";
                color: Style.colorNone;
            }
        }
        TextLabel {
            text: qsTr ("aka");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            textAlign: TextInput.AlignHCenter;
            validator: RegExpValidator { regExp: /^[xABCDEFabcdef0123456789]*$/; }
            implicitWidth: (metricsCanIdHex.contentWidth + padding * 2);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onAccepted: {
                if (routineOnCanFrame) {
                    routineOnCanFrame.canId = parseInt (text.trim ());
                }
            }

            Binding on text {
                value: (routineOnCanFrame
                        ? "0x" + routineOnCanFrame.canId.toString (16)
                        : "");
            }
            TextLabel {
                id: metricsCanIdHex;
                text: "0x000";
                color: Style.colorNone;
            }
        }
    }
}
