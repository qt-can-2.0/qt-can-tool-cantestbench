import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Circle {
    id: base;
    size: (Style.spacingNormal * 2.5);
    color: (io ? (io.value ? Style.colorSelection : Style.colorWindow) : Style.colorNone);
    enabled:  (io !== null);
    border {
        width: Style.lineSize;
        color: Style.colorBorder;
    }

    property DigitalOutput io : null;
}
