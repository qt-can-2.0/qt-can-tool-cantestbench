import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (actuator !== null);

    property AbstractActuator actuator : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        SliderBar {
            id: slider;
            useSplit: (actuator ? actuator.useSplitPoint : false);
            minValue: (actuator ? actuator.minRaw : 0);
            maxValue: (actuator ? actuator.maxRaw : 1);
            enabled: (actuator
                      && ((actuator.sourceLink
                           && actuator.sourceLink.detached)
                          || !actuator.sourceLink));
            handleSize: (Style.fontSizeNormal + Style.spacingSmall * 2);
            implicitWidth: -1;
            showTooltipWhenMoved: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { actuator.valRaw = value; }

            Binding on value { value: (actuator ? actuator.valRaw : 0); }
        }
        NumberBox {
            enabled: slider.enabled;
            padding: Style.spacingSmall;
            decimals: 0;
            minValue: (actuator ? actuator.minRaw : 0);
            maxValue: (actuator ? actuator.maxRaw : 1);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { actuator.valRaw = value; }

            Binding on value { value: (actuator ? actuator.valRaw : 0); }
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblActuatorRawVal;
            text: qsTr ("Raw value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            text: (actuator ? actuator.valRaw : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblActuatorRawVal.baseline;
        }
        TextLabel {
            text: "mV";
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorRawVal.baseline;
        }
        TextLabel {
            text: (actuator
                   ? "(" + [
                         actuator.minRaw,
                         actuator.maxRaw,
                     ].join (Style.charThreeDots) + ")"
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorRawVal.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblActuatorPercent;
            text: qsTr ("Percent value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            text: (actuator ? actuator.percents : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblActuatorPercent.baseline;
        }
        TextLabel {
            text: "%";
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorPercent.baseline;
        }
        TextLabel {
            text: (actuator
                   ? (actuator.useSplitPoint
                      ? ("(-100" + Style.charThreeDots + "100)")
                      : ("(0" + Style.charThreeDots + "100)"))
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorPercent.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblActuatorPhySpeed;
            text: qsTr ("Speed value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            text: (actuator ? Shared.format (actuator.valSpeed, actuator.decimals) : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblActuatorPhySpeed.baseline;
        }
        TextLabel {
            text: (actuator ? actuator.unit : "");
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorPhySpeed.baseline;
        }
        TextLabel {
            text: (actuator
                   ? "(" + [
                         Shared.format (actuator.minSpeed, actuator.decimals),
                         Shared.format (actuator.maxSpeed, actuator.decimals),
                     ].join (Style.charThreeDots) + ")"
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblActuatorPhySpeed.baseline;
        }
    }
    Repeater {
        model: (actuator ? actuator.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
    Repeater {
        model: (actuator ? actuator.targetLink : 0);
        delegate: DelegateLinkTarget {
            link: modelData;
        }
    }
}
