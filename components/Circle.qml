import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;

Rectangle {
    id: led;
    width: size;
    height: size;
    radius: (size / 2);
    antialiasing: radius;
    implicitWidth: size;
    implicitHeight: size;

    property int size : 0;
}
