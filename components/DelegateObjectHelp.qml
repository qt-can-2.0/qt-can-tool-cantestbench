import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingBig;

    property ObjectHelp objectHelp : null;

    signal insertCodeRequested (string     code);
    signal objectHelpRequested (ObjectHelp objectHelp);

    StretchColumnContainer {
        spacing: Style.spacingBig;

        Row {
            spacing: Style.spacingNormal;

            ClickableTextLabel {
                id: linkType;
                text: base.objectHelp.name;
                font.pixelSize: Style.fontSizeTitle;
                onClicked: { base.insertCodeRequested (text); }
            }
            TextLabel {
                text: "(%1)".arg (base.objectHelp.attribute);
                color: Style.colorBorder;
                anchors.baseline: linkType.baseline;
            }
        }
        StretchColumnContainer {
            spacing: Style.spacingNormal;

            Row {
                spacing: Style.spacingNormal;

                TextLabel {
                    text: qsTr ("Inherited from :");
                    anchors.baseline: linkBase.baseline;
                }
                DelegateLinkObjectHelp {
                    id: linkBase;
                    typeName: base.objectHelp.ancestor;
                    onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
                }
            }
            Row {
                spacing: Style.spacingNormal;
                visible: repeaterDerivates.count;

                TextLabel {
                    text: qsTr ("Inherited by :");
                }
                Column {
                    spacing: Style.lineSize;

                    Repeater {
                        id: repeaterDerivates;
                        model: base.objectHelp.derivates;
                        delegate: DelegateLinkObjectHelp {
                            typeName: modelData;
                            onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
                        }
                    }
                }
            }
            Row {
                spacing: Style.spacingNormal;
                visible: repeaterAccepted.count;

                TextLabel {
                    text: qsTr ("Accepts children :");
                }
                Column {
                    spacing: Style.lineSize;

                    Repeater {
                        id: repeaterAccepted;
                        model: base.objectHelp.accepted;
                        delegate: DelegateLinkObjectHelp {
                            typeName: modelData;
                            onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
                        }
                    }
                }
            }
        }
        StretchRowContainer {
            spacing: Style.spacingBig;

            TextLabel {
                text: qsTr ("Summary :");
                emphasis: true;
                font.pixelSize: Style.fontSizeBig;
            }
            Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
        }
        TextLabel {
            text: base.objectHelp.description;
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
            font.pixelSize: Style.fontSizeBig;
        }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: repeaterEnums.count;

        StretchRowContainer {
            spacing: Style.spacingBig;

            TextLabel {
                text: qsTr ("Enumerations :");
                emphasis: true;
                font.pixelSize: Style.fontSizeBig;
            }
            Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
        }
        Repeater {
            id: repeaterEnums;
            model: base.objectHelp.enumKeysList;
            delegate: DelegateEnumHelp {
                enumHelp: model.qtObject;
                onInsertCodeRequested: { base.insertCodeRequested (code); }
            }
        }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: repeaterProperties.count;

        StretchRowContainer {
            spacing: Style.spacingBig;

            TextLabel {
                text: qsTr ("Properties :");
                emphasis: true;
                font.pixelSize: Style.fontSizeBig;
            }
            Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
        }
        Repeater {
            id: repeaterProperties;
            model: base.objectHelp.propertiesList;
            delegate: DelegatePropertyHelp {
                propertyHelp: model.qtObject;
                onInsertCodeRequested: { base.insertCodeRequested (code); }
                onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
            }
        }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: repeaterMethods.count;

        StretchRowContainer {
            spacing: Style.spacingBig;

            TextLabel {
                text: qsTr ("Methods :");
                emphasis: true;
                font.pixelSize: Style.fontSizeBig;
            }
            Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
        }
        Repeater {
            id: repeaterMethods;
            model: base.objectHelp.methodsList;
            delegate: DelegateMethodHelp {
                methodHelp: model.qtObject;
                onInsertCodeRequested: { base.insertCodeRequested (code); }
                onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
            }
        }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: repeaterSignals.count;

        StretchRowContainer {
            spacing: Style.spacingBig;

            TextLabel {
                text: qsTr ("Signals :");
                emphasis: true;
                font.pixelSize: Style.fontSizeBig;
            }
            Line { implicitWidth: -1; anchors.verticalCenter: (parent ? parent.verticalCenter : undefined); }
        }
        Repeater {
            id: repeaterSignals;
            model: base.objectHelp.signalsList;
            delegate: DelegateSignalHelp {
                signalHelp: model.qtObject;
                onInsertCodeRequested: { base.insertCodeRequested (code); }
                onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
            }
        }
    }
}
