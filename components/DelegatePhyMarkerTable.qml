import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

ExpandableGroup {
    id: base;
    expandable: (dims.shouldBeVisible || !Shared.showOnlyEditable);
    iconItem: SvgIconLoader {
        icon: "others/target";
        size: Style.iconSize (1);
        color: Style.colorForeground;
    }
    uidLabel {
        text: (marker ? marker.uid : "");
        font.pixelSize: Style.fontSizeBig;
    }
    titleLabel {
        text: (marker ? marker.title : "");
        font.pixelSize: Style.fontSizeBig;
    }
    ExtraAnchors.horizontalFill: parent;

    property PhysicalMarker marker : null;

    signal needVisible (Item item);
    signal needDetails (PhysicalValue phyVal);

    DimensionsList {
        id: dims;
        alternate: !base.alternate;
        dimensionsList: (marker
                         ? [
                               marker.relativePos,
                               marker.absolutePos,
                           ]
                         : []);
        onNeedVisible: {
            base.expanded = true;
            base.needVisible (item);
        }
        onNeedDetails: {
            base.needDetails (phyVal);
        }
        onValueEdited: { }
    }
}
