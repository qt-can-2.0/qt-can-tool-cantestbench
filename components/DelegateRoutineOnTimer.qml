import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnTimer routineOnTimer : null;

    property alias routine : base.routineOnTimer;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnTimer ? routineOnTimer.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnTimer ? "(" + routineOnTimer.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on timer, every");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextBox {
            textAlign: TextInput.AlignHCenter;
            validator: IntValidator { bottom: 1; top: 3600000; }
            implicitWidth: Style.realPixels (80);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onAccepted: {
                if (routineOnTimer) {
                    routineOnTimer.interval = parseInt (text.trim ());
                }
            }

            Binding on text { value: (routineOnTimer ? routineOnTimer.interval : ""); }
        }
        TextLabel {
            text: qsTr ("ms");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
}
