import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDialogDetails {
    id: base;
    title: qsTr ("Details of digital input :");

    readonly property DigitalInput din : object;

    FormContainer {
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                text: (din ? (din.value ? "ON" : "OFF") : "");
                font.family: Style.fontFixedName;
            }
        }
        TextLabel {
            text: qsTr ("Linked as target to :");
            visible: repeaterLinksAsTarget.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsTarget.count;

            Repeater {
                id: repeaterLinksAsTarget;
                model: Shared.manager.getLinksAsTarget (din);
                delegate: DelegateLinkSource {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
    }
}
