import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

TextLabel {
    text: (actuator ? Shared.format (actuator.valSpeed, actuator.decimals, actuator.unit) : "");

    property AnalogActuator actuator : null;
}
