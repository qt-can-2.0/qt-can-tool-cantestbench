import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchRowContainer {
    spacing: Style.spacingNormal;
    visible: (link && link.source);

    property AbstractLink link : null;

    property alias showLabel : lbl.visible;

    TextButton {
        flat: true;
        icon: SymbolLoader {
            symbol: Components.symbolPlug;
            size: Style.fontSizeSmall;
        }
        padding: (Style.lineSize * 2);
        enabled: (link && link.enabled && link.source);
        textColor: (link && link.enabled && link.source
                    ? (link.detached
                       ? Style.colorError
                       : Style.colorLink)
                    : Style.colorBorder);
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        onClicked: { link.detached = !link.detached; }
    }
    SvgIconLoader {
        icon: (link && link.reversed
               ? "qrc:///icons/reverse.svg"
               : "qrc:///icons/normal.svg");
        size: Style.fontSizeSmall;
        color: Style.colorForeground;
        visible: (link && link.source);
        enabled: (link && link.enabled);
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }
    TextLabel {
        id: lbl;
        text: (link ? link.title + " :" : "");
        enabled: (link && link.enabled);
        emphasis: true;
        font.pixelSize: Style.fontSizeSmall;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }
    ClickableTextLabel {
        text: (link && link.source ? link && link.source.path : "(N/A)");
        broken: (link && link.detached);
        enabled: (link && link.enabled && link.source);
        clickable: (link && link.source &&
                    (link.source.family === ObjectFamily.IO ||
                     link.source.family === ObjectFamily.VALUE ||
                     link.source.family === ObjectFamily.SENSOR ||
                     link.source.family === ObjectFamily.ACTUATOR));
        font.pixelSize: Style.fontSizeSmall;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        onClicked: { Shared.highlightLinkedObject (link.source); }
    }
}
