import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

ExpandableGroup {
    id: base;
    expandable: (dims.shouldBeVisible || !Shared.showOnlyEditable);
    iconItem: Rectangle {
        color: (block ? block.color : Style.colorNone);
        implicitWidth: Style.iconSize (1);
        implicitHeight: Style.iconSize (1);
    }
    uidLabel {
        text: (block ? block.uid : "");
        font.pixelSize: Style.fontSizeBig;
    }
    titleLabel {
        text:  (block ? block.title : "");
        font.pixelSize: Style.fontSizeBig;
    }
    ExtraAnchors.horizontalFill: parent;

    property PhysicalBlock block : null;

    signal needVisible (Item item);
    signal needDetails (PhysicalValue phyVal);

    DimensionsList {
        id: dims;
        alternate: !base.alternate;
        dimensionsList: (block
                         ? [
                               block.pivotPos,
                               block.size,
                               block.angle,
                               block.absoluteAngle,
                           ]
                         : []);
        onNeedVisible: {
            base.expanded = true;
            base.needVisible (item);
        }
        onNeedDetails: {
            base.needDetails (phyVal);
        }
        onValueEdited: {
            if (dimension === block.absoluteAngle) {
                var oldAbs = phyVal.val;
                var newAbs = value;
                var delta  = (newAbs - oldAbs);
                var relVal = null;
                if (phyVal === block.absoluteAngle.yaw) {
                    relVal = block.angle.yaw;
                }
                else if (phyVal === block.absoluteAngle.pitch) {
                    relVal = block.angle.pitch;
                }
                else if (phyVal === block.absoluteAngle.roll) {
                    relVal = block.angle.roll;
                }
                else { }
                if (relVal) {
                    var tmp = (relVal.val + delta);
                    relVal.val = (tmp > relVal.max ? relVal.max : (tmp < relVal.min ? relVal.min : tmp));
                }
            }
            else {
                phyVal.val = value;
            }
        }
    }
}
