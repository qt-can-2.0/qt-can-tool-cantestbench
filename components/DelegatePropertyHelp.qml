import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;

    property PropertyHelp propertyHelp : null;

    signal insertCodeRequested (string     code);
    signal objectHelpRequested (ObjectHelp objectHelp);

    Flow {
        spacing: Style.spacingNormal;

        ClickableTextLabel {
            text: propertyHelp.name;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeBig;
            onClicked: { base.insertCodeRequested (propertyHelp.name); }
        }
        TextLabel { text: ":"; }
        DelegateLinkObjectHelp {
            typeName: propertyHelp.type;
            onObjectHelpRequested: { base.objectHelpRequested (objectHelp); }
        }
        TextLabel {
            text: "[]";
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeBig;
            visible: propertyHelp.isArray;
        }
        TextLabel {
            text: "(%1)".arg (propertyHelp.access);
            color: Style.colorBorder;
        }
    }
    TextLabel {
        text: propertyHelp.description;
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
        font.pixelSize: Style.fontSizeNormal;
    }
}
