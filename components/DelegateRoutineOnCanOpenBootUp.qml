import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenBootUp routineOnCanOpenBootUp : null;

    property alias routine : base.routineOnCanOpenBootUp;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenBootUp ? routineOnCanOpenBootUp.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenBootUp ? "(" + routineOnCanOpenBootUp.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on CANopen Boot Up frame");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
