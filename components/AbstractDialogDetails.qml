import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

ModalDialog {
    id: base;
    buttons: buttonOk;
    minWidth: layoutDetails.implicitWidth;
    maxWidth: layoutDetails.implicitWidth;
    message: (object ? [ "UID : %1".arg (object.path) ] : []);
    onButtonClicked: { hide (); }

    property BasicObject object : null;

    default property alias fields : layoutDetails.data;

    StretchColumnContainer {
        id: layoutDetails;
        spacing: Style.spacingNormal;
    }
}
