import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

BorderedBackground {
    id: base;
    height: implicitHeight;
    active: clicker.containsMouse;
    implicitHeight: (layoutIoTable.height + layoutIoTable.anchors.margins * 2);
    ExtraAnchors.horizontalFill: parent;

    property DigitalOutput io : null;

    MouseArea {
        id: clicker;
        hoverEnabled: true;
        anchors.fill: parent;
    }
    StretchRowContainer {
        id: layoutIoTable;
        spacing: Style.spacingNormal;
        anchors.margins: Style.spacingSmall;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            text: (io ? io.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        SvgIconLoader {
            icon: (io && io.link && io.link.reversed
                   ? "qrc:///icons/reverse.svg"
                   : "qrc:///icons/normal.svg");
            size: Style.fontSizeSmall;
            color: Style.colorForeground;
            visible: linkTarget.visible;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        ClickableTextLabel {
            id: linkTarget;
            text: (io && io.link && io.link.target
                   ? io.link.target.uid
                   : "");
            visible: (text !== "");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: {
                Shared.highlightActuator = null;
                Shared.highlightActuator = io.link.target;
            }
        }
        Stretcher { }
        Stretcher { implicitWidth: (Style.spacingBig * 3); }
        CheckableBox {
            size: Style.fontSizeBig;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { io.value = value; }

            Binding on value { value: (io ? io.value : false); }
        }
    }
}
