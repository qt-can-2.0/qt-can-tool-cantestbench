import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

ModalDialog {
    buttons: (!driverOptionsForm.visible ? buttonCancel : buttonNone);
    onButtonClicked: {
        switch (buttonType) {
        case buttonCancel:
            hide ();
            break;
        default:
            break;
        }
    }

    property CanBus canBus : null;

    DriverSelector {
        id: driverSelector;
        driverWrapper: canBus.driverWrapper;
    }
    DriverOptionsForm {
        id: driverOptionsForm;
        driverWrapper: canBus.driverWrapper;
        onDriverError: { shake (); }
        onDriverLoaded: { hide (); }
    }
    StretchColumnContainer {
        spacing: Style.spacingBig;
        visible: canBus.driverWrapper.driverLoaded;

        TextLabel {
            text: qsTr ("Driver '%1' loaded").arg (canBus.driverWrapper.driverName);
            horizontalAlignment: Text.AlignHCenter;
            font {
                weight: Font.Light;
                pixelSize: Style.fontSizeTitle;
            }
        }
        Line { }
        TextLabel {
            text: qsTr ("If you have issues or need to change config,\npress this button :");
            horizontalAlignment: Text.AlignHCenter;
        }
        StretchRowContainer {
            Stretcher { }
            TextButton {
                text: qsTr ("Stop and reconfigure");
                icon: SvgIconLoader { icon: "actions/stop"; }
                textColor: Style.colorError;
                onClicked: {
                    canBus.driverWrapper.selectDriver ("");
                    canBus.driverWrapper.loadDriver ({});
                }
            }
            Stretcher { }
        }
    }
}
