import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;

ExpandableGroup {
    id: base;
    iconItem: SvgIconLoader {
        icon: (expanded ? "filetypes/folder-opened" : "filetypes/folder-closed");
        size: Style.iconSize (1);
        color: Style.colorForeground;
    }
    uidLabel {
        text: (group ? group.uid : "");
        emphasis: true;
        font.pixelSize: Style.fontSizeBig;
    }
    titleLabel {
        text: (group ? group.title : "");
        font.pixelSize: Style.fontSizeBig;
    }
    ExtraAnchors.horizontalFill: parent;

    property ObjectsGroup group : null;

    signal needVisible (Item item);
    signal needDetails (PhysicalValue phyVal);

    Repeater {
        model: (group ? group.subObjects : 0);
        delegate: InstanceCreator {
            id: creator;
            component: {
                switch (modelData ["family"]) {
                case ObjectFamily.VALUE:  return Components.delegatePhyVal;
                case ObjectFamily.GROUP:  return Components.delegatePhyGroup;
                case ObjectFamily.BLOCK:  return Components.delegatePhyBlock;
                case ObjectFamily.MARKER: return Components.delegatePhyMarker;
                default:                  return null;
                }
            }
            properties: {
                switch (modelData ["family"]) {
                case ObjectFamily.VALUE:  return ({ "phyVal" : modelData, "alternate" : !base.alternate });
                case ObjectFamily.GROUP:  return ({ "group"  : modelData, "alternate" : !base.alternate });
                case ObjectFamily.BLOCK:  return ({ "block"  : modelData, "alternate" : !base.alternate });
                case ObjectFamily.MARKER: return ({ "marker" : modelData, "alternate" : !base.alternate });
                default:                  return ({});
                }
            }
            ExtraAnchors.horizontalFill: parent;

            Connections {
                target: creator.instance;
                ignoreUnknownSignals: true;
                onNeedVisible: {
                    base.expanded = true;
                    base.needVisible (item);
                }
                onNeedDetails: {
                    base.needDetails (phyVal);
                }
            }
        }
    }
}
