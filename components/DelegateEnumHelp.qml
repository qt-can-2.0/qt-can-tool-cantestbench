import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;

StretchRowContainer {
    id: base;
    spacing: Style.spacingBig;

    property EnumKeyHelp enumHelp : null;

    signal insertCodeRequested (string code);

    Stretcher {
        implicitHeight: linkName.height;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

        ClickableTextLabel {
            id: linkName;
            text: enumHelp.name;
            emphasis: true;
            font.family: Style.fontFixedName
            anchors.right: parent.right;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { insertCodeRequested (enumHelp.name); }
        }
    }
    Stretcher {
        implicitHeight: lblDescription.height;
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

        TextLabel {
            id: lblDescription;
            text: enumHelp.description;
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere;
            font.pixelSize: Style.fontSizeSmall;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            ExtraAnchors.horizontalFill: parent;
        }
    }
}
