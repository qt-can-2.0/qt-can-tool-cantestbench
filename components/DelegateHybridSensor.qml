import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (sensor !== null);

    property HybridSensor sensor : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("State :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        CheckableBox {
            size: Style.fontSizeBig;
            enabled: (sensor
                      && ((sensor.sourceLink
                           && (sensor.sourceLink.detached
                               || sensor.sourceLink.reversed))
                          || !sensor.sourceLink));
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { sensor.valState = value; }

            Binding on value { value: (sensor ? sensor.valState : false); }
        }
        TextLabel {
            text: (sensor
                   ? (sensor.valState
                      ? (sensor.invertState ? "Low" : "High")
                      : (sensor.invertState ? "High" : "Low"))
                   : "");
            font.family: Style.fontFixedName;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorPhyVal;
            text: qsTr ("Phy. value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorPhyVal;
            text: (sensor ? Shared.format (sensor.valPhy, sensor.decimals) : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblSensorPhyVal.baseline;
        }
        TextLabel {
            text: (sensor ? sensor.unit : "");
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorPhyVal.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorLowThreshold;
            text: qsTr ("Low threshold :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorLowThreshold;
            text: (sensor ? Shared.format (sensor.lowThresholdPhy, sensor.decimals) : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblSensorLowThreshold.baseline;
        }
        TextLabel {
            text: (sensor ? sensor.unit : "");
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorLowThreshold.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorHighThreshold;
            text: qsTr ("High threshold :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorHighThreshold;
            text: (sensor ? Shared.format (sensor.highThresholdPhy, sensor.decimals) : "");
            font.family: Style.fontFixedName;
            anchors.baseline: lblSensorHighThreshold.baseline;
        }
        TextLabel {
            text: (sensor ? sensor.unit : "");
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorHighThreshold.baseline;
        }
    }
    Repeater {
        model: (sensor ? sensor.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
    Repeater {
        model: (sensor ? sensor.targetLinks : 0);
        delegate: DelegateLinkTarget {
            link: modelData;
        }
    }
}
