import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDelegateRoutine {
    id: base;

    property RoutineOnCanOpenHeartbeatConsumer routineOnCanOpenHeartbeatConsumer : null;

    property alias routine : base.routineOnCanOpenHeartbeatConsumer;

    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: (routineOnCanOpenHeartbeatConsumer ? routineOnCanOpenHeartbeatConsumer.uid : "");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        TextLabel {
            text: (routineOnCanOpenHeartbeatConsumer ? "(" + routineOnCanOpenHeartbeatConsumer.title + ")" : "");
            visible: (text !== "" && text !== "()");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("triggered on CANopen Heartbeat Consumer change");
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
}
