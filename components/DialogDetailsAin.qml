import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

AbstractDialogDetails {
    id: base;
    title: qsTr ("Details of analog input :");

    readonly property AnalogInput ain : object;

    FormContainer {
        colSpacing: Style.spacingNormal;
        rowSpacing: Style.spacingNormal;

        TextLabel {
            text: qsTr ("Value :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblVal;
                text: (ain ? ain.valRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblVal.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Minimum :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblMin;
                text: (ain ? ain.minRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblMin.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Maximum :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblMax;
                text: (ain ? ain.maxRaw : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "mV";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblMax.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Resolution :");
            emphasis: true;
        }
        StretchRowContainer {
            spacing: Style.spacingSmall;

            Stretcher { }
            TextLabel {
                id: lblResolution;
                text: (ain ? ain.resolutionInPoints : "");
                font.family: Style.fontFixedName;
                anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            }
            TextLabel {
                text: "points";
                font.pixelSize: Style.fontSizeSmall;
                anchors.baseline: lblResolution.baseline;
            }
        }
        TextLabel {
            text: qsTr ("Linked as target to :");
            visible: repeaterLinksAsTarget.count;
            emphasis: true;
        }
        Column {
            spacing: Style.lineSize;
            visible: repeaterLinksAsTarget.count;

            Repeater {
                id: repeaterLinksAsTarget;
                model: Shared.manager.getLinksAsTarget (ain);
                delegate: DelegateLinkSource {
                    link: modelData;
                    showLabel: false;
                }
            }
        }
    }
}
