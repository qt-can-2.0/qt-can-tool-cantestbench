import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Row {
    spacing: Style.spacingSmall;

    TextLabel {
        text: (sensor ? (sensor.value ? sensor.trueLabel : sensor.falseLabel) : "");
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
    }
    Circle {
        size: (Style.spacingNormal * 2.0);
        color: (sensor
                ? (sensor.value
                   ? Style.colorSelection
                   : Style.colorEditable)
                : Style.colorNone);
        border {
            width: Style.lineSize;
            color: Style.colorBorder;
        }
        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);

        MouseArea {
            anchors.fill: parent;
            onClicked: { sensor.value = !sensor.value; }
        }
    }

    property DigitalSensor sensor : null;
}
