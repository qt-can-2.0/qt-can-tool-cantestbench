import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;

AbstractSymbol {
    id: base;

    Item {
        width: (size * 1 / 3);
        height: (size * 1 / 3);
        rotation: 45;
        anchors {
            top: parent.top;
            right: parent.right;
        }

        Rectangle {
            color: base.color;
            width: (parent.width / 2);
            height: parent.height;
            anchors.centerIn: parent;
        }
    }
    Item {
        width: (size * 1 / 3);
        height: (size * 1 / 3);
        rotation: 45;
        anchors {
            left: parent.left;
            bottom: parent.bottom;
        }

        Rectangle {
            color: base.color;
            width: (parent.width / 2);
            height: parent.height;
            anchors.centerIn: parent;
        }
    }
    Item {
        id: plug;
        width: subSize;
        height: subSize;
        rotation: 45;
        transformOrigin: Item.Center;
        anchors.centerIn: parent;

        readonly property real subSize : (size * 2 / 3);

        Item {
            clip: true;
            height: (parent.height * 2 / 5);
            anchors {
                top: parent.top;
                left: parent.left;
                right: parent.right;
            }

            Rectangle {
                color: base.color;
                width: plug.subSize;
                height: plug.subSize;
                radius: (plug.subSize / 2);
                antialiasing: true;
                anchors.top: parent.top;
            }
        }
        Item {
            clip: true;
            height: (parent.height * 2 / 5);
            anchors {
                left: parent.left;
                right: parent.right;
                bottom: parent.bottom;
            }

            Rectangle {
                color: base.color;
                width: plug.subSize;
                height: plug.subSize;
                radius: (plug.subSize / 2);
                antialiasing: true;
                anchors.bottom: parent.bottom;
            }
        }
    }
}
