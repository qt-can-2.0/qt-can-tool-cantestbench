import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    enabled: (routine !== null);
    spacing: Style.spacingSmall;

    property var routine;
}
