import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

CheckableBox {
    id: base;
    enabled: (io && ((io.link && io.link.detached) || !io.link));
    onEdited: { io.value = value; }

    property DigitalInput io : null;

    Binding on value { value: (io ? io.value : false); }
}
