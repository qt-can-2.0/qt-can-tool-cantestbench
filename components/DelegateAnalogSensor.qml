import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

StretchColumnContainer {
    id: base;
    spacing: Style.spacingSmall;
    enabled: (sensor !== null);

    property AnalogSensor sensor : null;

    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        SliderBar {
            id: slider;
            useSplit: (sensor ? sensor.useSplitPoint : false);
            minValue: (sensor ? sensor.minPhy : 0);
            maxValue: (sensor ? sensor.maxPhy : 1);
            enabled: (sensor
                      && ((sensor.sourceLink
                           && (sensor.sourceLink.detached
                               || sensor.sourceLink.reversed))
                          || !sensor.sourceLink));
            handleSize: (Style.fontSizeNormal + Style.spacingSmall * 2);
            implicitWidth: -1;
            showTooltipWhenMoved: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { sensor.valPhy = value; }

            Binding on value { value: (sensor ? sensor.valPhy : 0); }
        }
        NumberBox {
            enabled: slider.enabled;
            padding: Style.spacingSmall;
            decimals: (sensor ? sensor.decimals : 0);
            minValue: (sensor ? Shared.intToFloat (sensor.minPhy, sensor.decimals) : 0);
            maxValue: (sensor ? Shared.intToFloat (sensor.maxPhy, sensor.decimals) : 1);
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { sensor.valPhy = Shared.floatToInt (value, sensor.decimals); }

            Binding on value { value: (sensor ? Shared.intToFloat (sensor.valPhy, sensor.decimals) : 0); }
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorPhyVal;
            text: qsTr ("Phy. value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorPhyVal;
            text: (sensor ? Shared.format (sensor.valPhy, sensor.decimals) : "");
            font.family: Style.fontFixedName;
            color: (sensor
                    ? (sensor.valPhy >= sensor.minPhy && sensor.valPhy <= sensor.maxPhy
                       ? Style.colorForeground
                       : Style.colorError)
                      : Style.colorBorder);
            anchors.baseline: lblSensorPhyVal.baseline;
        }
        TextLabel {
            text: (sensor ? sensor.unit : "");
            color: viewSensorPhyVal.color;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorPhyVal.baseline;
        }
        TextLabel {
            text: (sensor
                   ? "(" + [
                         Shared.format (sensor.minPhy, sensor.decimals),
                         Shared.format (sensor.maxPhy, sensor.decimals),
                     ].join (Style.charThreeDots) + ")"
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorPhyVal.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorPercent;
            text: qsTr ("Percent value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorPercent;
            text: (sensor ? sensor.percents : "");
            color: (sensor
                    ? (sensor.percents >= (sensor.useSplitPoint ? -100 : 0) && sensor.percents <= 100
                       ? Style.colorForeground
                       : Style.colorError)
                      : Style.colorBorder);
            font.family: Style.fontFixedName;
            anchors.baseline: lblSensorPercent.baseline;
        }
        TextLabel {
            text: "%";
            color: viewSensorPercent.color;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorPercent.baseline;
        }
        TextLabel {
            text: (sensor
                   ? (sensor.useSplitPoint
                      ? ("(-100" + Style.charThreeDots + "100)")
                      : ("(0" + Style.charThreeDots + "100)"))
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorPercent.baseline;
        }
    }
    StretchRowContainer {
        spacing: Style.spacingNormal;
        ExtraAnchors.horizontalFill: parent;

        TextLabel {
            id: lblSensorRawVal;
            text: qsTr ("Raw value :");
            emphasis: true;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            id: viewSensorRawVal;
            text: (sensor ? sensor.valRaw : "");
            color: (sensor
                    ? (sensor.valRaw >= sensor.minRaw && sensor.valRaw <= sensor.maxRaw
                       ? Style.colorForeground
                       : Style.colorError)
                      : Style.colorBorder);
            font.family: Style.fontFixedName;
            anchors.baseline: lblSensorRawVal.baseline;
        }
        TextLabel {
            text: "mV";
            color: viewSensorRawVal.color;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorRawVal.baseline;
        }
        TextLabel {
            text: (sensor
                   ? "(" + [
                         sensor.minRaw,
                         sensor.maxRaw,
                     ].join (Style.charThreeDots) + ")"
                   : "");
            color: Style.colorBorder;
            font.family: Style.fontFixedName;
            font.pixelSize: Style.fontSizeSmall;
            anchors.baseline: lblSensorRawVal.baseline;
        }
    }
    Repeater {
        model: (sensor ? sensor.sourceLink : 0);
        delegate: DelegateLinkSource {
            link: modelData;
        }
    }
    Repeater {
        model: (sensor ? sensor.targetLinks : 0);
        delegate: DelegateLinkTarget {
            link: modelData;
        }
    }
}
