import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

ProgressJauge {
    id: basr;
    minValue: (io ? io.minRaw : 0);
    maxValue: (io ? io.maxRaw : 1);
    implicitWidth: Style.realPixels (80);

    property AnalogOutput io : null;

    Binding on value { value: (io ? io.valRaw : 0); }
}
