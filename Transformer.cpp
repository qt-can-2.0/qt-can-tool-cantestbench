
#include "Transformer.h"

#include "Manager.h"

AbstractTransformer::AbstractTransformer (const bool isReversible, QObject * parent)
    : BasicObject (ObjectFamily::TRANSFORMER, parent)
    , m_isReversible (isReversible)
{ }

AbstractTransformer::~AbstractTransformer (void) { }

void AbstractTransformer::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
}

int AbstractTransformer::targetFromSource (const int source) {
    return source;
}

int AbstractTransformer::sourceFromTarget (const int target) {
    return target;
}

float AbstractTransformer::targetFromSource (const float source) {
    return source;
}

float AbstractTransformer::sourceFromTarget (const float target) {
    return target;
}

AffineTransformer::AffineTransformer (QObject * parent)
    : AbstractTransformer (true, parent)
    , m_factor (1.0)
    , m_offset (0.0)
{
    Manager::instance ().registerObject (this);
}

AffineTransformer::~AffineTransformer (void) {
    Manager::instance ().unregisterObject (this);
}

void AffineTransformer::onComponentCompleted (void) {
    AbstractTransformer::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

int AffineTransformer::targetFromSource (const int source) {
    return int (m_offset + (source * m_factor));
}

int AffineTransformer::sourceFromTarget (const int target) {
    return int ((target - m_offset) / m_factor);
}

float AffineTransformer::targetFromSource (const float source) {
    return float (m_offset + (source * m_factor));
}

float AffineTransformer::sourceFromTarget (const float target) {
    return float ((target - m_offset) / m_factor);
}

CustomTransformer::CustomTransformer (QObject * parent)
    : AbstractTransformer (true, parent)
{
    Manager::instance ().registerObject (this);
}

CustomTransformer::~CustomTransformer (void) {
    Manager::instance ().unregisterObject (this);
}

void CustomTransformer::onComponentCompleted (void) {
    AbstractTransformer::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

int CustomTransformer::targetFromSource (const int source) {
    return callQmlFunction (m_fromSourceToTarget, source);
}

int CustomTransformer::sourceFromTarget (const int target) {
    return callQmlFunction (m_fromTargetToSource, target);
}

float CustomTransformer::targetFromSource (const float source) {
    return callQmlFunction (m_fromSourceToTarget, source);
}

float CustomTransformer::sourceFromTarget (const float target) {
    return callQmlFunction (m_fromTargetToSource, target);
}
