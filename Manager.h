#ifndef MANAGER_H
#define MANAGER_H

#include <QSet>
#include <QMap>
#include <QQueue>
#include <QStack>
#include <QString>
#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <QQmlEngine>
#include <QQuickItem>

#include "QQmlVariantListModel.h"
#include "QQmlObjectListModel.h"
#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"

#include "Actuator.h"
#include "CanBus.h"
#include "CanOpen.h"
#include "Dashboard.h"
#include "IdentifiableObject.h"
#include "IO.h"
#include "Link.h"
#include "NetworkDefinition.h"
#include "Node.h"
#include "Physics.h"
#include "Routine.h"
#include "Sensor.h"
#include "SerialBus.h"
#include "Transformer.h"

class CanMessage;

class Dictionary : public QObject {
    Q_OBJECT

public:
    explicit Dictionary (QObject * parent = Q_NULLPTR);

    Q_INVOKABLE BasicObject * getObject   (const QString & path) const;
    Q_INVOKABLE QQuickItem  * getDelegate (const QString & path) const;
};


class Manager : public QObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (qreal, dashboardZoom)
    QML_READONLY_VAR_PROPERTY (bool, ready)
    QML_READONLY_VAR_PROPERTY (bool, running)
    QML_READONLY_VAR_PROPERTY (bool, hasValidNetwork)
    QML_READONLY_CSTREF_PROPERTY (QString, currentFileUrl)
    QML_READONLY_PTR_PROPERTY (NetworkDefinition, currentNetwork)
    QML_CONSTANT_PTR_PROPERTY (QQmlVariantListModel, logsModel)
    QML_READONLY_PTR_PROPERTY (Dictionary, dict)
    QML_REFLIST_PROPERTY (Dictionary, testModel, 100)
    QML_REFLIST_PROPERTY (BasicObject, basicObjects, 5000)
    QML_REFLIST_PROPERTY (Node, nodes, 20)
    QML_REFLIST_PROPERTY (Board, boards, 200)
    QML_REFLIST_PROPERTY (AbstractIO, ios, 1000)
    QML_REFLIST_PROPERTY (AnalogInput, ains, 500)
    QML_REFLIST_PROPERTY (DigitalInput, dins, 500)
    QML_REFLIST_PROPERTY (AnalogOutput, aouts, 500)
    QML_REFLIST_PROPERTY (DigitalOutput, douts, 500)
    QML_REFLIST_PROPERTY (AbstractLink, links, 500)
    QML_REFLIST_PROPERTY (LinkAnalogActuatorToPhysicalValue, linksAnalogActuatorToPhysicalValue, 50)
    QML_REFLIST_PROPERTY (LinkAnalogOutputToAnalogActuator, linksAnalogOutputToAnalogActuator, 50)
    QML_REFLIST_PROPERTY (LinkAnalogSensorToAnalogInput, linksAnalogSensorToAnalogInput, 50)
    QML_REFLIST_PROPERTY (LinkDigitalOutputToDigitalActuator, linksDigitalOutputToDigitalActuator, 50)
    QML_REFLIST_PROPERTY (LinkDigitalSensorToDigitalInput, linksDigitalSensorToDigitalInput, 50)
    QML_REFLIST_PROPERTY (LinkPhysicalValueToAnalogSensor, linksPhysicalValueToAnalogSensor, 50)
    QML_REFLIST_PROPERTY (LinkAnalogOutputToAnalogInput, linksAnalogOutputToAnalogInput, 50)
    QML_REFLIST_PROPERTY (LinkDigitalOutputToDigitalInput, linksDigitalOutputToDigitalInput, 50)
    QML_REFLIST_PROPERTY (LinkPhysicalValueToHybridSensor, linksPhysicalValueToHybridSensor, 50)
    QML_REFLIST_PROPERTY (LinkHybridSensorToDigitalInput, linksHybridSensorToDigitalInput, 50)
    QML_REFLIST_PROPERTY (LinkDigitalOutputToHybridActuator, linksDigitalOutputToHybridActuator, 50)
    QML_REFLIST_PROPERTY (LinkHybridActuatorToPhysicalValue, linksHybridActuatorToPhysicalValue, 50)
    QML_REFLIST_PROPERTY (AbstractTransformer, transformers, 100)
    QML_REFLIST_PROPERTY (AffineTransformer, affineTransformers, 50)
    QML_REFLIST_PROPERTY (CustomTransformer, customTransformers, 20)
    QML_REFLIST_PROPERTY (AbstractSensor, sensors, 50)
    QML_REFLIST_PROPERTY (AnalogSensor, analogSensors, 30)
    QML_REFLIST_PROPERTY (DigitalSensor, digitalSensors, 30)
    QML_REFLIST_PROPERTY (HybridSensor, hybridSensors, 30)
    QML_REFLIST_PROPERTY (AbstractActuator, actuators, 50)
    QML_REFLIST_PROPERTY (AnalogActuator, analogActuators, 30)
    QML_REFLIST_PROPERTY (DigitalActuator, digitalActuators, 30)
    QML_REFLIST_PROPERTY (HybridActuator, hybridActuators, 30)
    QML_REFLIST_PROPERTY (AbstractRoutine, routines, 1000)
    QML_REFLIST_PROPERTY (RoutineOnTimer, routinesOnTimer, 300)
    QML_REFLIST_PROPERTY (RoutineOnEvent, routinesOnEvent, 300)
    QML_REFLIST_PROPERTY (RoutineOnCanFrame, routinesOnCanFrame, 300)
    QML_REFLIST_PROPERTY (RoutineOnSerialFrame, routinesOnSerialFrame, 30)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenStateChange, routinesOnCanOpenStateChange, 20)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenObdValChange, routinesOnCanOpenObdValChange, 100)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenSdoReadReply, routinesOnCanOpenSdoReadReply, 20)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenSdoWriteRequest, routinesOnCanOpenSdoWriteRequest, 20)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenBootUp, routinesOnCanOpenBootUp, 5)
    QML_REFLIST_PROPERTY (RoutineOnCanOpenHeartbeatConsumer, routinesOnCanOpenHeartbeatConsumer, 5)
    QML_REFLIST_PROPERTY (PhysicalValue, physicalValues, 500)
    QML_REFLIST_PROPERTY (PhysicalBlock, physicalBlocks, 10)
    QML_REFLIST_PROPERTY (PhysicalMarker, physicalMarkers, 20)
    QML_REFLIST_PROPERTY (ObjectsGroup, objectsGroups, 5)
    QML_REFLIST_PROPERTY (CanOpen, canOpens, 20)
    QML_REFLIST_PROPERTY (CanBus, canBuses, 5)
    QML_REFLIST_PROPERTY (SerialBus, serialBuses, 5)
    QML_REFLIST_PROPERTY (Dashboard, dashboards, 5)

public:
    static Manager & instance (void);

    static QString qmlLocation (QObject * object);

    QQmlEngine * qmlEngine;

    void dump (const QString & category, QObject * object) const;

    void log (const QString & type, const QString & module, QObject * object, const QString & msg) const;

    void registerObject   (QObject * object);
    void intializeObject  (QObject * object);
    void unregisterObject (QObject * object);

    BasicObject * getObjectByPath    (const QString & path) const;
    QQuickItem  * getDelegateForPath (const QString & path) const;

    Q_INVOKABLE void setDelegateForPath (const QString & path, QQuickItem * item);

    Q_INVOKABLE QVariantList getLinksAsSource (BasicObject * object) const;
    Q_INVOKABLE QVariantList getLinksAsTarget (BasicObject * object) const;

public slots:
    void load         (const QString & networkDefinition);
    void init         (void);
    void start        (void);
    void pause        (void);
    void reset        (void);
    void deinit       (void);
    void exportValues (const QString & filePath = "");
    void importValues (const QString & filePath);
    void printToHtml  (const QString & filePath, const bool ios, const bool sensors, const bool actuators);
    void logInfo      (const QString & module, QObject * object, const QString & msg);
    void logWarning   (const QString & module, QObject * object, const QString & msg);
    void logError     (const QString & module, QObject * object, const QString & msg);

protected slots:
    void onTick (void);

    void doInitObjects (void);
    void doBuildChains (void);

private:
    bool m_allowInit;
    QTimer * m_timer;
    QTimer * m_singleshot;
    QElapsedTimer m_chrono;
    QSet<QObject *> m_objectsInstances;
    QStack<QObject *> m_objectsToInit;
    QMap<QString, BasicObject *> m_objectsByPath;
    QMap<QString, QQuickItem *> m_delegatesByPath;
    Reference<PhysicalWorld> m_physicalWorld;

    explicit Manager (void);
};

#endif // MANAGER_H
