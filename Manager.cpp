
#include "Manager.h"

#include "CppUtils.h"

#include "CanDriver.h"
#include "CanMessage.h"
#include "CanDataQmlWrapper.h"
#include "CanDriverQmlWrapper.h"

#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QQmlComponent>
#include <QRegularExpression>
#include <QCoreApplication>
#include <QStringBuilder>

#include "QtQml/private/qqmldata_p.h"
#include "QtQml/private/qqmlcontext_p.h"
#include "QtQml/private/qqmlmetatype_p.h"
#include "QtQml/private/qqmlengine_p.h"

struct AbstractSensorChain   { };
struct AbstractActuatorChain { };

struct AnalogSensorChain : public AbstractSensorChain {
    PhysicalValue *                   value             = Q_NULLPTR;
    LinkPhysicalValueToAnalogSensor * linkValueToSensor = Q_NULLPTR;
    AnalogSensor *                    sensor            = Q_NULLPTR;
    LinkAnalogSensorToAnalogInput *   linkSensorToAin   = Q_NULLPTR;
    AnalogInput *                     ain               = Q_NULLPTR;
};

struct DigitalSensorChain : public AbstractSensorChain {
    DigitalSensor *                   sensor          = Q_NULLPTR;
    LinkDigitalSensorToDigitalInput * linkSensorToDin = Q_NULLPTR;
    DigitalInput *                    din             = Q_NULLPTR;
};

struct HybridSensorChain : public AbstractSensorChain {
    PhysicalValue *                   value             = Q_NULLPTR;
    LinkPhysicalValueToHybridSensor * linkValueToSensor = Q_NULLPTR;
    HybridSensor *                    sensor            = Q_NULLPTR;
    LinkHybridSensorToDigitalInput *  linkSensorToDin   = Q_NULLPTR;
    DigitalInput *                    din               = Q_NULLPTR;
};

struct AnalogActuatorChain : public AbstractActuatorChain {
    AnalogOutput *                      aout                = Q_NULLPTR;
    LinkAnalogOutputToAnalogActuator *  linkAoutToActuator  = Q_NULLPTR;
    AnalogActuator *                    actuator            = Q_NULLPTR;
    LinkAnalogActuatorToPhysicalValue * linkActuatorToValue = Q_NULLPTR;
    PhysicalValue *                     value               = Q_NULLPTR;
};

struct DigitalActuatorChain : public AbstractActuatorChain {
    DigitalOutput *                      dout               = Q_NULLPTR;
    LinkDigitalOutputToDigitalActuator * linkDoutToActuator = Q_NULLPTR;
    DigitalActuator *                    actuator           = Q_NULLPTR;
};

struct HybridActuatorChain : public AbstractActuatorChain {
    DigitalOutput *                     dout                = Q_NULLPTR;
    LinkDigitalOutputToHybridActuator * linkDoutToActuator  = Q_NULLPTR;
    HybridActuator *                    actuator            = Q_NULLPTR;
    LinkHybridActuatorToPhysicalValue * linkActuatorToValue = Q_NULLPTR;
    PhysicalValue *                     value               = Q_NULLPTR;
};

Dictionary::Dictionary (QObject * parent)
    : QObject (parent)
{ }

BasicObject * Dictionary::getObject (const QString & path) const {
    return Manager::instance ().getObjectByPath (path);
}

QQuickItem * Dictionary::getDelegate (const QString & path) const {
    return Manager::instance ().getDelegateForPath (path);
}

Manager & Manager::instance (void) {
    static Manager ret;
    return ret;
}

Manager::Manager (void)
    : QObject (Q_NULLPTR)
    , m_dashboardZoom (1.0)
    , m_ready (false)
    , m_running (false)
    , m_hasValidNetwork (false)
    , m_currentNetwork (Q_NULLPTR)
    , qmlEngine (Q_NULLPTR)
    , m_allowInit (false)
    , m_physicalWorld (Q_NULLPTR)
{
    m_dict = new Dictionary (this);
    m_singleshot = new QTimer (this);
    m_singleshot->setTimerType (Qt::CoarseTimer);
    m_singleshot->setSingleShot (true);
    m_timer = new QTimer (this);
    m_timer->setTimerType (Qt::PreciseTimer);
    m_logsModel = new QQmlVariantListModel (this);
    connect (m_timer,      &QTimer::timeout, this, &Manager::onTick);
    connect (m_singleshot, &QTimer::timeout, this, &Manager::doInitObjects);
}

QString Manager::qmlLocation (QObject * object) {
    QString ret;
    if (object) {
        // get object UID if any
        if (BasicObject * basicObject = qobject_cast<BasicObject *> (object)) {
            if (!basicObject->get_uid ().isEmpty ()) {
                ret += ("'" % basicObject->get_uid () % "'");
            }
        }
        else if (Dashboard * dashboard = qobject_cast<Dashboard *> (object)) {
            if (!dashboard->get_uid ().isEmpty ()) {
                ret += ("'" % dashboard->get_uid () % "'");
            }
        }
        // get QML source code location
        if (QQmlData * qmlData = QQmlData::get (object, false)) {
            if (qmlData->outerContext) {
                if (!ret.isEmpty ()) {
                    ret += " @ ";
                }
#if QT_VERSION >= 0x050500
                const QString url = qmlData->outerContext->urlString ();
#else
                const QString url = qmlData->outerContext->urlString;
#endif
                ret += QStringLiteral ("%1:%2:%3").arg (url).arg (qmlData->lineNumber).arg (qmlData->columnNumber);
            }
        }
    }
    return ret;
}

void Manager::dump (const QString & category, QObject * object) const {
    if (object && m_currentNetwork && m_currentNetwork->get_dumpObjectsOnInit ()) {
        if (BasicObject * basicObject = qobject_cast<BasicObject *> (object)) {
            if (AbstractLink * link = basicObject->as<AbstractLink> ()) {
                DUMP << qPrintable (category) << link << "from" << link->get_source () << "to" << link->get_target ();
            }
            else {
                DUMP << qPrintable (category) << basicObject << basicObject->get_path ();
            }
        }
        else if (Dashboard * dashboard = qobject_cast<Dashboard *> (object)) {
            DUMP << qPrintable (category) << dashboard << dashboard->get_uid ();
        }
        else { }
    }
}

void Manager::load (const QString & netDefUrl) {
    static QElapsedTimer bench;
    deinit ();
    bench.restart ();
    QQmlComponent compo (qmlEngine, QUrl (netDefUrl), QQmlComponent::PreferSynchronous);
    WARN << "Network loaded and parsed in" << bench.elapsed () << "ms";
    if (compo.isReady ()) {
        bench.restart ();
        QObject * rootObject = compo.create ();
        WARN << "Network instantiated in" << bench.elapsed () << "ms";
        if (NetworkDefinition * netDefObj = qobject_cast<NetworkDefinition *> (rootObject)) {
            if (netDefObj->get_uid ().isEmpty ()) {
                logWarning ("LOAD", netDefObj, "You should really set an UID on the NetworkDefinition to make snapshots a little safer !");
            }
            update_currentFileUrl (netDefUrl);
            update_currentNetwork (netDefObj);
            bench.restart ();
            init ();
            WARN << "Network initialized in" << bench.elapsed () << "ms";
            bench.restart ();
            update_hasValidNetwork (true);
            WARN << "Network activated in" << bench.elapsed () << "ms";
        }
        else {
            logError ("LOAD", rootObject, "Root object must be a NetworkDefinition !");
            if (rootObject) {
                delete rootObject;
            }
            deinit ();
        }
    }
    else {
        for (const QQmlError & err : arrayRange (compo.errors ())) {
            logError ("LOAD", Q_NULLPTR, err.toString ());
        }
    }
}

void Manager::init (void) {
    update_ready (false);
    m_allowInit = true;
    doInitObjects ();
    update_ready (true);
}

void Manager::start (void) {
    if (m_currentNetwork) {
        update_running (true);
        for (Node * node : arrayRange (m_nodes)) {
            node->start ();
        }
        for (CanOpen * canOpen : arrayRange (m_canOpens)) {
            canOpen->start ();
        }
        for (RoutineOnTimer * routine : arrayRange (m_routinesOnTimer)) {
            routine->start ();
        }
        m_chrono.restart ();
        m_timer->start (m_currentNetwork->get_clock ());
    }
}

void Manager::pause (void) {
    if (m_currentNetwork) {
        m_timer->stop ();
        update_running (false);
        for (RoutineOnTimer * routine : arrayRange (m_routinesOnTimer)) {
            routine->stop ();
        }
        for (CanOpen * canOpen : arrayRange (m_canOpens)) {
            canOpen->stop ();
        }
        for (Node * node : arrayRange (m_nodes)) {
            node->stop ();
        }
    }
}

void Manager::reset (void) {
    m_allowInit = false;
    m_singleshot->stop ();
    if (m_running) {
        pause ();
    }
    deinit ();
    load (m_currentFileUrl);
}

void Manager::deinit (void) {
    static QElapsedTimer bench;
    bench.restart ();
    update_ready (false);
    update_hasValidNetwork (false);
    if (m_currentNetwork) {
        NetworkDefinition * oldNet = m_currentNetwork;
        update_currentNetwork (Q_NULLPTR);
        QCoreApplication::processEvents ();
        m_objectsInstances.clear ();
        m_objectsByPath.clear ();
        m_delegatesByPath.clear ();
        m_basicObjects.clear ();
        m_physicalWorld.clear ();
        m_nodes.clear ();
        m_boards.clear ();
        m_ios.clear ();
        m_ains.clear ();
        m_dins.clear ();
        m_aouts.clear ();
        m_douts.clear ();
        m_sensors.clear ();
        m_analogSensors.clear ();
        m_digitalSensors.clear ();
        m_hybridSensors.clear ();
        m_actuators.clear ();
        m_analogActuators.clear ();
        m_digitalActuators.clear ();
        m_hybridActuators.clear ();
        m_physicalValues.clear ();
        m_physicalBlocks.clear ();
        m_physicalMarkers.clear ();
        m_objectsGroups.clear ();
        m_routines.clear ();
        m_routinesOnTimer.clear ();
        m_routinesOnEvent.clear ();
        m_routinesOnCanFrame.clear ();
        m_routinesOnSerialFrame.clear ();
        m_routinesOnCanOpenStateChange.clear ();
        m_routinesOnCanOpenObdValChange.clear ();
        m_routinesOnCanOpenSdoReadReply.clear ();
        m_routinesOnCanOpenSdoWriteRequest.clear ();
        m_routinesOnCanOpenBootUp.clear ();
        m_routinesOnCanOpenHeartbeatConsumer.clear ();
        m_links.clear ();
        m_linksAnalogActuatorToPhysicalValue.clear ();
        m_linksAnalogOutputToAnalogActuator.clear ();
        m_linksAnalogSensorToAnalogInput.clear ();
        m_linksDigitalOutputToDigitalActuator.clear ();
        m_linksDigitalSensorToDigitalInput.clear ();
        m_linksPhysicalValueToAnalogSensor.clear ();
        m_linksAnalogOutputToAnalogInput.clear ();
        m_linksDigitalOutputToDigitalInput.clear ();
        m_linksHybridSensorToDigitalInput.clear ();
        m_linksPhysicalValueToHybridSensor.clear ();
        m_linksHybridActuatorToPhysicalValue.clear ();
        m_linksDigitalOutputToHybridActuator.clear ();
        m_transformers.clear ();
        m_affineTransformers.clear ();
        m_customTransformers.clear ();
        m_canOpens.clear ();
        m_canBuses.clear ();
        m_serialBuses.clear ();
        m_dashboards.clear ();
        delete oldNet;
    }
    qmlEngine->collectGarbage ();
    qmlEngine->trimComponentCache ();
    QCoreApplication::processEvents ();
    WARN << "Network reset in" << bench.elapsed () << "ms";
}

void Manager::exportValues (const QString & filePath) {
    QFile file (!filePath.isEmpty ()
                ? (filePath.endsWith (".json") ? filePath : filePath % ".json")
                : QDir::homePath ()
                  % QStringLiteral ("/CanTestBench_")
                  % QDateTime::currentDateTime ().toString (QStringLiteral ("yyyy-MM-dd"))
                  % QStringLiteral ("_")
                  % QDateTime::currentDateTime ().toString (QStringLiteral ("hh:mm:ss"))
                  % QStringLiteral (".json"));
    if (file.open (QFile::WriteOnly)) {
        QJsonObject ioEntry { };
        QJsonObject sensorEntry { };
        QJsonObject actuatorEntry { };
        QJsonObject blockEntry { };
        for (AbstractIO * io : arrayRange (m_ios)) {
            ioEntry.insert (io->get_path (), io->exportState ());
        }
        for (AbstractSensor * sensor : arrayRange (m_sensors)) {
            sensorEntry.insert (sensor->get_path (), sensor->exportState ());
        }
        for (AbstractActuator * actuator : arrayRange (m_actuators)) {
            actuatorEntry.insert (actuator->get_path (), actuator->exportState ());
        }
        for (PhysicalBlock * block : arrayRange (m_physicalBlocks)) {
            blockEntry.insert (block->get_path (), block->exportState ());
        }
        const QJsonDocument doc {
            QJsonObject {
                { QStringLiteral ("uid"),       m_currentNetwork->get_uid () },
                { QStringLiteral ("ios"),       ioEntry       },
                { QStringLiteral ("blocks"),    blockEntry    },
                { QStringLiteral ("sensors"),   sensorEntry   },
                { QStringLiteral ("actuators"), actuatorEntry },
            }
        };
        file.write (doc.toJson () % QByteArrayLiteral ("\r\n"));
        file.flush ();
        file.close ();
        logInfo ("EXPORT", Q_NULLPTR, "Snapshot exported to " % file.fileName ());
    }
}

void Manager::importValues (const QString & filePath) {
    QFile file (filePath);
    if (file.open (QFile::ReadOnly)) {
        const QJsonObject tree = QJsonDocument::fromJson (file.readAll ()).object ();
        const QString uid = tree.value ("uid").toString ();
        if (uid == m_currentNetwork->get_uid ()) {
            const QJsonObject iosEntry = tree.value (QStringLiteral ("ios")).toObject ();
            for (KeyValueRange<QJsonObject>::It itIO : keyValueRange (iosEntry)) {
                if (BasicObject * io = m_objectsByPath.value (itIO.key (), Q_NULLPTR)) {
                    io->updateState (itIO.value ().toObject ());
                }
                else {
                    logWarning ("IMPORT", Q_NULLPTR, "Can't find IO " % itIO.key () % " to import state from snapshot !");
                }
            }
            const QJsonObject blocksEntry = tree.value (QStringLiteral ("blocks")).toObject ();
            for (KeyValueRange<QJsonObject>::It itBlock : keyValueRange (blocksEntry)) {
                if (BasicObject * block = m_objectsByPath.value (itBlock.key (), Q_NULLPTR)) {
                    block->updateState (itBlock.value ().toObject ());
                }
                else {
                    logWarning ("IMPORT", Q_NULLPTR, "Can't find BLOCK " % itBlock.key () % " to import state from snapshot !");
                }
            }
            const QJsonObject sensorsEntry = tree.value (QStringLiteral ("sensors")).toObject ();
            for (KeyValueRange<QJsonObject>::It itSensor : keyValueRange (sensorsEntry)) {
                if (BasicObject * sensor = m_objectsByPath.value (itSensor.key (), Q_NULLPTR)) {
                    sensor->updateState (itSensor.value ().toObject ());
                }
                else {
                    logWarning ("IMPORT", Q_NULLPTR, "Can't find SENSOR " % itSensor.key () % " to import state from snapshot !");
                }
            }
            const QJsonObject actuatorsEntry = tree.value (QStringLiteral ("actuators")).toObject ();
            for (KeyValueRange<QJsonObject>::It itActuator : keyValueRange (actuatorsEntry)) {
                if (BasicObject * actuator = m_objectsByPath.value (itActuator.key (), Q_NULLPTR)) {
                    actuator->updateState (itActuator.value ().toObject ());
                }
                else {
                    logWarning ("IMPORT", Q_NULLPTR, "Can't find ACTUATOR " % itActuator.key () % " to import state from snapshot !");
                }
            }
        }
        else {
            logWarning ("IMPORT", Q_NULLPTR, "Can't load snapshot, current machine UID is " % m_currentNetwork->get_uid () % " and snapshot was created with machine " % uid % " !");
        }
        file.close ();
    }
}

inline QString objectLinkToHtml (BasicObject * object) {
    return QS ("<a href='#%1'>%2</a>").arg (object->get_path ()).arg (object->get_path ());
}

inline QString objectTypeToHtml (const ObjectType::Type type) {
    switch (int (type)) {
        case ObjectType::ANALOG:  return QS ("Analog");
        case ObjectType::DIGITAL: return QS ("Digital");
        case ObjectType::HYBRID:  return QS ("Hybrid");
    }
    return QS ("Unknown");
}

inline QString objectDirectionToHtml (const ObjectDirection::Type direction) {
    switch (int (direction)) {
        case ObjectDirection::INPUT:  return QS ("Input");
        case ObjectDirection::OUTPUT: return QS ("Output");
    }
    return QS ("Unknown");
}

inline QString valueToHtml (const int value, const int decimals) {
    QString ret;
    const int dec = decimals;
    if (dec) {
        const int factor = int (qPow (10, dec));
        const int intPart = (value / factor);
        const int decPart = (value % factor);
        ret = QS ("%1.%2").arg (intPart).arg (decPart, dec, 10, QChar ('0'));
    }
    else {
        ret = QString::number (value);
    }
    return ret;
}

void Manager::printToHtml (const QString & filePath, const bool ios, const bool sensors, const bool actuators) {
    QString html;
    html += QS ("<html>");
    html += QS ("<head>");
    html += QS ("<title>%1</title>").arg (m_currentNetwork->get_title ());
    html += QS ("<style>");
    html += QS (":target { background: yellow; }");
    html += QS ("a { display: block; }");
    html += QS ("body { padding: 50px; font-family: 'Sans serif'; }");
    html += QS ("h1, h2 { text-align: center; }");
    html += QS ("table { border-collapse: collapse; margin: 20px; }");
    html += QS ("td, th { padding: 5px; text-align: center; }");
    html += QS ("table, td, th { border: 1px solid black; }");
    html += QS ("tr:nth-child(odd) { background-color: #EEEEEE; }");
    html += QS ("th { background-color: #BBBBBB; }");
    html += QS ("</style>");
    html += QS ("</head>");
    html += QS ("<body>");
    html += QS ("<center>");
    html += QS ("<h1>%1</h1>").arg (m_currentNetwork->get_title ());
    html += QS ("<hr>");
    if (ios) {
        html += QS ("<h2>I/O Nodes</h2>");
        for (Node * node : arrayRange (m_nodes)) {
            html += QS ("<table>");
            html += QS ("<tr><th colspan='5'>%1 - %2<br/>%3</th></tr>").arg (node->get_uid ()).arg (node->get_title ()).arg (node->get_description ());
            for (Board * board : arrayRange (node->getAsConstRef_boards ())) {
                html += QS ("<tr><td colspan='5'><i>%1 - %2</i></td></tr>").arg (board->get_uid ()).arg (board->get_title ());
                html += QS ("<tr><th>IO</th><th>Type</th><th>Direction</th><th>Configuration</th><th>Linked to</th></tr>");
                for (AbstractIO * io : arrayRange (board->getAsConstRef_ios ())) {
                    html += QS ("<tr>");
                    html += QS ("<td><a name='%1'>%2</a></td>").arg (io->get_path ()).arg (io->get_uid ());
                    html += QS ("<td>%1</td>").arg (objectTypeToHtml (io->get_type ()));
                    html += QS ("<td>%1</td>").arg (objectDirectionToHtml (io->get_direction ()));
                    if (AbstractAnalogIO * subio = io->as<AbstractAnalogIO> ()) {
                        html += QS ("<td>%1...%2 mV, %3 points</td>").arg (subio->get_minRaw ()).arg (subio->get_maxRaw ()).arg (subio->get_resolutionInPoints ());
                    }
                    else {
                        html += QS ("<td>N/A</td>");
                    }
                    html += QS ("<td>");
                    bool found = false;
                    switch (int (io->get_direction ())) {
                        case ObjectDirection::INPUT: {
                            const QVariantList tmp = getLinksAsTarget (io);
                            for (const QVariant & var : arrayRange (tmp)) {
                                if (AbstractLink * link = var.value<AbstractLink *> ()) {
                                    if (BasicObject * source = link->get_source ()) {
                                        html += objectLinkToHtml (source);
                                        found = true;
                                    }
                                }
                            }
                            break;
                        }
                        case ObjectDirection::OUTPUT: {
                            const QVariantList tmp = getLinksAsSource (io);
                            for (const QVariant & var : arrayRange (tmp)) {
                                if (AbstractLink * link = var.value<AbstractLink *> ()) {
                                    if (BasicObject * target = link->get_target ()) {
                                        html += objectLinkToHtml (target);
                                        found = true;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    if (!found) {
                        html += QS ("None");
                    }
                    html += QS ("</td>");
                    html += QS ("</tr>");
                }
            }
            html += QS ("</table>");
        }
    }
    if (sensors) {
        html += QS ("<h2>Sensors</h2>");
        html += QS ("<table>");
        html += QS ("<tr><th>Sensor</th><th>Description</th><th>Type</th><th>Configuration</th><th>Linked targets</th></tr>");
        for (AbstractSensor * sensor : arrayRange (m_sensors)) {
            html += QS ("<tr>");
            html += QS ("<td><a name='%1'>%2</a></td>").arg (sensor->get_path ()).arg (sensor->get_uid ());
            html += QS ("<td style='max-width:300px'>%1</td>").arg (sensor->get_description ());
            html += QS ("<td>%1</td>").arg (objectTypeToHtml (sensor->get_type ()));
            html += QS ("<td>");
            if (AnalogSensor * subsensor = sensor->as<AnalogSensor> ()) {
                const QString minPhyStr = valueToHtml (subsensor->get_minPhy (), subsensor->get_decimals ());
                const QString maxPhyStr = valueToHtml (subsensor->get_maxPhy (), subsensor->get_decimals ());
                html += QS ("%1 %2 = %3 mV").arg (minPhyStr).arg (subsensor->get_unit ()).arg (subsensor->get_minRaw ());
                html += QS ("<br/>");
                html += QS ("%1 %2 = %3 mV").arg (maxPhyStr).arg (subsensor->get_unit ()).arg (subsensor->get_maxRaw ());
            }
            else if (DigitalSensor * subsensor = sensor->as<DigitalSensor> ()) {
                html += QS ("0 = '%1'").arg (subsensor->get_falseLabel ());
                html += QS ("<br/>");
                html += QS ("1 = '%1'").arg (subsensor->get_trueLabel ());
            }
            else if (HybridSensor * subsensor = sensor->as<HybridSensor> ()) {
                const QString lowPhyStr  = valueToHtml (subsensor->get_lowThresholdPhy (),  subsensor->get_decimals ());
                const QString highPhyStr = valueToHtml (subsensor->get_highThresholdPhy (), subsensor->get_decimals ());
                html += QS ("&ge;%1 %2 = %3").arg (lowPhyStr).arg (subsensor->get_unit ()).arg (subsensor->get_invertState () ? "1" : "0");
                html += QS ("<br>");
                html += QS ("&le;%1 %2 = %3").arg (highPhyStr).arg (subsensor->get_unit ()).arg (subsensor->get_invertState () ? "0" : "1");
            }
            else {
                html += QS ("N/A");
            }
            html += QS ("</td>");
            html += QS ("<td>");
            bool found = false;
            const QVariantList tmp = getLinksAsSource (sensor);
            for (const QVariant & var : arrayRange (tmp)) {
                if (AbstractLink * link = var.value<AbstractLink *> ()) {
                    if (BasicObject * target = link->get_target ()) {
                        html += objectLinkToHtml (target);
                        found = true;
                    }
                }
            }
            if (!found) {
                html += QS ("None");
            }
            html += QS ("</td>");
            html += QS ("</tr>");
        }
        html += QS ("</table>");
    }
    if (actuators) {
        html += QS ("<h2>Actuators</h2>");
        html += QS ("<table>");
        html += QS ("<tr><th>Actuator</th><th>Description</th><th>Type</th><th>Configuration</th><th>Linked sources</th></tr>");
        for (AbstractActuator * actuator : arrayRange (m_actuators)) {
            html += QS ("<tr>");
            html += QS ("<td><a name='%1'>%2</a></td>").arg (actuator->get_path ()).arg (actuator->get_uid ());
            html += QS ("<td style='max-width:300px'>%1</td>").arg (actuator->get_description ());
            html += QS ("<td>%1</td>").arg (objectTypeToHtml (actuator->get_type ()));
            html += QS ("<td>");
            if (AnalogActuator * subactuator = actuator->as<AnalogActuator> ()) {
                const QString minPhyStr = valueToHtml (subactuator->get_minSpeed (), subactuator->get_decimals ());
                const QString maxPhyStr = valueToHtml (subactuator->get_maxSpeed (), subactuator->get_decimals ());
                html += QS ("%1 mV = %2 %3").arg (subactuator->get_minRaw ()).arg (minPhyStr).arg (subactuator->get_unit ());
                html += QS ("<br/>");
                html += QS ("%1 mV = %2 %3").arg (subactuator->get_maxRaw ()).arg (maxPhyStr).arg (subactuator->get_unit ());
            }
            else if (DigitalActuator * subactuator = actuator->as<DigitalActuator> ()) {
                html += QS ("0 = '%1'").arg (subactuator->get_falseLabel ());
                html += QS ("<br/>");
                html += QS ("1 = '%1'").arg (subactuator->get_trueLabel ());
            }
            else if (HybridActuator * subactuator = actuator->as<HybridActuator> ()) {
                const QString zeroPhyStr  = valueToHtml (0, subactuator->get_decimals ());
                const QString ratedPhyStr = valueToHtml (subactuator->get_ratedSpeed (), subactuator->get_decimals ());
                html += QS ("0 = %1 %2").arg (zeroPhyStr).arg (subactuator->get_unit ());
                html += QS ("<br>");
                html += QS ("1 = %1 %2").arg (ratedPhyStr).arg (subactuator->get_unit ());
            }
            else {
                html += QS ("N/A");
            }
            html += QS ("</td>");
            html += QS ("<td>");
            bool found = false;
            const QVariantList tmp = getLinksAsTarget (actuator);
            for (const QVariant & var : arrayRange (tmp)) {
                if (AbstractLink * link = var.value<AbstractLink *> ()) {
                    if (BasicObject * source = link->get_source ()) {
                        html += objectLinkToHtml (source);
                        found = true;
                    }
                }
            }
            if (!found) {
                html += QS ("None");
            }
            html += QS ("</td>");
            html += QS ("</tr>");
        }
        html += QS ("</table>");
    }
    html += QS ("</center>");
    html += QS ("</body>");
    html += QS ("</html>");
    QFile file ((filePath.endsWith (".html") ||
                 filePath.endsWith (".htm") ||
                 filePath.endsWith (".mhtml") ||
                 filePath.endsWith (".xhtml"))
                ? filePath
                : (filePath % ".html"));
    if (file.open (QFile::WriteOnly)) {
        file.write (html.toUtf8 ());
        file.flush ();
        file.close ();
    }
    else {
        logError ("PRINT", Q_NULLPTR, "Can't print to HTML file ! " % file.errorString ());
    }
}

void Manager::log (const QString & type, const QString & module, QObject * object, const QString & msg) const {
    const QDateTime now = QDateTime::currentDateTimeUtc ();
    const QVariantMap entry {
        { "when",   now                  },
        { "type",   type                 },
        { "msg",    msg                  },
        { "module", module               },
        { "object", qmlLocation (object) },
    };
    m_logsModel->append (entry);
}

void Manager::logInfo (const QString & module, QObject * object, const QString & msg) {
    INFO << qPrintable (module) << qPrintable (qmlLocation (object)) << ":" << qPrintable (msg);
    log ("INFO", module, object, msg);
}

void Manager::logWarning (const QString & module, QObject * object, const QString & msg) {
    WARN << qPrintable (module) << qPrintable (qmlLocation (object)) << ":" << qPrintable (msg);
    log ("WARNING", module, object, msg);
}

void Manager::logError (const QString & module, QObject * object, const QString & msg) {
    ERRO << qPrintable (module) << qPrintable (qmlLocation (object)) << ":" << qPrintable (msg);
    log ("ERROR", module, object, msg);
}

void Manager::onTick (void) {
    static QElapsedTimer bench;
    if (m_running) {
        bench.restart ();
        const int time = int (m_chrono.elapsed ());
        m_chrono.restart ();
        for (LinkPhysicalValueToAnalogSensor * link : arrayRange (m_linksPhysicalValueToAnalogSensor)) {
            link->sync (); // PHYSICAL VALUE -> ANALOG SENSOR
        }
        for (LinkPhysicalValueToHybridSensor * link : arrayRange (m_linksPhysicalValueToHybridSensor)) {
            link->sync (); // PHYSICAL VALUE -> HYBRID SENSOR
        }
        for (LinkAnalogSensorToAnalogInput * link : arrayRange (m_linksAnalogSensorToAnalogInput)) {
            link->sync (); // ANALOG SENSOR -> AIN
        }
        for (LinkDigitalSensorToDigitalInput * link : arrayRange (m_linksDigitalSensorToDigitalInput)) {
            link->sync (); // DIGITAL SENSOR -> DIN
        }
        for (LinkHybridSensorToDigitalInput * link : arrayRange (m_linksHybridSensorToDigitalInput)) {
            link->sync (); // HYBRID SENSOR -> DIN
        }
        for (LinkDigitalOutputToDigitalActuator * link : arrayRange (m_linksDigitalOutputToDigitalActuator)) {
            link->sync (); // DOUT -> DIGITAL ACTUATOR
        }
        for (LinkAnalogOutputToAnalogActuator * link : arrayRange (m_linksAnalogOutputToAnalogActuator)) {
            link->sync (); // AOUT -> ANALOG ACTUATOR
        }
        for (LinkDigitalOutputToHybridActuator * link : arrayRange (m_linksDigitalOutputToHybridActuator)) {
            link->sync (); // DOUT -> HYBRID ACTUATOR
        }
        for (LinkDigitalOutputToHybridActuator * link : arrayRange (m_linksDigitalOutputToHybridActuator)) {
            link->sync (); // AOUT -> HYBRID ACTUATOR
        }
        for (PhysicalValue * value : arrayRange (m_physicalValues)) {
            value->update_assignedSpeed (0); // reset assignement before running actuators
        }
        for (LinkAnalogActuatorToPhysicalValue * link : arrayRange (m_linksAnalogActuatorToPhysicalValue)) {
            link->sync (); // ANALOG ACTUATOR -> PHYSICAL VALUE
        }
        for (LinkHybridActuatorToPhysicalValue * link : arrayRange (m_linksHybridActuatorToPhysicalValue)) {
            link->sync (); // HYBRID ACTUATOR -> PHYSICAL VALUE
        }
        for (LinkAnalogOutputToAnalogInput * link : arrayRange (m_linksAnalogOutputToAnalogInput)) {
            link->sync (); // AOUT -> AIN
        }
        for (LinkDigitalOutputToDigitalInput * link : arrayRange (m_linksDigitalOutputToDigitalInput)) {
            link->sync (); // DOUT -> DIN
        }
        for (PhysicalValue * value : arrayRange (m_physicalValues)) {
            value->move (time); // move value after running actuators
        }
        if (m_physicalWorld) {
            m_physicalWorld->processPhysics ();
        }
        if (m_currentNetwork && m_currentNetwork->get_benchmarkSimulatorTick ()) {
            INFO << "TICK" << qPrintable (QLatin1String (QByteArray::number (qreal (bench.nsecsElapsed ()) / 1000000.0, 'f', 6))) << "ms";
        }
    }
}

void Manager::doInitObjects (void) {
    if (m_allowInit) {
        while (!m_objectsToInit.isEmpty ()) {
            if (QObject * object = m_objectsToInit.pop ()) {
                const QString className = object->metaObject ()->className ();
                if (BasicObject * basicObject = qobject_cast<BasicObject *> (object)) {
                    m_basicObjects.append (basicObject);
                    basicObject->refreshPath ();
                    bool usePath = false;
                    /// type specific specializations
                    if (Node * node = qobject_cast<Node *> (object)) {
                        dump ("NODE", node);
                        connect (node, &Node::modeChanged, this, &Manager::doBuildChains, Qt::UniqueConnection);
                        m_nodes.append (node);
                        node->init ();
                        if (m_running) {
                            node->start ();
                        }
                        else {
                            node->stop ();
                        }
                        usePath = true;
                    }
                    else if (Board * board = basicObject->as<Board> ()) {
                        dump ("BOARD", board);
                        m_boards.append (board);
                        usePath = true;
                    }
                    else if (CanOpen * canOpen = basicObject->as<CanOpen> ()) {
                        dump ("CANOPEN", canOpen);
                        m_canOpens.append (canOpen);
                        canOpen->init ();
                        if (m_running) {
                            canOpen->start ();
                        }
                        else {
                            canOpen->stop ();
                        }
                        usePath = true;
                    }
                    else if (CanBus * canBus = basicObject->as<CanBus> ()) {
                        dump ("CANBUS", canBus);
                        m_canBuses.append (canBus);
                        usePath = true;
                    }
                    else if (SerialBus * serialBus = basicObject->as<SerialBus> ()) {
                        dump ("SERIALBUS", serialBus);
                        m_serialBuses.append (serialBus);
                        usePath = true;
                    }
                    else if (AbstractIO * io = basicObject->as<AbstractIO> ()) {
                        dump ("IO", io);
                        m_ios.append (io);
                        if (AnalogInput * ain = io->as<AnalogInput> ()) {
                            m_ains.append (ain);
                        }
                        else if (AnalogOutput * aout = io->as<AnalogOutput> ()) {
                            m_aouts.append (aout);
                        }
                        else if (DigitalInput * din = io->as<DigitalInput> ()) {
                            m_dins.append (din);
                        }
                        else if (DigitalOutput * dout = io->as<DigitalOutput> ()) {
                            m_douts.append (dout);
                        }
                        else { }
                        usePath = true;
                    }
                    else if (AbstractSensor * sensor = basicObject->as<AbstractSensor> ()) {
                        dump ("SENSOR", sensor);
                        m_sensors.append (sensor);
                        if (AnalogSensor * analogSensor = sensor->as<AnalogSensor> ()) {
                            m_analogSensors.append (analogSensor);
                        }
                        else if (DigitalSensor * digitalSensor = sensor->as<DigitalSensor> ()) {
                            m_digitalSensors.append (digitalSensor);
                        }
                        else if (HybridSensor * hybridSensor = sensor->as<HybridSensor> ()) {
                            m_hybridSensors.append (hybridSensor);
                        }
                        else { }
                        usePath = true;
                    }
                    else if (AbstractActuator * actuator = basicObject->as<AbstractActuator> ()) {
                        dump ("ACTUATOR", actuator);
                        m_actuators.append (actuator);
                        if (AnalogActuator * analogActuator = actuator->as<AnalogActuator> ()) {
                            m_analogActuators.append (analogActuator);
                        }
                        else if (DigitalActuator * digitalActuator = actuator->as<DigitalActuator> ()) {
                            m_digitalActuators.append (digitalActuator);
                        }
                        else if (HybridActuator * hybridActuator = actuator->as<HybridActuator> ()) {
                            m_hybridActuators.append (hybridActuator);
                        }
                        else { }
                        usePath = true;
                    }
                    else if (AbstractRoutine * routine = basicObject->as<AbstractRoutine> ()) {
                        dump ("ROUTINE", routine);
                        m_routines.append (routine);
                        if (RoutineOnTimer * routineOnTimer = routine->as<RoutineOnTimer> ()) {
                            m_routinesOnTimer.append (routineOnTimer);
                            if (m_running) {
                                routineOnTimer->start ();
                            }
                            else {
                                routineOnTimer->stop ();
                            }
                        }
                        else if (RoutineOnEvent * routineOnEvent = routine->as<RoutineOnEvent> ()) {
                            m_routinesOnEvent.append (routineOnEvent);
                        }
                        else if (RoutineOnCanFrame * routineOnCanFrame = routine->as<RoutineOnCanFrame> ()) {
                            m_routinesOnCanFrame.append (routineOnCanFrame);
                        }
                        else if (RoutineOnSerialFrame * routineOnSerialFrame = routine->as<RoutineOnSerialFrame> ()) {
                            m_routinesOnSerialFrame.append (routineOnSerialFrame);
                        }
                        else if (RoutineOnCanOpenStateChange * routineOnCanOpenStateChange = routine->as<RoutineOnCanOpenStateChange> ()) {
                            m_routinesOnCanOpenStateChange.append (routineOnCanOpenStateChange);
                        }
                        else if (RoutineOnCanOpenObdValChange * routineOnCanOpenObdValChange = routine->as<RoutineOnCanOpenObdValChange> ()) {
                            m_routinesOnCanOpenObdValChange.append (routineOnCanOpenObdValChange);
                        }
                        else if (RoutineOnCanOpenSdoReadReply * routineOnCanOpenSdoReadReply = routine->as<RoutineOnCanOpenSdoReadReply> ()) {
                            m_routinesOnCanOpenSdoReadReply.append (routineOnCanOpenSdoReadReply);
                        }
                        else if (RoutineOnCanOpenSdoWriteRequest * routineOnCanOpenSdoWriteRequest = routine->as<RoutineOnCanOpenSdoWriteRequest> ()) {
                            m_routinesOnCanOpenSdoWriteRequest.append (routineOnCanOpenSdoWriteRequest);
                        }
                        else if (RoutineOnCanOpenBootUp * routineOnCanOpenBootUp = routine->as<RoutineOnCanOpenBootUp> ()) {
                            m_routinesOnCanOpenBootUp.append (routineOnCanOpenBootUp);
                        }
                        else if (RoutineOnCanOpenHeartbeatConsumer * routineOnCanOpenHeartbeatConsumer = routine->as<RoutineOnCanOpenHeartbeatConsumer> ()) {
                            m_routinesOnCanOpenBootUp.append (routineOnCanOpenHeartbeatConsumer);
                        }
                        else { }
                        usePath = true;
                    }
                    else if (AbstractLink * link = basicObject->as<AbstractLink> ()) {
                        dump ("LINK", link);
                        m_links.append (link);
                        link->init ();
                        if (LinkAnalogActuatorToPhysicalValue * sublink = link->as<LinkAnalogActuatorToPhysicalValue> ()) {
                            m_linksAnalogActuatorToPhysicalValue.append (sublink);
                        }
                        else if (LinkAnalogOutputToAnalogActuator * sublink = link->as<LinkAnalogOutputToAnalogActuator> ()) {
                            m_linksAnalogOutputToAnalogActuator.append (sublink);
                        }
                        else if (LinkAnalogSensorToAnalogInput * sublink = link->as<LinkAnalogSensorToAnalogInput> ()) {
                            m_linksAnalogSensorToAnalogInput.append (sublink);
                        }
                        else if (LinkDigitalOutputToDigitalActuator * sublink = link->as<LinkDigitalOutputToDigitalActuator> ()) {
                            m_linksDigitalOutputToDigitalActuator.append (sublink);
                        }
                        else if (LinkDigitalSensorToDigitalInput * sublink = link->as<LinkDigitalSensorToDigitalInput> ()) {
                            m_linksDigitalSensorToDigitalInput.append (sublink);
                        }
                        else if (LinkPhysicalValueToAnalogSensor * sublink = link->as<LinkPhysicalValueToAnalogSensor> ()) {
                            m_linksPhysicalValueToAnalogSensor.append (sublink);
                        }
                        else if (LinkAnalogOutputToAnalogInput * sublink = link->as<LinkAnalogOutputToAnalogInput> ()) {
                            m_linksAnalogOutputToAnalogInput.append (sublink);
                        }
                        else if (LinkDigitalOutputToDigitalInput * sublink = link->as<LinkDigitalOutputToDigitalInput> ()) {
                            m_linksDigitalOutputToDigitalInput.append (sublink);
                        }
                        else if (LinkPhysicalValueToHybridSensor * sublink = link->as<LinkPhysicalValueToHybridSensor> ()) {
                            m_linksPhysicalValueToHybridSensor.append (sublink);
                        }
                        else if (LinkHybridSensorToDigitalInput * sublink = link->as<LinkHybridSensorToDigitalInput> ()) {
                            m_linksHybridSensorToDigitalInput.append (sublink);
                        }
                        else if (LinkDigitalOutputToHybridActuator * sublink = link->as<LinkDigitalOutputToHybridActuator> ()) {
                            m_linksDigitalOutputToHybridActuator.append (sublink);
                        }
                        else if (LinkHybridActuatorToPhysicalValue * sublink = link->as<LinkHybridActuatorToPhysicalValue> ()) {
                            m_linksHybridActuatorToPhysicalValue.append (sublink);
                        }
                        else { }
                    }
                    else if (AbstractTransformer * transformer = basicObject->as<AbstractTransformer> ()) {
                        dump ("TRANSFORMER", transformer);
                        m_transformers.append (transformer);
                        if (AffineTransformer * affine = transformer->as<AffineTransformer> ()) {
                            m_affineTransformers.append (affine);
                        }
                        else if (CustomTransformer * custom = transformer->as<CustomTransformer> ()) {
                            m_customTransformers.append (custom);
                        }
                        else { }
                    }
                    else if (PhysicalValue * value = basicObject->as<PhysicalValue> ()) {
                        dump ("VALUE", value);
                        m_physicalValues.append (value);
                        usePath = true;
                    }
                    else if (PhysicalSize * size = basicObject->as<PhysicalSize> ()) {
                        dump ("SIZE", size);
                        usePath = true;
                    }
                    else if (PhysicalAngle * angle = basicObject->as<PhysicalAngle> ()) {
                        dump ("ANGLE", angle);
                        usePath = true;
                    }
                    else if (PhysicalPoint * point = basicObject->as<PhysicalPoint> ()) {
                        dump ("POINT", point);
                        usePath = true;
                    }
                    else if (PhysicalBlock * block = basicObject->as<PhysicalBlock> ()) {
                        dump ("BLOCK", block);
                        m_physicalBlocks.append (block);
                        block->refreshAncestors ();
                        block->refreshMatrixRotationX ();
                        block->refreshMatrixRotationY ();
                        block->refreshMatrixRotationZ ();
                        block->refreshMatrixTranslation ();
                        block->refreshVertexList ();
                        usePath = true;
                    }
                    else if (PhysicalMarker * marker = basicObject->as<PhysicalMarker> ()) {
                        dump ("MARKER", marker);
                        m_physicalMarkers.append (marker);
                        marker->refreshAncestors ();
                        usePath = true;
                    }
                    else if (ObjectsGroup * group = basicObject->as<ObjectsGroup> ()) {
                        dump ("GROUP", group);
                        m_objectsGroups.append (group);
                        usePath = true;
                    }
                    else if (PhysicalWorld * world = basicObject->as<PhysicalWorld> ()) {
                        dump ("WORLD", world);
                        m_physicalWorld = world;
                    }
                    else { logWarning ("INIT", object, "Unhandled type of basic object ! " % className); }
                    /// handle map of objects by path if required
                    if (usePath) {
                        const QString path = basicObject->get_path ();
                        if (!path.isEmpty ()) {
                            static QRegularExpression regexp ("^[A-Za-z0-9_\\/]+$");
                            if (regexp.match (path).hasMatch ()) {
                                if (!m_objectsByPath.contains (path) || m_objectsByPath.value (path) == object) {
                                    m_objectsByPath.insert (path, basicObject);
                                }
                                else { logWarning ("INIT", object, "There is already another object with UID " % path); }
                            }
                            else { logWarning ("INIT", object, "This path isn't valid, contains unauthorized chars " % path); }
                        }
                        else { logWarning ("INIT", object, "This object doesn't have a path, some features won't be available ! " % className); }
                    }
                }
                else if (Dashboard * dashboard = qobject_cast<Dashboard *> (object)) {
                    dump ("DASHBOARD", dashboard);
                    m_dashboards.append (dashboard);
                    if (dashboard->get_uid ().isEmpty ()) {
                        logWarning ("INIT", dashboard, "Dashboard doesn't have an UID !");
                    }
                }
                else { logWarning ("INIT", object, "Unhandled type of object ! " % className); }
            }
        }
        /// rebuild sensor chains
        doBuildChains ();
        /// update 3D if needed
        if (m_physicalWorld) {
            for (PhysicalBlock * block : arrayRange (m_physicalBlocks)) {
                connect (block, &PhysicalBlock::needsRedraw, m_physicalWorld.data (), &PhysicalWorld::physicsDirty, Qt::UniqueConnection);
            }
            m_physicalWorld->physicsDirty ();
            m_physicalWorld->processPhysics ();
        }
        /// inform QML that objects have been added
        emit dictChanged (m_dict);
    }
}

void Manager::doBuildChains (void) {
    /// prepare sets for mode/conflict detection
    QList<AnalogSensorChain *> analogSensorChains;
    QList<DigitalSensorChain *> digitalSensorChains;
    QList<HybridSensorChain *> hybridSensorChains;
    QList<AnalogActuatorChain *> analogActuatorChains;
    QList<DigitalActuatorChain *> digitalActuatorChains;
    QList<HybridActuatorChain *> hybridActuatorChains;
    QSet<Node *> activeNodes, passiveNodes, inactiveNodes;
    QSet<AnalogSensor *> invertedAnalogSensors;
    QSet<DigitalSensor *> invertedDigitalSensors;
    QSet<HybridSensor *> disabledHybridSensors;
    QSet<PhysicalValue *> forcedPhysicalValues;
    QSet<LinkAnalogSensorToAnalogInput *> invertedLinkSensorToAins;
    QSet<LinkDigitalSensorToDigitalInput *> invertedLinkSensorToDins;
    QSet<LinkPhysicalValueToAnalogSensor *> invertedLinkValueToSensors;
    QHash<AbstractIO *, Node *> nodeForIO;
    /// build chains
    for (AnalogSensor * sensor : arrayRange (m_analogSensors)) {
        LinkPhysicalValueToAnalogSensor * linkValueToSensor = sensor->get_sourceLink ();
        PhysicalValue * value = (linkValueToSensor ? linkValueToSensor->get_value () : Q_NULLPTR);
        bool fullChain = false;
        for (LinkAnalogSensorToAnalogInput * linkSensorToAin : arrayRange (sensor->get_targetLinks ().items ())) {
            AnalogInput * ain = (linkSensorToAin ? linkSensorToAin->get_input () : Q_NULLPTR);
            AnalogSensorChain * chain = new AnalogSensorChain;
            chain->sensor = sensor;
            chain->linkValueToSensor = linkValueToSensor;
            chain->value = value;
            chain->linkSensorToAin = linkSensorToAin;
            chain->ain = ain;
            analogSensorChains.append (chain);
            if (ain) {
                fullChain = true;
            }
        }
        if (!fullChain) {
            AnalogSensorChain * chain = new AnalogSensorChain;
            chain->sensor = sensor;
            chain->linkValueToSensor = linkValueToSensor;
            chain->value = value;
            analogSensorChains.append (chain);
        }
    }
    for (DigitalSensor * sensor : arrayRange (m_digitalSensors)) {
        for (LinkDigitalSensorToDigitalInput * linkSensorToDin : arrayRange (sensor->get_targetLinks ().items ())) {
            DigitalInput * din = (linkSensorToDin ? linkSensorToDin->get_input () : Q_NULLPTR);
            DigitalSensorChain * chain = new DigitalSensorChain;
            chain->sensor = sensor;
            chain->linkSensorToDin = linkSensorToDin;
            chain->din = din;
            digitalSensorChains.append (chain);
        }
    }
    for (HybridSensor * sensor : arrayRange (m_hybridSensors)) {
        LinkPhysicalValueToHybridSensor * linkValueToSensor = sensor->get_sourceLink ();
        PhysicalValue * value = (linkValueToSensor ? linkValueToSensor->get_value () : Q_NULLPTR);
        bool fullChain = false;
        for (LinkHybridSensorToDigitalInput * linkSensorToDin : arrayRange (sensor->get_targetLinks ().items ())) {
            DigitalInput * din = (linkSensorToDin ? linkSensorToDin->get_input () : Q_NULLPTR);
            HybridSensorChain * chain = new HybridSensorChain;
            chain->sensor = sensor;
            chain->linkValueToSensor = linkValueToSensor;
            chain->value = value;
            chain->linkSensorToDin = linkSensorToDin;
            chain->din = din;
            hybridSensorChains.append (chain);
            if (din) {
                fullChain = true;
            }
        }
        if (!fullChain) {
            HybridSensorChain * chain = new HybridSensorChain;
            chain->sensor = sensor;
            chain->linkValueToSensor = linkValueToSensor;
            chain->value = value;
            hybridSensorChains.append (chain);
        }
    }
    for (AnalogActuator * actuator : arrayRange (m_analogActuators)) {
        LinkAnalogOutputToAnalogActuator * linkAoutToActuator = actuator->get_sourceLink ();
        AnalogOutput * aout = (linkAoutToActuator ? linkAoutToActuator->get_output () : Q_NULLPTR);
        LinkAnalogActuatorToPhysicalValue * linkActuatorToValue = actuator->get_targetLink ();
        PhysicalValue * value = (linkActuatorToValue ? linkActuatorToValue->get_value () : Q_NULLPTR);
        AnalogActuatorChain * chain = new AnalogActuatorChain;
        chain->aout = aout;
        chain->linkAoutToActuator = linkAoutToActuator;
        chain->actuator = actuator;
        chain->linkActuatorToValue = linkActuatorToValue;
        chain->value = value;
        analogActuatorChains.append (chain);
    }
    for (DigitalActuator * actuator : arrayRange (m_digitalActuators)) {
        LinkDigitalOutputToDigitalActuator * linkDoutToActuator = actuator->get_sourceLink ();
        DigitalOutput * dout = (linkDoutToActuator ? linkDoutToActuator->get_output () : Q_NULLPTR);
        DigitalActuatorChain * chain = new DigitalActuatorChain;
        chain->dout = dout;
        chain->linkDoutToActuator = linkDoutToActuator;
        chain->actuator = actuator;
        digitalActuatorChains.append (chain);
    }
    for (HybridActuator * actuator : arrayRange (m_hybridActuators)) {
        LinkDigitalOutputToHybridActuator * linkDoutToActuator = actuator->get_sourceLink ();
        DigitalOutput * dout = (linkDoutToActuator ? linkDoutToActuator->get_output () : Q_NULLPTR);
        LinkHybridActuatorToPhysicalValue * linkActuatorToValue = actuator->get_targetLink ();
        PhysicalValue * value = (linkActuatorToValue ? linkActuatorToValue->get_value () : Q_NULLPTR);
        HybridActuatorChain * chain = new HybridActuatorChain;
        chain->dout = dout;
        chain->linkDoutToActuator = linkDoutToActuator;
        chain->actuator = actuator;
        chain->linkActuatorToValue = linkActuatorToValue;
        chain->value = value;
        hybridActuatorChains.append (chain);
    }
    /// reset all links and sensors to normal mode (disabled and unreversed)
    for (AbstractLink * link : arrayRange (m_links)) {
        link->update_enabled  (false);
        link->update_reversed (false);
    }
    for (AnalogSensor * sensor : arrayRange (m_analogSensors)) {
        sensor->update_inverted (false);
    }
    /// enable links that come from active/passive node I/Os, leave others disabled. Don't disable a link that have been enabled
    for (Node * node : arrayRange (m_nodes)) {
        switch (int (node->get_mode ())) {
            case Modes::ACTIVE: {
                activeNodes.insert (node);
                break;
            }
            case Modes::PASSIVE: {
                passiveNodes.insert (node);
                break;
            }
            case Modes::INACTIVE: {
                inactiveNodes.insert (node);
                break;
            }
            default: break;
        }
        for (Board * board : arrayRange (node->getAsConstRef_boards ())) {
            for (AbstractIO * io : arrayRange (board->getAsConstRef_ios ())) {
                nodeForIO.insert (io, node);
            }
        }
    }
    for (AnalogSensorChain * chain : arrayRange (analogSensorChains)) {
        bool enabled = true;
        if (chain->ain) {
            if (Node * node = nodeForIO.value (chain->ain)) {
                enabled = !inactiveNodes.contains (node);
            }
        }
        if (chain->linkSensorToAin && enabled) {
            chain->linkSensorToAin->update_enabled (true);
        }
        if (chain->linkValueToSensor && enabled) {
            chain->linkValueToSensor->update_enabled (true);
        }
    }
    for (DigitalSensorChain * chain : arrayRange (digitalSensorChains)) {
        bool enabled = true;
        if (chain->din) {
            if (Node * node = nodeForIO.value (chain->din)) {
                enabled = !inactiveNodes.contains (node);
            }
        }
        if (chain->linkSensorToDin && enabled) {
            chain->linkSensorToDin->update_enabled (true);
        }
    }
    for (AnalogActuatorChain * chain : arrayRange (analogActuatorChains)) {
        bool enabled = true;
        if (chain->aout) {
            if (Node * node = nodeForIO.value (chain->aout)) {
                enabled = !inactiveNodes.contains (node);
            }
        }
        if (chain->linkActuatorToValue && enabled) {
            chain->linkActuatorToValue->update_enabled (true);
        }
        if (chain->linkAoutToActuator && enabled) {
            chain->linkAoutToActuator->update_enabled (true);
        }
    }
    for (DigitalActuatorChain * chain : arrayRange (digitalActuatorChains)) {
        bool enabled = true;
        if (chain->dout) {
            if (Node * node = nodeForIO.value (chain->dout)) {
                enabled = !inactiveNodes.contains (node);
            }
        }
        if (chain->linkDoutToActuator && enabled) {
            chain->linkDoutToActuator->update_enabled (true);
        }
    }
    /// reverse links and sensors for passive node inputs, store forced physical values for later
    for (AnalogSensorChain * chain : arrayRange (analogSensorChains)) {
        bool inverted = false;
        if (chain->ain) {
            if (Node * node = nodeForIO.value (chain->ain)) {
                inverted = passiveNodes.contains (node);
            }
        }
        if (chain->sensor && inverted) {
            chain->sensor->update_inverted (true);
            invertedAnalogSensors.insert (chain->sensor);
        }
        if (chain->value && inverted) {
            forcedPhysicalValues.insert (chain->value);
        }
        if (chain->linkSensorToAin && inverted) {
            chain->linkSensorToAin->update_reversed (true);
            invertedLinkSensorToAins.insert (chain->linkSensorToAin);
        }
        if (chain->linkValueToSensor && inverted) {
            chain->linkValueToSensor->update_reversed (true);
            invertedLinkValueToSensors.insert (chain->linkValueToSensor);
        }
    }
    for (DigitalSensorChain * chain : arrayRange (digitalSensorChains)) {
        bool inverted = false;
        if (chain->din) {
            if (Node * node = nodeForIO.value (chain->din)) {
                inverted = passiveNodes.contains (node);
            }
        }
        if (chain->sensor && inverted) {
            invertedDigitalSensors.insert (chain->sensor);
        }
        if (chain->linkSensorToDin && inverted) {
            chain->linkSensorToDin->update_reversed (true);
            invertedLinkSensorToDins.insert (chain->linkSensorToDin);
        }
    }
    /// disable links between passive node inputs and hybrid sensors because they are not reversable
    for (HybridSensorChain * chain : arrayRange (hybridSensorChains)) {
        bool enabled = true;
        if (chain->din) {
            if (Node * node = nodeForIO.value (chain->din)) {
                enabled = (!inactiveNodes.contains (node) &&
                           !passiveNodes.contains  (node));
            }
        }
        if (chain->linkSensorToDin) {
            chain->linkSensorToDin->update_enabled (enabled);
        }
        if (chain->linkValueToSensor) {
            chain->linkValueToSensor->update_enabled (enabled);
        }
        if (chain->sensor) {
            chain->sensor->update_disabled (!enabled);
            disabledHybridSensors.insert (chain->sensor);
        }
    }
    for (HybridActuatorChain * chain : arrayRange (hybridActuatorChains)) {
        bool enabled = true;
        if (chain->dout) {
            if (Node * node = nodeForIO.value (chain->dout)) {
                enabled = (!inactiveNodes.contains (node) &&
                           !passiveNodes.contains  (node));
            }
        }
        if (chain->linkDoutToActuator) {
            chain->linkDoutToActuator->update_enabled (enabled);
        }
        if (chain->linkActuatorToValue) {
            chain->linkActuatorToValue->update_enabled (enabled);
        }
    }
    /// disable links between forced physical values and actuators to avoid conflicts between relative speed and forced absolute value
    for (AnalogActuatorChain * chain : arrayRange (analogActuatorChains)) {
        bool conflict = false;
        if (chain->value) {
            conflict = forcedPhysicalValues.contains (chain->value);
        }
        if (chain->linkActuatorToValue && conflict) {
            chain->linkActuatorToValue->update_enabled (false);
        }
    }
    for (HybridActuatorChain * chain : arrayRange (hybridActuatorChains)) {
        bool conflict = false;
        if (chain->value) {
            conflict = forcedPhysicalValues.contains (chain->value);
        }
        if (chain->linkActuatorToValue && conflict) {
            chain->linkActuatorToValue->update_enabled (false);
        }
    }
    /// handle special direct mappings
    for (LinkAnalogOutputToAnalogInput * link : arrayRange (m_linksAnalogOutputToAnalogInput)) {
        bool enabled  = true;
        bool reversed = false;
        if (AnalogOutput * output = qobject_cast<AnalogOutput *> (link->get_source ())) {
            if (Node * node = nodeForIO.value (output)) {
                if (inactiveNodes.contains (node)) {
                    enabled = false;
                }
                if (passiveNodes.contains (node)) {
                    reversed = true;
                }
            }
        }
        if (AnalogInput * input = qobject_cast<AnalogInput *> (link->get_target ())) {
            if (Node * node = nodeForIO.value (input)) {
                if (inactiveNodes.contains (node)) {
                    enabled = false;
                }
                if (passiveNodes.contains (node)) {
                    reversed = true;
                }
            }
        }
        link->update_enabled  (enabled);
        link->update_reversed (reversed);
    }
    for (LinkDigitalOutputToDigitalInput * link : arrayRange (m_linksDigitalOutputToDigitalInput)) {
        bool enabled  = true;
        bool reversed = false;
        if (DigitalOutput * output = qobject_cast<DigitalOutput *> (link->get_source ())) {
            if (Node * node = nodeForIO.value (output)) {
                if (inactiveNodes.contains (node)) {
                    enabled = false;
                }
                if (passiveNodes.contains (node)) {
                    reversed = true;
                }
            }
        }
        if (DigitalInput * input = qobject_cast<DigitalInput *> (link->get_target ())) {
            if (Node * node = nodeForIO.value (input)) {
                if (inactiveNodes.contains (node)) {
                    enabled = false;
                }
                if (passiveNodes.contains (node)) {
                    reversed = true;
                }
            }
        }
        link->update_enabled  (enabled);
        link->update_reversed (reversed);
    }
    /// clear old chains
    qDeleteAll (analogSensorChains);
    qDeleteAll (digitalSensorChains);
    qDeleteAll (hybridSensorChains);
    qDeleteAll (analogActuatorChains);
    qDeleteAll (digitalActuatorChains);
    qDeleteAll (hybridActuatorChains);
}

void Manager::registerObject (QObject * object) {
    if (object != Q_NULLPTR) {
        m_singleshot->stop ();
        if (!m_objectsInstances.contains (object)) {
            m_objectsInstances.insert (object);
        }
        else {
            WARN << "Weird, object" << object << "called registerObject twice !";
        }
    }
}

void Manager::intializeObject (QObject * object) {
    if (object != Q_NULLPTR) {
        m_singleshot->stop ();
        if (!m_objectsToInit.contains (object)) {
            m_objectsToInit.push (object);
            if (m_allowInit) {
                m_singleshot->start (0);
            }
        }
        else {
            WARN << "Weird, object" << object << "called intializeObject twice !";
        }
    }
}

void Manager::unregisterObject (QObject * object) {
    if (object != Q_NULLPTR) {
        m_singleshot->stop ();
        if (m_objectsToInit.contains (object)) {
            m_objectsToInit.removeAll (object);
        }
        if (m_objectsInstances.contains (object)) {
            m_objectsInstances.remove (object);
        }
        if (BasicObject * basicObject = qobject_cast<BasicObject *> (object)) {
            m_objectsByPath.remove (basicObject->get_path ());
            m_basicObjects.remove (basicObject);
        }
        else if (QQuickItem * quickItem = qobject_cast<QQuickItem *> (object)) {
            QStringList tmp;
            for (QMap<QString, QQuickItem *>::const_iterator it : keyValueRange (m_delegatesByPath)) {
                if (it.value () == quickItem) {
                    tmp.append (it.key ());
                }
            }
            for (const QString & path : arrayRange (tmp)) {
                m_delegatesByPath.remove (path);
            }
        }
        else { }
    }
}

BasicObject * Manager::getObjectByPath (const QString & path) const {
    return m_objectsByPath.value (path, Q_NULLPTR);
}

QQuickItem * Manager::getDelegateForPath (const QString & path) const {
    return m_delegatesByPath.value (path, Q_NULLPTR);
}

void Manager::setDelegateForPath (const QString & path, QQuickItem * item) {
    connect (item, &QObject::destroyed, this, &Manager::unregisterObject, Qt::UniqueConnection);
    m_delegatesByPath.insert (path, item);
}

QVariantList Manager::getLinksAsSource (BasicObject * object) const {
    QVariantList ret;
    if (object) {
        for (AbstractLink * link : arrayRange (m_links)) {
            if (link->get_source () == object) {
                ret.append (QVariant::fromValue (link));
            }
        }
    }
    return ret;
}

QVariantList Manager::getLinksAsTarget (BasicObject * object) const {
    QVariantList ret;
    if (object) {
        for (AbstractLink * link : arrayRange (m_links)) {
            if (link->get_target () == object) {
                ret.append (QVariant::fromValue (link));
            }
        }
    }
    return ret;
}
