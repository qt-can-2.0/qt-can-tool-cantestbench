
#include "Memory.h"

#include "CppUtils.h"
#include "Manager.h"
#include "QtCAN.h"

#include <QStringBuilder>
#include <QStringList>
#include <QQmlEngine>
#include <qqml.h>

#if (QT_VERSION >= 0x080200)
#   include <private/qv8engine_p.h>
#   include <private/qv4engine_p.h>
#endif

static inline const QByteArray hex32bit (int num) {
    return QStringLiteral ("0x%1").arg (num, 8, 16, QChar::fromLatin1 ('0')).toLocal8Bit ();
}

static inline const QByteArray noSpace (const QByteArray & str) {
    QByteArray ret;
    ret.reserve (str.size ());
    for (const char * src = str.data (); *src != '\0'; ++src) {
        if (*src != ' ') {
            ret.append (*src);
        }
    }
    return ret;
}

AbstractVarWrapper::AbstractVarWrapper (const quint size, const VarType type) : size (size), type (type) { }

AbstractVarWrapper::~AbstractVarWrapper (void) { }

Memory::Memory (QObject * parent)
    : QObject (parent)
    , m_dumpTreeAfterInit (false)
    , m_bug   (false)
    , m_init  (Uninitialized)
{
    m_varsModel = new QQmlObjectListModel<MemoryModelItem> (this, "", "path");
    m_basicTypeNames.append ("int8");
    m_basicTypeNames.append ("uint8");
    m_basicTypeNames.append ("int16");
    m_basicTypeNames.append ("uint16");
    m_basicTypeNames.append ("int32");
    m_basicTypeNames.append ("uint32");
#ifndef NO_64_BIT_IN_QML
    m_basicTypeNames.append ("int64");
    m_basicTypeNames.append ("uint64");
#endif
}

Memory::~Memory (void) {
    reset ();
}

void Memory::initialize (void) {
    m_init = Initializing;
    emit init (this);
    m_init = Initialized;
    if (m_dumpTreeAfterInit) {
        tree (); // debug all the declared consts, vars and types
    }
}

void Memory::reset (void) {
    m_init = Deinitializing;
    m_constants.clear ();
    m_structDefs.clear ();
    m_varsTypes.clear ();
    m_varsByName.clear ();
    m_varsModel->clear ();
    qDeleteAll (m_varWrappers);
    m_varWrappers.clear ();
    m_bug = false;
    m_init = Uninitialized;
}

void Memory::dump (void) const {
    int total = 0;
    QByteArray bytes;
    for (AbstractVarWrapper * var : m_varWrappers) {
        total += var->size;
        bytes += var->hex ();
    }
    DUMP << "Allocated memory bytes :" << total << "bytes";
    int byte = 0;
    int chunkIdx = 0;
    const int chunkSize = 16;
    QByteArray chunkStr;
    chunkStr.reserve (chunkSize * 3);
    while (byte < total) {
        chunkStr.append (bytes.mid (byte * 2, 2) % ' ');
        byte++;
        if ((byte % chunkSize == 0) || (byte == total)) {
            DUMP << "    "
                 << qPrintable (QLatin1String (hex32bit (chunkIdx * chunkSize) % '-' % hex32bit ((chunkIdx +1) * chunkSize)))
                 << ":"
                 << qPrintable (QLatin1String (chunkStr));
            chunkStr.clear ();
            chunkIdx++;
        }
    }
}

void Memory::tree (void) const {
    static const QByteArray & INDENT = QByteArrayLiteral ("    ");
    if (!m_constants.isEmpty ()) {
        DUMP << "List of declared constants :";
        for (KeyValueRange<QMap<QByteArray, int>>::It itConst : keyValueRange (m_constants)) {
            DUMP << qPrintable (QLatin1String (INDENT % itConst.key ())) << "value=" << itConst.value ();
        }
    }
    if (!m_basicTypeNames.isEmpty ()) {
        DUMP << "List of basic types :";
        for (const QByteArray & type : m_basicTypeNames) {
            DUMP << qPrintable (QLatin1String (INDENT % type));
        }
    }
    if (!m_structDefs.isEmpty ()) {
        DUMP << "List of declared user types :";
        for (KeyValueRange<QMap<QByteArray, ByteArraysMap *>>::It itStruct : keyValueRange (m_structDefs)) {
            ByteArraysMap * structDef = itStruct.value ();
            QStringList tmp;
            for (KeyValueRange<ByteArraysMap>::It itMember : keyValueRange (* structDef)) {
                tmp << QLatin1String (itMember.key () % ':' % itMember.value ());
            }
            DUMP << qPrintable (QLatin1String (INDENT % itStruct.key ())) << "members=" << qPrintable (tmp.join (QLatin1String (", ")));
        }
    }
    if (!m_varsTypes.isEmpty ()) {
        DUMP << "List of declared variables :";
        for (ByteArraysMap::const_iterator itVar = m_varsTypes.constBegin (); itVar != m_varsTypes.constEnd (); ++itVar) {
            DUMP << qPrintable (QLatin1String (INDENT % itVar.key () % ':' % itVar.value ()));
        }
    }
}

bool Memory::declConst (const QByteArray & name, const QmlBiggestInt value) {
    bool ret = false;
    if (!m_bug) {
        if (m_init == Initializing) {
            const QByteArray key = noSpace (name);
            if (!key.isEmpty ()) {
                if (!m_constants.contains (key)) {
                    m_constants.insert (key, int (value));
                    ret = true;
                }
                else {
                    handleError ("Can't create new constant with same name as existing one : " % key);
                }
            }
            else {
                handleError ("Can't declare new constant with empty name !");
            }
        }
        else {
            handleError ("Can't declare new constant when not in Initializing mode !");
        }
    }
    return ret;
}

bool Memory::declType (const QByteArray & type, const QVariantMap & structure) {
    bool ret = false;
    if (!m_bug) {
        if (m_init == Initializing) {
            const QByteArray key = noSpace (type);
            if (!key.isEmpty ()) {
                if (!m_structDefs.contains (key) && !m_basicTypeNames.contains (key)) {
                    if (!structure.isEmpty ()) {
                        ByteArraysMap * structDef = new ByteArraysMap;
                        bool valid = true;
                        for (KeyValueRange<QMap<QString, QVariant>>::It itSub : keyValueRange (structure)) {
                            const QByteArray subvar  = noSpace (itSub.key ().toLocal8Bit ());
                            const QByteArray subtype = noSpace (itSub.value ().value<QString> ().toLocal8Bit ());
                            if (!subtype.isEmpty ()) {
                                const int bracketStart = subtype.indexOf ('[');
                                const int bracketEnd   = subtype.indexOf (']');
                                if (bracketStart < 0 && bracketEnd < 0) { // not array
                                    if (m_basicTypeNames.contains (subtype) || m_structDefs.contains (subtype)) {
                                        structDef->insert (subvar, subtype);
                                    }
                                    else {
                                        valid = false;
                                        handleError ("Can't declare sub var " % subvar % " with unknown type : " % subtype);
                                        break;
                                    }
                                }
                                else if (bracketStart >= 0 && bracketEnd >= 0) { // array
                                    bool ok = true;
                                    const QByteArray base = subtype.left (bracketStart);
                                    const QByteArray num  = subtype.mid  (bracketStart +1, (bracketEnd - bracketStart -1));
                                    const int count = (m_constants.contains (num)
                                                       ? m_constants.value (num)
                                                       : num.toInt (&ok, (num.startsWith ("0x") ? 16 : 10)));
                                    if (m_basicTypeNames.contains (base) || m_structDefs.contains (base)) {
                                        if (ok) {
                                            for (int idx = 0; idx < count; ++idx) {
                                                structDef->insert (subvar % '[' % QByteArray::number (idx) % ']', base);
                                            }
                                        }
                                        else {
                                            valid = false;
                                            handleError ("Can't declare sub var " % subvar % " as array with invalid size : " % num);
                                            break;
                                        }
                                    }
                                    else {
                                        valid = false;
                                        handleError ("Can't declare sub var " % subvar % " with unknown type : " % subtype);
                                        break;
                                    }
                                }
                                else {
                                    valid = false;
                                    handleError ("Can't declare sub var " % subvar % " with incorrect brackets : " % subtype);
                                    break;
                                }
                            }
                            else {
                                valid = false;
                                handleError ("Can't declare sub var " % subvar % " with empty type !");
                                break;
                            }
                        }
                        if (valid && !structDef->isEmpty ()) {
                            m_structDefs.insert (key, structDef);
                            ret = true;
                        }
                        else {
                            delete structDef;
                        }
                    }
                    else {
                        handleError ("Can't create new type with empty structure !");
                    }
                }
                else {
                    handleError ("Can't create new type with same name as existing one : " % key);
                }
            }
            else {
                handleError ("Can't create new type with empty name !");
            }
        }
        else {
            handleError ("Can't declare new type when not in Initializing mode !");
        }
    }
    return ret;
}

bool Memory::declVar (const QByteArray & name, const QByteArray & type) {
    bool ret = false;
    if (!m_bug) {
        if (m_init == Initializing) {
            const QByteArray key = noSpace (name);
            if (!key.isEmpty ()) {
                if (!m_varsByName.contains (key)) {
                    const QByteArray def = noSpace (type);
                    if (!def.isEmpty ()) {
                        const int bracketStart = def.indexOf ('[');
                        const int bracketEnd   = def.indexOf (']');
                        if (bracketStart < 0 && bracketEnd < 0) { // not array
                            if (m_basicTypeNames.contains (def)) {
                                AbstractVarWrapper * varWrapper = Q_NULLPTR;
                                if (def == "int8") {
                                    varWrapper = new VarWrapper<qint8>;
                                }
                                else if (def == "int16") {
                                    varWrapper = new VarWrapper<qint16>;
                                }
                                else if (def == "int32") {
                                    varWrapper = new VarWrapper<qint32>;
                                }
                                else if (def == "int64") {
                                    varWrapper = new VarWrapper<qint64>;
                                }
                                else if (def == "uint8") {
                                    varWrapper = new VarWrapper<quint8>;
                                }
                                else if (def == "uint16") {
                                    varWrapper = new VarWrapper<quint16>;
                                }
                                else if (def == "uint32") {
                                    varWrapper = new VarWrapper<quint32>;
                                }
                                else if (def == "uint64") {
                                    varWrapper = new VarWrapper<quint64>;
                                }
                                else { }
                                m_varWrappers.append (varWrapper);
                                m_varsByName.insert (key, varWrapper);
                                m_varsTypes.insert (key, def);
                                m_varsModel->append (new MemoryModelItem (key, def, varWrapper));
                                ret = true;
                            }
                            else if (m_structDefs.contains (def)) {
                                ByteArraysMap * structDef = m_structDefs.value (def);
                                bool error = false;
                                for (KeyValueRange<ByteArraysMap>::It itMember : keyValueRange (* structDef)) {
                                    if (!declVar (key % '.' % itMember.key (), itMember.value ())) {
                                        error = true;
                                        break;
                                    }
                                }
                                ret = !error;
                            }
                            else {
                                handleError ("Can't create new variable with unknown type : " % def);
                            }
                        }
                        else if (bracketStart >= 0 && bracketEnd >= 0) { // array
                            bool ok = true;
                            const QByteArray base = def.left (bracketStart);
                            const QByteArray num  = def.mid  (bracketStart +1, (bracketEnd - bracketStart -1));
                            const quint count = (m_constants.contains (num)
                                                 ? quint (m_constants.value (num))
                                                 : num.toUInt (&ok, (num.startsWith ("0x") ? 16 : 10)));
                            if (m_basicTypeNames.contains (base) || m_structDefs.contains (base)) {
                                if (ok) {
                                    bool error = false;
                                    for (quint idx = 0; idx < count; ++idx) {
                                        if (!declVar (key % '[' % QByteArray::number (idx) % ']', base)) {
                                            error = true;
                                            break;
                                        }
                                    }
                                    ret = !error;
                                }
                                else {
                                    handleError ("Can't declare new array var " % key % " with invalid size : " % num);
                                }
                            }
                            else {
                                handleError ("Can't declare new array variable " % key % " with unknown type : " % base);
                            }
                        }
                        else {
                            handleError ("Can't declare new variable " % key % " with incorrect brackets : " % def);
                        }
                    }
                    else {
                        handleError ("Can't create new variable with empty type !");
                    }
                }
                else {
                    handleError ("Can't create new variable with name alreay in use : " % key);
                }
            }
            else {
                handleError ("Can't create new variable with empty name !");
            }
        }
        else {
            handleError ("Can't create new variable when not in Initializing mode !");
        }
    }
    return ret;
}

QmlBiggestInt Memory::getVar (const QByteArray & path) {
    QmlBiggestInt ret = 0;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->get ();
        }
        else {
            handleError ("Can't get value of unknown variable " % path % " !");
        }
    }
    return ret;
}

void Memory::setVar (const QByteArray & path, const QmlBiggestInt data) {
    if (!m_bug) {
        const QByteArray key = noSpace (path);
        AbstractVarWrapper * varWrapper = m_varsByName.value (key);
        if (varWrapper != Q_NULLPTR) {
            if (varWrapper->set (data)) {
                MemoryModelItem * item = m_varsModel->getByUid (key);
                if (item != Q_NULLPTR) {
                    emit item->valueChanged ();
                    emit item->hexChanged ();
                }
            }
        }
        else {
            handleError ("Can't set value of unknown variable " % path % " !");
        }
    }
}

void Memory::addVar (const QByteArray & path, const QmlBiggestInt data) {
    if (!m_bug) {
        const QByteArray key = noSpace (path);
        AbstractVarWrapper * varWrapper = m_varsByName.value (key);
        if (varWrapper != Q_NULLPTR) {
            varWrapper->add (data);
            MemoryModelItem * item = m_varsModel->getByUid (key);
            if (item != Q_NULLPTR) {
                emit item->valueChanged ();
                emit item->hexChanged ();
            }
        }
        else {
            handleError ("Can't add value to unknown variable " % path % " !");
        }
    }
}

void Memory::mulVar (const QByteArray & path, const QmlBiggestInt data) {
    if (!m_bug) {
        const QByteArray key = noSpace (path);
        AbstractVarWrapper * varWrapper = m_varsByName.value (key);
        if (varWrapper != Q_NULLPTR) {
            varWrapper->mul (data);
            MemoryModelItem * item = m_varsModel->getByUid (key);
            if (item != Q_NULLPTR) {
                emit item->valueChanged ();
                emit item->hexChanged ();
            }
        }
        else {
            handleError ("Can't multiply value to unknown variable " % path % " !");
        }
    }
}

void Memory::divVar (const QByteArray & path, const QmlBiggestInt data) {
    if (!m_bug) {
        const QByteArray key = noSpace (path);
        AbstractVarWrapper * varWrapper = m_varsByName.value (key);
        if (varWrapper != Q_NULLPTR) {
            varWrapper->div (data);
            MemoryModelItem * item = m_varsModel->getByUid (key);
            if (item != Q_NULLPTR) {
                emit item->valueChanged ();
                emit item->hexChanged ();
            }
        }
        else {
            handleError ("Can't divide value to unknown variable " % path % " !");
        }
    }
}

bool Memory::isEqualTo (const QByteArray & path, const QmlBiggestInt data) {
    bool ret = false;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->isEqualTo (data);
        }
        else {
            handleError ("Can't compare value to unknown variable " % path % " !");
        }
    }
    return ret;
}

bool Memory::isLesserThan (const QByteArray & path, const QmlBiggestInt data) {
    bool ret = false;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->isLesserThan (data);
        }
        else {
            handleError ("Can't compare value to unknown variable " % path % " !");
        }
    }
    return ret;
}

bool Memory::isGreaterThan (const QByteArray & path, const QmlBiggestInt data) {
    bool ret = false;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->isGreaterThan (data);
        }
        else {
            handleError ("Can't compare value to unknown variable " % path % " !");
        }
    }
    return ret;
}

bool Memory::isDifferentFrom (const QByteArray & path, const QmlBiggestInt data) {
    bool ret = false;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->isDifferentFrom (data);
        }
        else {
            handleError ("Can't compare value to unknown variable " % path % " !");
        }
    }
    return ret;
}

QmlBiggestInt Memory::absDelta (const QByteArray & path, const QmlBiggestInt data) {
    QmlBiggestInt ret = 0;
    if (!m_bug) {
        AbstractVarWrapper * varWrapper = m_varsByName.value (noSpace (path));
        if (varWrapper != Q_NULLPTR) {
            ret = varWrapper->absoluteDelta (data);
        }
        else {
            handleError ("Can't compute delta to value for unknown variable " % path % " !");
        }
    }
    return ret;
}

void Memory::handleError (const QByteArray & message) {
    Manager::instance ().logError ("MEMORY", Q_NULLPTR, message);
    m_bug = true;
#if (QT_VERSION >= 0x050500 && QT_VERSION < 0x050C00)
    QV8Engine::getV4 (qmlEngine (parent ()))->throwError (QLatin1String (message));
#elif (QT_VERSION >= 0x050200 && QT_VERSION < 0x050500)
    QV8Engine::getV4 (qmlEngine (this))->currentContext ()->throwError (QLatin1String (message));
#else
    ERRO << qPrintable (message);
#endif
}

MemoryModelItem::MemoryModelItem (const QString & path, const QString & type, AbstractVarWrapper * wrapper, QObject * parent)
    : QObject (parent)
    , m_path (path)
    , m_type (type)
    , m_varWrapper (wrapper)
{ }

QmlBiggestInt MemoryModelItem::getValue (void) const {
    return m_varWrapper->get ();
}

QByteArray MemoryModelItem::getHex (void) const {
    return m_varWrapper->hex ();
}
