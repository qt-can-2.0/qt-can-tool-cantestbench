
#include "CanOpen.h"

#include "Manager.h"
#include "Routine.h"
#include "CanBus.h"

#include "CanDriverQmlWrapper.h"
#include "ByteArrayQmlWrapper.h"

#include "CanOpenDefs.h"
#include "CanOpenEntry.h"
#include "CanOpenSubEntry.h"
#include "CanOpenObjDict.h"
#include "CanOpenProtocolManager.h"
#include "AbstractObdPreparator.h"

CanOpen::CanOpen (QObject * parent)
    : BasicObject (ObjectFamily::CANOPEN, parent)
    , m_nodeId (0)
    , m_isMaster (false)
    , m_canBus (Q_NULLPTR)
    , m_ready (false)
    , m_running (false)
    , m_canOpenMan (Q_NULLPTR)
{
    set_uid ("CANOPEN");
    connect (this, &CanOpen::obdUrlChanged,   this, &CanOpen::onCanOpenParamChanged);
    connect (this, &CanOpen::nodeIdChanged,   this, &CanOpen::onCanOpenParamChanged);
    connect (this, &CanOpen::isMasterChanged, this, &CanOpen::onCanOpenParamChanged);
    Manager::instance ().registerObject (this);
}

CanOpen::~CanOpen (void) {
    Manager::instance ().unregisterObject (this);
}

void CanOpen::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_ready = true;
    Manager::instance ().intializeObject (this);
}

void CanOpen::init (void) {
    if (m_nodeId > 0 && m_nodeId < 128) {
        m_canOpenMan = new CanOpenProtocolManager (false, false, this);
        m_canOpenMan->setLocalNodeId (CanOpenNodeId (m_nodeId));
        m_canOpenMan->setLocalNetworkPosition (m_isMaster ? CanOpenNetPosition::Master : CanOpenNetPosition::Slave);
        if (!m_obdUrl.isEmpty ()) {
            CanOpenObdPreparator preparator;
            QQmlContext * ctx = qmlContext (parent ());
            const QUrl tmp = ctx->resolvedUrl (m_obdUrl);
            if (preparator.loadFromQmlFile (tmp.toString (), m_canOpenMan->getObjDict ())) {
                Manager::instance ().logInfo ("CANOPEN", Q_NULLPTR, QS ("Loaded QML OBD %1").arg (m_obdUrl));
            }
            else {
                Manager::instance ().logError ("CANOPEN", this, QS ("Couldn't load QML OBD %1 !").arg (m_obdUrl));
            }
        }
    }
    emit initialized ();
}

void CanOpen::start (void) {
    if (m_canOpenMan) {
        if (m_canBus) {
            m_canOpenMan->start (m_canBus->get_driverProxy ());
        }
        else {
            Manager::instance ().logWarning ("CANOPEN", this, "No CAN bus defined for CANopen object !");
        }
        emit started ();
        m_running = true;
    }
}

void CanOpen::stop (void) {
    if (m_canOpenMan) {
        m_canOpenMan->stop ();
        emit stopped ();
        m_running = false;
    }
}

void CanOpen::reset (void) {
    emit resetting ();
    if (m_canOpenMan != Q_NULLPTR) {
        delete m_canOpenMan;
        m_canOpenMan.clear ();
    }
}

CanOpenObjDict * CanOpen::getObjectDict (void) const {
    return (m_canOpenMan ? m_canOpenMan->getObjDict () : Q_NULLPTR);
}

CanOpenProtocolManager * CanOpen::getProtocolManager (void) const {
    return m_canOpenMan;
}

ByteArrayWrapper * CanOpen::createBufferFromInt8 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<qint8> (qint8 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromInt16 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<qint16> (qint16 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromInt32 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<qint32> (qint32 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromUInt8 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<quint8> (quint8 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromUInt16 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<quint16> (quint16 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromUInt32 (const QmlBiggestInt value) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->fromInt<quint32> (quint32 (value));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromHex (const QString & hex) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->setBytes (QByteArray::fromHex (hex.toLocal8Bit ()));
    return ret;
}

ByteArrayWrapper * CanOpen::createBufferFromString (const QString & str) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    ret->setBytes (str.toLatin1 ());
    return ret;
}

ByteArrayWrapper * CanOpen::readValue (const int index, const int subIndex) {
    ByteArrayWrapper * ret = new ByteArrayWrapper;
    if (CanOpenObjDict * obd = getObjectDict ()) {
        if (CanOpenEntry * entry = obd->getEntry (CanOpenIndex (index))) {
            if (CanOpenSubEntry * subEntry = entry->getSubEntry (CanOpenSubIndex (subIndex))) {
                QByteArray tmp (int (subEntry->getDataLen ()), 0x00);
                subEntry->read (tmp.data (), quint (tmp.size ()));
                ret->setBytes (tmp);
            }
        }
    }
    return ret;
}

void CanOpen::changeValue (const int index, const int subIndex, ByteArrayWrapper * buffer) {
    if (buffer) {
        if (CanOpenObjDict * obd = getObjectDict ()) {
            if (CanOpenEntry * entry = obd->getEntry (CanOpenIndex (index))) {
                if (CanOpenSubEntry * subEntry = entry->getSubEntry (CanOpenSubIndex (subIndex))) {
                    const QByteArray tmp = buffer->getBytes ();
                    if (subEntry->getDataLen () == CanOpenDataLen (tmp.size ())) {
                        subEntry->write (tmp.constData (), CanOpenDataLen (tmp.size ()));
                    }
                }
            }
        }
    }
}

void CanOpen::sendNmtChangeRequest (const int nodeId, const CanOpenNmtCmd::Type state) {
    if (m_canOpenMan) {
        m_canOpenMan->sendNmtChangeRequest (CanOpenNodeId (nodeId),
                                            CanOpenNmtCmdSpecif (state));
    }
}

void CanOpen::sendLssActionRequest (const CanOpenLssCommands::Type cmd, const int argument) {
    if (m_canOpenMan) {
        m_canOpenMan->sendLssCommand (CanOpenLssCmd (cmd),
                                      quint8 (argument));
    }
}

void CanOpen::sendSdoReadRequest (const int nodeId, const int index, const int subIndex, const bool blockMode) {
    if (m_canOpenMan) {
        m_canOpenMan->sendSdoReadRequest (CanOpenNodeId (nodeId),
                                          CanOpenIndex (index),
                                          CanOpenSubIndex (subIndex),
                                          blockMode);
    }
}

void CanOpen::sendSdoWriteRequest (const int nodeId, const int index, const int subIndex, ByteArrayWrapper * buffer) {
    if (m_canOpenMan && buffer) {
        const QByteArray tmp = buffer->getBytes ();
        m_canOpenMan->sendSdoWriteRequest (CanOpenNodeId (nodeId),
                                           CanOpenIndex (index),
                                           CanOpenSubIndex (subIndex),
                                           tmp.constData (),
                                           CanOpenDataLen (tmp.size ()));
    }
}

void CanOpen::onCanOpenParamChanged (void) {
    if (m_ready) {
        const bool wasRunning = m_running;
        stop ();
        reset ();
        init ();
        if (wasRunning) {
            start ();
        }
    }
}
