
#include "Dashboard.h"

#include "Manager.h"

Dashboard::Dashboard (QQuickItem * parent)
    : QQuickItem (parent)
{
    Manager::instance ().registerObject (this);
}

Dashboard::~Dashboard (void) {
    Manager::instance ().unregisterObject (this);
}

void Dashboard::classBegin (void) { }

void Dashboard::componentComplete (void) {
    Manager::instance ().intializeObject (this);
}
