#ifndef GRAPHER_H
#define GRAPHER_H

#include <QQuickItem>
#include <QColor>
#include <QTimer>
#include <QPointF>
#include <QSGNode>
#include <QSGFlatColorMaterial>
#include <QSGMaterial>
#include <QSGGeometry>
#include <QSGGeometryNode>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"

class Grapher : public QQuickItem {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, samplingInterval)
    QML_WRITABLE_VAR_PROPERTY (int, timeFrame)
    QML_WRITABLE_VAR_PROPERTY (bool, running)
    QML_WRITABLE_VAR_PROPERTY (qreal, currentValue)
    QML_WRITABLE_VAR_PROPERTY (qreal, minValue)
    QML_WRITABLE_VAR_PROPERTY (qreal, maxValue)
    QML_WRITABLE_VAR_PROPERTY (qreal, lineSize)
    QML_WRITABLE_CSTREF_PROPERTY (QColor, lineColor)
    QML_WRITABLE_VAR_PROPERTY (qreal, markerPos)
    QML_READONLY_VAR_PROPERTY (qreal, markerVal)

public:
    explicit Grapher (QQuickItem * parent = Q_NULLPTR);

    Q_INVOKABLE void reset (void);

    Q_INVOKABLE QVariantList getValues (void) const;

protected:
    void componentComplete (void) Q_DECL_FINAL;
    QSGNode * updatePaintNode (QSGNode * oldNode, UpdatePaintNodeData * nodeData) Q_DECL_FINAL;

protected slots:
    void doInit (void);
    void onTick (void);
    void doMark (void);

private:
    QList<qreal> m_values;
    QTimer * m_timer;
};

#endif // GRAPHER_H
