import QtQml 2.2;
import QtCAN.CanTestBench 2.0;

NetworkDefinition {
    uid: "TEST NG";
    title: "Test network definition NG (robotic arm)";
    dumpObjectsOnInit: false;
    benchmarkSimulatorTick: false;
    benchmarkCanFramesTiming: false;
    world {
        bounds {
            toTop {
                min: blockBase.size.toTop.val + blockPivot.size.toTop.val;
                max: blockArm.size.toFront.max + blockBase.size.toTop.val + blockPivot.size.toTop.val + blockExtraArm.size.toFront.val;
            }
            toLeft {
                min: blockBase.size.toLeft.val;
                max: blockArm.size.toFront.max + blockExtraArm.size.toFront.val;
            }
            toRight {
                min: blockBase.size.toRight.val;
                max: blockArm.size.toFront.max + blockExtraArm.size.toFront.val;
            }
            toBack {
                min: blockBase.size.toBack.val;
                max: blockArm.size.toFront.max + blockExtraArm.size.toFront.val;
            }
            toFront {
                min: blockBase.size.toFront.val;
                max: blockArm.size.toFront.max + blockExtraArm.size.toFront.val;
            }
            toBottom {
                min: blockBase.size.toBottom.val + blockWheelFL.size.toTop.val + blockWheelFL.size.toBottom.val;
                max: blockBase.size.toBottom.val + blockWheelFL.size.toTop.val + blockWheelFL.size.toBottom.val;
            }
        }
    }

    /// CAN BUSES

    CanBus {
        id: can1;
        uid: "CAN_1";
        title: "Main CAN";
    }
    CanBus {
        id: can2;
        uid: "CAN_2";
        title: "Backup CAN";
    }

    /// SERIAL BUSES

    SerialBus {
        id: com1;
        uid: "COM_1";
        title: "UC serial";
    }
    SerialBus {
        id: com2;
        uid: "COM_2";
        title: "GSM modem";
    }

    /// NODES

    Node {
        id: node1;
        uid: "CPU_A";
        title: "Master CPU";
        description: "Main node, does control-command and safety checks";
        memory {
            dumpTreeAfterInit: false;
            onInit: {
                // create struct to store commands
                memory.declType ("Command", {
                                     "direction1" : "uint8",
                                     "direction2" : "uint8",
                                 });
                // declare needed vars
                memory.declVar ("isMoving",     "uint8");
                memory.declVar ("isAuthorized", "uint8");
                memory.declVar ("hasDefect",    "uint8");
                memory.declVar ("cycleCounter", "uint32");
                memory.declVar ("safetyExt",    "uint16");
                memory.declVar ("safetySlew",   "uint16");
                memory.declVar ("safetyAng",    "uint16");
                memory.declVar ("cmdExt",       "Command");
                memory.declVar ("cmdSlew",      "Command");
                memory.declVar ("cmdAng",       "Command");
                memory.declVar ("m32c_cmdId",   "uint8");
                memory.declVar ("m32c_cmdVal",  "uint32");
            }
        }

        CanOpen {
            canBus: can2;
            nodeId: 12;
            isMaster: true;
            obdUrl: "";
        }
        RoutineOnTimer {
            id: controlCommand;
            uid: "CTRL_CMD";
            title: "Control-command";
            interval: 250;
            benchmarkExecutionTime: false;
            onTriggered: {
                // increase cycle counter
                memory.addVar ("cycleCounter", 1);
                // check consistency defect
                if (memory.absDelta ("safetyExt", cpuA_ain1.getValueInPoints ()) > 170) { // ~5cm/s
                    memory.setVar ("hasDefect", 1);
                }
                else if (memory.absDelta ("safetySlew", cpuA_ain2.getValueInPoints ()) > 115) { // ~5°/s
                    memory.setVar ("hasDefect", 1);
                }
                else if (memory.absDelta ("safetyAng", cpuA_ain3.getValueInPoints ()) > 115) { // ~5°/s
                    memory.setVar ("hasDefect", 1);
                }
                else {
                    memory.setVar ("hasDefect", 0);
                }
                // check authorization
                if (memory.isEqualTo ("isAuthorized", 1)) {
                    memory.setVar ("isMoving", 0);
                    // handle extension
                    if (memory.isGreaterThan ("cmdExt.direction1", 0)) { // ext out
                        pwm1.setValueInPercents (0);
                        pwm2.setValueInPercents (memory.getVar ("cmdExt.direction1"));
                        memory.setVar ("isMoving", 1);
                    }
                    else if (memory.isGreaterThan ("cmdExt.direction2", 0)) { // ext in
                        pwm2.setValueInPercents (0);
                        pwm1.setValueInPercents (memory.getVar ("cmdExt.direction2"));
                        memory.setVar ("isMoving", 1);
                    }
                    else {
                        pwm1.setValueInPercents (0);
                        pwm2.setValueInPercents (0);
                    }
                    // handle slewing
                    if (memory.isGreaterThan ("cmdSlew.direction1", 0)) { // ang cw
                        pwm4.setValueInPercents (0);
                        pwm3.setValueInPercents (memory.getVar ("cmdSlew.direction1"));
                        memory.setVar ("isMoving", 1);
                    }
                    else if (memory.isGreaterThan ("cmdSlew.direction2", 0)) { // ang ccw
                        pwm3.setValueInPercents (0);
                        pwm4.setValueInPercents (memory.getVar ("cmdSlew.direction2"));
                        memory.setVar ("isMoving", 1);
                    }
                    else {
                        pwm3.setValueInPercents (0);
                        pwm4.setValueInPercents (0);
                    }
                    // handle angle up/down
                    if (memory.isGreaterThan ("cmdAng.direction1", 0)) { // ang up
                        pwm6.setValueInPercents (0);
                        pwm5.setValueInPercents (memory.getVar ("cmdAng.direction1"));
                        memory.setVar ("isMoving", 1);
                    }
                    else if (memory.isGreaterThan ("cmdAng.direction2", 0)) { // ang down
                        pwm5.setValueInPercents (0);
                        pwm6.setValueInPercents (memory.getVar ("cmdAng.direction2"));
                        memory.setVar ("isMoving", 1);
                    }
                    else {
                        pwm5.setValueInPercents (0);
                        pwm6.setValueInPercents (0);
                    }
                }
                else {
                    pwm1.setValueInPercents (0);
                    pwm2.setValueInPercents (0);
                    pwm3.setValueInPercents (0);
                    pwm4.setValueInPercents (0);
                    pwm5.setValueInPercents (0);
                    pwm6.setValueInPercents (0);
                }
                // send notification frame
                can1.frameTx.prepare (2);
                can1.frameTx.setBitsAs (VarTypes.UInt8, 0, 8, memory.getVar ("isMoving")); // light if moving
                can1.frameTx.setBitsAs (VarTypes.UInt8, 8, 8, memory.getVar ("hasDefect")); // buzzer if defect
                can1.frameTx.send (0x500);
                // send sync frame
                can1.frameTx.prepare (0);
                can1.frameTx.send (0x100);
            }
        }
        RoutineOnCanFrame {
            canId: 0x200; // safety
            canBus: can1;
            uid: "PARSE_SAFETY";
            title: "Parse sensors safety frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // parse sensors value (16 bits each)
                memory.setVar ("safetyExt",  frameRx.getBitsAs (VarTypes.UInt16,  0, 16));
                memory.setVar ("safetySlew", frameRx.getBitsAs (VarTypes.UInt16, 16, 16));
                memory.setVar ("safetyAng",  frameRx.getBitsAs (VarTypes.UInt16, 32, 16));
            }
        }
        RoutineOnCanFrame {
            canId: 0x300; // commands
            canBus: can1;
            uid: "PARSE_JOYSTICKS";
            title: "Parse joysticks frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // parse commands (8 bits each)
                memory.setVar ("cmdExt.direction1",  frameRx.getBitsAs (VarTypes.UInt8,  0, 8));
                memory.setVar ("cmdExt.direction2",  frameRx.getBitsAs (VarTypes.UInt8,  8, 8));
                memory.setVar ("cmdSlew.direction1", frameRx.getBitsAs (VarTypes.UInt8, 16, 8));
                memory.setVar ("cmdSlew.direction2", frameRx.getBitsAs (VarTypes.UInt8, 24, 8));
                memory.setVar ("cmdAng.direction1",  frameRx.getBitsAs (VarTypes.UInt8, 32, 8));
                memory.setVar ("cmdAng.direction2",  frameRx.getBitsAs (VarTypes.UInt8, 40, 8));
            }
        }
        RoutineOnCanFrame {
            canId: 0x400; // authorization
            canBus: can1;
            uid: "PARSE_AUTH";
            title: "Parse authorization frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // parse flag (8 bits)
                memory.setVar ("isAuthorized", frameRx.getBitsAs (VarTypes.UInt8, 0, 8));
            }
        }
        RoutineOnSerialFrame {
            uid: "PARSE_M32C_CMD";
            serialBus: com1;
            onTriggered: {
                if (com1.count () >= 5) {
                    memory.setVar ("m32c_cmdId",  com1.readUInt8 ());
                    memory.setVar ("m32c_cmdVal", com1.readUInt32 ());
                }
            }
        }
        Board {
            uid: "SENSORS";
            title: "Sensors inputs";

            AnalogInput { id: cpuA_ain1; uid: "AIN1"; resolutionInPoints: 8192; }
            AnalogInput { id: cpuA_ain2; uid: "AIN2"; resolutionInPoints: 8192; }
            AnalogInput { id: cpuA_ain3; uid: "AIN3"; resolutionInPoints: 8192; }
        }
        Board {
            uid: "PWMS";
            title: "Power outputs"

            AnalogOutput { id: pwm1; uid: "PWM1"; resolutionInPoints: 8192; }
            AnalogOutput { id: pwm2; uid: "PWM2"; resolutionInPoints: 8192; }
            AnalogOutput { id: pwm3; uid: "PWM3"; resolutionInPoints: 8192; }
            AnalogOutput { id: pwm4; uid: "PWM4"; resolutionInPoints: 8192; }
            AnalogOutput { id: pwm5; uid: "PWM5"; resolutionInPoints: 8192; }
            AnalogOutput { id: pwm6; uid: "PWM6"; resolutionInPoints: 8192; }
        }
    }
    Node {
        id: node2;
        uid: "CPU_B";
        title: "Safety CPU";

        RoutineOnCanFrame {
            canId: 0x100; // sync
            canBus: can1;
            uid: "SEND_SAFETY";
            title: "Send sensors safety frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // compose reply (sensors values)
                can1.frameTx.prepare (6);
                can1.frameTx.setBitsAs (VarTypes.Int16,  0, 16, cpuB_ain1.getValueInPoints ());
                can1.frameTx.setBitsAs (VarTypes.Int16, 16, 16, cpuB_ain2.getValueInPoints ());
                can1.frameTx.setBitsAs (VarTypes.Int16, 32, 16, cpuB_ain3.getValueInPoints ());
                can1.frameTx.send (0x200); // safety
            }
        }
        Board {
            uid: "SENSORS";
            title: "Sensors inputs";

            AnalogInput { id: cpuB_ain1; uid: "AIN1"; resolutionInPoints: 8192; }
            AnalogInput { id: cpuB_ain2; uid: "AIN2"; resolutionInPoints: 8192; }
            AnalogInput { id: cpuB_ain3; uid: "AIN3"; resolutionInPoints: 8192; }
        }
    }
    Node {
        id: node3;
        uid: "DASH";
        title: "Slave dashboard";

        RoutineOnCanFrame {
            canId: 0x100; // sync
            canBus: can1;
            uid: "SEND_JOYSTICKS";
            title: "Send joysticks frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // compose reply (joysticks values)
                can1.frameTx.prepare (6);
                can1.frameTx.setBitsAs (VarTypes.UInt8,  0, 8, (joyExt.percents  > 0 ? +joyExt.percents  : 0));
                can1.frameTx.setBitsAs (VarTypes.UInt8,  8, 8, (joyExt.percents  < 0 ? -joyExt.percents  : 0));
                can1.frameTx.setBitsAs (VarTypes.UInt8, 16, 8, (joySlew.percents > 0 ? +joySlew.percents : 0));
                can1.frameTx.setBitsAs (VarTypes.UInt8, 24, 8, (joySlew.percents < 0 ? -joySlew.percents : 0));
                can1.frameTx.setBitsAs (VarTypes.UInt8, 32, 8, (joyAng.percents  > 0 ? +joyAng.percents  : 0));
                can1.frameTx.setBitsAs (VarTypes.UInt8, 40, 8, (joyAng.percents  < 0 ? -joyAng.percents  : 0));
                can1.frameTx.send (0x300); // manipulators
            }
        }
        RoutineOnCanFrame {
            canId: 0x500; // notifications
            canBus: can1;
            uid: "PARSE_NOTIF";
            title: "Parse notification frame";
            benchmarkExecutionTime: false;
            onTriggered: {
                // apply values
                dout1.value = frameRx.getBitsAs (VarTypes.UInt8, 0, 8);
                dout2.value = frameRx.getBitsAs (VarTypes.UInt8, 8, 8);
            }
        }
        RoutineOnEvent {
            uid: "SEND_AUTH";
            title: "Send authorization frame";
            emiter: din1;
            signalName: "valueChanged";
            benchmarkExecutionTime: false;
            onTriggered: {
                // send authorization flag
                can1.frameTx.prepare (1);
                can1.frameTx.setBitsAs (VarTypes.UInt8, 0, 8, din1.value);
                can1.frameTx.send (0x400); // access level
                if (extraBoard) {
                    node3.boards.remove (extraBoard);
                    extraBoard.destroy ();
                    extraBoard = null;
                }
                if (din1.value) {
                    extraBoard = compoExtra.createObject (node3, { });
                    node3.boards.append (extraBoard);
                }
            }

            property Board extraBoard : null;
        }
        Board {
            uid: "JOY";
            title: "Joysticks";

            AnalogInput { id: dash_ain1; uid: "AIN1"; resolutionInPoints: 8192; }
            AnalogInput { id: dash_ain2; uid: "AIN2"; resolutionInPoints: 8192; }
            AnalogInput { id: dash_ain3; uid: "AIN3"; resolutionInPoints: 8192; }
        }
        Board {
            uid: "UI";
            title: "User interface";

            DigitalInput  { id: din1;  uid: "DIN1"; }
            DigitalOutput { id: dout1; uid: "DOUT1"; }
            DigitalOutput { id: dout2; uid: "DOUT2"; }
        }
        Component {
            id: compoExtra;

            Board {
                uid: "EXT";
                title: "Extension";

                DigitalInput  { uid: "DIN1"; id: extDin1; }
                DigitalInput  { uid: "DIN2"; }
                DigitalInput  { uid: "DIN3"; }
                DigitalInput  { uid: "DIN4";  }
                DigitalOutput { uid: "DOUT1"; id: extDout1; }
                DigitalOutput { uid: "DOUT2"; }
                DigitalOutput { uid: "DOUT3"; }
                DigitalOutput { uid: "DOUT4"; }

                LinkDigitalOutputToDigitalInput { // manual link
                    source: extDout1;
                    target: extDin1;
                }
            }
        }
    }

    /// SENSORS

    // switch
    DigitalSensor {
        id: sensorAuth;
        uid: "RFID_KEY";
        title: "Authorization Key";
        description: "Dongle for security access";
        value: false;
        trueLabel: "Present";
        falseLabel: "Absent";
        targetLinks: LinkDigitalSensorToDigitalInput {
            target: din1;
        }
    }
    // joysticks
    AnalogSensor {
        id: joyExt;
        uid: "JOY_EXT";
        title: "Joystick Ext.";
        description: "User command for arm extension in/out";
        minPhy: -1000;
        maxPhy: +1000;
        useSplitPoint: true;
        upperMargin: 100;
        lowerMargin: 100;
        middleMargin: 100;
        targetLinks: LinkAnalogSensorToAnalogInput {
            target: dash_ain1;
        }
    }
    AnalogSensor {
        id: joySlew;
        uid: "JOY_SLEW";
        title: "Joystick slew";
        description: "User command for arm rotation left/right";
        minPhy: -1000;
        maxPhy: +1000;
        useSplitPoint: true;
        upperMargin: 100;
        lowerMargin: 100;
        middleMargin: 100;
        targetLinks: LinkAnalogSensorToAnalogInput {
            target: dash_ain2;
        }
    }
    AnalogSensor {
        id: joyAng;
        uid: "JOY_ANG";
        title: "Joystick Ang.";
        description: "User command for arm movement up/down";
        minPhy: -1000;
        maxPhy: +1000;
        useSplitPoint: true;
        upperMargin: 100;
        lowerMargin: 100;
        middleMargin: 100;
        targetLinks: LinkAnalogSensorToAnalogInput {
            target: dash_ain3;
        }
    }
    // sensors
    AnalogSensor  {
        id: sensorExt;
        uid: "SENSOR_EXT";
        title: "Ext. sensor";
        description: "Sensor measuring arm total length extension";
        maxPhy: 3500;
        unit: "m";
        decimals: 2;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: blockArm.size.toFront;
            transformer: AffineTransformer {
                offset: -blockArm.size.toFront.min;
            }
        }
        targetLinks: [
            LinkAnalogSensorToAnalogInput {
                target: cpuA_ain1;
            },
            LinkAnalogSensorToAnalogInput {
                target: cpuB_ain1;
            }
        ]
    }
    HybridSensor {
        id: sensorArmFullIn;
        uid: "SENSOR_ARM_FULL_IN";
        title: "Arm full-in sensor";
        unit: "m";
        description: "Sensor checking if arm is fully retracted in";
        decimals: 1;
        invertState: true;
        lowThresholdPhy: 60;
        highThresholdPhy: 70;
        sourceLink: LinkPhysicalValueToHybridSensor {
            source: blockArm.size.toFront;
        }
        targetLinks: LinkHybridSensorToDigitalInput { }
    }
    HybridSensor {
        id: sensorArmFullOut;
        uid: "SENSOR_ARM_FULL_OUT";
        title: "Arm full-out sensor";
        description: "Sensor checking if arm is fully telescoped out";
        unit: "m";
        decimals: 1;
        invertState: false;
        lowThresholdPhy: 330;
        highThresholdPhy: 340;
        sourceLink: LinkPhysicalValueToHybridSensor {
            source: blockArm.size.toFront;
        }
        targetLinks: LinkHybridSensorToDigitalInput { }
    }
    AnalogSensor {
        id: sensorSlewAng;
        uid: "SENSOR_SLEW";
        title: "Slew angle sensor";
        description: "Sensor measuring arm horizontal rotation angle";
        maxPhy: 3600;
        unit: "°";
        decimals: 1;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: blockPivot.angle.yaw;
        }
        targetLinks: [
            LinkAnalogSensorToAnalogInput {
                target: cpuA_ain2;
            },
            LinkAnalogSensorToAnalogInput {
                target: cpuB_ain2;
            }
        ]
    }
    AnalogSensor {
        id: sensorAngUpDown;
        uid: "SENSOR_ANG";
        title: "Angle sensor";
        description: "Sensor measuring arm vertical rotation angle";
        maxPhy: 900;
        unit: "°";
        decimals: 1;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: blockArm.angle.pitch;
        }
        targetLinks: [
            LinkAnalogSensorToAnalogInput {
                target: cpuA_ain3;
            },
            LinkAnalogSensorToAnalogInput {
                target: cpuB_ain3;
            }
        ]
    }
    AnalogSensor {
        id: sensorArmElev;
        uid: "SENSOR_ELEV";
        title: "Elevation sensor";
        description: "Sensor measuring elevation of arm tip from ground";
        maxPhy: 5000;
        minPhy: -500;
        unit: "m";
        decimals: 2;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: markerTip.absolutePos.onBottomToTop;
        }
    }
    AnalogSensor {
        id: sensorExtraArmCline;
        uid: "SENSOR_CLINE";
        title: "Clinometer sensor";
        description: "Sensor measuring absolute horizontalty of extra arm compared to ground";
        maxPhy: 3600;
        unit: "°";
        decimals: 1;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: blockExtraArm.absoluteAngle.pitch;
        }
    }
    AnalogSensor {
        id: sensorRoll;
        uid: "SENSOR_ROLL";
        title: "Roll sensor";
        description: "Sensor measuring angle of the ground";
        minPhy: -200
        maxPhy: +200;
        useSplitPoint: true;
        unit: "°";
        decimals: 1;
        sourceLink: LinkPhysicalValueToAnalogSensor {
            source: blockBase.angle.roll;
        }
    }
    DigitalSensor {
        uid: "DYN_EXT";
        title: "Dynamic Extension";
        targetLinks: LinkDigitalSensorToDigitalInput {
            target: Shared.manager.dict.getObject ("DASH/EXT/DIN4");
        }
    }

    /// ACTUATORS

    // motors
    AnalogActuator {
        id: motorExtIn;
        uid: "MOV_EXT_IN";
        title: "Ext. in";
        minSpeed: 0;
        maxSpeed: -500;
        decimals: 2;
        unit: "m/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm1;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockArm.size.toFront;
        }
    }
    AnalogActuator {
        id: motorExtOut;
        uid: "MOV_EXT_OUT";
        title: "Ext. out";
        minSpeed: 0;
        maxSpeed: +500;
        decimals: 2;
        unit: "m/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm2;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockArm.size.toFront;
        }
    }
    AnalogActuator {
        id: motorSlewCW;
        uid: "MOV_SLEW_CW";
        title: "Rotation CW";
        minSpeed: 0;
        maxSpeed: +150;
        decimals: 1;
        unit: "°/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm3;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockPivot.angle.yaw;
        }
    }
    AnalogActuator {
        id: motorSlewCCW;
        uid: "MOV_SLEW_CCW";
        title: "Rotation CCW";
        minSpeed: 0;
        maxSpeed: -150;
        decimals: 1;
        unit: "°/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm4;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockPivot.angle.yaw;
        }
    }
    AnalogActuator {
        id: motorAngUp;
        uid: "MOV_ANG_UP";
        title: "Rotation Up";
        minSpeed: 0;
        maxSpeed: +120;
        decimals: 1;
        unit: "°/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm5;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockArm.angle.pitch;
        }
    }
    AnalogActuator {
        id: motorAngDown;
        uid: "MOV_ANG_DOWN";
        title: "Rotation Down";
        minSpeed: 0;
        maxSpeed: -120;
        decimals: 1;
        unit: "°/s";
        sourceLink: LinkAnalogOutputToAnalogActuator {
            source: pwm6;
        }
        targetLink: LinkAnalogActuatorToPhysicalValue {
            target: blockArm.angle.pitch;
        }
    }
    HybridActuator {
        uid: "VERTICAL_STATION";
        title: "Put machine in vertical station";
        ratedSpeed: +250;
        decimals: 1;
        unit: "°/s";
        targetLink: LinkHybridActuatorToPhysicalValue {
            target: blockArm.angle.pitch;
        }
    }
    // notifications
    DigitalActuator {
        id: light;
        uid: "LIGHT";
        title: "Light";
        trueLabel: "Shining";
        falseLabel: "Off";
        sourceLink: LinkDigitalOutputToDigitalActuator {
            source: dout1;
        }
    }
    DigitalActuator {
        id: buzzer;
        uid: "BUZZER";
        title: "Buzzer";
        trueLabel: "Sounding";
        falseLabel: "Silent";
        sourceLink: LinkDigitalOutputToDigitalActuator {
            source: dout2;
        }
    }

    /// WORLD

    PhysicalValue {
        id: valuePressure;
        uid: "PRESSURE";
        min: 400;
        val: 1000;
        max: 1500;
    }
    PhysicalValue {
        id: valueTemperature;
        uid: "TEMPERATURE";
        min: -273;
        val: 25;
        max: 300;
    }
    PhysicalBlock {
        id: blockBase;
        uid: "BASE";
        title: "Base";
        color: "gray";
        pivotPos { // centered on world
            onLeftToRight.val: 0;
            onBottomToTop.val: 0;
            onBackToFront.val: 0;
        }
        angle {
            roll {
                min: -45;
                val: 0;
                max: +45;
            }
        }
        size {
            toTop.val: 1;
            toLeft.val: 5;
            toRight.val: 5;
            toBack.val: 5;
            toFront.val: 15;
            toBottom.val: 1;
        }
    }
    ObjectsGroup {
        uid: "CHASSIS";

        PhysicalBlock {
            id: blockWheelFR;
            uid: "WHEEL_FR";
            title: "Wheel Front Right";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: +3.5;
                onBottomToTop.val: -1.5;
                onBackToFront.val: +12;
            }
            size {
                toTop.val: 1.5;
                toLeft.val: 0;
                toRight.val: 0.5;
                toBack.val: 1.5;
                toFront.val: 1.5;
                toBottom.val: 1.5;
            }
        }
        PhysicalBlock {
            id: blockWheelFL;
            uid: "WHEEL_FL";
            title: "Wheel Front Left";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: -3.5;
                onBottomToTop.val: -1.5;
                onBackToFront.val: +12;
            }
            size {
                toTop.val: 1.5;
                toLeft.val: 0.5;
                toRight.val: 0;
                toBack.val: 1.5;
                toFront.val: 1.5;
                toBottom.val: 1.5;
            }
        }
        PhysicalBlock {
            id: blockWheelRL;
            uid: "WHEEL_RL";
            title: "Wheel Rear Left";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: -3.5;
                onBottomToTop.val: -1.5;
                onBackToFront.val: -2;
            }
            size {
                toTop.val: 1.5;
                toLeft.val: 0.5;
                toRight.val: 0;
                toBack.val: 1.5;
                toFront.val: 1.5;
                toBottom.val: 1.5;
            }
        }
        PhysicalBlock {
            id: blockWheelRR;
            uid: "WHEEL_RR";
            title: "Wheel Rear Right";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: +3.5;
                onBottomToTop.val: -1.5;
                onBackToFront.val: -2;
            }
            size {
                toTop.val: 1.5;
                toLeft.val: 0;
                toRight.val: 0.5;
                toBack.val: 1.5;
                toFront.val: 1.5;
                toBottom.val: 1.5;
            }
        }
        PhysicalBlock {
            id: blockBarFront;
            uid: "BAR_FRONT";
            title: "Wheel Bar Front";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: 0;
                onBottomToTop.val: -1.5;
                onBackToFront.val: +12;
            }
            size {
                toTop.val: 0.5;
                toLeft.val: 3.5;
                toRight.val: 3.5;
                toBack.val: 0.5;
                toFront.val: 0.5;
                toBottom.val: 0.5;
            }
        }
        PhysicalBlock {
            id: blockBarRear;
            uid: "BAR_REAR";
            title: "Wheel Bar Rear";
            color: "#3C3C3C";
            referenceBlock: blockBase;
            anchorSide: Sides.BOTTOM;
            roundedAxis: Axis.LEFT_TO_RIGHT;
            pivotPos {
                onLeftToRight.val: 0;
                onBottomToTop.val: -1.5;
                onBackToFront.val: -2;
            }
            size {
                toTop.val: 0.5;
                toLeft.val: 3.5;
                toRight.val: 3.5;
                toBack.val: 0.5;
                toFront.val: 0.5;
                toBottom.val: 0.5;
            }
        }
    }
    PhysicalBlock {
        id: blockPivot;
        uid: "PIVOT";
        title: "Pivot table";
        color: "darkred";
        referenceBlock: blockBase;
        anchorSide: Sides.TOP;
        pivotPos { // centered on base top
            onLeftToRight.val: 0;
            onBottomToTop.val: 0;
            onBackToFront.val: 0;
        }
        size {
            toLeft.val: 3;
            toRight.val: 3;
            toFront.val: 3;
            toBack.val: 3;
            toTop.val: 3;
            toBottom.val: 0;
        }
        angle { // slew
            yaw {
                val: 0;
                min: 0;
                max: 359;
                loop: true;
                accelLimit: 5; // +-5°/s/s
                decelLimit: 10; // +-10°/s/s
            }
        }
    }
    PhysicalBlock {
        id: blockArm;
        uid: "ARM";
        title: "Robotic Arm";
        color: "darkgreen";
        referenceBlock: blockPivot;
        anchorSide: Sides.TOP;
        pivotPos { // centered on pivot top
            onLeftToRight.val: 0;
            onBottomToTop.val: 0;
            onBackToFront.val: 0;
        }
        size { // extension to top
            toLeft.val: 1;
            toRight.val: 1;
            toFront {
                val: 20;
                min: 5;
                max: 35;
            }
            toBack.val: 1;
            toBottom.val: 1;
            toTop.val: 1;
        }
        angle { // up/down
            pitch {
                val: 45;
                min: 0;
                max: 90;
            }
        }
    }
    PhysicalBlock {
        id: blockExtraArm;
        uid: "EXTRA_ARM";
        title: "Robotic Extra Arm";
        color: "darkblue";
        anchorSide: Sides.FRONT;
        referenceBlock: blockArm;
        pivotPos { // centered on pivot top
            onLeftToRight.val: 0;
            onBottomToTop.val: 0;
            onBackToFront.val: 0;
        }
        size { // extension to top
            toLeft.val: 0.5;
            toRight.val: 0.5;
            toFront.val: 10;
            toBack.val: 0.5;
            toBottom.val: 0.5;
            toTop.val: 0.5;
        }
        angle { // up/down
            pitch {
                val: -45;
                min: -45;
                max: +45;
            }
        }
        absoluteAngle {
            pitch {
                min: -360;
                max: +360;
            }
        }
    }
    PhysicalMarker {
        id: markerTip;
        uid: "TIP";
        anchorSide: Sides.FRONT; // centered on extra arm end
        referenceBlock: blockExtraArm;
        relativePos {
            onLeftToRight.val: 0;
            onBottomToTop.val: 0;
            onBackToFront.val: 0;
        }
        absolutePos.onBottomToTop.uid: "ALTITUDE";
    }

    /// DASHBOARD

    Dashboard {
        uid: "CTRL_PANEL";
        title: "Control Panel";
        width: 800;
        height: 480;

        NumberValueDisplay {
            id: display1;
            size: 60;
            unit: "m";
            value: Shared.intToFloat (sensorArmElev.valPhy, sensorArmElev.decimals);
            legend: "Altitude";
            digits: 2;
            decimals: sensorArmElev.decimals;
            anchors {
                top: parent.top;
                left: parent.left;
                margins: 20;
            }
        }
        OnOffSwitcher {
            id: switcher1;
            side: Borders.LEFT;
            value: sensorAuth.value;
            legend: "Authorize movements";
            anchors {
                top: led1.bottom;
                right: jauge1.left;
                margins: 20;
            }
            onValueChanged: { sensorAuth.value = value; }
        }
        LedLight {
            id: led1;
            active: light.value;
            legend: "Machine moving";
            anchors {
                top: parent.top;
                right: jauge1.left;
                margins: 20;
            }
        }
        CircleButton {
            id: button1;
            label: "START\nSTOP";
            anchors {
                top: switcher1.bottom;
                right: jauge1.left;
                margins: 20;
            }
            onClicked: { }
        }
        RotativeKnob {
            id: knob1;
            size: 120;
            legend: "Cant angle";
            minValue: blockBase.angle.roll.min;
            maxValue: blockBase.angle.roll.max;
            anchors {
                top: button1.bottom;
                right: jauge1.left;
                margins: 20;
            }
            onValueChanged: { blockBase.angle.roll.val = value; }

            Binding on value {
                value: blockBase.angle.roll.val;
            }
        }
        LinearGauge {
            id: jauge1;
            legend: "Extension speed";
            value: (motorExtOut.valSpeed > 0
                    ? motorExtOut.valSpeed
                    : 0);
            minValue: 0;
            maxValue: motorExtOut.maxSpeed;
            anchors {
                top: parent.top;
                right: parent.right;
                bottom: parent.bottom;
                margins: 50;
            }
        }
        TicksJauge {
            id: jauge3;
            legend: "Boom angle speed";
            value: motorAngUp.valSpeed;
            minValue: motorAngUp.minSpeed;
            maxValue: motorAngUp.maxSpeed;
            anchors {
                top: display1.bottom;
                left: parent.left;
                margins: 20;
            }
        }
        CircularGauge {
            id: jauge2;
            legend: "Slewing speed";
            anchors.centerIn: parent;
        }
    }
    Dashboard {
        uid: "OSCILLO";
        title: "Oscilloscope";
        width: 800;
        height: 480;

        Oscilloscope {
            lineSize: 2;
            timeFrame: 5000;
            samplingInterval: 25;
            anchors.fill: parent;

            PlotParams {
                unit: "m";
                legend: "Extension sensor";
                lineColor: "darkcyan";
                currentValue: Shared.intToFloat (sensorExt.valPhy, sensorExt.decimals);
                minValue: Shared.intToFloat (sensorExt.minPhy, sensorExt.decimals);
                maxValue: Shared.intToFloat (sensorExt.maxPhy, sensorExt.decimals);
                decimals: 0;
                markValues: [5, 10, 15, 20, 25, 30];
            }
            PlotParams {
                unit: "°";
                legend: "Slewing sensor";
                lineColor: "darkgreen";
                currentValue: Shared.intToFloat (sensorSlewAng.valPhy, sensorSlewAng.decimals);
                minValue: Shared.intToFloat (sensorSlewAng.minPhy, sensorSlewAng.decimals);
                maxValue: Shared.intToFloat (sensorSlewAng.maxPhy, sensorSlewAng.decimals);
                decimals: 0;
                markValues: [90, 180, 270];
            }
            PlotParams {
                unit: "°";
                legend: "Angle sensor";
                lineColor: "darkred";
                currentValue: Shared.intToFloat (sensorAngUpDown.valPhy, sensorAngUpDown.decimals);
                minValue: Shared.intToFloat (sensorAngUpDown.minPhy, sensorAngUpDown.decimals);
                maxValue: Shared.intToFloat (sensorAngUpDown.maxPhy, sensorAngUpDown.decimals);
                decimals: 0;
                markValues: [15, 45];
            }
        }
    }
    Dashboard {
        uid: "DUMB";
        width: 800;
        height: 480;
    }
}
