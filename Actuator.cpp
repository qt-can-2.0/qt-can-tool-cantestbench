
#include "Actuator.h"

#include "IO.h"
#include "Physics.h"
#include "MathUtils.h"
#include "Manager.h"
#include "Link.h"

AbstractActuator::AbstractActuator (const ObjectType::Type type, QObject * parent)
    : BasicObject (ObjectFamily::ACTUATOR, parent)
    , m_type (type)
{ }

AbstractActuator::~AbstractActuator (void) { }

AnalogActuator::AnalogActuator (QObject * parent)
    : AbstractActuator (ObjectType::ANALOG, parent)
    , m_valRaw (0)
    , m_minRaw (0)
    , m_maxRaw (5000)
    , m_minSpeed (0)
    , m_maxSpeed (1)
    , m_valSpeed (0)
    , m_percents (0)
    , m_decimals (0)
    , m_useSplitPoint (false)
    , m_sourceLink (Q_NULLPTR)
    , m_targetLink (Q_NULLPTR)
{
    connect (this, &AnalogActuator::valRawChanged, this, &AnalogActuator::refreshValSpeed);
    connect (this, &AnalogActuator::minRawChanged, this, &AnalogActuator::refreshValSpeed);
    connect (this, &AnalogActuator::maxRawChanged, this, &AnalogActuator::refreshValSpeed);
    connect (this, &AnalogActuator::valRawChanged, this, &AnalogActuator::refreshPercents);
    connect (this, &AnalogActuator::minRawChanged, this, &AnalogActuator::refreshPercents);
    connect (this, &AnalogActuator::maxRawChanged, this, &AnalogActuator::refreshPercents);
    Manager::instance ().registerObject (this);
}

AnalogActuator::~AnalogActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void AnalogActuator::onComponentCompleted (void) {
    AbstractActuator::onComponentCompleted ();
    if (m_sourceLink) {
        m_sourceLink->set_target (this);
    }
    if (m_targetLink) {
        m_targetLink->set_source (this);
    }
    Manager::instance ().intializeObject (this);
}

void AnalogActuator::refreshValSpeed (void) {
    update_valSpeed (MathUtils::convert (m_valRaw, m_minRaw, m_maxRaw, m_minSpeed, m_maxSpeed));
}

void AnalogActuator::refreshPercents (void) {
    if (m_useSplitPoint) {
        const int midRaw = (m_maxRaw + m_minRaw) / 2;
        if (m_valRaw > midRaw) { // positive value
            update_percents (MathUtils::convert (m_valRaw, midRaw, m_maxRaw, 0, +100));
        }
        else if (m_valRaw < midRaw) { // negative value
            update_percents (MathUtils::convert (m_valRaw, midRaw, m_minRaw, 0, -100));
        }
        else {
            update_percents (0);
        }
    }
    else {
        update_percents (MathUtils::convert (m_valRaw, m_minRaw, m_maxRaw, 0, 100));
    }
}

QJsonObject AnalogActuator::exportState (void) const {
    return QJsonObject {
        { "valRaw", m_valRaw },
    };
}

DigitalActuator::DigitalActuator (QObject * parent)
    : AbstractActuator (ObjectType::DIGITAL, parent)
    , m_value (false)
    , m_trueLabel ("ON")
    , m_falseLabel ("OFF")
    , m_sourceLink (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

DigitalActuator::~DigitalActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void DigitalActuator::onComponentCompleted (void) {
    AbstractActuator::onComponentCompleted ();
    if (m_sourceLink) {
        m_sourceLink->set_target (this);
    }
    Manager::instance ().intializeObject (this);
}

QJsonObject DigitalActuator::exportState (void) const {
    return QJsonObject {
        { "value", m_value },
    };
}

HybridActuator::HybridActuator (QObject * parent)
    : AbstractActuator (ObjectType::HYBRID, parent)
    , m_value (false)
    , m_ratedSpeed (0)
    , m_valSpeed (0)
    , m_decimals (0)
    , m_unit ("")
    , m_sourceLink (Q_NULLPTR)
    , m_targetLink (Q_NULLPTR)
{
    connect (this, &HybridActuator::valueChanged,      this, &HybridActuator::refreshValSpeed);
    connect (this, &HybridActuator::ratedSpeedChanged, this, &HybridActuator::refreshValSpeed);
    connect (this, &HybridActuator::decimalsChanged,   this, &HybridActuator::refreshValSpeed);
    Manager::instance ().registerObject (this);
}

HybridActuator::~HybridActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void HybridActuator::onComponentCompleted (void) {
    AbstractActuator::onComponentCompleted ();
    if (m_sourceLink) {
        m_sourceLink->set_target (this);
    }
    if (m_targetLink) {
        m_targetLink->set_source (this);
    }
    Manager::instance ().intializeObject (this);
}

void HybridActuator::refreshValSpeed (void) {
    update_valSpeed (m_value ? m_ratedSpeed : 0);
}
