
#include "IdentifiableObject.h"

#include "Manager.h"
#include "CppUtils.h"

#include <QStringBuilder>

BasicObject::BasicObject (const ObjectFamily::Type family, QObject * parent)
    : QObject (parent)
    , m_family (family)
{ }

BasicObject::~BasicObject (void) { }

void BasicObject::classBegin (void) { }

void BasicObject::componentComplete (void) {
    onComponentCompleted ();
}

void BasicObject::onComponentCompleted (void) { }

void BasicObject::refreshPath (void) {
    QString tmp;
    if (!m_uid.isEmpty ()) {
        BasicObject * ancestor = qobject_cast<BasicObject *> (parent ());
        if (ancestor != Q_NULLPTR && !ancestor->get_path ().isEmpty ()) {
            tmp += ancestor->get_path ();
            tmp += '/';
        }
        tmp += m_uid;
    }
    update_path (tmp);
}

QJsonObject BasicObject::exportState (void) const {
    return QJsonObject { };
}

void BasicObject::updateState (const QJsonObject & values) {
    for (KeyValueRange<QJsonObject>::It itVal : keyValueRange (values)) {
        const QByteArray propName = itVal.key ().toLocal8Bit ();
        if (BasicObject * subObj = property (propName.constData ()).value<BasicObject *> ()) {
            subObj->updateState (itVal.value ().toObject ());
        }
        else {
            setProperty (propName.constData (), itVal.value ().toVariant ());
        }
    }
}

ObjectsGroup::ObjectsGroup (QObject * parent)
    : BasicObject (ObjectFamily::GROUP, parent)
    , m_subObjects (this)
{
    Manager::instance ().registerObject (this);
}

ObjectsGroup::~ObjectsGroup (void) {
    Manager::instance ().unregisterObject (this);
}

void ObjectsGroup::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

AbstractObjectListModel::AbstractObjectListModel (QObject * parent) : QAbstractListModel (parent) { }

AbstractObjectListModel::~AbstractObjectListModel (void) { }

int AbstractObjectListModel::getCount (void) const {
    return implCount ();
}

void AbstractObjectListModel::insert (QObject * object, const int idx) {
    doInsertAt (object, idx);
}

void AbstractObjectListModel::append (QObject * object) {
    doInsertAt (object, implCount ());
}

void AbstractObjectListModel::prepend (QObject * object) {
    doInsertAt (object, 0);
}

void AbstractObjectListModel::remove (QObject * object) {
    const int idx = implIndexOf (object);
    if (implValid (idx)) {
        beginRemoveRows (QModelIndex (), idx, idx);
        doUnregisterItem (object);
        implRemove (object);
        endRemoveRows ();
        emit countChanged ();
        emit itemRemoved (object);
    }
}

void AbstractObjectListModel::clear (void) {
    beginResetModel ();
    const int loops = implCount ();
    for (int idx = 0; idx < loops; ++idx) {
        doUnregisterItem (implGet (idx));
    }
    implClear ();
    endResetModel ();
    emit countChanged ();
}

QObject * AbstractObjectListModel::get (const int idx) const {
    return implGet (idx);
}

void AbstractObjectListModel::doInsertAt (QObject * object, const int idx) {
    if (implAccept (object) && !implContains (object) && (implValid (idx) || idx == implCount ())) {
        beginInsertRows (QModelIndex (), idx, idx);
        doRegisterItem (object);
        implInsert (object, idx);
        endInsertRows ();
        emit countChanged ();
        emit itemAdded (object);
    }
}

void AbstractObjectListModel::doRegisterItem (QObject * object) {
    if (object) {
        connect (object, &QObject::destroyed, this, &AbstractObjectListModel::onItemDestroyed);
    }
}

void AbstractObjectListModel::doUnregisterItem (QObject * object) {
    if (object) {
        disconnect (object, &QObject::destroyed, this, &AbstractObjectListModel::onItemDestroyed);
    }
}

void AbstractObjectListModel::onItemDestroyed (QObject * object) {
    remove (object);
}
