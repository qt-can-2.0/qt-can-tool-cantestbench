
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFileInfo>

#include "QtQmlTricksPlugin_UiElements.h"
#include "QtQmlTricksPlugin_SmartDataModels.h"

#include "CanDriverQmlWrapper.h"

#include "SharedObject.h"
#include "Manager.h"

int main (int argc, char * argv []) {
    //qputenv ("QML_DISABLE_DISK_CACHE", "1");
    QGuiApplication app (argc, argv);
    QQmlApplicationEngine engine (&app);
    registerQtQmlTricksUiElements (&engine);
    registerQtQmlTricksSmartDataModel (&engine);
    CanDriverWrapper::registerQmlTypes (&engine);
    SharedObject::registerQmlTypes (&engine);
    Manager::instance ().qmlEngine = &engine;
    engine.load (QUrl ("qrc:///splash_testbench.qml"));
    const QList<QObject *> rootObjects = engine.rootObjects ();
    if (!rootObjects.isEmpty ()) {
        const QStringList argsList = app.arguments ().mid (1);
        for (const QString & arg : argsList) {
            const QFileInfo info (arg);
            if (info.exists () && info.isFile () && info.suffix ().toLower () == "qml") {
                Manager::instance ().load (QUrl::fromLocalFile (info.absoluteFilePath ()).toString ());
                break;
            }
        }
        return app.exec ();
    }
    else {
        return -1;
    }
}

