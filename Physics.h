#ifndef COORD3AXIS_H
#define COORD3AXIS_H

#include <QObject>
#include <QVector3D>
#include <QMatrix4x4>
#include <QColor>
#include <QtMath>

#include "IdentifiableObject.h"

QML_ENUM_CLASS (Axis,
                UNKNOWN_AXIS = -1,
                LEFT_TO_RIGHT, // x
                BACK_TO_FRONT, // y
                BOTTOM_TO_TOP, // z
                NUMBER_OF_AXIS)

QML_ENUM_CLASS (Sides,
                UNKNOWN_SIDE = -1,
                LEFT, // -x
                RIGHT, // +x
                BACK, // -y
                FRONT, // +y
                BOTTOM, // -z
                TOP, // +z
                NUMBER_OF_SIDES)

QML_ENUM_CLASS (Corners,
                UNKNOWN_VERTEX = -1,
                FRONT_TOP_RIGHT,
                FRONT_TOP_LEFT,
                FRONT_BOTTOM_RIGHT,
                FRONT_BOTTOM_LEFT,
                BACK_TOP_RIGHT,
                BACK_TOP_LEFT,
                BACK_BOTTOM_RIGHT,
                BACK_BOTTOM_LEFT,
                NUMBER_OF_CORNERS)

struct Cube {
    Q_GADGET

public:
    enum VertexType {
        BASE_VERTEX      = 0,
        TRANS_VERTEX     = 1,
        NUMBER_OF_VERTEX = 2,
    };

    enum Circles {
        NEAR_CIRCLE       = 0,
        FAR_CIRCLE        = 1,
        NUMBER_OF_CIRCLES = 2,
    };

    const QVector3D vectorOrigin;
    const QVector3D vectorUpward;
    const QVector3D vectorForward;

    const QVector3D vertexFrontTopRight;
    const QVector3D vertexFrontTopLeft;
    const QVector3D vertexFrontBottomRight;
    const QVector3D vertexFrontBottomLeft;
    const QVector3D vertexBackTopRight;
    const QVector3D vertexBackTopLeft;
    const QVector3D vertexBackBottomRight;
    const QVector3D vertexBackBottomLeft;

    static const int CIRCLE_POINTS = 16;
    float cosinus [CIRCLE_POINTS];
    float sinus   [CIRCLE_POINTS];

    explicit Cube (void)
        : vectorOrigin           ( 0,  0,  0)
        , vectorUpward           ( 0,  0, +1)
        , vectorForward          ( 0, +1,  0)
        , vertexFrontTopRight    (+1, +1, +1)
        , vertexFrontTopLeft     (-1, +1, +1)
        , vertexFrontBottomRight (+1, +1, -1)
        , vertexFrontBottomLeft  (-1, +1, -1)
        , vertexBackTopRight     (+1, -1, +1)
        , vertexBackTopLeft      (-1, -1, +1)
        , vertexBackBottomRight  (+1, -1, -1)
        , vertexBackBottomLeft   (-1, -1, -1)
    {
        const qreal CIRCLE_DIV = (M_PI * 2.0 / qreal (CIRCLE_POINTS));
        for (int idx = 0; idx < CIRCLE_POINTS; ++idx) {
            const qreal angle = (qreal (idx) * CIRCLE_DIV);
            cosinus [idx] = float (qCos (angle));
            sinus   [idx] = float (qSin (angle));
        }
    }
};

class PhysicalValue : public BasicObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (bool,  loop)
    QML_WRITABLE_VAR_PROPERTY (float, val)
    QML_WRITABLE_VAR_PROPERTY (float, min)
    QML_WRITABLE_VAR_PROPERTY (float, max)
    QML_WRITABLE_VAR_PROPERTY (float, accelLimit)
    QML_WRITABLE_VAR_PROPERTY (float, decelLimit)
    QML_READONLY_VAR_PROPERTY (float, currentSpeed)
    QML_READONLY_VAR_PROPERTY (float, assignedSpeed)

public:
    explicit PhysicalValue (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalValue (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QJsonObject exportState (void) const Q_DECL_FINAL;

    void move (const int time);
};

class PhysicalPoint : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, onLeftToRight)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, onBackToFront)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, onBottomToTop)

public:
    explicit PhysicalPoint (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalPoint (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class PhysicalSize : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toLeft)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toRight)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toBottom)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toTop)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toBack)
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, toFront)

public:
    explicit PhysicalSize (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalSize (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QVector3D point (const bool leftOrRight, const bool backOrFront, const bool bottomOrTop) const;

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class PhysicalAngle : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, yaw)   // z
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, pitch) // x
    QML_CONSTANT_PTR_PROPERTY (PhysicalValue, roll)  // y

public:
    explicit PhysicalAngle (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalAngle (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class PhysicalBlock : public BasicObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY    (bool,          visible)
    QML_WRITABLE_CSTREF_PROPERTY (QColor,        color)
    QML_WRITABLE_PTR_PROPERTY    (PhysicalBlock, referenceBlock)
    QML_WRITABLE_VAR_PROPERTY    (Sides::Type,   anchorSide) // side
    QML_WRITABLE_VAR_PROPERTY    (Axis::Type,    roundedAxis) // axis that must be round instead of square
    QML_CONSTANT_PTR_PROPERTY    (PhysicalPoint, pivotPos) // position of anchor / rotation center
    QML_CONSTANT_PTR_PROPERTY    (PhysicalSize,  size)  // dimensions on 6 sides
    QML_CONSTANT_PTR_PROPERTY    (PhysicalAngle, angle) // rotation on 3 axis
    QML_CONSTANT_PTR_PROPERTY    (PhysicalAngle, absoluteAngle) // yaw / pitch / roll

public:
    explicit PhysicalBlock (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalBlock (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QMatrix4x4 matrixRotationX;
    QMatrix4x4 matrixRotationY;
    QMatrix4x4 matrixRotationZ;
    QMatrix4x4 matrixTranslation;

    QVector<PhysicalBlock *> ancestors;

    QVector3D sidesVectors [Sides::NUMBER_OF_SIDES];

    QVector3D blockVertexList [Corners::NUMBER_OF_CORNERS][Cube::NUMBER_OF_VERTEX];

    QVector3D cylinderVertexList [Cube::CIRCLE_POINTS][Cube::NUMBER_OF_CIRCLES][Cube::NUMBER_OF_VERTEX];

    void refreshAncestors         (void);
    void refreshVertexList        (void);
    void refreshMatrixRotationX   (void);
    void refreshMatrixRotationY   (void);
    void refreshMatrixRotationZ   (void);
    void refreshMatrixTranslation (void);

    QJsonObject exportState (void) const Q_DECL_FINAL;

signals:
    void needsRedraw (void);

private:
    bool m_ready;
};

class PhysicalMarker : public BasicObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (Sides::Type,   anchorSide) // side
    QML_WRITABLE_PTR_PROPERTY (PhysicalBlock, referenceBlock)
    QML_CONSTANT_PTR_PROPERTY (PhysicalPoint, relativePos) // position on block
    QML_CONSTANT_PTR_PROPERTY (PhysicalPoint, absolutePos) // position on world

public:
    explicit PhysicalMarker (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalMarker (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QVector<PhysicalBlock *> ancestors;

    void refreshAncestors (void);
};

class PhysicalWorld : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (PhysicalSize, bounds) // boundaries on 6 sides

public:
    explicit PhysicalWorld (QObject * parent = Q_NULLPTR);
    virtual ~PhysicalWorld (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QVector<QVector3D> vertices;
    QVector<QVector3D> colors;

    void physicsDirty   (void);
    void processPhysics (void);

signals:
    void needsRedraw (void);

protected:
    inline void addQuad (const QVector3D & p1,
                         const QVector3D & p2,
                         const QVector3D & p3,
                         const QVector3D & p4,
                         const QColor & color);

    inline void addTriangle (const QVector3D & p1,
                             const QVector3D & p2,
                             const QVector3D & p3,
                             const QColor & color);

    inline QVector3D vectorFromColor (const QColor & col) const;

private:
    bool m_dirty;
};

#endif // COORD3AXIS_H
