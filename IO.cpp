
#include "IO.h"

#include "Sensor.h"
#include "Actuator.h"
#include "MathUtils.h"
#include "Manager.h"

AbstractIO::AbstractIO (const ObjectType::Type type, const ObjectDirection::Type direction, QObject * parent)
    : BasicObject (ObjectFamily::IO, parent)
    , m_type (type)
    , m_direction (direction)
{ }

AbstractIO::~AbstractIO (void) { }

void AbstractIO::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
}

AbstractAnalogIO::AbstractAnalogIO (const ObjectDirection::Type direction, QObject * parent)
    : AbstractIO (ObjectType::ANALOG, direction, parent)
    , m_valRaw (0)
    , m_minRaw (0)
    , m_maxRaw (5000)
    , m_resolutionInPoints (8192)
{ }

QJsonObject AbstractAnalogIO::exportState (void) const {
    return QJsonObject {
        { "valRaw", m_valRaw },
    };
}

void AbstractAnalogIO::onComponentCompleted (void) {
    AbstractIO::onComponentCompleted ();
}

int AbstractAnalogIO::getValueInPoints (void) {
    return MathUtils::convert (get_valRaw (), get_minRaw (), get_maxRaw (), 0, get_resolutionInPoints () -1);
}

int AbstractAnalogIO::getValueInPercents (void) {
    return MathUtils::convert (get_valRaw (), get_minRaw (), get_maxRaw (), 0, 100);
}
void AbstractAnalogIO::setValueInPoints (const int points) {
    set_valRaw (MathUtils::convert (points, 0, get_resolutionInPoints () -1, get_minRaw (), get_maxRaw ()));
}

void AbstractAnalogIO::setValueInPercents (const int percents) {
    set_valRaw (MathUtils::convert (percents, 0, 100, get_minRaw (), get_maxRaw ()));
}

AbstractDigitalIO::AbstractDigitalIO (const ObjectDirection::Type direction, QObject * parent)
    : AbstractIO (ObjectType::DIGITAL, direction, parent)
    , m_value (false)
{ }

QJsonObject AbstractDigitalIO::exportState (void) const {
    return QJsonObject {
        { "value", m_value },
    };
}

void AbstractDigitalIO::onComponentCompleted (void) {
    AbstractIO::onComponentCompleted ();
}

AnalogInput::AnalogInput (QObject * parent)
    : AbstractAnalogIO (ObjectDirection::INPUT, parent)
{
    Manager::instance ().registerObject (this);
}

AnalogInput::~AnalogInput (void) {
    Manager::instance ().unregisterObject (this);
}

void AnalogInput::onComponentCompleted (void) {
    Manager::instance ().intializeObject (this);
}

AnalogOutput::AnalogOutput (QObject * parent)
    : AbstractAnalogIO (ObjectDirection::OUTPUT, parent)
{
    Manager::instance ().registerObject (this);
}

AnalogOutput::~AnalogOutput (void) {
    Manager::instance ().unregisterObject (this);
}

void AnalogOutput::onComponentCompleted (void) {
    Manager::instance ().intializeObject (this);
}

DigitalInput::DigitalInput (QObject * parent)
    : AbstractDigitalIO (ObjectDirection::INPUT, parent)
{
    Manager::instance ().registerObject (this);
}

DigitalInput::~DigitalInput (void) {
    Manager::instance ().unregisterObject (this);
}

void DigitalInput::onComponentCompleted (void) {
    Manager::instance ().intializeObject (this);
}

DigitalOutput::DigitalOutput (QObject * parent)
    : AbstractDigitalIO (ObjectDirection::OUTPUT, parent)
{
    Manager::instance ().registerObject (this);
}

DigitalOutput::~DigitalOutput (void) {
    Manager::instance ().unregisterObject (this);
}

void DigitalOutput::onComponentCompleted (void) {
    Manager::instance ().intializeObject (this);
}
