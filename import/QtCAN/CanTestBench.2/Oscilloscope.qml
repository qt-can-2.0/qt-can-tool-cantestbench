import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    id: base;
    implicitWidth: 400;
    implicitHeight: 300;

    property int timeFrame        : 5000;
    property int samplingInterval : 20;

    property int lineSize : 2;

    property bool running : false;

    property list<PlotParams> plotsParams;

    default property alias content : base.plotsParams;

    function saveAsImage (path) {
        base.grabToImage (function (result) {
            result.saveToFile (path);
        });
    }

    function saveAsCsv (path) {
        var csv = "";
        var listOfLists = [];
        var maxCount = 0;
        for (var graphIdx = 0; graphIdx < plotsParams.length; ++graphIdx) {
            var params = plotsParams [graphIdx];
            csv += params.legend;
            csv += ";";
            var tmp = repeaterGraphers.itemAt (graphIdx).getValues ();
            if (tmp.length > maxCount) {
                maxCount = tmp.length;
            }
            listOfLists.push (tmp);
        }
        csv += "\n";
        for (var valueIdx = 0; valueIdx < maxCount; ++valueIdx) {
            for (var listIdx = 0; listIdx < listOfLists.length; ++listIdx) {
                var val = listOfLists [listIdx][valueIdx];
                if (val !== undefined) {
                    csv += val.toString ();
                }
                csv += ";";
            }
            csv += "\n";
        }
        FileSystem.writeTextFile (path, csv);
    }

    StretchRowContainer {
        id: layoutHeader;
        spacing: Style.spacingNormal;
        anchors {
            top: parent.top;
            left: frame.left;
            right: frame.right;
            topMargin: Style.spacingBig;
        }

        TextButton {
            text: "Record";
            icon: SymbolLoader {
                size: Style.fontSizeNormal;
                color: Style.colorError;
                symbol: AbstractSymbol {
                    id: circle;

                    Rectangle {
                        color: circle.color;
                        width: circle.size;
                        height: circle.size;
                        radius: (circle.size * 0.5);
                        antialiasing: true;
                        anchors.centerIn: parent;
                    }
                }
            }
            checked: running;
            padding: Style.spacingNormal;
            textColor: Style.colorForeground;
            autoColorIcon: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { running = !running; }
        }
        TextButton {
            text: "Reset";
            icon: SymbolLoader {
                size: Style.fontSizeNormal;
                color: Style.colorError;
                symbol: Style.symbolCross;
            }
            padding: Style.spacingNormal;
            textColor: Style.colorForeground;
            autoColorIcon: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: {
                for (var idx = 0; idx < repeaterGraphers.count; idx++) {
                    repeaterGraphers.itemAt (idx).reset ();
                }
            }
        }
        TextButton {
            text: "Dump";
            padding: Style.spacingNormal;
            textColor: Style.colorForeground;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onClicked: { compoDumpDialog.createObject (Introspector.window (base)); }

            Component {
                id: compoDumpDialog;

                ModalDialog {
                    title: qsTr ("Dump capture to disk");
                    minWidth: fsSelector.implicitWidth;
                    maxWidth: fsSelector.implicitHeight;
                    onButtonClicked: {
                        switch (buttonType) {
                        case buttonOk:
                            if (fsSelector.currentPath !== "") {
                                switch (comboFormat.currentKey) {
                                case "IMG":
                                    base.saveAsImage (fsSelector.currentPath);
                                    hide ();
                                    break;
                                case "CSV":
                                    base.saveAsCsv (fsSelector.currentPath);
                                    hide ();
                                    break;
                                default:
                                    shake ();
                                    break;
                                }
                            }
                            else {
                                shake ();
                            }
                            break;
                        case buttonCancel:
                            hide ();
                            break;
                        }
                    }

                    FileSelector {
                        id: fsSelector;
                        selectionType: (selectFile | selectAllowNew);
                        implicitWidth: 400;
                        implicitHeight: 300;
                    }
                    StretchRowContainer {
                        spacing: Style.spacingNormal;

                        TextLabel {
                            text: qsTr ("Saving format :");
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                        ComboList {
                            id: comboFormat;
                            implicitWidth: -1;
                            model: [
                                { "key" : "IMG", "value" : qsTr ("Image of Oscilloscope") },
                                { "key" : "CSV", "value" : qsTr ("CSV with captured values") },
                            ];
                            delegate: ComboListDelegateForModelDataAttributes { }
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                    }
                }
            }
        }
        Stretcher { }
        TextLabel {
            text: "Rate :";
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        NumberBox {
            minValue: 10;
            maxValue: 1000;
            showButtons: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { base.samplingInterval = value.toFixed (); }

            Binding on value { value: base.samplingInterval; }
        }
        TextLabel {
            text: "ms";
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        Stretcher { }
        TextLabel {
            text: "Duration :";
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
        NumberBox {
            minValue: 1;
            maxValue: 60;
            showButtons: false;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
            onEdited: { base.timeFrame = (value.toFixed () * 1000); }

            Binding on value { value: Math.ceil (base.timeFrame / 1000); }
        }
        TextLabel {
            text: "s";
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
    Row {
        id: layoutValuesScale;
        spacing: Style.spacingNormal;
        anchors {
            top: frame.top;
            right: parent.right;
            rightMargin: Style.spacingBig;
            bottom: frame.bottom;
        }

        Repeater {
            model: plotsParams;
            delegate: Item {
                id: holder;
                width: Math.max (lblMin.contentWidth, lblMax.contentWidth) + Style.spacingSmall * 3 + Style.fontSizeSmall;
                ExtraAnchors.verticalFill: parent;

                readonly property PlotParams params : modelData;

                TextLabel {
                    text: holder.params.legend;
                    color: holder.params.lineColor;
                    rotation: -90;
                    font.pixelSize: Style.fontSizeSmall;
                    anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                    anchors.horizontalCenter: line.left;
                    anchors.horizontalCenterOffset: (-0.5 * font.pixelSize);
                }
                Line {
                    id: line;
                    color: holder.params.lineColor;
                    width: (Style.lineSize / Shared.manager.dashboardZoom);
                    anchors.leftMargin: Style.fontSizeSmall;
                    ExtraAnchors.leftDock: parent;
                }
                Line {
                    color: holder.params.lineColor;
                    width: Style.spacingNormal;
                    height: (Style.lineSize / Shared.manager.dashboardZoom);
                    anchors.left: line.left;
                    anchors.bottom: line.bottom;

                    TextLabel {
                        id: lblMin;
                        text: (holder.params.minValue.toFixed (holder.params.decimals) + " " + holder.params.unit);
                        color: holder.params.lineColor;
                        anchors.left: parent.right;
                        anchors.margins: Style.spacingSmall;
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                    }
                }
                Line {
                    color: holder.params.lineColor;
                    width: Style.spacingNormal;
                    height: (Style.lineSize / Shared.manager.dashboardZoom);
                    anchors.top: line.top;
                    anchors.left: line.left;

                    TextLabel {
                        id: lblMax;
                        text: (holder.params.maxValue.toFixed (holder.params.decimals) + " " + holder.params.unit);
                        color: holder.params.lineColor;
                        anchors.left: parent.right;
                        anchors.margins: Style.spacingSmall;
                        anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                    }
                }
                Repeater {
                    model: holder.params.markValues;
                    delegate: Line {
                        color: holder.params.lineColor;
                        width: Style.spacingNormal;
                        height: (Style.lineSize / Shared.manager.dashboardZoom);
                        anchors.left: line.left;
                        anchors.bottom: line.bottom;
                        anchors.bottomMargin: ((line.height - height)
                                               * (modelData - holder.params.minValue)
                                               / (holder.params.maxValue - holder.params.minValue));

                        TextLabel {
                            text: (modelData.toFixed (holder.params.decimals) + " " + holder.params.unit);
                            color: holder.params.lineColor;
                            anchors.left: parent.right;
                            anchors.margins: Style.spacingSmall;
                            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
                        }
                    }
                }
            }
        }
    }
    Column {
        id: layoutTimeScale;
        spacing: Style.spacingSmall;
        anchors {
            left: frame.left;
            right: frame.right;
            bottom: parent.bottom;
            bottomMargin: Style.spacingBig;
        }

        TextLabel {
            text: qsTr ("Time");
            color: Style.colorBorder;
            font.pixelSize: Style.fontSizeSmall;
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
        }
        Item {
            height: Style.spacingNormal;
            ExtraAnchors.horizontalFill: parent;

            Line {
                color: Style.colorBorder;
                width: (Style.lineSize / Shared.manager.dashboardZoom);
                ExtraAnchors.topDock: parent;
            }
            Line {
                color: Style.colorBorder;
                width: (Style.lineSize / Shared.manager.dashboardZoom);
                ExtraAnchors.leftDock: parent;
            }
            Line {
                color: Style.colorBorder;
                width: (Style.lineSize / Shared.manager.dashboardZoom);
                ExtraAnchors.rightDock: parent;
            }
            Line {
                color: Style.colorBorder;
                width: (Style.lineSize / Shared.manager.dashboardZoom);
                anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                ExtraAnchors.verticalFill: parent;
            }
        }
        Item {
            height: Style.fontSizeNormal;
            ExtraAnchors.horizontalFill: parent;

            TextLabel {
                text: "%1 ms ago".arg (timeFrame);
                color: Style.colorBorder;
                anchors.left: parent.left;
            }
            TextLabel {
                text: "%1 ms ago".arg (Math.round (timeFrame / 2));
                color: Style.colorBorder;
                anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
            }
            TextLabel {
                text: "now";
                color: Style.colorBorder;
                anchors.right: parent.right;
            }
        }
    }
    Rectangle {
        color: Style.colorEditable;
        border {
            width: (Style.lineSize / Shared.manager.dashboardZoom);
            color: Style.colorBorder;
        }
        anchors.fill: frame;
        anchors.margins: -border.width;
    }
    MouseArea {
        id: frame;
        hoverEnabled: true;
        anchors {
            top: layoutHeader.bottom;
            left: parent.left;
            right: layoutValuesScale.left;
            bottom: layoutTimeScale.top;
            topMargin: Style.spacingNormal;
            leftMargin: Style.spacingBig;
            rightMargin: Style.spacingNormal;
            bottomMargin: Style.spacingBig;
        }
        onPositionChanged: {
            if (containsMouse && markerState === stateMovingMarker) {
                markerRatio = (mouse.x / width);
            }
        }
        onClicked: {
            switch (markerState) {
            case stateNoMarker:     markerState = stateMovingMarker; break;
            case stateMovingMarker: markerState = stateFixedMarker;  break;
            case stateFixedMarker:  markerState = stateNoMarker;     break;
            }
            markerRatio = (mouse.x / width);
        }

        property int markerState : stateNoMarker;
        property real markerRatio : 1;

        readonly property int stateNoMarker     : 0;
        readonly property int stateMovingMarker : 1;
        readonly property int stateFixedMarker  : 2;

        Repeater {
            id: repeaterGraphers;
            model: plotsParams;
            delegate: Grapher {
                id: grapher;
                running: base.running;
                lineSize: base.lineSize;
                lineColor: params.lineColor;
                minValue: params.minValue;
                maxValue: params.maxValue;
                currentValue: params.currentValue;
                timeFrame: base.timeFrame;
                samplingInterval: base.samplingInterval;
                markerPos: frame.markerRatio;
                antialiasing: true;
                anchors.fill: parent;

                readonly property PlotParams params : modelData;

                Item {
                    id: dot;
                    x: (grapher.width * grapher.markerPos);
                    y: (grapher.height - grapher.height * (grapher.markerVal - grapher.minValue) / (grapher.maxValue - grapher.minValue));

                    Rectangle {
                        color: Style.colorNone;
                        width: (Style.spacingNormal * 1.65 / Shared.manager.dashboardZoom);
                        height: width;
                        radius: (width * 0.5);
                        visible: (frame.markerState !== frame.stateNoMarker && !isNaN (grapher.markerVal));
                        antialiasing: true;
                        border {
                            color: grapher.params.lineColor;
                            width: (Style.lineSize / Shared.manager.dashboardZoom);
                        }
                        anchors.centerIn: parent;
                    }
                    MouseArea {
                        id: detector;
                        width: (Style.spacingNormal * 2 / Shared.manager.dashboardZoom);
                        height: width;
                        hoverEnabled: true;
                        anchors.centerIn: parent;
                    }
                    TextLabel {
                        text: (grapher.markerVal.toFixed (grapher.params.decimals) + " " + grapher.params.unit);
                        color: grapher.params.lineColor;
                        visible: detector.containsMouse;
                        anchors.right: parent.horizontalCenter;
                        anchors.bottom: parent.verticalCenter;
                        anchors.rightMargin: (dot.x < (width + Style.spacingNormal * 2)
                                              ? (-width - Style.spacingNormal)
                                              : Style.spacingNormal);
                        anchors.bottomMargin: (dot.y < (height + Style.spacingNormal * 2)
                                               ? (-height - Style.spacingNormal)
                                               : Style.spacingNormal);
                    }
                }
            }
        }
        Line {
            id: marker;
            color: Style.colorSelection;
            width: (Style.lineSize / Shared.manager.dashboardZoom);
            visible: (frame.markerState !== frame.stateNoMarker);
            anchors.left: frame.left;
            anchors.leftMargin: (frame.markerRatio * frame.width);
            ExtraAnchors.verticalFill: frame;
        }
    }
}
