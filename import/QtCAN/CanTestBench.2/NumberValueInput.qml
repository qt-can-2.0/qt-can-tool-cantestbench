import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    id: base;
    implicitWidth: (column.width + column.anchors.margins * 2);
    implicitHeight: (column.height + column.anchors.margins * 2);

    property int    size       : 50;
    property int    decimals   : 0;
    property int    digits     : 3;
    property bool   sign       : true;
    property real   value      : 0.0;
    property real   minValue   : 0.0;
    property real   maxValue   : 100.0;
    property color  background : Style.colorWindow;
    property string unit       : "";
    property string legend     : "";

    Rectangle {
        radius: Math.round (size * 0.15);
        antialiasing: radius;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom) * 2;
            color: Style.colorBorder;
        }
        gradient: Gradient {
            GradientStop { position: 0; color: Qt.darker  (background, 1.15); }
            GradientStop { position: 1; color: Qt.lighter (background, 1.15); }
        }
        anchors.fill: parent;
    }
    Column {
        id: column;
        spacing: Style.spacingSmall;
        anchors.centerIn: parent;
        anchors.margins: Style.spacingNormal;

        Row {
            id: row;
            spacing: Style.spacingSmall;
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);

            TextLabel {
                id: lblHolder;
                text: {
                    var ret = "";
                    if (sign) {
                        ret += "-";
                    }
                    for (var digitIdx = 0; digitIdx < digits; ++digitIdx) {
                        ret += "0";
                    }
                    if (decimals > 0) {
                        ret += ".";
                        for (var decimalIdx = 0; decimalIdx < decimals; ++decimalIdx) {
                            ret += "0";
                        }
                    }
                    return ret;
                }
                color: Style.colorNone;
                font.pixelSize: size;

                TextInput {
                    id: inputValue;
                    color: (acceptableInput ? (((background.r + background.g + background.b) / 3) < 0.5 ? "white" : "black") : "red");
                    selectByMouse: true;
                    selectionColor: Style.colorSelection;
                    selectedTextColor: Style.colorEditable;
                    activeFocusOnPress: true;
                    horizontalAlignment: TextInput.AlignHCenter;
                    maximumLength: Math.max (minValue.toFixed (decimals).length,
                                             maxValue.toFixed (decimals).length);
                    validator: DoubleValidator {
                        top: base.maxValue;
                        bottom: base.minValue;
                        locale: "C";
                        decimals: base.decimals;
                        notation: DoubleValidator.StandardNotation;
                    }
                    font {
                        pixelSize: size;
                        underline: (display !== text);
                    }
                    anchors.fill: parent;
                    onDisplayChanged: { text = display; }
                    onAccepted: { value = number; }

                    property real   number  : parseFloat (text.trim ());
                    property string display : ""; Binding on display { value: (!isNaN (base.value) ? base.value.toFixed (decimals) : ""); }

                }
            }
            TextLabel {
                text: unit;
                color: inputValue.color;
                visible: (text !== "");
                font.pixelSize: (size * 0.65);
                anchors.baseline: lblHolder.baseline;
            }
        }
        TextLabel {
            text: legend;
            color: inputValue.color;
            opacity: 0.65;
            visible: (text !== "");
            font.pixelSize: (size * 0.35);
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
        }
    }
}
