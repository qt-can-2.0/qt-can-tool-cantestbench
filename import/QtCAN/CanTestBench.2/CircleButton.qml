import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

MouseArea {
    implicitWidth: size;
    implicitHeight: size;

    property int    size       : 50;
    property color  background : Style.colorClickable;
    property string label      : "";

    Rectangle {
        color: background;
        width: size;
        height: size;
        radius: (size * 0.5);
        antialiasing: radius;
        border {
            color: Style.colorBorder;
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
        }
        anchors.centerIn: parent;

        Rectangle {
            color: (pressed ? Qt.darker (background, 1.15) : Qt.lighter (background, 1.15));
            width: parent.width;
            height: parent.height;
            radius: parent.radius;
            antialiasing: parent.antialiasing;
            border.width: parent.border.width;
            border.color: parent.border.color;
            anchors {
                centerIn: parent;
                verticalCenterOffset: (pressed ? 0 : (size / -8));
            }

            TextLabel {
                text: label;
                color: (((background.r + background.g + background.b) / 3) < 0.5 ? "white" : "black");
                fontSizeMode: Text.Fit;
                minimumPixelSize: 6;
                verticalAlignment: Text.AlignVCenter;
                horizontalAlignment: Text.AlignHCenter;
                font.pixelSize: size;
                anchors {
                    fill: parent;
                    margins: (size * 0.15);
                }
            }
        }
    }
}
