import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: size;
    implicitHeight: size;

    property int    size     : 200;
    property int    step     : 5;
    property int    marks    : 20;
    property real   value    : 0;
    property real   minValue : 0;
    property real   maxValue : 100;
    property string legend   : "";

    readonly property int divisions : Math.ceil ((maxValue - minValue) / step);

    Rectangle {
        id: back;
        width: size;
        height: size;
        radius: (size * 0.5);
        antialiasing: true;
        gradient: Style.gradientIdle (Style.colorWindow);
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
            color: Style.colorBorder;
        }
        anchors.centerIn: parent;
    }
    Repeater {
        model: divisions +1;
        delegate: Item {
            width: size;
            rotation: (90 + 45) + (360 - 90) * (model.index / divisions);
            anchors.centerIn: parent;

            readonly property int  tickValue : (minValue + ((model.index / divisions) * (maxValue - minValue)));
            readonly property bool tickMark  : (tickValue % marks === 0);

            Rectangle {
                id: tick;
                color: Style.colorForeground;
                width: (tickMark ? size * 0.05 : size * 0.02);
                height: 2 * Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
                antialiasing: true;
                anchors {
                    right: parent.right;
                    margins: back.border.width;
                    verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
            }
            TextLabel {
                text: tickValue;
                visible: tickMark;
                rotation: -parent.rotation;
                anchors {
                    right: tick.left;
                    margins: (size * 0.01);
                    verticalCenter: (parent ? parent.verticalCenter : undefined);
                }
            }
        }
    }
    Rectangle {
        color: Style.colorForeground;
        width: (size * 0.10);
        height: (size * 0.10);
        radius: (size * 0.05);
        rotation: (90 + 45) + (360 - 90) * ((value - minValue) / (maxValue - minValue));
        antialiasing: true;
        anchors.centerIn: parent;

        Behavior on rotation {
            NumberAnimation {
                easing.type: Easing.OutQuad;
                duration: 350;
            }
        }
        Rectangle {
            z: -1;
            color: Style.colorError;
            width: (size * 0.45);
            height: (Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom) * 3);
            antialiasing: true;
            anchors.left: parent.horizontalCenter;
            anchors.verticalCenter: (parent ? parent.verticalCenter : undefined);
        }
    }
    TextLabel {
        text: legend;
        width: (size * 0.45);
        opacity: 0.65;
        fontSizeMode: Text.HorizontalFit;
        minimumPixelSize: 5;
        font.pixelSize: Style.fontSizeTitle;
        anchors {
            bottom: parent.bottom;
            margins: (size * 0.10);
            horizontalCenter: (parent ? parent.horizontalCenter : undefined);
        }
    }
}
