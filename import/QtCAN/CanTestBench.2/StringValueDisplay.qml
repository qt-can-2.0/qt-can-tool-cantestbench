import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: (column.width + column.anchors.margins * 2);
    implicitHeight: (column.height + column.anchors.margins * 2);

    property int    size       : 50;
    property string value      : "";
    property int    len        : 0;
    property color  background : Style.colorWindow;
    property string unit       : "";
    property string legend     : "";
    property bool   fullString : true;

    Rectangle {
        radius: Math.round (size * 0.15);
        antialiasing: radius;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom) * 2;
            color: Style.colorBorder;
        }
        gradient: Gradient {
            GradientStop { position: 0; color: Qt.darker  (background, 1.15); }
            GradientStop { position: 1; color: Qt.lighter (background, 1.15); }
        }
        anchors.fill: parent;
    }
    Column {
        id: column;
        spacing: Style.spacingSmall;
        anchors.centerIn: parent;
        anchors.margins: Style.spacingNormal;

        Repeater {
            model: value.trim ().split ("\n");
            delegate: TextLabel {
                id: lblHolder;
                text: {
                    var ret = "";
                    for (var charIdx = 0; charIdx < len; ++charIdx) {
                        ret += (fullString ? "W" : "0");
                    }
                    return ret;
                }
                color: Style.colorNone;
                font.pixelSize: size;
                anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);

                TextLabel {
                    id: lblValue;
                    text: modelData.substring (0, len).trim ();
                    color: lblLegend.color;
                    horizontalAlignment: TextInput.AlignHCenter;
                    font.pixelSize: size;
                    anchors.fill: parent;
                }
            }
        }
        TextLabel {
            id: lblLegend;
            text: legend;
            color: (((background.r + background.g + background.b) / 3) < 0.5 ? "white" : "black");
            visible: (text !== "");
            opacity: 0.65;
            font.pixelSize: (size * 0.35);
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
        }
    }
}
