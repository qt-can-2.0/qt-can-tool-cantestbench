import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: (size * 2);
    implicitHeight: size;

    property int    size   : 40;
    property bool   value  : false;
    property int    side   : Borders.BOTTOM;
    property string legend : "";

    Item {
        id: frame;
        anchors.fill: parent;

        Rectangle {
            color: Style.colorWindow;
            anchors.right: parent.horizontalCenter;
            ExtraAnchors.leftDock: parent;

            TextLabel {
                text: "OFF";
                color: (((parent.color.r + parent.color.g + parent.color.b) / 3) < 0.5 ? "white" : "black");
                fontSizeMode: Text.Fit;
                minimumPixelSize: 5;
                verticalAlignment: Text.AlignVCenter;
                horizontalAlignment: Text.AlignHCenter;
                font.pixelSize: parent.height;
                anchors {
                    fill: parent;
                    margins: (size * 0.15);
                }
            }
        }
        Rectangle {
            color: Style.colorHighlight;
            anchors.left: parent.horizontalCenter;
            ExtraAnchors.rightDock: parent;

            TextLabel {
                text: "ON";
                color: (((parent.color.r + parent.color.g + parent.color.b) / 3) < 0.5 ? "white" : "black");
                minimumPixelSize: 5;
                fontSizeMode: Text.Fit;
                verticalAlignment: Text.AlignVCenter;
                horizontalAlignment: Text.AlignHCenter;
                font.pixelSize: parent.height;
                anchors {
                    fill: parent;
                    margins: (size * 0.15);
                }
            }
        }
    }
    Rectangle {
        color: Style.colorNone;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
            color: Style.colorBorder;
        }
        anchors.fill: parent;
    }
    MouseArea {
        anchors.fill: parent;
        onClicked: { value = !value; }

        Rectangle {
            x: (!value ? parent.width - width : 0);
            width: (parent.width * 0.5);
            gradient: Style.gradientIdle ();
            border {
                width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
                color: Style.colorBorder;
            }
            ExtraAnchors.verticalFill: parent;

            Behavior on x {
                NumberAnimation {
                    duration: 350;
                    easing.type: Easing.OutQuad;
                }
            }
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        anchors.margins: Style.spacingSmall;
        states: [
            State {
                when: (side === Borders.TOP);

                AnchorChanges {
                    target: lbl;
                    anchors.bottom: frame.top;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
            },
            State {
                when: (side === Borders.RIGHT);

                AnchorChanges {
                    target: lbl;
                    anchors.left: frame.right;
                    anchors.verticalCenter: frame.verticalCenter;
                }
            },
            State {
                when: (side === Borders.LEFT);

                AnchorChanges {
                    target: lbl;
                    anchors.right: frame.left;
                    anchors.verticalCenter: frame.verticalCenter;
                }
            },
            State {
                when: (side === Borders.BOTTOM);

                AnchorChanges {
                    target: lbl;
                    anchors.top: frame.bottom;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
            }
        ]
    }
}
