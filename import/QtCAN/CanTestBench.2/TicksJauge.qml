import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: layout.width;
    implicitHeight: layout.height;

    property real value    : 0;
    property real minValue : 0;
    property real maxValue : 100;

    property int divisions : 20;

    property color baseColor : Style.colorSelection;

    property int tickWidth  : 8;
    property int tickHeight : 30;

    property string legend : "";

    readonly property real tickValue : (maxValue - minValue) / divisions;

    Row {
        id: layout;
        height: (tickHeight * 1.65);
        spacing: Math.floor (tickWidth * 0.35);
        anchors.centerIn: parent;

        Repeater {
            model: divisions;
            delegate: Rectangle {
                color: (value > threshold ? baseColor : Style.colorBorder);
                width: tickWidth;
                height: tickHeight;
                radius: Math.round (tickWidth * 0.35);
                antialiasing: radius;
                anchors.bottom: parent.bottom;
                anchors.margins: (Math.sin (Math.PI * (model.index +1) / divisions) * (layout.height - height));

                readonly property real threshold : (minValue + model.index * tickValue);

                TextLabel {
                    text: {
                        if (model.index === 0) {
                            return minValue;
                        }
                        else if (model.index === divisions -1) {
                            return maxValue;
                        }
                        else {
                            return "";
                        }
                    }
                    font.pixelSize: (tickWidth * 2);
                    anchors.top: parent.bottom;
                    anchors.margins: tickWidth;
                    anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                }
            }
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        anchors.top: layout.bottom;
        anchors.horizontalCenter: layout.horizontalCenter;
    }
}
