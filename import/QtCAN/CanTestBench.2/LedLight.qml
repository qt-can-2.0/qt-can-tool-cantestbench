import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: size;
    implicitHeight: size;

    property int    size      : 20;
    property bool   active    : true;
    property color  baseColor : "cyan";
    property int    side      : Borders.LEFT;
    property string legend    : "";

    Item {
        id: frame;
        width: size;
        height: size;
        anchors.centerIn: parent;
    }
    Repeater {
        model: 5;
        delegate: Rectangle {
            color: (active ? baseColor : Style.colorBorder);
            width: diameter;
            height: diameter;
            radius: (diameter * 0.5);
            opacity: (model.index ? 0.1 : 1.0);
            visible: (active || !model.index);
            antialiasing: true;
            border {
                color: Qt.darker (color);
                width: (model.index ? 0 : Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom));
            }
            anchors.centerIn: parent;
            anchors.alignWhenCentered: false;

            readonly property int diameter : (size + (model.index * model.index));
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        anchors.margins: Style.spacingSmall;
        states: [
            State {
                when: (side === Borders.TOP);

                AnchorChanges {
                    target: lbl;
                    anchors.bottom: frame.top;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
            },
            State {
                when: (side === Borders.RIGHT);

                AnchorChanges {
                    target: lbl;
                    anchors.left: frame.right;
                    anchors.verticalCenter: frame.verticalCenter;
                }
            },
            State {
                when: (side === Borders.LEFT);

                AnchorChanges {
                    target: lbl;
                    anchors.right: frame.left;
                    anchors.verticalCenter: frame.verticalCenter;
                }
            },
            State {
                when: (side === Borders.BOTTOM);

                AnchorChanges {
                    target: lbl;
                    anchors.top: frame.bottom;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
            }
        ]
    }
}
