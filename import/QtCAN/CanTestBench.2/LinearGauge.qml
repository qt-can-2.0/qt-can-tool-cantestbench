import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    id: base;
    implicitWidth: 20;
    implicitHeight: 200;

    property real   value     : 0;
    property real   minValue  : 0;
    property real   maxValue  : 100;
    property color  colorMin  : "blue";
    property color  colorMax  : "red";
    property int    divisions : 10;
    property string legend    : "";
    property int    side      : Borders.RIGHT;

    Item {
        clip: true;
        height: (base.height * (value - minValue) / (maxValue - minValue));
        ExtraAnchors.bottomDock: parent;

        Rectangle {
            height: base.height;
            gradient: Gradient {
                GradientStop { position: 0; color: colorMax; }
                GradientStop { position: 1; color: colorMin; }
            }
            ExtraAnchors.bottomDock: parent;
        }
    }
    Rectangle {
        width: parent.height;
        height: parent.width;
        opacity: 0.25;
        rotation: 90;
        gradient: Gradient {
            GradientStop { position: 0; color: "white"; }
            GradientStop { position: 1; color: "black"; }
        }
        anchors.centerIn: parent;
        anchors.alignWhenCentered: false;
    }
    Rectangle {
        id: frame;
        color: Style.colorNone;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
            color: Style.colorForeground;
        }
        anchors.fill: parent;

        Repeater {
            model: (divisions - 1);
            delegate: Rectangle {
                color: Style.colorForeground;
                height: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
                anchors {
                    left: parent.left;
                    right: parent.horizontalCenter;
                    bottom: parent.bottom;
                    bottomMargin: ((model.index +1) * (parent.height - height) / divisions);
                }
            }
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        anchors.verticalCenter: frame.verticalCenter;
        states: [
            State {
                when: (side === Borders.RIGHT);

                AnchorChanges {
                    target: lbl;
                    anchors.horizontalCenter: frame.right;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: -90;
                    anchors.horizontalCenterOffset: (Style.spacingSmall + Style.fontSizeNormal);
                }
            },
            State {
                when: (side === Borders.LEFT);

                AnchorChanges {
                    target: lbl;
                    anchors.horizontalCenter: frame.left;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: +90;
                    anchors.horizontalCenterOffset: -(Style.spacingSmall + Style.fontSizeNormal);
                }
            }
        ]
    }
}
