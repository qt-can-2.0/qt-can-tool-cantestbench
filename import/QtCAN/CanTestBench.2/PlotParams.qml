import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

QtObject {
    id: base;

    property real currentValue :  0.0;
    property real minValue     : -1.0;
    property real maxValue     : +1.0;

    property var markValues : [];

    property int decimals : 0;

    property color lineColor : Style.colorForeground;

    property string legend : "";
    property string unit   : "";
}
