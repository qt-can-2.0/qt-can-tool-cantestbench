import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: size;
    implicitHeight: size;

    property int    size     : 80;
    property int    decimals : 0;
    property real   value    : 0;
    property real   minValue : 0;
    property real   maxValue : 100;
    property string legend   : "";
    property int    side     : Borders.BOTTOM;

    MouseArea {
        anchors.fill: parent;
        onPressed: {
            pressedVal = value;
            pressedAng = radToDeg (Math.atan2 (mouse.y - (height / 2), mouse.x - (width  / 2)));
        }
        onPositionChanged: {
            var originAng = (360 * (pressedVal - minValue) / (maxValue - minValue));
            var currentAng = radToDeg (Math.atan2 (mouse.y - (height / 2), mouse.x - (width  / 2)));
            var angle = ((originAng + (currentAng - pressedAng)) + 360) % 360;
            value = minValue + ((angle / 360) * (maxValue - minValue));
        }
        onReleased: { }

        property real pressedVal : 0.0;
        property real pressedAng : 0.0;

        function radToDeg (rad) {
            return (((rad * 180 / Math.PI) + 360) % 360);
        }
    }
    Rectangle {
        id: frame;
        width: (size * 1.15);
        height: (size * 1.15);
        radius: (size * 1.15 * 0.5);
        opacity: 0.65;
        gradient: Style.gradientPressed ();
        antialiasing: true;
        anchors.centerIn: parent;

        Rectangle {
            color: Style.colorBorder;
            width: (size * 0.05);
            height: (size * 0.15);
            anchors {
                top: parent.top;
                horizontalCenter: (parent ? parent.horizontalCenter : undefined);
            }
        }
    }
    Rectangle {
        width: size;
        height: size;
        radius: (size * 0.5);
        gradient: Style.gradientIdle ();
        antialiasing: true;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom);
            color: Style.colorBorder;
        }
        anchors.centerIn: parent;
    }
    TextLabel {
        text: value.toFixed (decimals);
        color: Style.colorForeground;
        fontSizeMode: Text.Fit;
        minimumPixelSize: 10;
        verticalAlignment: Text.AlignVCenter;
        horizontalAlignment: Text.AlignHCenter;
        font.pixelSize: height;
        anchors.fill: parent;
        anchors.margins: (size * 0.15);
    }
    Item {
        width: size;
        height: size;
        rotation: (360 * (value - minValue) / (maxValue - minValue));
        anchors.centerIn: parent;

        Rectangle {
            color: Style.colorBorder;
            width: (size * 0.05);
            height: (size * 0.15);
            antialiasing: true;
            anchors {
                top: parent.top;
                horizontalCenter: (parent ? parent.horizontalCenter : undefined);
            }
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        states: [
            State {
                when: (side === Borders.TOP);

                AnchorChanges {
                    target: lbl;
                    anchors.bottom: frame.top;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: 0;
                    anchors.bottomMargin: Style.spacingSmall;
                }
            },
            State {
                when: (side === Borders.RIGHT);

                AnchorChanges {
                    target: lbl;
                    anchors.verticalCenter: frame.verticalCenter;
                    anchors.horizontalCenter: frame.right;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: -90;
                    anchors.horizontalCenterOffset: (Style.spacingSmall + Style.fontSizeNormal);
                }
            },
            State {
                when: (side === Borders.LEFT);

                AnchorChanges {
                    target: lbl;
                    anchors.verticalCenter: frame.verticalCenter;
                    anchors.horizontalCenter: frame.left;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: +90;
                    anchors.horizontalCenterOffset: -(Style.spacingSmall + Style.fontSizeNormal);
                }
            },
            State {
                when: (side === Borders.BOTTOM);

                AnchorChanges {
                    target: lbl;
                    anchors.top: frame.bottom;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: 0;
                    anchors.topMargin: Style.spacingSmall;
                }
            }
        ]
    }
}
