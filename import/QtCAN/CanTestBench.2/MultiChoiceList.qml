import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    id: frame;
    width: implicitWidth;
    height: implicitHeight;
    implicitWidth: layout.width;
    implicitHeight: layout.height;

    property int size : 20;

    property int side : Borders.TOP;

    property var model : [];

    property string legend : "";

    property int currentIdx : 0;

    readonly property var currentKey   : (Array.isArray (model) && currentIdx >= 0 && currentIdx < model.length ? model [currentIdx]["key"]   : undefined);
    readonly property var currentValue : (Array.isArray (model) && currentIdx >= 0 && currentIdx < model.length ? model [currentIdx]["value"] : undefined);

    StretchColumnContainer {
        id: layout;

        Repeater {
            model: frame.model;
            delegate: MouseArea {
                implicitWidth: (txt.contentWidth + txt.anchors.margins * 2);
                implicitHeight: (txt.contentHeight + txt.anchors.margins * 2);
                onClicked: { currentIdx = model.index; }

                Rectangle {
                    color: Style.colorSelection;
                    visible: (model.index === currentIdx);
                    anchors.fill: parent;
                }
                TextLabel {
                    id: txt;
                    text: modelData ["value"];
                    font.pixelSize: size;
                    anchors.margins: (size * 0.35);
                    anchors.centerIn: parent;
                }
            }
        }
    }
    TextLabel {
        id: lbl;
        text: legend;
        opacity: 0.65;
        font.pixelSize: (size * 0.85);
        states: [
            State {
                when: (side === Borders.TOP);

                AnchorChanges {
                    target: lbl;
                    anchors.bottom: frame.top;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: 0;
                    anchors.bottomMargin: Style.spacingSmall;
                }
            },
            State {
                when: (side === Borders.RIGHT);

                AnchorChanges {
                    target: lbl;
                    anchors.verticalCenter: frame.verticalCenter;
                    anchors.horizontalCenter: frame.right;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: -90;
                    anchors.horizontalCenterOffset: (Style.spacingSmall + Style.fontSizeNormal);
                }
            },
            State {
                when: (side === Borders.LEFT);

                AnchorChanges {
                    target: lbl;
                    anchors.verticalCenter: frame.verticalCenter;
                    anchors.horizontalCenter: frame.left;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: +90;
                    anchors.horizontalCenterOffset: -(Style.spacingSmall + Style.fontSizeNormal);
                }
            },
            State {
                when: (side === Borders.BOTTOM);

                AnchorChanges {
                    target: lbl;
                    anchors.top: frame.bottom;
                    anchors.horizontalCenter: frame.horizontalCenter;
                }
                PropertyChanges {
                    target: lbl;
                    rotation: 0;
                    anchors.topMargin: Style.spacingSmall;
                }
            }
        ]
    }
}
