import QtQuick 2.1;
import QtQmlTricks.UiElements 2.0;
import QtCAN.CanTestBench 2.0;

Item {
    implicitWidth: (column.width + column.anchors.margins * 2);
    implicitHeight: (column.height + column.anchors.margins * 2);

    property int    size       : 50;
    property int    decimals   : 0;
    property int    digits     : 3;
    property bool   sign       : true;
    property real   value      : 0.0;
    property color  background : Style.colorWindow;
    property string unit       : "";
    property string legend     : "";

    Rectangle {
        radius: Math.round (size * 0.15);
        antialiasing: radius;
        border {
            width: Math.max (Style.lineSize, Style.lineSize / Shared.manager.dashboardZoom) * 2;
            color: Style.colorBorder;
        }
        gradient: Gradient {
            GradientStop { position: 0; color: Qt.darker  (background, 1.15); }
            GradientStop { position: 1; color: Qt.lighter (background, 1.15); }
        }
        anchors.fill: parent;
    }
    Column {
        id: column;
        spacing: Style.spacingSmall;
        anchors.centerIn: parent;
        anchors.margins: Style.spacingNormal;

        Row {
            id: row;
            spacing: Style.spacingSmall;
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);

            TextLabel {
                id: lblHolder;
                text: {
                    var ret = "";
                    if (sign) {
                        ret += "-";
                    }
                    for (var digitIdx = 0; digitIdx < digits; ++digitIdx) {
                        ret += "0";
                    }
                    if (decimals > 0) {
                        ret += ".";
                        for (var decimalIdx = 0; decimalIdx < decimals; ++decimalIdx) {
                            ret += "0";
                        }
                    }
                    return ret;
                }
                color: Style.colorNone;
                font.pixelSize: size;

                TextLabel {
                    id: lblValue;
                    text: value.toFixed (decimals);
                    color: (((background.r + background.g + background.b) / 3) < 0.5 ? "white" : "black");
                    horizontalAlignment: TextInput.AlignHCenter;
                    font.pixelSize: size;
                    anchors.fill: parent;
                }
            }
            TextLabel {
                text: unit;
                color: lblValue.color;
                visible: (text !== "");
                font.pixelSize: (size * 0.65);
                anchors.baseline: lblHolder.baseline;
            }
        }
        TextLabel {
            text: legend;
            color: lblValue.color;
            visible: (text !== "");
            opacity: 0.65;
            font.pixelSize: (size * 0.35);
            anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
        }
    }
}
