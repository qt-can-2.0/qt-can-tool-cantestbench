
#include "Grapher.h"

#include <QtMath>
#include <QtGlobal>

Grapher::Grapher (QQuickItem * parent)
    : QQuickItem (parent)
    , m_samplingInterval (20)
    , m_timeFrame        (10000)
    , m_running          (false)
    , m_currentValue     (0.0)
    , m_minValue         (-1.0)
    , m_maxValue         (+1.0)
    , m_lineSize         (2.0)
    , m_lineColor        (Qt::red)
{
    setFlag (QQuickItem::ItemHasContents);
    m_timer = new QTimer (this);
    m_timer->setTimerType (Qt::PreciseTimer);
    connect (m_timer, &QTimer::timeout,                  this, &Grapher::onTick);
    connect (this,    &Grapher::markerPosChanged,        this, &Grapher::doMark);
    connect (this,    &Grapher::timeFrameChanged,        this, &Grapher::doInit);
    connect (this,    &Grapher::samplingIntervalChanged, this, &Grapher::doInit);
    connect (this,    &Grapher::runningChanged,          this, &Grapher::doInit);
    connect (this,    &Grapher::lineSizeChanged,         this, &Grapher::update);
    connect (this,    &Grapher::lineColorChanged,        this, &Grapher::update);
    connect (this,    &Grapher::minValueChanged,         this, &Grapher::update);
    connect (this,    &Grapher::maxValueChanged,         this, &Grapher::update);
    connect (this,    &Grapher::visibleChanged,          this, &Grapher::update);
    connect (this,    &Grapher::widthChanged,            this, &Grapher::update);
    connect (this,    &Grapher::heightChanged,           this, &Grapher::update);
}

void Grapher::reset (void) {
    m_values.clear ();
    m_values.reserve ((m_timeFrame / m_samplingInterval) +1);
    doInit ();
    update ();
}

QVariantList Grapher::getValues (void) const {
    QVariantList ret;
    ret.reserve (m_values.count ());
    for (qreal val : m_values) {
        ret.append (QVariant::fromValue (val));
    }
    return ret;
}

void Grapher::doInit (void) {
    if (m_timer->isActive ()) {
        m_timer->stop ();
    }
    if (m_running) {
        m_timer->start (m_samplingInterval);
    }
}

void Grapher::componentComplete (void) {
    QQuickItem::componentComplete ();
    reset ();
    doInit ();
    doMark ();
}

void Grapher::onTick (void) {
    m_values.append (m_currentValue);
    if (m_timeFrame > 0) {
        const int maxCount = (m_timeFrame / m_samplingInterval) +1;
        while (m_values.count () > maxCount) {
            m_values.removeFirst ();
        }
    }
    doMark ();
    update ();
}

void Grapher::doMark (void) {
    int markerIdx = -1;
    if (!m_values.isEmpty ()) {
        if (m_timeFrame > 0) {
            const int maxCount = (m_timeFrame / m_samplingInterval) +1;
            const int hypotheticIdx = qRound (m_markerPos * (maxCount -1));
            markerIdx = (m_values.count () < maxCount
                         ? hypotheticIdx - (maxCount - m_values.count ())
                         : hypotheticIdx);
        }
        else {
            markerIdx = qRound (m_markerPos * (m_values.count () -1));
        }
    }
    update_markerVal (markerIdx >= 0 && markerIdx < m_values.count () ? m_values.at (markerIdx) : qQNaN ());
}

QSGNode * Grapher::updatePaintNode (QSGNode * oldNode, UpdatePaintNodeData * nodeData) {
    Q_UNUSED (nodeData)
    QSGGeometryNode      * node     = Q_NULLPTR;
    QSGGeometry          * geometry = Q_NULLPTR;
    QSGFlatColorMaterial * material = Q_NULLPTR;
    const int vertexCount = m_values.size ();
    const int maxCount    = (m_timeFrame / m_samplingInterval) +1;
    if (oldNode) {
        node = static_cast<QSGGeometryNode *> (oldNode);
        geometry = node->geometry ();
        geometry->allocate (vertexCount);
        material = static_cast<QSGFlatColorMaterial *> (node->material ());
    }
    else {
        node = new QSGGeometryNode;
        geometry = new QSGGeometry (QSGGeometry::defaultAttributes_Point2D (), vertexCount);
        geometry->setDrawingMode (GL_LINE_STRIP);
        node->setGeometry (geometry);
        node->setFlag (QSGNode::OwnsGeometry);
        material = new QSGFlatColorMaterial;
        node->setMaterial (material);
        node->setFlag (QSGNode::OwnsMaterial);
    }
    geometry->setLineWidth (float (m_lineSize));
    material->setColor (m_lineColor);
    if (vertexCount > 0) {
        QSGGeometry::Point2D * vertexList = geometry->vertexDataAsPoint2D ();
        const qreal xOrigin = qreal (0.0);
        const qreal xRange  = qreal (width ());
        const qreal xOffset = qreal (m_timeFrame > 0 ? (maxCount - vertexCount) : 0);
        const qreal xTotal  = qreal (m_timeFrame > 0 ? maxCount : vertexCount);
        const qreal yOrigin = qreal (m_lineSize * 0.5);
        const qreal yRange  = qreal (height () - m_lineSize);
        const qreal yTotal  = qreal (m_maxValue - m_minValue);
        for (int vertexIdx = 0; vertexIdx < vertexCount; ++vertexIdx) {
            const qreal xRatio = (qreal (vertexIdx + xOffset) / xTotal);
            const qreal yRatio = (1.0 - (qreal (m_values.at (vertexIdx) - m_minValue) / yTotal));
            vertexList [vertexIdx].set (float (xOrigin + xRatio * xRange),
                                        float (yOrigin + yRatio * yRange));
        }
    }
    node->markDirty (QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);
    return node;
}
