#ifndef ABSTRACTSENSOR_H
#define ABSTRACTSENSOR_H

#include <QObject>

#include "IdentifiableObject.h"

class AnalogInput;
class DigitalInput;
class PhysicalValue;
class LinkAnalogSensorToAnalogInput;
class LinkDigitalSensorToDigitalInput;
class LinkPhysicalValueToAnalogSensor;
class LinkPhysicalValueToHybridSensor;
class LinkHybridSensorToDigitalInput;

class AbstractSensor : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (ObjectType::Type, type)
    QML_WRITABLE_CSTREF_PROPERTY (QString, description)

public:
    explicit AbstractSensor (const ObjectType::Type type = ObjectType::UNKNOWN_TYPE, QObject * parent = Q_NULLPTR);
    virtual ~AbstractSensor (void);

    void onComponentCompleted (void) Q_DECL_OVERRIDE;
};

class AnalogSensor : public AbstractSensor {
    Q_OBJECT
    // mode
    QML_READONLY_VAR_PROPERTY (bool, inverted)
    QML_WRITABLE_VAR_PROPERTY (bool, bypassScale)
    // phy values
    QML_WRITABLE_VAR_PROPERTY (int, valPhy)
    QML_WRITABLE_VAR_PROPERTY (int, minPhy)
    QML_WRITABLE_VAR_PROPERTY (int, maxPhy)
    // raw values
    QML_WRITABLE_VAR_PROPERTY (int, minRaw)
    QML_WRITABLE_VAR_PROPERTY (int, maxRaw)
    QML_READONLY_VAR_PROPERTY (int, valRaw)
    // percent value
    QML_READONLY_VAR_PROPERTY (int, percents)
    // display format
    QML_WRITABLE_VAR_PROPERTY (int, decimals)
    QML_WRITABLE_CSTREF_PROPERTY (QString, unit)
    // percent computation
    QML_WRITABLE_VAR_PROPERTY (bool, useSplitPoint)
    QML_WRITABLE_VAR_PROPERTY (int, upperMargin)
    QML_WRITABLE_VAR_PROPERTY (int, middleMargin)
    QML_WRITABLE_VAR_PROPERTY (int, lowerMargin)
    // source link
    QML_WRITABLE_PTR_PROPERTY (LinkPhysicalValueToAnalogSensor, sourceLink)
    // target links
    QML_LIST_PROPERTY (LinkAnalogSensorToAnalogInput, targetLinks)

public:
    explicit AnalogSensor (QObject * parent = Q_NULLPTR);
    virtual ~AnalogSensor (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void refreshValRaw   (void);
    void refreshValPhy   (void);
    void refreshPercents (void);

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class DigitalSensor : public AbstractSensor {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (bool, value)
    QML_WRITABLE_CSTREF_PROPERTY (QString, trueLabel)
    QML_WRITABLE_CSTREF_PROPERTY (QString, falseLabel)
    // target links
    QML_LIST_PROPERTY (LinkDigitalSensorToDigitalInput, targetLinks)

public:
    explicit DigitalSensor (QObject * parent = Q_NULLPTR);
    virtual ~DigitalSensor (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class HybridSensor : public AbstractSensor {
    Q_OBJECT
    // mode
    QML_READONLY_VAR_PROPERTY (bool, disabled)
    // physical values
    QML_WRITABLE_VAR_PROPERTY (int, valPhy)
    QML_WRITABLE_VAR_PROPERTY (int, lowThresholdPhy)
    QML_WRITABLE_VAR_PROPERTY (int, highThresholdPhy)
    // digital state
    QML_WRITABLE_VAR_PROPERTY (bool, invertState)
    QML_READONLY_VAR_PROPERTY (bool, valState)
    // display format
    QML_WRITABLE_VAR_PROPERTY (int, decimals)
    QML_WRITABLE_CSTREF_PROPERTY (QString, unit)
    // source link
    QML_WRITABLE_PTR_PROPERTY (LinkPhysicalValueToHybridSensor, sourceLink)
    // target links
    QML_LIST_PROPERTY (LinkHybridSensorToDigitalInput, targetLinks)

public:
    explicit HybridSensor (QObject * parent = Q_NULLPTR);
    virtual ~HybridSensor (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void refreshValState (void);
};

#endif // ABSTRACTSENSOR_H
