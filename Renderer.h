#ifndef RENDERER_H
#define RENDERER_H

#include <QObject>
#include <QQuickItem>
#include <QQuickFramebufferObject>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>
#include <QSize>
#include <QColor>
#include <QTimer>

#include "IdentifiableObject.h"

#include "Physics.h"

class RendererEngine : public QQuickFramebufferObject::Renderer, public QOpenGLFunctions {
public:
    explicit RendererEngine (void);

    void render (void) Q_DECL_FINAL;

    void synchronize (QQuickFramebufferObject * fboItem) Q_DECL_FINAL;

    QOpenGLFramebufferObject * createFramebufferObject (const QSize & size) Q_DECL_FINAL;

private:
    QMatrix4x4 m_worldToCameraMatrix;
    QMatrix4x4 m_projectionMatrix;
    QVector<QVector3D> m_vertices;
    QVector<QVector3D> m_colors;
    QOpenGLShaderProgram m_glslProgram;
};

class RendererItem : public QQuickFramebufferObject {
    Q_OBJECT
    QML_WRITABLE_PTR_PROPERTY (PhysicalWorld, world)
    QML_WRITABLE_VAR_PROPERTY (Sides::Type, viewingSide)
    QML_WRITABLE_VAR_PROPERTY (qreal, viewingZoom)
    QML_WRITABLE_VAR_PROPERTY (bool, usePerspective)

public:
    explicit RendererItem (QQuickItem * parent = Q_NULLPTR);

    QQuickFramebufferObject::Renderer * createRenderer (void) const Q_DECL_FINAL;

protected:
    void onWorldChanged (void);
};

#endif // RENDERER_H
