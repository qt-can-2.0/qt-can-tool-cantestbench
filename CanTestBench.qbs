import qbs;

Project {
    name: "CanTestBench";

    Product {
        name: (project.namePrefixTools + "cantestbench");
        type: "application";
        targetName: (project.targetPrefixTools + "CanTestBench");
        consoleApplication: false;
        cpp.dynamicLibraries: {
            var ret = [];
            if (Qt.core.versionMinor < 4) { // NOTE : prior to Qt5.4 we had to link libGL manually
                ret.push ("GL");
            }
            return ret;
        }
        cpp.rpaths: ["$ORIGIN", "$ORIGIN/lib"];
        cpp.cxxLanguageVersion: "c++11";
        cpp.cxxStandardLibrary: "libstdc++"; // NOTE : because there are issues with libc++
        Qt.core.resourcePrefix: "/";
        Qt.core.resourceSourceBase: sourceDirectory;
        Qt.core.resourceFileBaseName: "data_testbench";

        readonly property stringList qmlImportPaths : [sourceDirectory + "/import"]; // equivalent to QML_IMPORT_PATH += $$PWD/import

        Depends { name: "cpp"; }
        Depends {
            name: "Qt";
            submodules: ["core", "gui", "qml", "quick", "network", "serialport", "qml-private"];
        }
        Depends { name: (project.namePrefixBase + "base"); }
        Depends { name: (project.namePrefixBase + "utils"); }
        Depends { name: (project.namePrefixProtocols + "canopen"); }
        Depends { name: "libqtqmltricks-qtsupermacros"; }
        Depends { name: "libqtqmltricks-qtqmlmodels"; }
        Depends { name: "libqtqmltricks-qtquickuielements"; }
        Group {
            name: "C++ Sources";
            files: [
                "Actuator.cpp",
                "CanBus.cpp",
                "CanOpen.cpp",
                "Dashboard.cpp",
                "Grapher.cpp",
                "Help.cpp",
                "IdentifiableObject.cpp",
                "IO.cpp",
                "Link.cpp",
                "main_testBench.cpp",
                "Manager.cpp",
                "Memory.cpp",
                "NetworkDefinition.cpp",
                "Node.cpp",
                "Physics.cpp",
                "Renderer.cpp",
                "Routine.cpp",
                "Sensor.cpp",
                "SerialBus.cpp",
                "SharedObject.cpp",
                "SyntaxHighlighter.cpp",
                "Transformer.cpp",
            ]
        }
        Group {
            name: "C++ Headers";
            files: [
                "Actuator.h",
                "CanBus.h",
                "CanOpen.h",
                "CppUtils.h",
                "Dashboard.h",
                "Grapher.h",
                "Help.h",
                "IdentifiableObject.h",
                "IO.h",
                "Link.h",
                "Manager.h",
                "MathUtils.h",
                "Memory.h",
                "NetworkDefinition.h",
                "Node.h",
                "Physics.h",
                "Renderer.h",
                "Routine.h",
                "Sensor.h",
                "SerialBus.h",
                "SharedObject.h",
                "SyntaxHighlighter.h",
                "Transformer.h",
            ]
        }
        Group {
            name: "Images";
            fileTags: "qt.core.resource_data";
            files: [
                "icons/3d.svg",
                "icons/actuator.svg",
                "icons/circuit.svg",
                "icons/logic.svg",
                "icons/memory.svg",
                "icons/normal.svg",
                "icons/reverse.svg",
                "icons/routine.svg",
                "icons/sensor.svg",
                "vcan_testbench_logo.svg",
            ]
        }
        Group {
            files: [
                "testbench.ico",
            ]
            name: "Icons";
            qbs.install: true;
            fileTags: "qt.core.resource_data";
        }
        Group {
            name: "GLSL shaders";
            fileTags: "qt.core.resource_data";
            files: [
                "glsl/Renderer.fsh",
                "glsl/Renderer.vsh",
            ]
        }
        Group {
            name: "QML components";
            fileTags: "qt.core.resource_data";
            files: [
                "components/AbstractDelegateRoutine.qml",
                "components/AbstractDialogDetails.qml",
                "components/BorderedBackground.qml",
                "components/Circle.qml",
                "components/ClickableTextLabel.qml",
                "components/Components.qml",
                "components/DelegateAnalogActuator.qml",
                "components/DelegateAnalogActuatorMini.qml",
                "components/DelegateAnalogActuatorWide.qml",
                "components/DelegateAnalogSensor.qml",
                "components/DelegateAnalogSensorMini.qml",
                "components/DelegateAnalogSensorWide.qml",
                "components/DelegateArgumentDetailsHelp.qml",
                "components/DelegateArgumentHelp.qml",
                "components/DelegateCanBus.qml",
                "components/DelegateControlAin.qml",
                "components/DelegateControlAout.qml",
                "components/DelegateControlDin.qml",
                "components/DelegateControlDout.qml",
                "components/DelegateDigitalActuator.qml",
                "components/DelegateDigitalActuatorMini.qml",
                "components/DelegateDigitalSensor.qml",
                "components/DelegateDigitalSensorMini.qml",
                "components/DelegateEnumHelp.qml",
                "components/DelegateHybridActuator.qml",
                "components/DelegateHybridActuatorMini.qml",
                "components/DelegateHybridSensor.qml",
                "components/DelegateHybridSensorMini.qml",
                "components/DelegateLinkObjectHelp.qml",
                "components/DelegateLinkSource.qml",
                "components/DelegateLinkTarget.qml",
                "components/DelegateMethodHelp.qml",
                "components/DelegateObjectHelp.qml",
                "components/DelegatePhyBlockTable.qml",
                "components/DelegatePhyGroupTable.qml",
                "components/DelegatePhyMarkerTable.qml",
                "components/DelegatePhyValTable.qml",
                "components/DelegatePropertyHelp.qml",
                "components/DelegateRawAinTable.qml",
                "components/DelegateRawAoutTable.qml",
                "components/DelegateRawBoardTable.qml",
                "components/DelegateRawDinTable.qml",
                "components/DelegateRawDoutTable.qml",
                "components/DelegateRawNodeTable.qml",
                "components/DelegateRoutineOnCanFrame.qml",
                "components/DelegateRoutineOnCanOpenBootUp.qml",
                "components/DelegateRoutineOnCanOpenHeartbeatConsumer.qml",
                "components/DelegateRoutineOnCanOpenNmtStateChange.qml",
                "components/DelegateRoutineOnCanOpenObdValChange.qml",
                "components/DelegateRoutineOnCanOpenSdoReadReply.qml",
                "components/DelegateRoutineOnCanOpenSdoWriteRequest.qml",
                "components/DelegateRoutineOnEvent.qml",
                "components/DelegateRoutineOnSerialFrame.qml",
                "components/DelegateRoutineOnTimer.qml",
                "components/DelegateSerialBus.qml",
                "components/DelegateSignalHelp.qml",
                "components/DelegateTokenId.qml",
                "components/DialogConfigCanBus.qml",
                "components/DialogConfigSerialBus.qml",
                "components/DialogDetailsAin.qml",
                "components/DialogDetailsAout.qml",
                "components/DialogDetailsDin.qml",
                "components/DialogDetailsDout.qml",
                "components/DialogDetailsPhy.qml",
                "components/DialogExportSnapshot.qml",
                "components/DialogImportSnapshot.qml",
                "components/DialogOpenNetworkDefinition.qml",
                "components/DialogPrintDatasheet.qml",
                "components/DimensionsList.qml",
                "components/ExpandableGroup.qml",
                "components/InstanceCreator.qml",
                "components/SymbolPlug.qml",
                "components/WindowCodeEditor.qml",
                "import/QtCAN/CanTestBench.2/CircleButton.qml",
                "import/QtCAN/CanTestBench.2/CircularGauge.qml",
                "import/QtCAN/CanTestBench.2/LedLight.qml",
                "import/QtCAN/CanTestBench.2/LinearGauge.qml",
                "import/QtCAN/CanTestBench.2/MultiChoiceList.qml",
                "import/QtCAN/CanTestBench.2/NumberValueDisplay.qml",
                "import/QtCAN/CanTestBench.2/NumberValueInput.qml",
                "import/QtCAN/CanTestBench.2/OnOffSwitcher.qml",
                "import/QtCAN/CanTestBench.2/Oscilloscope.qml",
                "import/QtCAN/CanTestBench.2/PlotParams.qml",
                "import/QtCAN/CanTestBench.2/RotativeKnob.qml",
                "import/QtCAN/CanTestBench.2/StringValueDisplay.qml",
                "import/QtCAN/CanTestBench.2/TicksJauge.qml",
                "import/QtCAN/CanTestBench.2/qmldir",
                "splash_testbench.qml",
                "ui_testBench.qml",
            ]
        }
        Group {
            qbs.install: true;
            fileTagsFilter: product.type;
        }
    }
    Product {
        name: (project.namePrefixTests + "cantestbench");

        Group {
            name: "QML network definitions";
            files: [
                "test/example_network1.qml",
            ]
        }
    }
}
