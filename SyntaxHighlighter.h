#ifndef SYNTAXHIGHLIGHTER_H
#define SYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QQuickTextDocument>
#include <QQmlParserStatus>
#include <QStringList>
#include <QVariantList>
#include <QList>
#include <QTimer>

#include "QQmlPtrPropertyHelpers.h"
#include "QQmlVarPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlObjectListModel.h"

class ObjectHelp;

class QmlSyntaxTokenId : public QObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (int, position)
    QML_CONSTANT_CSTREF_PROPERTY (QString, identifier)
    QML_CONSTANT_CSTREF_PROPERTY (QString, typeName)

public:
    explicit QmlSyntaxTokenId (const int position = 0, const QString & identifier = "", const QString & typeName = "", QObject * parent = Q_NULLPTR);
};

template<> struct qLess<QmlSyntaxTokenId *> {
    bool operator() (QmlSyntaxTokenId * left, QmlSyntaxTokenId * right) const {
        return (left->get_identifier () < right->get_identifier ());
    }
};

class QmlSyntaxHighlighter : public QSyntaxHighlighter {
    Q_OBJECT
    QML_READONLY_VAR_PROPERTY (bool, useDarkTheme)

public:
    explicit QmlSyntaxHighlighter (QTextDocument * document = Q_NULLPTR);

    enum BlockStates {
        Normal,
        Commented,
    };

protected:
    void highlightBlock (const QString & text) Q_DECL_FINAL;
    void refreshFormats (void);

private:
    QTextCharFormat keywordFormat;
    QTextCharFormat memberFormat;
    QTextCharFormat numberFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat objectFormat;
    QTextCharFormat identifierFormat;
    QTextCharFormat commentFormat;
    struct Rule {
        QString           pattern;
        QTextCharFormat * format;
    };
    struct RuleMultiline {
        QString           patternStart;
        QString           patternEnd;
        QTextCharFormat * format;
        BlockStates       state;
    };
    const QStringList            keywords;
    const QVector<Rule>          highlightingRules;
    const QVector<RuleMultiline> highlightingRulesMultiline;
};

class DocInfo : public QObject {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (QTextDocument, document)

public:
    explicit DocInfo (QObject * parent = Q_NULLPTR);

    Q_INVOKABLE int getLine   (const int position) const;
    Q_INVOKABLE int getColumn (const int position) const;

    Q_INVOKABLE int getPosition (const int line, const int column) const;

    Q_INVOKABLE int getLinesOfCode (void) const;
};

class SyntaxHighlighter : public QObject, public QQmlParserStatus {
    Q_OBJECT
    Q_INTERFACES (QQmlParserStatus)
    QML_WRITABLE_VAR_PROPERTY (bool, useDarkTheme)
    QML_READONLY_PTR_PROPERTY (DocInfo, info)
    QML_WRITABLE_PTR_PROPERTY (QQuickTextDocument, document)
    QML_OBJMODEL_PROPERTY (QmlSyntaxTokenId, idsList)

public:
    explicit SyntaxHighlighter (QObject * parent = Q_NULLPTR);

    Q_INVOKABLE QVariantList getAutoCompletionAtPos (const int position);
    Q_INVOKABLE ObjectHelp * getContextualHelpAtPos (const int position);

protected:
    void classBegin        (void) Q_DECL_FINAL;
    void componentComplete (void) Q_DECL_FINAL;
    void refreshAst        (void);

private:
    struct SyntaxToken {
        enum Category {
            Id,
            Type,
            Property,
            OpenBrace,
            CloseBrace,
        } category;
        QString name;
        int position;
    };
    bool m_dirty;
    QTimer * m_inhibitReparse;
    QList<SyntaxToken *> m_ast;
};

#endif // SYNTAXHIGHLIGHTER_H
