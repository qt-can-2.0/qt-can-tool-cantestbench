#ifndef NODE_H
#define NODE_H

#include <QObject>

#include "IdentifiableObject.h"
#include "Routine.h"
#include "IO.h"

QML_ENUM_CLASS (Modes,
                INACTIVE,
                ACTIVE,
                PASSIVE,
                NB_MODES)

class Memory;

class Board : public BasicObject {
    Q_OBJECT
    QML_DEFAULT_PROPERTY (subObjects)
    QML_LIST_PROPERTY (QObject, subObjects)
    QML_REFLIST_PROPERTY (AbstractIO, ios, 20)

public:
    explicit Board (QObject * parent = Q_NULLPTR);
    virtual ~Board (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

class Node : public BasicObject {
    Q_OBJECT
    Q_INTERFACES (QQmlParserStatus)
    QML_DEFAULT_PROPERTY (subObjects)
    QML_LIST_PROPERTY (QObject, subObjects)
    QML_REFLIST_PROPERTY (Board, boards, 10)
    QML_REFLIST_PROPERTY (AbstractRoutine, routines, 10)
    QML_CONSTANT_PTR_PROPERTY (Memory, memory)
    QML_WRITABLE_VAR_PROPERTY (Modes::Type, mode)
    QML_WRITABLE_CSTREF_PROPERTY (QString, description)

public:
    explicit Node (QObject * parent = Q_NULLPTR);
    virtual ~Node (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

public slots:
    void init  (void);
    void start (void);
    void stop  (void);
    void reset (void);

signals:
    void started (void);
    void stopped (void);
};

#endif // NODE_H
