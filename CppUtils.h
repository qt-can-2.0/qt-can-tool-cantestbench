#ifndef CPPUTILS_H
#define CPPUTILS_H

#include <QtGlobal>

template<class T> struct KeyValueRange {
    typedef typename T::const_iterator It;

    const T & _hashmap;

    explicit KeyValueRange (const T & hashmap) : _hashmap (hashmap) { }

    struct iterator {
        It _it;

        explicit iterator (const It & it) : _it (it) { }

        It operator* (void) const {
            return _it;
        }

        iterator & operator++ (void) {
            ++_it;
            return (* this);
        }

        bool operator!= (const iterator & other) const {
            return (this->_it != other._it);
        }
    };
    iterator begin (void) const { return iterator (_hashmap.constBegin ()); }
    iterator end   (void) const { return iterator (_hashmap.constEnd ()); }
};

template<class T> const T & arrayRange (const T & list) Q_DECL_NOEXCEPT {
    return list;
}

template<class T> KeyValueRange<T> keyValueRange (const T & hashmap) Q_DECL_NOEXCEPT {
    return KeyValueRange<T> (hashmap);
}

#endif // CPPUTILS_H
