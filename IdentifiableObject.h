#ifndef BasicObject_H
#define BasicObject_H

#include <QObject>
#include <QString>
#include <QStringBuilder>
#include <QJsonObject>
#include <QQmlProperty>
#include <QQmlScriptString>
#include <QQmlParserStatus>
#include <QQmlExpression>

#include "QQmlEnumClassHelper.h"
#include "QQmlObjectListModel.h"
#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlListPropertyHelper.h"

#define QML_DEFAULT_PROPERTY(NAME) Q_CLASSINFO ("DefaultProperty", #NAME)

#define QS QStringLiteral

QML_ENUM_CLASS (ObjectFamily,
                UNKNOWN_FAMILY,
                NODE,
                BOARD,
                IO,
                SENSOR,
                ACTUATOR,
                LINK,
                TRANSFORMER,
                ROUTINE,
                VALUE,
                POSITION,
                SIZE,
                ANGLE,
                BLOCK,
                MARKER,
                GROUP,
                WORLD,
                CANOPEN,
                CANBUS,
                SERIALBUS,
                NB_FAMILIES)

QML_ENUM_CLASS (ObjectType,
                UNKNOWN_TYPE,
                ANALOG,
                DIGITAL,
                HYBRID,
                NB_TYPES)

QML_ENUM_CLASS (ObjectDirection,
                UNKNOWN_DIRECTION,
                INPUT,
                OUTPUT,
                NB_DIRECTIONS)

template<class T> struct Reference {
public:
    explicit Reference (T * ptr = Q_NULLPTR) : _ptr (ptr) {
        watch ();
    }

    T * data (void) {
        return _ptr;
    }
    T * data (void) const {
        return _ptr;
    }
    inline void clear (void) {
        set (Q_NULLPTR);
    }
    inline void operator = (T * ptr) {
        set (ptr);
    }
    inline T * operator -> (void) const {
        return _ptr;
    }
    inline operator T * (void) const {
        return _ptr;
    }
    inline operator bool (void) const {
        return (_ptr != Q_NULLPTR);
    }
    inline bool operator == (T * ptr) const {
        return (_ptr == ptr);
    }
    inline bool operator != (T * ptr) const {
        return (_ptr != ptr);
    }

protected:
    inline void set (T * ptr) {
        if (ptr != _ptr) {
            unwatch ();
            _ptr = ptr;
            watch ();
        }
    }
    inline void watch (void) {
        if (_ptr) {
            _connection = QObject::connect (_ptr, &T::destroyed, [this] (void) {
                this->_ptr = Q_NULLPTR;
            });
        }
    }
    inline void unwatch (void) {
        if (_ptr) {
             QObject::disconnect (_connection);
        }
    }

private:
    T * _ptr;
    QMetaObject::Connection _connection;
};

class BasicObject : public QObject, public QQmlParserStatus {
    Q_OBJECT
    Q_INTERFACES (QQmlParserStatus)
    QML_CONSTANT_VAR_PROPERTY (ObjectFamily::Type, family)
    QML_WRITABLE_CSTREF_PROPERTY (QString, uid)
    QML_READONLY_CSTREF_PROPERTY (QString, path)
    QML_WRITABLE_CSTREF_PROPERTY (QString, title)

public:
    explicit BasicObject (const ObjectFamily::Type family = ObjectFamily::UNKNOWN_FAMILY, QObject * parent = Q_NULLPTR);
    virtual ~BasicObject (void);

    void classBegin        (void) Q_DECL_FINAL;
    void componentComplete (void) Q_DECL_FINAL;

    virtual void onComponentCompleted (void);

    void refreshPath (void);

    void updateState (const QJsonObject & values);

    virtual QJsonObject exportState (void) const;

    template<class T> T * as (void) {
        return qobject_cast<T *> (this);
    }
};

class ObjectsGroup : public BasicObject {
    Q_OBJECT
    QML_DEFAULT_PROPERTY (subObjects)
    QML_LIST_PROPERTY (QObject, subObjects)

public:
    explicit ObjectsGroup (QObject * parent = Q_NULLPTR);
    virtual ~ObjectsGroup (void);

    void onComponentCompleted (void) Q_DECL_FINAL;
};

class AbstractObjectListModel : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY (int count READ getCount NOTIFY countChanged)

public:
    explicit AbstractObjectListModel (QObject * parent = Q_NULLPTR);
    virtual ~AbstractObjectListModel (void);

    int getCount (void) const;

    Q_INVOKABLE void insert (QObject * object, const int idx);
    Q_INVOKABLE void append  (QObject * object);
    Q_INVOKABLE void prepend (QObject * object);
    Q_INVOKABLE void remove (QObject * object);
    Q_INVOKABLE void clear (void);
    Q_INVOKABLE QObject * get (const int idx) const;

protected:
    virtual void      implInsert   (QObject * object, const int idx) = 0;
    virtual void      implRemove   (QObject * object) = 0;
    virtual void      implClear    (void) = 0;
    virtual int       implCount    (void) const = 0;
    virtual int       implIndexOf  (QObject * object) const = 0;
    virtual bool      implContains (QObject * object) const = 0;
    virtual bool      implAccept   (QObject * object) const = 0;
    virtual bool      implValid    (const int idx) const = 0;
    virtual QObject * implGet      (const int idx) const = 0;

    inline void doInsertAt (QObject * object, const int idx);

protected slots:
    void doRegisterItem (QObject * object);
    void doUnregisterItem (QObject * object);
    void onItemDestroyed (QObject * object);

signals:
    void countChanged (void);
    void itemAdded    (QObject * item);
    void itemRemoved  (QObject * item);
};

template<class T> class ObjectListModel : public AbstractObjectListModel { // NOTE : DOESN'T OWN ANYTHING
public:
    explicit ObjectListModel (const int reserved = 0, const QStringList & roles = QStringList (), QObject * parent = Q_NULLPTR)
        : AbstractObjectListModel (parent)
    {
        m_items.reserve (reserved);
        int role = Qt::DisplayRole;
        m_roles.insert (role, "object");
        // TODO : handle extra roles
        Q_UNUSED (roles)
    }
    virtual ~ObjectListModel (void) { }

    T * at (const int idx) const {
        return (implValid (idx) ? m_items.at (idx) : Q_NULLPTR);
    }

    using  iterator = typename QVector<T *>::const_iterator;
    inline iterator begin (void) const { return m_items.constBegin (); }
    inline iterator end   (void) const { return m_items.constEnd   (); }

protected:
    int rowCount (const QModelIndex & parent) const Q_DECL_FINAL {
        return (!parent.isValid () ? implCount () : 0);
    }
    QVariant data (const QModelIndex & idx, int role) const Q_DECL_FINAL {
        QVariant ret;
        if (!idx.parent ().isValid ()) {
            if (T * item = at (idx.row ())) {
                if (role == Qt::DisplayRole) {
                    ret = QVariant::fromValue (item);
                }
                else {
                    // TODO : handle extra roles
                }
            }
        }
        return ret;
    }
    QHash<int, QByteArray> roleNames  (void) const Q_DECL_FINAL {
        return m_roles;
    }
    void implInsert (QObject * object, const int idx) Q_DECL_FINAL {
        m_items.insert (idx, qobject_cast<T *> (object));
    }
    void implRemove (QObject * object) Q_DECL_FINAL {
        m_items.remove (implIndexOf (qobject_cast<T *> (object)));
    }
    void implClear (void) Q_DECL_FINAL {
        m_items.clear ();
    }
    int implCount (void) const Q_DECL_FINAL {
        return m_items.count ();
    }
    int implIndexOf (QObject * object) const Q_DECL_FINAL {
        return m_items.indexOf (qobject_cast<T *> (object));
    }
    bool implContains (QObject * object) const Q_DECL_FINAL {
        return m_items.contains (qobject_cast<T *> (object));
    }
    bool implAccept (QObject * object) const Q_DECL_FINAL {
        return (qobject_cast<T *> (object) != Q_NULLPTR);
    }
    bool implValid (const int idx) const Q_DECL_FINAL {
        return (idx >= 0 && idx < m_items.count ());
    }
    QObject * implGet (const int idx) const Q_DECL_FINAL {
        return at (idx);
    }

private:
    QVector<T *> m_items;
    QHash<int, QByteArray> m_roles;
};

#define QML_REFLIST_PROPERTY(TYPE,NAME,SIZE) \
    private: \
        Q_PROPERTY (AbstractObjectListModel * NAME READ getAsPtr_##NAME CONSTANT) \
    protected: \
        ObjectListModel<TYPE> m_##NAME {SIZE}; \
    public: \
        ObjectListModel<TYPE> & getAsRef_##NAME (void) { return m_##NAME; } \
        const ObjectListModel<TYPE> & getAsConstRef_##NAME (void) const { return m_##NAME; } \
        AbstractObjectListModel * getAsPtr_##NAME (void) { return &m_##NAME; } \
    private:

#endif // BasicObject_H
