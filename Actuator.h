#ifndef ABSTRACTACTUATOR_H
#define ABSTRACTACTUATOR_H

#include <QObject>

#include "IdentifiableObject.h"

class AnalogOutput;
class DigitalOutput;
class PhysicalValue;
class LinkAnalogOutputToAnalogActuator;
class LinkDigitalOutputToDigitalActuator;
class LinkAnalogActuatorToPhysicalValue;
class LinkDigitalOutputToHybridActuator;
class LinkHybridActuatorToPhysicalValue;

class AbstractActuator : public BasicObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (ObjectType::Type, type)
    QML_WRITABLE_CSTREF_PROPERTY (QString, description)

public:
    explicit AbstractActuator (const ObjectType::Type type = ObjectType::UNKNOWN_TYPE, QObject * parent = Q_NULLPTR);
    virtual ~AbstractActuator (void);
};

class AnalogActuator : public AbstractActuator {
    Q_OBJECT
    // digital values
    QML_WRITABLE_VAR_PROPERTY (int, valRaw)
    QML_WRITABLE_VAR_PROPERTY (int, minRaw)
    QML_WRITABLE_VAR_PROPERTY (int, maxRaw)
    // speed values
    QML_WRITABLE_VAR_PROPERTY (int, minSpeed)
    QML_WRITABLE_VAR_PROPERTY (int, maxSpeed)
    QML_READONLY_VAR_PROPERTY (int, valSpeed)
    // percent value
    QML_READONLY_VAR_PROPERTY (int, percents)
    // display options
    QML_WRITABLE_VAR_PROPERTY (int, decimals)
    QML_WRITABLE_VAR_PROPERTY (bool, useSplitPoint)
    QML_WRITABLE_CSTREF_PROPERTY (QString, unit)
    // source link
    QML_WRITABLE_PTR_PROPERTY (LinkAnalogOutputToAnalogActuator, sourceLink)
    // target links
    QML_WRITABLE_PTR_PROPERTY (LinkAnalogActuatorToPhysicalValue, targetLink)

public:
    explicit AnalogActuator (QObject * parent = Q_NULLPTR);
    virtual ~AnalogActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void refreshValSpeed (void);
    void refreshPercents (void);

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class DigitalActuator : public AbstractActuator {
    Q_OBJECT
    // digital value
    QML_WRITABLE_VAR_PROPERTY (bool, value)
    // display options
    QML_WRITABLE_CSTREF_PROPERTY (QString, trueLabel)
    QML_WRITABLE_CSTREF_PROPERTY (QString, falseLabel)
    // source link
    QML_WRITABLE_PTR_PROPERTY (LinkDigitalOutputToDigitalActuator, sourceLink)

public:
    explicit DigitalActuator (QObject * parent = Q_NULLPTR);
    virtual ~DigitalActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    QJsonObject exportState (void) const Q_DECL_FINAL;
};

class HybridActuator : public AbstractActuator {
    Q_OBJECT
    // digital value
    QML_WRITABLE_VAR_PROPERTY (bool, value)
    // speed values
    QML_WRITABLE_VAR_PROPERTY (int, ratedSpeed)
    QML_READONLY_VAR_PROPERTY (int, valSpeed)
    // display options
    QML_WRITABLE_VAR_PROPERTY (int, decimals)
    QML_WRITABLE_CSTREF_PROPERTY (QString, unit)
    // source link
    QML_WRITABLE_PTR_PROPERTY (LinkDigitalOutputToHybridActuator, sourceLink)
    // target links
    QML_WRITABLE_PTR_PROPERTY (LinkHybridActuatorToPhysicalValue, targetLink)

public:
    explicit HybridActuator (QObject * parent = Q_NULLPTR);
    virtual ~HybridActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void refreshValSpeed (void);
};

#endif // ABSTRACTACTUATOR_H
