#ifndef NETWORKDEFINITION_H
#define NETWORKDEFINITION_H

#include <QObject>
#include <QQmlParserStatus>

#include "QQmlObjectListModel.h"
#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlListPropertyHelper.h"

#include "IdentifiableObject.h"

class PhysicalWorld;

QML_ENUM_CLASS (Tabs,
                DATASHEET,
                NODES_IO,
                LOGIC,
                PHYSIC_3D,
                DASHBOARD)

class NetworkDefinition : public QObject, public QQmlParserStatus {
    Q_OBJECT
    Q_INTERFACES (QQmlParserStatus)
    QML_DEFAULT_PROPERTY (subObjects)
    QML_LIST_PROPERTY (QObject, subObjects)
    QML_WRITABLE_CSTREF_PROPERTY (QString, uid)
    QML_WRITABLE_CSTREF_PROPERTY (QString, title)
    QML_WRITABLE_VAR_PROPERTY (int, clock)
    QML_WRITABLE_VAR_PROPERTY (bool, dumpObjectsOnInit)
    QML_WRITABLE_VAR_PROPERTY (bool, benchmarkSimulatorTick)
    QML_WRITABLE_VAR_PROPERTY (bool, benchmarkCanFramesTiming)
    QML_WRITABLE_VAR_PROPERTY (bool, hideSensorsPanel)
    QML_WRITABLE_VAR_PROPERTY (bool, hideActuatorsPanel)
    QML_WRITABLE_VAR_PROPERTY (bool, hideMessagesPanel)
    QML_WRITABLE_VAR_PROPERTY (Tabs::Type, startTab)
    QML_CONSTANT_PTR_PROPERTY (PhysicalWorld, world)

public:
    explicit NetworkDefinition (QObject * parent = Q_NULLPTR);

    void classBegin        (void) Q_DECL_FINAL;
    void componentComplete (void) Q_DECL_FINAL;
};

#endif // NETWORKDEFINITION_H
