
#include "Renderer.h"

#include "Physics.h"
#include "MathUtils.h"
#include "CppUtils.h"
#include "QtCAN.h"

#include <QQuickWindow>

RendererEngine::RendererEngine (void)
    : QQuickFramebufferObject::Renderer ()
    , QOpenGLFunctions ()
{
    initializeOpenGLFunctions ();

    QOpenGLShader * vertexShader = new QOpenGLShader (QOpenGLShader::Vertex, &m_glslProgram);
    vertexShader->compileSourceFile (":/glsl/Renderer.vsh");
    DUMP << "Vertex shader compiled :" << vertexShader->isCompiled ();

    QOpenGLShader * fragmentShader = new QOpenGLShader (QOpenGLShader::Fragment, &m_glslProgram);
    fragmentShader->compileSourceFile (":/glsl/Renderer.fsh");
    DUMP << "Fragment shader compiled :" << fragmentShader->isCompiled ();

    const bool vertexShaderAdded = m_glslProgram.addShader (vertexShader);
    DUMP << "Vertex shader added :" << vertexShaderAdded;

    const bool fragmentShaderAdded = m_glslProgram.addShader (fragmentShader);
    DUMP << "Fragment shader added :" << fragmentShaderAdded;

    const bool shaderLinked = m_glslProgram.link ();
    DUMP << "Shader program linked :" << shaderLinked;

    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void RendererEngine::synchronize (QQuickFramebufferObject * fboItem) {
    static const Cube cube;
    static const float viewAngleY (70.0f);
    static const float tanY (float (qTan (qreal (qDegreesToRadians (viewAngleY / 2.0f)))));

    if (RendererItem * rendererItem = qobject_cast<RendererItem *> (fboItem)) {
        if (PhysicalWorld * world = rendererItem->get_world ()) { // draw blocks
            m_vertices = world->vertices;
            m_colors   = world->colors;

            const float viewWidth  (float (rendererItem->width  ()));
            const float viewHeight (float (rendererItem->height ()));
            const float viewRatio  (viewWidth / viewHeight);

            const float tanX (viewRatio * tanY);

            const float percent (float (rendererItem->get_viewingZoom ()));

            const float minLeft (float (world->get_bounds ()->get_toLeft ()->get_min ()));
            const float maxLeft (float (world->get_bounds ()->get_toLeft ()->get_max ()));

            const float minRight (float (world->get_bounds ()->get_toRight ()->get_min ()));
            const float maxRight (float (world->get_bounds ()->get_toRight ()->get_max ()));

            const float minBack (float (world->get_bounds ()->get_toBack ()->get_min ()));
            const float maxBack (float (world->get_bounds ()->get_toBack ()->get_max ()));

            const float minFront (float (world->get_bounds ()->get_toFront ()->get_min ()));
            const float maxFront (float (world->get_bounds ()->get_toFront ()->get_max ()));

            const float minTop (float (world->get_bounds ()->get_toTop ()->get_min ()));
            const float maxTop (float (world->get_bounds ()->get_toTop ()->get_max ()));

            const float minBottom (float (world->get_bounds ()->get_toBottom ()->get_min ()));
            const float maxBottom (float (world->get_bounds ()->get_toBottom ()->get_max ()));

            QVector3D cameraNormal;
            QVector3D verticalNormal;
            float sceneMinHalfWidth = 0;
            float sceneMaxHalfWidth = 0;
            float sceneMinHalfHeight = 0;
            float sceneMaxHalfHeight = 0;
            float sceneSolidDepth = 0;
            float sceneWorldDepth = 0;
            switch (rendererItem->get_viewingSide ()) {
                case Sides::LEFT: {
                    cameraNormal = QVector3D (-1, 0, 0);
                    verticalNormal = cube.vectorUpward;
                    sceneMinHalfWidth = qMax (minBack, minFront);
                    sceneMaxHalfWidth = qMax (maxBack, maxFront);
                    sceneMinHalfHeight = qMax (minBottom, minTop);
                    sceneMaxHalfHeight = qMax (maxBottom, maxTop);
                    sceneSolidDepth = minLeft;
                    sceneWorldDepth = maxLeft;
                    break;
                }
                case Sides::RIGHT: {
                    cameraNormal = QVector3D (+1, 0, 0);
                    verticalNormal = cube.vectorUpward;
                    sceneMinHalfWidth = qMax (minBack, minFront);
                    sceneMaxHalfWidth = qMax (maxBack, maxFront);
                    sceneMinHalfHeight = qMax (minBottom, minTop);
                    sceneMaxHalfHeight = qMax (maxBottom, maxTop);
                    sceneSolidDepth = minRight;
                    sceneWorldDepth = maxRight;
                    break;
                }
                case Sides::BACK: {
                    cameraNormal = QVector3D (0, -1, 0);
                    verticalNormal = cube.vectorUpward;
                    sceneMinHalfWidth = qMax (minLeft, minRight);
                    sceneMaxHalfWidth = qMax (maxLeft, maxRight);
                    sceneMinHalfHeight = qMax (minBottom, minTop);
                    sceneMaxHalfHeight = qMax (maxBottom, maxTop);
                    sceneSolidDepth = minBack;
                    sceneWorldDepth = maxBack;
                    break;
                }
                case Sides::FRONT: {
                    cameraNormal = QVector3D (0, +1, 0);
                    verticalNormal = cube.vectorUpward;
                    sceneMinHalfWidth = qMax (minLeft, minRight);
                    sceneMaxHalfWidth = qMax (maxLeft, maxRight);
                    sceneMinHalfHeight = qMax (minBottom, minTop);
                    sceneMaxHalfHeight = qMax (maxBottom, maxTop);
                    sceneSolidDepth = minFront;
                    sceneWorldDepth = maxFront;
                    break;
                }
                case Sides::BOTTOM: {
                    cameraNormal = QVector3D (0, 0, -1);
                    verticalNormal = cube.vectorForward;
                    sceneMinHalfWidth = qMax (minLeft, minRight);
                    sceneMaxHalfWidth = qMax (maxLeft, maxRight);
                    sceneMinHalfHeight = qMax (minBack, minFront);
                    sceneMaxHalfHeight = qMax (maxBack, maxFront);
                    sceneSolidDepth = minBottom;
                    sceneWorldDepth = maxBottom;
                    break;
                }
                case Sides::TOP: {
                    cameraNormal = QVector3D (0, 0, +1);
                    verticalNormal = cube.vectorForward;
                    sceneMinHalfWidth = qMax (minLeft, minRight);
                    sceneMaxHalfWidth = qMax (maxLeft, maxRight);
                    sceneMinHalfHeight = qMax (minBack, minFront);
                    sceneMaxHalfHeight = qMax (maxBack, maxFront);
                    sceneSolidDepth = minTop;
                    sceneWorldDepth = maxTop;
                    break;
                }
                default: break;
            }

            const float distanceToFitMinWidth = (sceneMinHalfWidth / tanX);
            const float distanceToFitMaxWidth = (sceneMaxHalfWidth / tanX);

            const float distanceToFitMinHeight = (sceneMinHalfHeight / tanY);
            const float distanceToFitMaxHeight = (sceneMaxHalfHeight / tanY);

            const float minDistance = (qMax (distanceToFitMinWidth, distanceToFitMinHeight) + sceneSolidDepth +1);
            const float maxDistance = (qMax (distanceToFitMaxWidth, distanceToFitMaxHeight) + sceneWorldDepth +1);

            const float sceneHalfWidth  = MathUtils::convert (percent, 0.0f, 100.0f, sceneMinHalfWidth,  sceneMaxHalfWidth);
            const float sceneHalfHeight = MathUtils::convert (percent, 0.0f, 100.0f, sceneMinHalfHeight, sceneMaxHalfHeight);

            const float distance = MathUtils::convert (percent, 0.0f, 100.0f, minDistance, maxDistance);

            const QVector3D minMiddle ((minRight - minLeft) / 2.0f,
                                       (minFront - minBack) / 2.0f,
                                       (minTop - minBottom) / 2.0f);

            // projection : perspective : view angle Y, view ratio width/height, near plane, far plane
            m_projectionMatrix.setToIdentity ();
            if (rendererItem->get_usePerspective ()) {
                m_projectionMatrix.perspective (viewAngleY, viewRatio, 1.0f, 1000.0f);
            }
            else {
                // TODO : adjust window ratio to avoid stretching object
                const float sceneHalf = qMax (sceneHalfWidth, sceneHalfHeight);
                m_projectionMatrix.ortho (-(sceneHalf * viewRatio), +(sceneHalf * viewRatio), -sceneHalf, +sceneHalf, 1.0f, 1000.0f);
            }

            // camera : look at : eye pos, center of scene, up vector
            m_worldToCameraMatrix.setToIdentity ();
            m_worldToCameraMatrix.lookAt ((minMiddle + cameraNormal * distance), minMiddle, verticalNormal);
        }
    }
}

void RendererEngine::render (void) {
    glDepthMask (true);

    glClearColor (0,0,0,0);

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFrontFace (GL_CW);
    glCullFace (GL_FRONT);

    glEnable (GL_CULL_FACE);
    glEnable (GL_DEPTH_TEST);

    if (!m_vertices.isEmpty () && m_colors.count () == m_vertices.count ()) {
        m_glslProgram.bind ();

        m_glslProgram.setUniformValue ("projectionMatrix",    m_projectionMatrix);
        m_glslProgram.setUniformValue ("worldToCameraMatrix", m_worldToCameraMatrix);

        m_glslProgram.enableAttributeArray ("currentVertex");
        m_glslProgram.enableAttributeArray ("currentColor");

        m_glslProgram.setAttributeArray ("currentVertex", m_vertices.constData ());
        m_glslProgram.setAttributeArray ("currentColor",  m_colors.constData ());

        glDrawArrays (GL_TRIANGLES, 0, m_vertices.size ());

        m_glslProgram.disableAttributeArray ("currentVertex");
        m_glslProgram.disableAttributeArray ("currentColor");

        m_glslProgram.release ();
    }

    glDisable (GL_DEPTH_TEST);
    glDisable (GL_CULL_FACE);

    update (); // need update in SG
}

QOpenGLFramebufferObject * RendererEngine::createFramebufferObject (const QSize & size) {
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment (QOpenGLFramebufferObject::CombinedDepthStencil);
    format.setSamples (2);
    return new QOpenGLFramebufferObject (size, format);
}

RendererItem::RendererItem (QQuickItem * parent)
    : QQuickFramebufferObject (parent)
    , m_world (Q_NULLPTR)
    , m_viewingSide (Sides::FRONT)
    , m_viewingZoom (100)
    , m_usePerspective (true)
{
    connect (this, &RendererItem::usePerspectiveChanged, this, &RendererItem::update,         Qt::UniqueConnection);
    connect (this, &RendererItem::viewingSideChanged,    this, &RendererItem::update,         Qt::UniqueConnection);
    connect (this, &RendererItem::viewingZoomChanged,    this, &RendererItem::update,         Qt::UniqueConnection);
    connect (this, &RendererItem::worldChanged,          this, &RendererItem::onWorldChanged, Qt::UniqueConnection);
}

QQuickFramebufferObject::Renderer * RendererItem::createRenderer (void) const {
    return new RendererEngine;
}

void RendererItem::onWorldChanged (void) {
    if (m_world != Q_NULLPTR) {
        connect (m_world, &PhysicalWorld::needsRedraw, this, &RendererItem::update, Qt::UniqueConnection);
        update ();
    }
}
