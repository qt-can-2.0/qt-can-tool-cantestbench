import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlTricks.UiElements 2.0;
import QtQmlTricks.SmartDataModels 2.0;
import QtCAN.CanTestBench 2.0;
import QtCAN.Utils 2.0;
import "components";

Window {
    title: "Virtual Testbench - Qt CAN 2.0";
    color: Style.colorWindow;
    width: 1280;
    height: 800;
    visible: true;
    visibility: Window.Maximized;
    minimumWidth: 1280;
    minimumHeight: 640;
    onVisibleChanged: {
        if (!visible) {
            Qt.quit ();
        }
    }
    Component.onDestruction: { Qt.quit (); }

    Item {
        states: State {
            when: (Style !== null);

            PropertyChanges {
                target: Style;
                useDarkTheme: Shared.rememberTheme;
                roundness: 5;
                spacingSmall: 4;
                spacingNormal: 6;
                spacingBig: 16;
                themeFadeTime: 0;
                fontSizeNormal: 15;
                onUseDarkThemeChanged: { Shared.rememberTheme = useDarkTheme; }
            }
        }
    }
    WindowIconHelper {
        iconPath: "qrc:///vcan_testbench_logo.svg";
    }
    Loader {
        id: loader;
        source: "ui_testBench.qml";
        active: false;
        asynchronous: false;
        anchors.fill: parent;
        onStatusChanged: {
            if (status === Loader.Ready) {
                splash.visible = false;
            }
        }
    }
    Rectangle {
        id: splash;
        color: Style.colorSecondary;
        anchors.fill: parent;

        Column {
            spacing: (Style.spacingBig * 3);
            anchors.centerIn: parent;

            Image {
                anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
                onStatusChanged: {
                    if (status === Image.Ready) {
                        timer.start ();
                    }
                }

                SvgIconHelper on source {
                    icon: "qrc:///vcan_testbench_logo.svg";
                    size: 256;
                    color: Style.colorNone;
                }
            }
            Line { implicitWidth: (lbl.width * 1.35); }
            TextLabel {
                id: lbl;
                text: title;
                font.pixelSize: (Style.fontSizeTitle * 3);
                anchors.horizontalCenter: (parent ? parent.horizontalCenter : undefined);
            }
        }
    }
    Timer {
        id: timer;
        repeat: false;
        running: false;
        interval: 10;
        onTriggered: { loader.active = true; }
    }
}
