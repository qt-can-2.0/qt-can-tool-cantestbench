
#include "Physics.h"

#include "Manager.h"
#include "MathUtils.h"
#include "CppUtils.h"

Q_DECL_CONSTEXPR static inline bool compareFloats (const float p1, const float p2) {
    return (qAbs (p1 - p2) * 100000.f <= qMin (qAbs (p1), qAbs (p2)));
}

PhysicalValue::PhysicalValue (QObject * parent)
    : BasicObject (ObjectFamily::VALUE, parent)
    , m_loop          (0.0)
    , m_val           (0.0)
    , m_min           (0.0)
    , m_max           (0.0)
    , m_accelLimit    (0.0)
    , m_decelLimit    (0.0)
    , m_currentSpeed  (0.0)
    , m_assignedSpeed (0.0)
{
    Manager::instance ().registerObject (this);
}

PhysicalValue::~PhysicalValue (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalValue::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

QJsonObject PhysicalValue::exportState (void) const {
    return QJsonObject {
        { "val", qreal (m_val) },
    };
}

void PhysicalValue::move (const int time) {
    if (time > 0) {
        // NOTE : find speed evolution
        bool stopFirst = false;
        enum SpeedChange {
            Constant,
            Acceleration,
            Deceleration,
        } speedChange = Constant;
        if (m_currentSpeed >= 0.0f && m_assignedSpeed >= 0.0f) {
            if (m_assignedSpeed > m_currentSpeed) {
                speedChange = Acceleration;
            }
            else if (m_assignedSpeed < m_currentSpeed) {
                speedChange = Deceleration;
            }
            else { }
        }
        else if (m_currentSpeed <= 0.0f && m_assignedSpeed <= 0.0f) {
            if (m_assignedSpeed < m_currentSpeed) {
                speedChange = Acceleration;
            }
            else if (m_assignedSpeed > m_currentSpeed) {
                speedChange = Deceleration;
            }
            else { }
        }
        else if ((m_currentSpeed >= 0.0f && m_assignedSpeed <= 0.0f) ||
                 (m_currentSpeed <= 0.0f && m_assignedSpeed >= 0.0f)) {
            speedChange = Deceleration;
            stopFirst = true;
        }
        else { }
        // NOTE : try to bring currentSpeed to match assignedSpeed, respecting accelLimit/decelLimit
        const float timeRatio = (float (time) / 1000.0f);
        const float deltaSpeed = (((stopFirst ? 0.0f : m_assignedSpeed) - m_currentSpeed) / timeRatio);
        if ((speedChange == Acceleration) && (qAbs (m_accelLimit) > 0.0f)) {
            const float accel = MathUtils::clamp (deltaSpeed, -qAbs (m_accelLimit), +qAbs (m_accelLimit));
            update_currentSpeed (m_currentSpeed + (accel * timeRatio));
        }
        else if ((speedChange == Deceleration) && (qAbs (m_decelLimit) > 0.0f)) {
            const float decel = MathUtils::clamp (deltaSpeed, -qAbs (m_decelLimit), +qAbs (m_decelLimit));
            update_currentSpeed (m_currentSpeed + (decel * timeRatio));
        }
        else {
            update_currentSpeed (m_assignedSpeed);
        }
        // NOTE : move value according to delta computed from currentSpeed
        const float deltaVal = MathUtils::convert (float (time), 0.0f, 1000.0f, 0.0f, m_currentSpeed);
        if (qAbs (deltaVal) > 0.0f) {
            float tmp = (m_val + deltaVal);
            if (!m_loop) {
                set_val (MathUtils::clamp (tmp, m_min, m_max));
            }
            else {
                const float range = (m_max - m_min);
                while (tmp > m_max) {
                    tmp -= range;
                }
                while (tmp < m_min) {
                    tmp += range;
                }
                set_val (tmp);
            }
        }
    }
}

PhysicalPoint::PhysicalPoint (QObject * parent)
    : BasicObject (ObjectFamily::POSITION, parent)
{
    m_onLeftToRight = new PhysicalValue (this);
    m_onBackToFront = new PhysicalValue (this);
    m_onBottomToTop = new PhysicalValue (this);
    m_onLeftToRight->set_uid ("ON_LEFT_TO_RIGHT");
    m_onBackToFront->set_uid ("ON_BACK_TO_FRONT");
    m_onBottomToTop->set_uid ("ON_BOTTOM_TO_TOP");
    Manager::instance ().registerObject (this);
}

PhysicalPoint::~PhysicalPoint (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalPoint::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_onLeftToRight->onComponentCompleted ();
    m_onBackToFront->onComponentCompleted ();
    m_onBottomToTop->onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

QJsonObject PhysicalPoint::exportState (void) const {
    return QJsonObject {
        { "onLeftToRight", m_onLeftToRight->exportState () },
        { "onBackToFront", m_onBackToFront->exportState () },
        { "onBottomToTop", m_onBottomToTop->exportState () },
    };
}

PhysicalSize::PhysicalSize (QObject * parent)
    : BasicObject (ObjectFamily::SIZE, parent)
{
    m_toLeft   = new PhysicalValue (this);
    m_toRight  = new PhysicalValue (this);
    m_toBottom = new PhysicalValue (this);
    m_toTop    = new PhysicalValue (this);
    m_toBack   = new PhysicalValue (this);
    m_toFront  = new PhysicalValue (this);
    m_toLeft->set_uid ("TO_LEFT");
    m_toRight->set_uid ("TO_RIGHT");
    m_toBottom->set_uid ("TO_BOTTOM");
    m_toTop->set_uid ("TO_TOP");
    m_toBack->set_uid ("TO_BACK");
    m_toFront->set_uid ("TO_FRONT");
    Manager::instance ().registerObject (this);
}

PhysicalSize::~PhysicalSize (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalSize::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_toLeft->onComponentCompleted ();
    m_toRight->onComponentCompleted ();
    m_toBottom->onComponentCompleted ();
    m_toTop->onComponentCompleted ();
    m_toBack->onComponentCompleted ();
    m_toFront->onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

QVector3D PhysicalSize::point (const bool leftOrRight, const bool backOrFront, const bool bottomOrTop) const {
    return QVector3D ((leftOrRight ? +m_toRight->get_val () : -m_toLeft->get_val ()),
                      (backOrFront ? +m_toFront->get_val () : -m_toBack->get_val ()),
                      (bottomOrTop ? +m_toTop->get_val ()   : -m_toBottom->get_val ()));
}

QJsonObject PhysicalSize::exportState (void) const {
    return QJsonObject {
        { "toLeft",   m_toLeft->exportState ()   },
        { "toRight ", m_toRight->exportState ()  },
        { "toBottom", m_toBottom->exportState () },
        { "toTop",    m_toTop->exportState ()    },
        { "toBack",   m_toBack->exportState ()   },
        { "toFront",  m_toFront->exportState ()  },
    };
}

PhysicalAngle::PhysicalAngle (QObject * parent)
    : BasicObject (ObjectFamily::ANGLE, parent)
{
    m_yaw   = new PhysicalValue (this);
    m_pitch = new PhysicalValue (this);
    m_roll  = new PhysicalValue (this);
    m_yaw->set_uid   ("YAW");
    m_pitch->set_uid ("PITCH");
    m_roll->set_uid  ("ROLL");
    Manager::instance ().registerObject (this);
}

PhysicalAngle::~PhysicalAngle (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalAngle::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_yaw->onComponentCompleted ();
    m_pitch->onComponentCompleted ();
    m_roll->onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

QJsonObject PhysicalAngle::exportState (void) const {
    return QJsonObject {
        { "yaw",   m_yaw->exportState ()   },
        { "pitch", m_pitch->exportState () },
        { "roll",  m_roll->exportState ()  },
    };
}

PhysicalMarker::PhysicalMarker (QObject * parent)
    : BasicObject (ObjectFamily::MARKER, parent)
    , m_anchorSide (Sides::UNKNOWN_SIDE)
    , m_referenceBlock (Q_NULLPTR)
{
    m_relativePos = new PhysicalPoint (this);
    m_absolutePos = new PhysicalPoint (this);
    m_relativePos->set_uid ("REL_POS");
    m_absolutePos->set_uid ("ABS_POS");
    Manager::instance ().registerObject (this);
}

PhysicalMarker::~PhysicalMarker (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalMarker::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_relativePos->onComponentCompleted ();
    m_absolutePos->onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

void PhysicalMarker::refreshAncestors (void) {
    ancestors.clear ();
    PhysicalBlock * tmp = m_referenceBlock;
    while (tmp != Q_NULLPTR) {
        if (!ancestors.contains (tmp)) {
            ancestors.prepend (tmp);
            tmp = tmp->get_referenceBlock ();
        }
        else {
            Manager::instance ().logError ("MARKER", this, "Cycle dependency in marker ancestors list ! Item won't be attached until it's fixed.");
            ancestors.clear ();
            break;
        }
    }
}

PhysicalBlock::PhysicalBlock (QObject * parent)
    : BasicObject (ObjectFamily::BLOCK, parent)
    , m_visible (true)
    , m_referenceBlock (Q_NULLPTR)
    , m_anchorSide (Sides::UNKNOWN_SIDE)
    , m_roundedAxis (Axis::UNKNOWN_AXIS)
    , m_ready (false)
{
    m_absoluteAngle = new PhysicalAngle (this);
    m_pivotPos      = new PhysicalPoint (this);
    m_angle         = new PhysicalAngle (this);
    m_size          = new PhysicalSize  (this);
    m_size->set_uid ("SIZE");
    m_angle->set_uid ("ANGLE");
    m_pivotPos->set_uid ("POS");
    m_absoluteAngle->set_uid ("ABS_ANGLE");
    // vertex
    connect (this, &PhysicalBlock::roundedAxisChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toLeft (),   &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toRight (),  &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toBottom (), &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toTop (),    &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toBack (),   &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    connect (m_size->get_toFront (),  &PhysicalValue::valChanged, this, &PhysicalBlock::refreshVertexList);
    // translation
    connect (m_pivotPos->get_onLeftToRight (), &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixTranslation);
    connect (m_pivotPos->get_onBottomToTop (), &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixTranslation);
    connect (m_pivotPos->get_onBackToFront (), &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixTranslation);
    // rotation
    connect (m_angle->get_pitch (), &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixRotationX);
    connect (m_angle->get_roll (),  &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixRotationY);
    connect (m_angle->get_yaw (),   &PhysicalValue::valChanged, this, &PhysicalBlock::refreshMatrixRotationZ);
    // drawing
    connect (this, &PhysicalBlock::colorChanged,   this, &PhysicalBlock::needsRedraw);
    connect (this, &PhysicalBlock::visibleChanged, this, &PhysicalBlock::needsRedraw);
    Manager::instance ().registerObject (this);
}

PhysicalBlock::~PhysicalBlock (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalBlock::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_size->onComponentCompleted ();
    m_angle->onComponentCompleted ();
    m_pivotPos->onComponentCompleted ();
    m_absoluteAngle->onComponentCompleted ();
    m_ready = true;
    Manager::instance ().intializeObject (this);
}

void PhysicalBlock::refreshAncestors (void) {
    if (m_ready) {
        ancestors.clear ();
        PhysicalBlock * tmp = this;
        while (tmp != Q_NULLPTR) {
            if (!ancestors.contains (tmp)) {
                ancestors.prepend (tmp);
                tmp = tmp->get_referenceBlock ();
            }
            else {
                Manager::instance ().logError ("BLOCK", this, "Cycle dependency in block ancestors list ! Item won't be attached until it's fixed.");
                ancestors.clear ();
                ancestors.append (this);
                break;
            }
        }
    }
}

void PhysicalBlock::refreshVertexList (void) {
    static const Cube cube;
    sidesVectors [Sides::LEFT]   = QVector3D (-m_size->get_toLeft ()->get_val (), 0, 0);
    sidesVectors [Sides::RIGHT]  = QVector3D (+m_size->get_toRight ()->get_val (), 0, 0);
    sidesVectors [Sides::BACK]   = QVector3D (0, -m_size->get_toBack ()->get_val (), 0);
    sidesVectors [Sides::FRONT]  = QVector3D (0, +m_size->get_toFront ()->get_val (), 0);
    sidesVectors [Sides::BOTTOM] = QVector3D (0, 0, -m_size->get_toBottom ()->get_val ());
    sidesVectors [Sides::TOP]    = QVector3D (0, 0, +m_size->get_toTop ()->get_val ());
    if (m_ready) {
        if (m_roundedAxis == Axis::UNKNOWN_AXIS) {
            blockVertexList [Corners::FRONT_TOP_RIGHT][Cube::BASE_VERTEX]    = m_size->point (true,  true,  true);
            blockVertexList [Corners::FRONT_TOP_LEFT][Cube::BASE_VERTEX]     = m_size->point (false, true,  true);
            blockVertexList [Corners::FRONT_BOTTOM_RIGHT][Cube::BASE_VERTEX] = m_size->point (true,  true,  false);
            blockVertexList [Corners::FRONT_BOTTOM_LEFT][Cube::BASE_VERTEX]  = m_size->point (false, true,  false);
            blockVertexList [Corners::BACK_TOP_RIGHT][Cube::BASE_VERTEX]     = m_size->point (true,  false, true);
            blockVertexList [Corners::BACK_TOP_LEFT][Cube::BASE_VERTEX]      = m_size->point (false, false, true);
            blockVertexList [Corners::BACK_BOTTOM_RIGHT][Cube::BASE_VERTEX]  = m_size->point (true,  false, false);
            blockVertexList [Corners::BACK_BOTTOM_LEFT][Cube::BASE_VERTEX]   = m_size->point (false, false, false);
        }
        else {
            switch (m_roundedAxis) {
                case Axis::LEFT_TO_RIGHT: {
                    const float s1 = m_size->get_toTop ()->get_val ();
                    const float s2 = m_size->get_toBack ()->get_val ();
                    const float s3 = m_size->get_toFront ()->get_val ();
                    const float s4 = m_size->get_toBottom ()->get_val ();
                    const float near = +m_size->get_toRight ()->get_val ();
                    const float far  = -m_size->get_toLeft  ()->get_val ();
                    if (compareFloats (s1, s2) && compareFloats (s2, s3) && compareFloats (s3, s4)) {
                        for (int idx = 0; idx < Cube::CIRCLE_POINTS; ++idx) {
                            cylinderVertexList [idx][Cube::NEAR_CIRCLE][Cube::BASE_VERTEX] = QVector3D (near, cube.cosinus [idx] * s1, cube.sinus [idx] * s1);
                            cylinderVertexList [idx][Cube::FAR_CIRCLE][Cube::BASE_VERTEX]  = QVector3D (far,  cube.cosinus [idx] * s1, cube.sinus [idx] * s1);
                        }
                    }
                    else {
                        Manager::instance ().logWarning ("BLOCK", this, "To use cylinder block on axis LEFT_to_RIGHT, block must have same size on to_TOP, to_BOTTOM, to_FRONT, and to_BACK ! It won't be rendered.");
                    }
                    break;
                }
                case Axis::BACK_TO_FRONT: {
                    const float s1 = m_size->get_toTop ()->get_val ();
                    const float s2 = m_size->get_toLeft ()->get_val ();
                    const float s3 = m_size->get_toRight ()->get_val ();
                    const float s4 = m_size->get_toBottom ()->get_val ();
                    const float near = +m_size->get_toFront ()->get_val ();
                    const float far  = -m_size->get_toBack  ()->get_val ();
                    if (compareFloats (s1, s2) && compareFloats (s2, s3) && compareFloats (s3, s4)) {
                        for (int idx = 0; idx < Cube::CIRCLE_POINTS; ++idx) {
                            cylinderVertexList [idx][Cube::NEAR_CIRCLE][Cube::BASE_VERTEX] = QVector3D (cube.cosinus [idx] * s1, near, cube.sinus [idx] * s1);
                            cylinderVertexList [idx][Cube::FAR_CIRCLE][Cube::BASE_VERTEX]  = QVector3D (cube.cosinus [idx] * s1, far,  cube.sinus [idx] * s1);
                        }
                    }
                    else {
                        Manager::instance ().logWarning ("BLOCK", this, "To use cylinder block on axis BACK_to_FRONT, block must have same size on to_TOP, to_BOTTOM, to_RIGHT, and to_LEFT ! It won't be rendered.");
                    }
                    break;
                }
                case Axis::BOTTOM_TO_TOP: {
                    const float s1 = m_size->get_toLeft ()->get_val ();
                    const float s2 = m_size->get_toBack ()->get_val ();
                    const float s3 = m_size->get_toRight ()->get_val ();
                    const float s4 = m_size->get_toFront ()->get_val ();
                    const float near = +m_size->get_toTop ()->get_val ();
                    const float far  = -m_size->get_toBottom ()->get_val ();
                    if (compareFloats (s1, s2) && compareFloats (s2, s3) && compareFloats (s3, s4)) {
                        for (int idx = 0; idx < Cube::CIRCLE_POINTS; ++idx) {
                            cylinderVertexList [idx][Cube::NEAR_CIRCLE][Cube::BASE_VERTEX] = QVector3D (cube.cosinus [idx] * s1, cube.sinus [idx] * s1, near);
                            cylinderVertexList [idx][Cube::FAR_CIRCLE][Cube::BASE_VERTEX]  = QVector3D (cube.cosinus [idx] * s1, cube.sinus [idx] * s1, far);
                        }
                    }
                    else {
                        Manager::instance ().logWarning ("BLOCK", this, "To use cylinder block on axis BOTTOM_to_TOP, block must have same size on to_FRONT, to_BACK, to_RIGHT, and to_LEFT ! It won't be rendered.");
                    }
                    break;
                }
                default: break;
            }
        }
        emit needsRedraw ();
    }
}

void PhysicalBlock::refreshMatrixRotationX (void) {
    if (m_ready) {
        QMatrix4x4 tmp;
        tmp.rotate (m_angle->get_pitch ()->get_val (), QVector3D (1, 0, 0));
        if (tmp != matrixRotationX) {
            matrixRotationX = tmp;
            emit needsRedraw ();
        }
    }
}

void PhysicalBlock::refreshMatrixRotationY (void) {
    if (m_ready) {
        QMatrix4x4 tmp;
        tmp.rotate (m_angle->get_roll ()->get_val (), QVector3D (0, 1, 0));
        if (tmp != matrixRotationY) {
            matrixRotationY = tmp;
            emit needsRedraw ();
        }
    }
}

void PhysicalBlock::refreshMatrixRotationZ (void) {
    if (m_ready) {
        QMatrix4x4 tmp;
        tmp.rotate (-m_angle->get_yaw ()->get_val (), QVector3D (0, 0, 1));
        if (tmp != matrixRotationZ) {
            matrixRotationZ = tmp;
            emit needsRedraw ();
        }
    }
}

void PhysicalBlock::refreshMatrixTranslation (void) {
    if (m_ready) {
        QMatrix4x4 tmp;
        tmp.translate (QVector3D (m_pivotPos->get_onLeftToRight ()->get_val (),
                                  m_pivotPos->get_onBackToFront ()->get_val (),
                                  m_pivotPos->get_onBottomToTop ()->get_val ()));
        if (tmp != matrixTranslation) {
            matrixTranslation = tmp;
            emit needsRedraw ();
        }
    }
}

QJsonObject PhysicalBlock::exportState (void) const {
    return QJsonObject {
        { "size",     m_size->exportState ()     },
        { "angle",    m_angle->exportState ()    },
        { "pivotPos", m_pivotPos->exportState () },
    };
}

PhysicalWorld::PhysicalWorld (QObject * parent)
    : BasicObject (ObjectFamily::WORLD, parent)
    , m_dirty (false)
{
    m_bounds = new PhysicalSize (this);
    m_bounds->set_uid ("BOUNDS");
    Manager::instance ().registerObject (this);
}

PhysicalWorld::~PhysicalWorld (void) {
    Manager::instance ().unregisterObject (this);
}

void PhysicalWorld::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    m_bounds->onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

void PhysicalWorld::physicsDirty (void) {
    m_dirty = true;
}

void PhysicalWorld::processPhysics (void) {
    static const Cube cube;
    if (m_dirty) {

        // erase previous pipeline
        vertices.clear ();
        colors.clear ();

        // blocks / cylinders
        for (PhysicalBlock * block : arrayRange (Manager::instance ().getAsConstRef_physicalBlocks ())) {

            // init transformation matrix
            QMatrix4x4 matrixTransforms;

            PhysicalBlock * previous = Q_NULLPTR;
            for (PhysicalBlock * ancestor : arrayRange (block->ancestors)) {
                // apply translation
                matrixTransforms *= ancestor->matrixTranslation;

                // move to reference point on ancestor if any
                const int anchorSide = ancestor->get_anchorSide ();
                if (previous != Q_NULLPTR && anchorSide > Sides::UNKNOWN_SIDE && anchorSide < Sides::NUMBER_OF_SIDES) {
                    matrixTransforms.translate (previous->sidesVectors [anchorSide]);
                }

                // apply rotations
                matrixTransforms *= ancestor->matrixRotationY;
                matrixTransforms *= ancestor->matrixRotationX;
                matrixTransforms *= ancestor->matrixRotationZ;

                // update last ancestor reference
                previous = ancestor;
            }

            // compute yaw pitch roll
            const float yaw = float (qRadiansToDegrees (qAtan2 (qreal (matrixTransforms (1,0)),
                                                                qreal (matrixTransforms (0,0)))));
            const float roll = float (qRadiansToDegrees (qAtan2 (qreal (matrixTransforms (2,0)),
                                                                 qreal (qSqrt (qreal (matrixTransforms (2,1)) *
                                                                               qreal (matrixTransforms (2,1)) +
                                                                               qreal (matrixTransforms (2,2)) *
                                                                               qreal (matrixTransforms (2,2)))))));
            const float pitch = float (qRadiansToDegrees (qAtan2 (qreal (matrixTransforms (2,1)),
                                                                  qreal (matrixTransforms (2,2)))));
            block->get_absoluteAngle ()->get_yaw ()->set_val   (yaw <= 0.0f ? -yaw : 360.0f - yaw);
            block->get_absoluteAngle ()->get_pitch ()->set_val (pitch);
            block->get_absoluteAngle ()->get_roll ()->set_val  (roll);

            const QColor colorNormal = block->get_color ();
            if (block->get_roundedAxis () == Axis::UNKNOWN_AXIS) {

                // transform corners vertices
                for (int idxVertex = 0; idxVertex < Corners::NUMBER_OF_CORNERS; ++idxVertex) {
                    block->blockVertexList [idxVertex][Cube::TRANS_VERTEX]
                            = (matrixTransforms * block->blockVertexList [idxVertex][Cube::BASE_VERTEX]);
                }

                // add front side quad
                addQuad (block->blockVertexList [Corners::FRONT_TOP_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        colorNormal.lighter (100));

                // add back side quad
                addQuad (block->blockVertexList [Corners::BACK_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_TOP_LEFT][Cube::TRANS_VERTEX],
                        colorNormal.lighter (100));

                // add right side quad
                addQuad (block->blockVertexList [Corners::FRONT_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        colorNormal.darker (120));

                // add left side quad
                addQuad (block->blockVertexList [Corners::BACK_TOP_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_TOP_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        colorNormal.darker (120));

                // add top side quad
                addQuad (block->blockVertexList [Corners::BACK_TOP_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_TOP_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_TOP_LEFT][Cube::TRANS_VERTEX],
                        colorNormal.lighter (120));

                // add bottom side quad
                addQuad (block->blockVertexList [Corners::FRONT_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::FRONT_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_BOTTOM_RIGHT][Cube::TRANS_VERTEX],
                        block->blockVertexList [Corners::BACK_BOTTOM_LEFT][Cube::TRANS_VERTEX],
                        colorNormal.darker (160));
            }
            else {

                // transform near/far circles vertices
                for (int idxVertex = 0; idxVertex < Cube::CIRCLE_POINTS; ++idxVertex) {
                    for (int idxCircle = 0; idxCircle < Cube::NUMBER_OF_CIRCLES; ++idxCircle) {
                        block->cylinderVertexList [idxVertex][idxCircle][Cube::TRANS_VERTEX]
                                = (matrixTransforms * block->cylinderVertexList [idxVertex][idxCircle][Cube::BASE_VERTEX]);
                    }
                }

                // add circles vertex
                for (int idxCircle = 0; idxCircle < Cube::NUMBER_OF_CIRCLES; ++idxCircle) {
                    for (int idxFirst = 1, idxSecond = 2; idxSecond < Cube::CIRCLE_POINTS; ++idxFirst, ++idxSecond) {
                        addTriangle (block->cylinderVertexList [0][idxCircle][Cube::TRANS_VERTEX],
                                block->cylinderVertexList [idxFirst  % Cube::CIRCLE_POINTS][idxCircle][Cube::TRANS_VERTEX],
                                block->cylinderVertexList [idxSecond % Cube::CIRCLE_POINTS][idxCircle][Cube::TRANS_VERTEX],
                                colorNormal);
                    }
                }

                // add sides quads vertex
                for (int idxLoop = 0; idxLoop < Cube::CIRCLE_POINTS; ++idxLoop) {
                    const int idxNext = ((idxLoop +1) % Cube::CIRCLE_POINTS);
                    addQuad (block->cylinderVertexList [idxNext][Cube::FAR_CIRCLE][Cube::TRANS_VERTEX],
                            block->cylinderVertexList [idxLoop][Cube::FAR_CIRCLE][Cube::TRANS_VERTEX],
                            block->cylinderVertexList [idxLoop][Cube::NEAR_CIRCLE][Cube::TRANS_VERTEX],
                            block->cylinderVertexList [idxNext][Cube::NEAR_CIRCLE][Cube::TRANS_VERTEX],
                            colorNormal.lighter (int (100.0f + (30.0f * cube.cosinus [idxLoop]))));
                }
            }
        }

        // markers
        for (PhysicalMarker * marker : arrayRange (Manager::instance ().getAsConstRef_physicalMarkers ())) {
            // init transformation matrix
            QMatrix4x4 matrixTransforms;

            PhysicalBlock * previous = Q_NULLPTR;
            for (PhysicalBlock * ancestor : arrayRange (marker->ancestors)) {
                // apply translation
                matrixTransforms *= ancestor->matrixTranslation;

                // move to reference point on ancestor if any
                const int anchorSide = ancestor->get_anchorSide ();
                if (previous != Q_NULLPTR && anchorSide > Sides::UNKNOWN_SIDE && anchorSide < Sides::NUMBER_OF_SIDES) {
                    matrixTransforms.translate (previous->sidesVectors [anchorSide]);
                }

                // apply rotations
                matrixTransforms *= ancestor->matrixRotationY;
                matrixTransforms *= ancestor->matrixRotationX;
                matrixTransforms *= ancestor->matrixRotationZ;

                // update last ancestor reference
                previous = ancestor;
            }

            // move to the wanted side of reference block
            const int anchorSide = marker->get_anchorSide ();
            if (anchorSide > Sides::UNKNOWN_SIDE && anchorSide < Sides::NUMBER_OF_SIDES) {
                matrixTransforms.translate (previous->sidesVectors [anchorSide]);
            }

            // get relative position
            const QVector3D relPos (marker->get_relativePos ()->get_onLeftToRight ()->get_val (),
                                    marker->get_relativePos ()->get_onBackToFront ()->get_val (),
                                    marker->get_relativePos ()->get_onBottomToTop ()->get_val ());

            // compute absolute position
            const QVector3D absPos (matrixTransforms * relPos);
            marker->get_absolutePos ()->get_onLeftToRight ()->set_val (absPos.x ());
            marker->get_absolutePos ()->get_onBackToFront ()->set_val (absPos.y ());
            marker->get_absolutePos ()->get_onBottomToTop ()->set_val (absPos.z ());
        }

        m_dirty = false;

        emit needsRedraw ();
    }
}

void PhysicalWorld::addQuad (const QVector3D & p1, const QVector3D & p2, const QVector3D & p3, const QVector3D & p4, const QColor & color) {
    addTriangle (p1, p2, p4, color);
    addTriangle (p3, p4, p2, color);
}

QVector3D PhysicalWorld::vectorFromColor (const QColor & col) const {
    return QVector3D (float (col.redF ()),
                      float (col.greenF ()),
                      float (col.blueF ()));
}

void PhysicalWorld::addTriangle (const QVector3D & p1, const QVector3D & p2, const QVector3D & p3, const QColor & color) {
    const QVector3D tmp = vectorFromColor (color);

    // front
    vertices << p1; colors << tmp;
    vertices << p2; colors << tmp;
    vertices << p3; colors << tmp;

    // back
    vertices << p3; colors << tmp;
    vertices << p2; colors << tmp;
    vertices << p1; colors << tmp;
}
