
#include "NetworkDefinition.h"

#include "Manager.h"
#include "Physics.h"

NetworkDefinition::NetworkDefinition (QObject * parent)
    : QObject (parent)
    , m_subObjects (this)
    , m_clock (50)
    , m_dumpObjectsOnInit (false)
    , m_benchmarkSimulatorTick (false)
    , m_benchmarkCanFramesTiming (false)
    , m_hideSensorsPanel (false)
    , m_hideActuatorsPanel (false)
    , m_hideMessagesPanel (false)
    , m_startTab (Tabs::NODES_IO)
{
    m_world = new PhysicalWorld (this);
}

void NetworkDefinition::classBegin (void) { }

void NetworkDefinition::componentComplete (void) {
    Manager::instance ().intializeObject (m_world);
}
