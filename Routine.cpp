
#include "Routine.h"

#include "Manager.h"
#include "Node.h"
#include "Memory.h"
#include "CppUtils.h"
#include "CanOpen.h"
#include "CanBus.h"
#include "SerialBus.h"

#include "CanOpenDefs.h"
#include "CanOpenEntry.h"
#include "CanOpenSubEntry.h"
#include "CanOpenObjDict.h"
#include "CanOpenProtocolManager.h"

#include "CanDataQmlWrapper.h"
#include "ByteArrayQmlWrapper.h"

#include <QMetaObject>
#include <QMetaMethod>

AbstractRoutine::AbstractRoutine (const Triggers::Type triggerType, QObject * parent)
    : BasicObject (ObjectFamily::ROUTINE, parent)
    , m_triggerType (triggerType)
    , m_benchmarkExecutionTime (false)
    , m_dumpMemoryAfterExecution (false)
    , m_node (Q_NULLPTR)
    , m_memory (Q_NULLPTR)
    , m_ready (false)
{ }

AbstractRoutine::~AbstractRoutine (void) { }

void AbstractRoutine::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    update_node (qobject_cast<Node *> (parent ()));
    if (m_node) {
        update_memory (m_node->get_memory ());
        m_ready = true;
    }
}

void AbstractRoutine::execute (void) {
    if (m_node->get_mode () != Modes::INACTIVE) {
        if (m_benchmarkExecutionTime) {
            m_benchmark.restart ();
        }
        trigger ();
        if (m_benchmarkExecutionTime) {
            const qint64 ns = m_benchmark.nsecsElapsed ();
            DUMP << qPrintable (get_path ())
                 << "Execution time :"
                 << qPrintable (QLatin1String (QByteArray::number (qreal (ns) / 1000000.0, 'f', 6)))
                 << "ms";
        }
        if (m_dumpMemoryAfterExecution) {
            m_memory->dump ();
        }
    }
}

void AbstractRoutine::trigger (void) { }

RoutineOnTimer::RoutineOnTimer (QObject * parent)
    : AbstractRoutine (Triggers::TIMER_TICK, parent)
    , m_interval (0)
    , m_loops (0)
{
    m_timer = new QTimer (this);
    m_timer->setTimerType (Qt::PreciseTimer);
    connect (m_timer, &QTimer::timeout, this, &RoutineOnTimer::execute);
    connect (this, &RoutineOnTimer::intervalChanged, this, &RoutineOnTimer::updateTimer);
    Manager::instance ().registerObject (this);
}

RoutineOnTimer::~RoutineOnTimer (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnTimer::start (void) {
    if (m_interval > 0) {
        m_timer->start (m_interval);
    }
}

void RoutineOnTimer::stop (void) {
    if (m_timer->isActive ()) {
        m_timer->stop ();
    }
}

void RoutineOnTimer::updateTimer (void) {
    if (m_ready) {
        if (m_timer->isActive ()) {
            m_timer->stop ();
            if (m_interval > 0) {
                m_timer->start (m_interval);
            }
        }
    }
}

void RoutineOnTimer::onComponentCompleted (void) {
    AbstractRoutine::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

void RoutineOnTimer::trigger (void) {
    ++m_loops;
    emit triggered (m_loops);
}

RoutineOnCanFrame::RoutineOnCanFrame (QObject * parent)
    : AbstractRoutine (Triggers::CAN_FRAME_RECV, parent)
    , m_canId (0x000)
    , m_canBus (Q_NULLPTR)
{
    m_canDataWrapperRx = new CanDataWrapperRx (this);
    connect (this, &RoutineOnCanFrame::canIdChanged,  this, &RoutineOnCanFrame::updateSubscription);
    connect (this, &RoutineOnCanFrame::canBusChanged, this, &RoutineOnCanFrame::updateSubscription);
    Manager::instance ().registerObject (this);
}

RoutineOnCanFrame::~RoutineOnCanFrame (void) {
    Manager::instance ().unregisterObject (this);
}

CanDataWrapperRx * RoutineOnCanFrame::getFrameRx (void) const {
    return m_canDataWrapperRx;
}

void RoutineOnCanFrame::onComponentCompleted (void) {
    AbstractRoutine::onComponentCompleted ();
    updateSubscription ();
    Manager::instance ().intializeObject (this);
}

void RoutineOnCanFrame::trigger (void) {
    emit triggered (m_canDataWrapperRx);
}

void RoutineOnCanFrame::updateSubscription (void) {
    if (m_ready) {
        if (m_canBus != Q_NULLPTR) {
            m_canBus->subscribeRoutineToCanFrameId (this, m_canId);
        }
        else {
            Manager::instance ().logWarning ("ROUTINE_ON_CAN_FRAME", this, "No CAN bus defined for routine on CAN frame !");
        }
    }
}

RoutineOnEvent::RoutineOnEvent (QObject * parent)
    : AbstractRoutine (Triggers::EVENT_SIGNAL, parent)
    , m_emiter (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

RoutineOnEvent::~RoutineOnEvent (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnEvent::onComponentCompleted (void) {
    AbstractRoutine::onComponentCompleted ();
    if (m_emiter != Q_NULLPTR) {
        const QMetaMethod slotMethod = metaObject ()->method (metaObject ()->indexOfSlot ("execute()"));
        const QByteArray signalName = m_signalName.toLocal8Bit ();
        const QMetaObject * metaObj = m_emiter->metaObject ();
        bool found = false;
        for (int metaMethodIdx = 0; metaMethodIdx < metaObj->methodCount (); ++metaMethodIdx) {
            const QMetaMethod metaMethod = metaObj->method (metaMethodIdx);
            if (metaMethod.methodType () == QMetaMethod::Signal) {
                if (metaMethod.name () == signalName) {
                    found = true;
                    connect (m_emiter, metaMethod, this, slotMethod);
                    break;
                }
            }
        }
        if (!found) {
            Manager::instance ().logError ("ROUTINE_ON_EVENT", this, "signal " % QString::fromLatin1 (signalName) % " not found in object " % metaObj->className () % " !");
        }
    }
    Manager::instance ().intializeObject (this);
}

void RoutineOnEvent::trigger (void) {
    emit triggered ();
}

AbstractCanOpenRoutine::AbstractCanOpenRoutine (const Triggers::Type triggerType, QObject * parent)
    : AbstractRoutine (triggerType, parent)
    , m_canOpen (Q_NULLPTR)
{ }

AbstractCanOpenRoutine::~AbstractCanOpenRoutine (void) { }

void AbstractCanOpenRoutine::onComponentCompleted (void) {
    AbstractRoutine::onComponentCompleted ();
    if (m_canOpen != Q_NULLPTR) {
        connect (m_canOpen, &CanOpen::initialized, this, &AbstractCanOpenRoutine::prepare, Qt::UniqueConnection);
        connect (m_canOpen, &CanOpen::resetting,   this, &AbstractCanOpenRoutine::cleanup, Qt::UniqueConnection);
    }
    else {
        Manager::instance ().logWarning ("ROUTINE_ON_CANOPEN", this, "No CANopen wrapper defined for routine on CANopen !");
    }
    Manager::instance ().intializeObject (this);
}

RoutineOnCanOpenStateChange::RoutineOnCanOpenStateChange (QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_STATE_CHANGE, parent)
    , m_canMgr (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenStateChange::~RoutineOnCanOpenStateChange (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenStateChange::prepare (void) {
    if (get_canOpen () && !m_canMgr) {
        if ((m_canMgr = get_canOpen ()->getProtocolManager ())) {
            connect (m_canMgr.data (), &CanOpenProtocolManager::localNodeStateChanged, this, &RoutineOnCanOpenStateChange::execute, Qt::UniqueConnection);
        }
    }
}

void RoutineOnCanOpenStateChange::cleanup (void) {
    if (m_canMgr) {
        disconnect (m_canMgr.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_canMgr.clear ();
    }
}

void RoutineOnCanOpenStateChange::trigger (void) {
    if (m_canMgr) {
        emit triggered (CanOpenNmtState::Type (m_canMgr->getLocalNodeState ()));
    }
}

RoutineOnCanOpenObdValChange::RoutineOnCanOpenObdValChange (QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_OBD_VAL_CHANGE, parent)
    , m_index (0x0000)
    , m_subIndex (0x00)
    , m_subEntry (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenObdValChange::~RoutineOnCanOpenObdValChange (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenObdValChange::prepare (void) {
    if (get_canOpen () && !m_subEntry) {
        if (CanOpenObjDict * obd = get_canOpen ()->getObjectDict ()) {
            if ((m_subEntry = obd->getSubEntry (CanOpenIndex (m_index), CanOpenSubIndex (m_subIndex)))) {
                connect (m_subEntry.data (), &CanOpenSubEntry::dataChanged, this, &RoutineOnCanOpenObdValChange::execute, Qt::UniqueConnection);
            }
        }
    }
}

void RoutineOnCanOpenObdValChange::cleanup (void) {
    if (m_subEntry) {
        disconnect (m_subEntry.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_subEntry.clear ();
    }
}

void RoutineOnCanOpenObdValChange::trigger (void) {
    if (m_subEntry) {
        emit triggered (m_subEntry->readToQtVariant ());
    }
}

RoutineOnCanOpenSdoWriteRequest::RoutineOnCanOpenSdoWriteRequest (QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_SDO_WRITE_REQUEST, parent)
    , m_canMgr (Q_NULLPTR)
    , m_currNodeId (0)
    , m_currIndex (0x0000)
    , m_currSubIndex (0x00)
    , m_currStatusCode (CanOpenSdoAbortCode::NoError)
{
    m_currBuffer = new ByteArrayWrapper (this);
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenSdoWriteRequest::~RoutineOnCanOpenSdoWriteRequest (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenSdoWriteRequest::prepare (void) {
    if (get_canOpen () && !m_canMgr) {
        if ((m_canMgr = get_canOpen ()->getProtocolManager ())) {
            connect (m_canMgr.data (), &CanOpenProtocolManager::recvSdoWriteRequest, this, &RoutineOnCanOpenSdoWriteRequest::onRecvSdoWriteRequest, Qt::UniqueConnection);
        }
    }
}

void RoutineOnCanOpenSdoWriteRequest::cleanup (void) {
    if (m_canMgr) {
        disconnect (m_canMgr.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_canMgr.clear ();
    }
}

void RoutineOnCanOpenSdoWriteRequest::trigger (void) {
    if (m_canMgr) {
        emit triggered (m_currNodeId, m_currIndex, m_currSubIndex, m_currBuffer);
    }
}

void RoutineOnCanOpenSdoWriteRequest::onRecvSdoWriteRequest (const CanOpenNodeId nodeId, const CanOpenIndex idx, const CanOpenSubIndex subIdx, const CanOpenSdoAbortCode statusCode, const QByteArray & buffer) {
    m_currNodeId = nodeId;
    m_currIndex = idx;
    m_currSubIndex = subIdx;
    m_currStatusCode = statusCode;
    m_currBuffer->setBytes (buffer);
    execute ();
}

RoutineOnCanOpenSdoReadReply::RoutineOnCanOpenSdoReadReply (QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_SDO_READ_REPLY, parent)
    , m_canMgr (Q_NULLPTR)
    , m_currNodeId (0)
    , m_currIndex (0x0000)
    , m_currSubIndex (0x00)
    , m_currStatusCode (CanOpenSdoAbortCode::NoError)
{
    m_currBuffer = new ByteArrayWrapper (this);
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenSdoReadReply::~RoutineOnCanOpenSdoReadReply (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenSdoReadReply::prepare (void) {
    if (get_canOpen () && !m_canMgr) {
        if ((m_canMgr = get_canOpen ()->getProtocolManager ())) {
            connect (m_canMgr.data (), &CanOpenProtocolManager::recvSdoReadReply, this, &RoutineOnCanOpenSdoReadReply::onRecvSdoReadReply, Qt::UniqueConnection);
        }
    }
}

void RoutineOnCanOpenSdoReadReply::cleanup (void) {
    if (m_canMgr) {
        disconnect (m_canMgr.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_canMgr.clear ();
    }
}

void RoutineOnCanOpenSdoReadReply::trigger (void) {
    if (m_canMgr) {
        emit triggered (m_currNodeId, m_currIndex, m_currSubIndex, m_currBuffer);
    }
}

void RoutineOnCanOpenSdoReadReply::onRecvSdoReadReply (const CanOpenNodeId nodeId, const CanOpenIndex idx, const CanOpenSubIndex subIdx, const CanOpenSdoAbortCode statusCode, const QByteArray & buffer) {
    m_currNodeId = nodeId;
    m_currIndex = idx;
    m_currSubIndex = subIdx;
    m_currStatusCode = statusCode;
    m_currBuffer->setBytes (buffer);
    execute ();
}

RoutineOnCanOpenBootUp::RoutineOnCanOpenBootUp (QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_BOOTUP, parent)
    , m_canMgr (Q_NULLPTR)
    , m_currNodeId (0)
{
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenBootUp::~RoutineOnCanOpenBootUp (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenBootUp::prepare (void) {
    if (get_canOpen () && !m_canMgr) {
        if ((m_canMgr = get_canOpen ()->getProtocolManager ())) {
            connect (m_canMgr.data (), &CanOpenProtocolManager::remoteNodeStateChanged, this, &RoutineOnCanOpenBootUp::onRecvHeartbeatState);
        }
    }
}

void RoutineOnCanOpenBootUp::cleanup (void) {
    if (m_canMgr) {
        disconnect (m_canMgr.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_canMgr.clear ();
    }
}

void RoutineOnCanOpenBootUp::trigger (void) {
    if (m_canMgr) {
        emit triggered (m_currNodeId);
    }
}

void RoutineOnCanOpenBootUp::onRecvHeartbeatState (const CanOpenNodeId nodeId, const CanOpenHeartBeatState state) {
    if (state == CanOpenHeartBeatStates::Initializing) {
        m_currNodeId = nodeId;
        execute ();
    }
}

RoutineOnSerialFrame::RoutineOnSerialFrame (QObject * parent)
    : AbstractRoutine (Triggers::SERIAL_FRAME_RECV, parent)
    , m_serialBus (Q_NULLPTR)
{
    connect (this, &RoutineOnSerialFrame::serialBusChanged, this, &RoutineOnSerialFrame::updateSubscription);
    Manager::instance ().registerObject (this);
}

RoutineOnSerialFrame::~RoutineOnSerialFrame (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnSerialFrame::onComponentCompleted (void) {
    AbstractRoutine::onComponentCompleted ();
    updateSubscription ();
    Manager::instance ().intializeObject (this);
}

void RoutineOnSerialFrame::updateSubscription (void) {
    if (m_ready) {
        if (m_serialBus) {
            m_serialBus->subscribeRoutineToSerialFrame (this);
        }
        else {
            Manager::instance ().logWarning ("ROUTINE_ON_SERIAL_FRAME", this, "No serial bus defined for routine on serial frame !");
        }
    }
}

void RoutineOnSerialFrame::trigger (void) {
    emit triggered ();
}

RoutineOnCanOpenHeartbeatConsumer::RoutineOnCanOpenHeartbeatConsumer(QObject * parent)
    : AbstractCanOpenRoutine (Triggers::CANOPEN_HB_CONSUMER, parent)
    , m_canMgr (Q_NULLPTR)
    , m_currNodeId (0)
    , m_currState (CanOpenHeartBeatStates::Initializing)
    , m_currAlive (false)
{
    Manager::instance ().registerObject (this);
}

RoutineOnCanOpenHeartbeatConsumer::~RoutineOnCanOpenHeartbeatConsumer (void) {
    Manager::instance ().unregisterObject (this);
}

void RoutineOnCanOpenHeartbeatConsumer::prepare (void) {
    if (get_canOpen () && !m_canMgr) {
        if ((m_canMgr = get_canOpen ()->getProtocolManager ())) {
            connect (m_canMgr.data (), &CanOpenProtocolManager::remoteNodeAliveChanged,
                     this, &RoutineOnCanOpenHeartbeatConsumer::onRemoteNodeAliveChanged);
            connect (m_canMgr.data (), &CanOpenProtocolManager::remoteNodeStateChanged,
                     this, &RoutineOnCanOpenHeartbeatConsumer::onRemoteNodeStateChanged);
        }
    }
}

void RoutineOnCanOpenHeartbeatConsumer::cleanup (void) {
    if (m_canMgr) {
        disconnect (m_canMgr.data (), Q_NULLPTR, this, Q_NULLPTR);
        m_canMgr.clear ();
    }
}

void RoutineOnCanOpenHeartbeatConsumer::trigger (void) {
    if (m_canMgr) {
        emit triggered (m_currNodeId, m_currAlive, m_currState);
    }
}

void RoutineOnCanOpenHeartbeatConsumer::onRemoteNodeStateChanged (const CanOpenNodeId nodeId, const CanOpenHeartBeatState state) {
    m_currNodeId = nodeId;
    m_currState = state;
    execute ();
}

void RoutineOnCanOpenHeartbeatConsumer::onRemoteNodeAliveChanged (const CanOpenNodeId nodeId, const bool alive) {
    m_currNodeId = nodeId;
    m_currAlive = alive;
    execute ();
}
