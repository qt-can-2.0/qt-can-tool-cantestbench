#ifndef SHAREDOBJECT_H
#define SHAREDOBJECT_H

#include <QObject>
#include <QString>
#include <QSettings>
#include <QJSEngine>
#include <QQmlEngine>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlVariantListModel.h"

class Manager;
class NetworkDefinition;
class Node;
class Dashboard;
class AbstractIO;
class AbstractSensor;
class AbstractActuator;
class PhysicalValue;
class BasicObject;

class SharedObject : public QObject {
    Q_OBJECT
    QML_CONSTANT_PTR_PROPERTY (Manager, manager)
    QML_CONSTANT_PTR_PROPERTY (QQmlVariantListModel, mruModel)
    QML_WRITABLE_VAR_PROPERTY (bool, rememberTheme)
    QML_WRITABLE_VAR_PROPERTY (bool, showOnlyEditable)
    QML_WRITABLE_VAR_PROPERTY (bool, showDescriptions)
    QML_WRITABLE_CSTREF_PROPERTY (QString, lastFolderUsed)
    QML_WRITABLE_PTR_PROPERTY (Node,             currentNode)
    QML_WRITABLE_PTR_PROPERTY (Dashboard,        currentDashboard)
    QML_WRITABLE_PTR_PROPERTY (AbstractIO,       highlightIO)
    QML_WRITABLE_PTR_PROPERTY (AbstractSensor,   highlightSensor)
    QML_WRITABLE_PTR_PROPERTY (AbstractActuator, highlightActuator)
    QML_WRITABLE_PTR_PROPERTY (PhysicalValue,    highlightPhyVal)

public:
    explicit SharedObject (QObject * parent = Q_NULLPTR);

    static void registerQmlTypes (QQmlEngine * qmlEngine);

    static QObject * qmlSingletonProvider (QQmlEngine * qmlEngine, QJSEngine * jsEngine);

    Q_INVOKABLE qreal   intToFloat (const int   value, const int decimals) const;
    Q_INVOKABLE int     floatToInt (const qreal value, const int decimals) const;
    Q_INVOKABLE QString format     (const int   value, const int decimals, const QString unit = "") const;

    Q_INVOKABLE void highlightLinkedObject (BasicObject * object);

public slots:
    void addMru    (const QString & filePath);
    void removeMru (const QString & filePath);

protected:
    void onRememberThemeChanged  (void);
    void onLastFolderUsedChanged (void);

private:
    QSettings m_settings;
};

#endif // SHAREDOBJECT_H
