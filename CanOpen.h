#ifndef CANOPEN_H
#define CANOPEN_H

#include <QObject>
#include <QPointer>

#include "TypesQmlWrapper.h"

#include "IdentifiableObject.h"

#include "CanDriver.h"

QML_ENUM_CLASS (CanOpenNmtCmd,
                StartNode = 0x01,
                StopNode  = 0x02,
                SetPreOp  = 0x80,
                ResetNode = 0x81,
                ResetComm = 0x82,
                NB_CMDS)

QML_ENUM_CLASS (CanOpenLssCommands,
                SwitchState   = 0x04,
                ChangeNodeId  = 0x11,
                ChangeBitrate = 0x13,
                StoreConfig   = 0x17,
                NB_CMDS)

class CanBus;
class ByteArrayWrapper;
class CanOpenObjDict;
class CanOpenProtocolManager;

class CanOpen : public BasicObject {
    Q_OBJECT
    QML_WRITABLE_VAR_PROPERTY (int, nodeId)
    QML_WRITABLE_VAR_PROPERTY (bool, isMaster)
    QML_WRITABLE_PTR_PROPERTY (CanBus, canBus)
    QML_WRITABLE_CSTREF_PROPERTY (QString, obdUrl)

public:
    explicit CanOpen (QObject * parent = Q_NULLPTR);
    virtual ~CanOpen (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void init  (void);
    void start (void);
    void stop  (void);
    void reset (void);

    CanOpenObjDict         * getObjectDict      (void) const;
    CanOpenProtocolManager * getProtocolManager (void) const;

    Q_INVOKABLE ByteArrayWrapper * createBufferFromInt8   (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromInt16  (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromInt32  (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromUInt8  (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromUInt16 (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromUInt32 (const QmlBiggestInt value);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromHex    (const QString & hex);
    Q_INVOKABLE ByteArrayWrapper * createBufferFromString (const QString & str);

    Q_INVOKABLE ByteArrayWrapper * readValue (const int index, const int subIndex);

    Q_INVOKABLE void changeValue (const int index, const int subIndex, ByteArrayWrapper * buffer);

    Q_INVOKABLE void sendNmtChangeRequest (const int nodeId, const CanOpenNmtCmd::Type state);

    Q_INVOKABLE void sendLssActionRequest (const CanOpenLssCommands::Type cmd, const int argument = 0);

    Q_INVOKABLE void sendSdoReadRequest  (const int nodeId, const int index, const int subIndex, const bool blockMode = false);
    Q_INVOKABLE void sendSdoWriteRequest (const int nodeId, const int index, const int subIndex, ByteArrayWrapper * buffer);

signals:
    void initialized (void);
    void started     (void);
    void stopped     (void);
    void resetting   (void);

protected slots:
    void onCanOpenParamChanged (void);

private:
    bool m_ready;
    bool m_running;
    QPointer<CanOpenProtocolManager> m_canOpenMan;
};

#endif // CANOPEN_H
