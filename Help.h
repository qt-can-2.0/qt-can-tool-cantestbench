#ifndef HELP_H
#define HELP_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QQmlEngine>
#include <QJSEngine>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"
#include "QQmlSingletonHelper.h"
#include "QQmlObjectListModel.h"

class EnumKeyHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)

public:
    explicit EnumKeyHelp (const QString & name = "",
                          const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
    { }
};

class PropertyHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_CONSTANT_CSTREF_PROPERTY (QString, type)
    QML_CONSTANT_CSTREF_PROPERTY (QString, access)
    QML_CONSTANT_VAR_PROPERTY (bool, isArray)

public:
    explicit PropertyHelp (const QString & name = "",
                           const QString & type = "",
                           const QString & access = "",
                           const bool      isArray = false,
                           const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
        , m_type (type)
        , m_access (access)
        , m_isArray (isArray)
    { }
};

class ArgumentHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_CONSTANT_CSTREF_PROPERTY (QString, type)
    QML_CONSTANT_CSTREF_PROPERTY (QString, fallback)
    QML_CONSTANT_VAR_PROPERTY (bool, isArray)

public:
    explicit ArgumentHelp (const QString & name = "",
                           const QString & type = "",
                           const QString & fallback = "",
                           const bool      isArray = false,
                           const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
        , m_type (type)
        , m_fallback (fallback)
        , m_isArray (isArray)
    { }
};

class MethodHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_CONSTANT_CSTREF_PROPERTY (QString, returnType)
    QML_OBJMODEL_PROPERTY (ArgumentHelp, argumentsList)

public:
    explicit MethodHelp (const QString & name = "",
                         const QString & returnType = "",
                         const QList<ArgumentHelp *> & argumentsList = { },
                         const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
        , m_returnType (returnType)
    {
        m_argumentsList = new QQmlObjectListModel<ArgumentHelp> (this, "name", "name");
        m_argumentsList->append (argumentsList);
    }
};

class SignalHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_OBJMODEL_PROPERTY (ArgumentHelp, argumentsList)

public:
    explicit SignalHelp (const QString & name = "",
                         const QList<ArgumentHelp *> & argumentsList = { },
                         const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
    {
        m_argumentsList = new QQmlObjectListModel<ArgumentHelp> (this, "name", "name");
        m_argumentsList->append (argumentsList);
    }
};

class ObjectHelp : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, description)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_CONSTANT_CSTREF_PROPERTY (QString, ancestor)
    QML_CONSTANT_CSTREF_PROPERTY (QStringList, derivates)
    QML_CONSTANT_CSTREF_PROPERTY (QStringList, accepted)
    QML_CONSTANT_CSTREF_PROPERTY (QString, attribute)
    QML_OBJMODEL_PROPERTY (PropertyHelp, propertiesList)
    QML_OBJMODEL_PROPERTY (MethodHelp, methodsList)
    QML_OBJMODEL_PROPERTY (SignalHelp, signalsList)
    QML_OBJMODEL_PROPERTY (EnumKeyHelp, enumKeysList)

public:
    explicit ObjectHelp (const QString & name = "",
                         const QString & ancestor = "",
                         const QStringList & derivates = { },
                         const QStringList & accepted = { },
                         const QString & attribute = "",
                         const QList<PropertyHelp *> & propertiesList = { },
                         const QList<MethodHelp *> & methodsList = { },
                         const QList<SignalHelp *> & signalsList = { },
                         const QList<EnumKeyHelp *> & enumKeysList = { },
                         const QString & description = "")
        : QObject (Q_NULLPTR)
        , m_description (description)
        , m_name (name)
        , m_ancestor (ancestor)
        , m_derivates (derivates)
        , m_accepted (accepted)
        , m_attribute (attribute)
    {
        m_propertiesList = new QQmlObjectListModel<PropertyHelp> (this, "name", "name");
        m_propertiesList->append (propertiesList);
        m_methodsList = new QQmlObjectListModel<MethodHelp> (this, "name", "name");
        m_methodsList->append (methodsList);
        m_signalsList = new QQmlObjectListModel<SignalHelp> (this, "name", "name");
        m_signalsList->append (signalsList);
        m_enumKeysList = new QQmlObjectListModel<EnumKeyHelp> (this, "name", "name");
        m_enumKeysList->append (enumKeysList);
    }
};

class HelpPage : public QObject {
    Q_OBJECT
    QML_CONSTANT_VAR_PROPERTY (int, lvl)
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)

public:
    explicit HelpPage (const int lvl = 0,
                       const QString & name = "")
        : QObject (Q_NULLPTR)
        , m_lvl (lvl)
        , m_name (name)
    { }
};

class HelpSection : public QObject {
    Q_OBJECT
    QML_CONSTANT_CSTREF_PROPERTY (QString, name)
    QML_OBJMODEL_PROPERTY (HelpPage, pagesList)

public:
    explicit HelpSection (const QString & name = "",
                          const QList<HelpPage *> & pagesList = { })
        : QObject (Q_NULLPTR)
        , m_name (name)
    {
        m_pagesList = new QQmlObjectListModel<HelpPage> (this, "name", "name");
        m_pagesList->append (pagesList);
    }
};

class Help : public QObject {
    Q_OBJECT
    QML_SINGLETON_IMPL (Help)
    QML_OBJMODEL_PROPERTY (HelpSection, sectionsList)

public:
    Q_INVOKABLE ObjectHelp * getHelpForTypeByName (const QString & name);

protected:
    void initMap   (void);
    void initIndex (void);

private:
    explicit Help (QObject * parent = Q_NULLPTR);

    inline void registerHelp (ObjectHelp & help);

    QHash<QString, ObjectHelp *> m_objectsByName;
};

#endif // HELP_H
