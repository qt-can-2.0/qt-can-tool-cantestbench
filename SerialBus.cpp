
#include "SerialBus.h"

#include "Manager.h"
#include "CppUtils.h"
#include "IdentifiableObject.h"
#include "Routine.h"

#include <QTimer>
#include <QStringBuilder>
#include <QSerialPortInfo>

SerialBus::SerialBus (QObject * parent)
    : BasicObject (ObjectFamily::SERIALBUS, parent)
    , m_portLoaded (false)
    , m_serialPort (Q_NULLPTR)
{
    QTimer::singleShot (0, this, &SerialBus::onTimer);
    Manager::instance ().registerObject (this);
}

SerialBus::~SerialBus (void) {
    Manager::instance ().unregisterObject (this);
}

void SerialBus::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    Manager::instance ().intializeObject (this);
}

void SerialBus::subscribeRoutineToSerialFrame (RoutineOnSerialFrame * routine) {
    if (m_routineForSerialFrame.isNull ()) {
        if (routine) {
            m_routineForSerialFrame = routine;
        }
    }
    else {
        Manager::instance ().logWarning ("SERIALBUS", this, "Already has routine for reception (can only have 1, serial bus can't be concurrently accessed !)");
    }
}

bool SerialBus::init (const QString & portName, const int baudrate, const int dataBits, const int stopBits, const int parity) {
    if (!m_serialPort) {
        m_serialPort = new QSerialPort (portName, this);
        m_serialPort->setBaudRate (qint32 (baudrate));
        m_serialPort->setDataBits (QSerialPort::DataBits (dataBits));
        m_serialPort->setStopBits (QSerialPort::StopBits (stopBits));
        m_serialPort->setParity   (QSerialPort::Parity (parity));
        if (m_serialPort->open (QSerialPort::ReadWrite)) {
            connect (m_serialPort, &QSerialPort::readyRead, this, &SerialBus::onDataReady);
            update_portLoaded (true);
            update_portName (portName);
            emit started ();
            return true;
        }
        else {
            update_portLoaded (false);
            update_portName ("");
            Manager::instance ().logError ("SERIALBUS", this, m_serialPort->errorString ());
            emit error (m_serialPort->errorString ());
            m_serialPort->deleteLater ();
            m_serialPort = Q_NULLPTR;
        }
    }
    return false;
}

bool SerialBus::stop (void) {
    if (m_serialPort && m_serialPort->isOpen ()) {
        disconnect (this, Q_NULLPTR, m_serialPort, Q_NULLPTR);
        disconnect (m_serialPort, Q_NULLPTR, this, Q_NULLPTR);
        m_serialPort->clear ();
        m_serialPort->close ();
        m_serialPort->deleteLater ();
        m_serialPort = Q_NULLPTR;
        update_portName ("");
        update_portLoaded (false);
        emit stopped ();
    }
    return true;
}

int SerialBus::count (void) {
    return (m_serialPort ? int (m_serialPort->bytesAvailable ()) : 0);
}

void SerialBus::writeBool (const bool value) {
    writeAs<bool> (bool (value));
}

void SerialBus::writeInt8 (const QmlBiggestInt value) {
    writeAs<qint8> (qint8 (value));
}

void SerialBus::writeInt16 (const QmlBiggestInt value) {
    writeAs<qint16> (qint16 (value));
}

void SerialBus::writeInt32 (const QmlBiggestInt value) {
    writeAs<qint32> (qint32 (value));
}

void SerialBus::writeUInt8 (const QmlBiggestInt value) {
    writeAs<quint8> (quint8 (value));
}

void SerialBus::writeUInt16 (const QmlBiggestInt value) {
    writeAs<quint16> (quint16 (value));
}

void SerialBus::writeUInt32 (const QmlBiggestInt value) {
    writeAs<quint32> (quint32 (value));
}

void SerialBus::writeString (const QString & value, const int len) {
    const QByteArray tmp = value.toLatin1 ().leftJustified (len, '\0', true);
    if (m_serialPort && m_serialPort->isWritable ()) {
        m_serialPort->write (tmp);
    }
}

bool SerialBus::readBool (void) {
    return bool (readAs<bool> ());
}

QmlBiggestInt SerialBus::readInt8 (void) {
    return QmlBiggestInt (readAs<qint8> ());
}

QmlBiggestInt SerialBus::readInt16 (void) {
    return QmlBiggestInt (readAs<qint16> ());
}

QmlBiggestInt SerialBus::readInt32 (void) {
    return QmlBiggestInt (readAs<qint32> ());
}

QmlBiggestInt SerialBus::readUInt8 (void) {
    return QmlBiggestInt (readAs<quint8> ());
}

QmlBiggestInt SerialBus::readUInt16 (void) {
    return QmlBiggestInt (readAs<quint16> ());
}

QmlBiggestInt SerialBus::readUInt32 (void) {
    return QmlBiggestInt (readAs<quint32> ());
}

QString SerialBus::readString (const int len) {
    QByteArray ret;
    if (m_serialPort && m_serialPort->bytesAvailable () >= len) {
        ret = m_serialPort->read (len);
    }
    return QString::fromLatin1 (ret);
}

void SerialBus::onDataReady (void) {
    if (m_serialPort && m_serialPort->bytesAvailable ()) {
        if (m_routineForSerialFrame) {
            m_routineForSerialFrame->execute ();
        }
    }
}

void SerialBus::onTimer (void) {
    if (!m_portLoaded) {
        QVariantList portsList;
        const QList<QSerialPortInfo> infoList = QSerialPortInfo::availablePorts ();
        for (const QSerialPortInfo & info : arrayRange (infoList)) {
            if (!info.isNull ()) {
                const QString tmp = QString (info.manufacturer () % ' ' % info.description ()).trimmed ();
                const QVariantMap entry {
                    { "key",   info.portName () },
                    { "value", (!tmp.isEmpty ()
                                ? QString (info.portName () % " (" % tmp % ")")
                                : info.portName ()) },
                };
                portsList.append (entry);
            }
        }
        update_portsList (portsList);
    }
    QTimer::singleShot (5000, this, &SerialBus::onTimer);
}
