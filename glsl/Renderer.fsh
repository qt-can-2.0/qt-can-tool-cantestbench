varying mediump vec4 computedColor;

void main (void) {
    // simply color the pixel with color from vertex shader
    gl_FragColor = computedColor;
}
