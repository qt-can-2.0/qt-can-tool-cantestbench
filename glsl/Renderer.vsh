attribute highp   vec3 currentVertex;
attribute mediump vec3 currentNormal;
attribute mediump vec3 currentColor;

uniform mediump mat4 projectionMatrix;
uniform mediump mat4 worldToCameraMatrix;

varying mediump vec4 computedColor;

void main (void) {
    // pass color from attribute to pixel shader
    computedColor = vec4 (currentColor, 1.0);

    // apply projection and camera matrix to vertex
    gl_Position = (projectionMatrix * worldToCameraMatrix * vec4 (currentVertex, 1));
}
