#ifndef LINK_H
#define LINK_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>

#include "IdentifiableObject.h"
#include "Transformer.h"

class AnalogInput;
class AnalogOutput;
class AnalogSensor;
class AnalogActuator;
class DigitalInput;
class DigitalOutput;
class DigitalSensor;
class DigitalActuator;
class HybridSensor;
class HybridActuator;
class PhysicalValue;

class AbstractLink : public BasicObject {
    Q_OBJECT
    QML_READONLY_VAR_PROPERTY (bool, valid) // wheter link has matching source and target
    QML_READONLY_VAR_PROPERTY (bool, enabled) // whether link can be used
    QML_READONLY_VAR_PROPERTY (bool, reversed) // whether link is reversed to acheive passive node
    QML_WRITABLE_VAR_PROPERTY (bool, detached) // whether user has detached link manually
    QML_WRITABLE_PTR_PROPERTY (BasicObject, source)
    QML_WRITABLE_PTR_PROPERTY (BasicObject, target)
    QML_WRITABLE_PTR_PROPERTY (AbstractTransformer, transformer)

public:
    explicit AbstractLink (QObject * parent = Q_NULLPTR);
    virtual ~AbstractLink (void);

    virtual void onComponentCompleted (void) Q_DECL_OVERRIDE;

public slots:
    void init (void);
    void sync (void);

protected slots:
    void onSourceChanged   (void);
    void onTargetChanged   (void);
    void onSourceDestroyed (void);
    void onTargetDestroyed (void);

protected:
    virtual void initHandler (void);
    virtual void ensureValid (void);
    virtual void syncNormal  (void);
    virtual void syncReverse (void);

    template<typename T> T transformNormal (const T val) {
        return (m_transformer ? m_transformer->targetFromSource (val) : val);
    }
    template<typename T> T transformReverse (const T val) {
        return (m_transformer ? m_transformer->sourceFromTarget (val) : val);
    }

private:
    QMutex m_mutex;
};

class LinkPhysicalValueToAnalogSensor : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (PhysicalValue, value)
    QML_READONLY_PTR_PROPERTY (AnalogSensor, sensor)

public:
    explicit LinkPhysicalValueToAnalogSensor (QObject * parent = Q_NULLPTR);
    virtual ~LinkPhysicalValueToAnalogSensor (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkAnalogSensorToAnalogInput : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (AnalogSensor, sensor)
    QML_READONLY_PTR_PROPERTY (AnalogInput, input)

public:
    explicit LinkAnalogSensorToAnalogInput (QObject * parent = Q_NULLPTR);
    virtual ~LinkAnalogSensorToAnalogInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkDigitalSensorToDigitalInput : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (DigitalSensor, sensor)
    QML_READONLY_PTR_PROPERTY (DigitalInput, input)

public:
    explicit LinkDigitalSensorToDigitalInput (QObject * parent = Q_NULLPTR);
    virtual ~LinkDigitalSensorToDigitalInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkAnalogOutputToAnalogActuator : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (AnalogOutput, output)
    QML_READONLY_PTR_PROPERTY (AnalogActuator, actuator)

public:
    explicit LinkAnalogOutputToAnalogActuator (QObject * parent = Q_NULLPTR);
    virtual ~LinkAnalogOutputToAnalogActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkDigitalOutputToDigitalActuator : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (DigitalOutput, output)
    QML_READONLY_PTR_PROPERTY (DigitalActuator, actuator)

public:
    explicit LinkDigitalOutputToDigitalActuator (QObject * parent = Q_NULLPTR);
    virtual ~LinkDigitalOutputToDigitalActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkAnalogActuatorToPhysicalValue : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (AnalogActuator, actuator)
    QML_READONLY_PTR_PROPERTY (PhysicalValue, value)

public:
    explicit LinkAnalogActuatorToPhysicalValue (QObject * parent = Q_NULLPTR);
    virtual ~LinkAnalogActuatorToPhysicalValue (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkAnalogOutputToAnalogInput : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (AnalogOutput, output)
    QML_READONLY_PTR_PROPERTY (AnalogInput, input)

public:
    explicit LinkAnalogOutputToAnalogInput (QObject * parent = Q_NULLPTR);
    virtual ~LinkAnalogOutputToAnalogInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkDigitalOutputToDigitalInput : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (DigitalOutput, output)
    QML_READONLY_PTR_PROPERTY (DigitalInput, input)

public:
    explicit LinkDigitalOutputToDigitalInput (QObject * parent = Q_NULLPTR);
    virtual ~LinkDigitalOutputToDigitalInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkPhysicalValueToHybridSensor : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (PhysicalValue, value)
    QML_READONLY_PTR_PROPERTY (HybridSensor, sensor)

public:
    explicit LinkPhysicalValueToHybridSensor (QObject * parent = Q_NULLPTR);
    virtual ~LinkPhysicalValueToHybridSensor (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkHybridSensorToDigitalInput : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (HybridSensor, sensor)
    QML_READONLY_PTR_PROPERTY (DigitalInput, input)

public:
    explicit LinkHybridSensorToDigitalInput (QObject * parent = Q_NULLPTR);
    virtual ~LinkHybridSensorToDigitalInput (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkDigitalOutputToHybridActuator : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (DigitalOutput, output)
    QML_READONLY_PTR_PROPERTY (HybridActuator, actuator)

public:
    explicit LinkDigitalOutputToHybridActuator (QObject * parent = Q_NULLPTR);
    virtual ~LinkDigitalOutputToHybridActuator (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

class LinkHybridActuatorToPhysicalValue : public AbstractLink {
    Q_OBJECT
    QML_READONLY_PTR_PROPERTY (HybridActuator, actuator)
    QML_READONLY_PTR_PROPERTY (PhysicalValue, value)

public:
    explicit LinkHybridActuatorToPhysicalValue (QObject * parent = Q_NULLPTR);
    virtual ~LinkHybridActuatorToPhysicalValue (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void initHandler (void) Q_DECL_FINAL;
    void ensureValid (void) Q_DECL_FINAL;
    void syncNormal  (void) Q_DECL_FINAL;
    void syncReverse (void) Q_DECL_FINAL;
};

#endif // LINK_H
