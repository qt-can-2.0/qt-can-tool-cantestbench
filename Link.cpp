
#include "Link.h"

#include "IO.h"
#include "Sensor.h"
#include "Actuator.h"
#include "Physics.h"
#include "MathUtils.h"
#include "Manager.h"

#include "QtCAN.h"

AbstractLink::AbstractLink (QObject * parent)
    : BasicObject (ObjectFamily::LINK, parent)
    , m_valid       (false)
    , m_enabled     (false)
    , m_reversed    (false)
    , m_detached    (false)
    , m_source      (Q_NULLPTR)
    , m_target      (Q_NULLPTR)
    , m_transformer (Q_NULLPTR)
{
    connect (this, &AbstractLink::sourceChanged, this, &AbstractLink::onSourceChanged);
    connect (this, &AbstractLink::targetChanged, this, &AbstractLink::onTargetChanged);
}

AbstractLink::~AbstractLink (void) {
    disconnect (this, &AbstractLink::sourceChanged, this, &AbstractLink::onSourceChanged);
    disconnect (this, &AbstractLink::targetChanged, this, &AbstractLink::onTargetChanged);
    if (m_source) {
        disconnect (m_source, &BasicObject::destroyed, this, &AbstractLink::onSourceDestroyed);
    }
    if (m_target) {
        disconnect (m_target, &BasicObject::destroyed, this, &AbstractLink::onTargetDestroyed);
    }
}

void AbstractLink::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
}

void AbstractLink::init (void) {
    initHandler ();
    ensureValid ();
}

void AbstractLink::sync (void) {
    QMutexLocker lock (&m_mutex);
    if (m_valid && m_enabled) {
        if (!m_detached) {
            if (!m_reversed) {
                syncNormal ();
            }
            else {
                syncReverse ();
            }
        }
    }
}

void AbstractLink::onSourceChanged (void) {
    if (m_source) {
        connect (m_source, &BasicObject::destroyed, this, &AbstractLink::onSourceDestroyed, Qt::UniqueConnection);
    }
    initHandler ();
    ensureValid ();
}

void AbstractLink::onTargetChanged (void) {
    if (m_target) {
        connect (m_target, &BasicObject::destroyed, this, &AbstractLink::onTargetDestroyed, Qt::UniqueConnection);
    }
    initHandler ();
    ensureValid ();
}

void AbstractLink::onSourceDestroyed (void) {
    update_valid (false);
    m_source = Q_NULLPTR;
    emit sourceChanged (m_target);
    initHandler ();
    ensureValid ();
}

void AbstractLink::onTargetDestroyed (void) {
    update_valid (false);
    m_target = Q_NULLPTR;
    emit targetChanged (m_target);
    initHandler ();
    ensureValid ();
}

void AbstractLink::initHandler (void) { }

void AbstractLink::ensureValid (void) { }

void AbstractLink::syncNormal (void) { }

void AbstractLink::syncReverse (void) { }

LinkPhysicalValueToAnalogSensor::LinkPhysicalValueToAnalogSensor (QObject * parent)
    : AbstractLink (parent)
    , m_value (Q_NULLPTR)
    , m_sensor (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkPhysicalValueToAnalogSensor::~LinkPhysicalValueToAnalogSensor (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkPhysicalValueToAnalogSensor::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Measure");
    }
    Manager::instance ().intializeObject (this);
}

void LinkPhysicalValueToAnalogSensor::initHandler (void) {
    update_value  (get_source () ? get_source ()->as<PhysicalValue> () : Q_NULLPTR);
    update_sensor (get_target () ? get_target ()->as<AnalogSensor> ()  : Q_NULLPTR);
}

void LinkPhysicalValueToAnalogSensor::ensureValid (void) {
    update_valid (m_value && m_sensor);
}

void LinkPhysicalValueToAnalogSensor::syncNormal (void) {
    m_sensor->set_valPhy (transformNormal (MathUtils::floatToInt (m_value->get_val (), m_sensor->get_decimals ())));
}

void LinkPhysicalValueToAnalogSensor::syncReverse (void) {
    m_value->set_val (transformReverse (MathUtils::intToFloat (m_sensor->get_valPhy (), m_sensor->get_decimals ())));
}

LinkAnalogSensorToAnalogInput::LinkAnalogSensorToAnalogInput (QObject * parent)
    : AbstractLink (parent)
    , m_sensor (Q_NULLPTR)
    , m_input (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkAnalogSensorToAnalogInput::~LinkAnalogSensorToAnalogInput (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkAnalogSensorToAnalogInput::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Input");
    }
    Manager::instance ().intializeObject (this);
}

void LinkAnalogSensorToAnalogInput::initHandler (void) {
    update_sensor (get_source () ? get_source ()->as<AnalogSensor> () : Q_NULLPTR);
    update_input  (get_target () ? get_target ()->as<AnalogInput> ()  : Q_NULLPTR);
}

void LinkAnalogSensorToAnalogInput::ensureValid (void) {
    update_valid (m_sensor && m_input);
}

void LinkAnalogSensorToAnalogInput::syncNormal (void) {
    m_input->set_valRaw (int (transformNormal (m_sensor->get_bypassScale () ? m_sensor->get_valPhy () : m_sensor->get_valRaw ())));
}

void LinkAnalogSensorToAnalogInput::syncReverse (void) {
    m_sensor->update_valRaw (int (transformReverse (m_input->get_valRaw ())));
}

LinkDigitalSensorToDigitalInput::LinkDigitalSensorToDigitalInput (QObject * parent)
    : AbstractLink (parent)
    , m_sensor (Q_NULLPTR)
    , m_input (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkDigitalSensorToDigitalInput::~LinkDigitalSensorToDigitalInput (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkDigitalSensorToDigitalInput::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Input");
    }
    Manager::instance ().intializeObject (this);
}

void LinkDigitalSensorToDigitalInput::initHandler (void) {
    update_sensor (get_source () ? get_source ()->as<DigitalSensor> () : Q_NULLPTR);
    update_input  (get_target () ? get_target ()->as<DigitalInput> ()  : Q_NULLPTR);
}

void LinkDigitalSensorToDigitalInput::ensureValid (void) {
    update_valid (m_sensor && m_input);
}

void LinkDigitalSensorToDigitalInput::syncNormal (void) {
    m_input->set_value (m_sensor->get_value ());
}

void LinkDigitalSensorToDigitalInput::syncReverse (void) {
    m_sensor->set_value (m_input->get_value ());
}

LinkAnalogOutputToAnalogActuator::LinkAnalogOutputToAnalogActuator (QObject * parent)
    : AbstractLink (parent)
    , m_output (Q_NULLPTR)
    , m_actuator (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkAnalogOutputToAnalogActuator::~LinkAnalogOutputToAnalogActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkAnalogOutputToAnalogActuator::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Output");
    }
    Manager::instance ().intializeObject (this);
}

void LinkAnalogOutputToAnalogActuator::initHandler (void) {
    update_output   (get_source () ? get_source ()->as<AnalogOutput> ()   : Q_NULLPTR);
    update_actuator (get_target () ? get_target ()->as<AnalogActuator> () : Q_NULLPTR);
}

void LinkAnalogOutputToAnalogActuator::ensureValid (void) {
    update_valid (m_output && m_actuator);
}

void LinkAnalogOutputToAnalogActuator::syncNormal (void) {
    m_actuator->set_valRaw (transformNormal (m_output->get_valRaw ()));
}

void LinkAnalogOutputToAnalogActuator::syncReverse (void) {
    m_output->set_valRaw (transformNormal (m_actuator->get_valRaw ()));
}

LinkDigitalOutputToDigitalActuator::LinkDigitalOutputToDigitalActuator (QObject * parent)
    : AbstractLink (parent)
    , m_output (Q_NULLPTR)
    , m_actuator (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkDigitalOutputToDigitalActuator::~LinkDigitalOutputToDigitalActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkDigitalOutputToDigitalActuator::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Output");
    }
    Manager::instance ().intializeObject (this);
}

void LinkDigitalOutputToDigitalActuator::initHandler (void) {
    update_output   (get_source () ? get_source ()->as<DigitalOutput> ()   : Q_NULLPTR);
    update_actuator (get_target () ? get_target ()->as<DigitalActuator> () : Q_NULLPTR);
}

void LinkDigitalOutputToDigitalActuator::ensureValid (void) {
    update_valid (m_output && m_actuator);
}

void LinkDigitalOutputToDigitalActuator::syncNormal (void) {
    m_actuator->set_value (m_output->get_value ());
}

void LinkDigitalOutputToDigitalActuator::syncReverse (void) {
    m_output->set_value (m_actuator->get_value ());
}

LinkAnalogActuatorToPhysicalValue::LinkAnalogActuatorToPhysicalValue (QObject * parent)
    : AbstractLink (parent)
    , m_actuator (Q_NULLPTR)
    , m_value (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkAnalogActuatorToPhysicalValue::~LinkAnalogActuatorToPhysicalValue (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkAnalogActuatorToPhysicalValue::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Apply");
    }
    Manager::instance ().intializeObject (this);
}

void LinkAnalogActuatorToPhysicalValue::initHandler (void) {
    if (get_source ()) {
        update_actuator (get_source ()->as<AnalogActuator> ());
    }
    if (get_target ()) {
        update_value (get_target ()->as<PhysicalValue> ());
    }
}

void LinkAnalogActuatorToPhysicalValue::ensureValid (void) {
    update_valid (m_actuator && m_value);
}

void LinkAnalogActuatorToPhysicalValue::syncNormal (void) {
    if (m_actuator->get_valSpeed () != 0) {
        m_value->update_assignedSpeed (MathUtils::intToFloat (transformNormal (m_actuator->get_valSpeed ()), m_actuator->get_decimals ()));
    }
}

void LinkAnalogActuatorToPhysicalValue::syncReverse (void) { }

LinkAnalogOutputToAnalogInput::LinkAnalogOutputToAnalogInput (QObject * parent)
    : AbstractLink (parent)
    , m_output (Q_NULLPTR)
    , m_input (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkAnalogOutputToAnalogInput::~LinkAnalogOutputToAnalogInput (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkAnalogOutputToAnalogInput::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Bind");
    }
    Manager::instance ().intializeObject (this);
}

void LinkAnalogOutputToAnalogInput::initHandler (void) {
    update_output (get_source () ? get_source ()->as<AnalogOutput> () : Q_NULLPTR);
    update_input  (get_target () ? get_target ()->as<AnalogInput> ()  : Q_NULLPTR);
}

void LinkAnalogOutputToAnalogInput::ensureValid (void) {
    update_valid (m_output && m_input);
}

void LinkAnalogOutputToAnalogInput::syncNormal (void) {
    m_input->set_valRaw (transformNormal (MathUtils::convert (m_output->get_valRaw (),
                                                              m_output->get_minRaw (), m_output->get_maxRaw (),
                                                              m_input->get_minRaw (), m_input->get_maxRaw ())));
}

void LinkAnalogOutputToAnalogInput::syncReverse (void) {
    m_output->set_valRaw (transformReverse (MathUtils::convert (m_input->get_valRaw (),
                                                                m_input->get_minRaw (), m_input->get_maxRaw (),
                                                                m_output->get_minRaw (), m_output->get_maxRaw ())));
}

LinkDigitalOutputToDigitalInput::LinkDigitalOutputToDigitalInput (QObject * parent)
    : AbstractLink (parent)
    , m_output (Q_NULLPTR)
    , m_input (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkDigitalOutputToDigitalInput::~LinkDigitalOutputToDigitalInput (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkDigitalOutputToDigitalInput::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Bind");
    }
    Manager::instance ().intializeObject (this);
}

void LinkDigitalOutputToDigitalInput::initHandler (void) {
    update_output (get_source () ? get_source ()->as<DigitalOutput> () : Q_NULLPTR);
    update_input  (get_target () ? get_target ()->as<DigitalInput> ()  : Q_NULLPTR);
}

void LinkDigitalOutputToDigitalInput::ensureValid (void) {
    update_valid (m_output && m_input);
}

void LinkDigitalOutputToDigitalInput::syncNormal (void) {
    m_input->set_value (m_output->get_value ());
}

void LinkDigitalOutputToDigitalInput::syncReverse (void) {
    m_output->set_value (m_input->get_value ());
}

LinkPhysicalValueToHybridSensor::LinkPhysicalValueToHybridSensor (QObject * parent)
    : AbstractLink (parent)
    , m_value (Q_NULLPTR)
    , m_sensor (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkPhysicalValueToHybridSensor::~LinkPhysicalValueToHybridSensor (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkPhysicalValueToHybridSensor::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Watch");
    }
    Manager::instance ().intializeObject (this);
}

void LinkPhysicalValueToHybridSensor::initHandler (void) {
    update_value  (get_source () ? get_source ()->as<PhysicalValue> () : Q_NULLPTR);
    update_sensor (get_target () ? get_target ()->as<HybridSensor>  () : Q_NULLPTR);
}

void LinkPhysicalValueToHybridSensor::ensureValid (void) {
    update_valid (m_value && m_sensor);
}

void LinkPhysicalValueToHybridSensor::syncNormal (void) {
    m_sensor->set_valPhy (transformNormal (MathUtils::floatToInt (m_value->get_val (), m_sensor->get_decimals ())));
}

void LinkPhysicalValueToHybridSensor::syncReverse (void) {
    m_value->set_val (transformReverse (MathUtils::intToFloat (m_sensor->get_valPhy (), m_sensor->get_decimals ())));
}

LinkHybridSensorToDigitalInput::LinkHybridSensorToDigitalInput (QObject * parent)
    : AbstractLink (parent)
    , m_sensor (Q_NULLPTR)
    , m_input (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkHybridSensorToDigitalInput::~LinkHybridSensorToDigitalInput (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkHybridSensorToDigitalInput::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Set");
    }
    Manager::instance ().intializeObject (this);
}

void LinkHybridSensorToDigitalInput::initHandler (void) {
    update_sensor (get_source () ? get_source ()->as<HybridSensor> () : Q_NULLPTR);
    update_input  (get_target () ? get_target ()->as<DigitalInput> () : Q_NULLPTR);
}

void LinkHybridSensorToDigitalInput::ensureValid (void) {
    update_valid (m_sensor && m_input);
}

void LinkHybridSensorToDigitalInput::syncNormal (void) {
    m_input->set_value (m_sensor->get_valState ());
}

void LinkHybridSensorToDigitalInput::syncReverse () {
    m_sensor->update_valState (m_input->get_value ());
}

LinkDigitalOutputToHybridActuator::LinkDigitalOutputToHybridActuator (QObject * parent)
    : AbstractLink (parent)
    , m_output (Q_NULLPTR)
    , m_actuator (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkDigitalOutputToHybridActuator::~LinkDigitalOutputToHybridActuator (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkDigitalOutputToHybridActuator::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Output");
    }
    Manager::instance ().intializeObject (this);
}

void LinkDigitalOutputToHybridActuator::initHandler (void) {
    update_output   (get_source () ? get_source ()->as<DigitalOutput>  () : Q_NULLPTR);
    update_actuator (get_target () ? get_target ()->as<HybridActuator> () : Q_NULLPTR);
}

void LinkDigitalOutputToHybridActuator::ensureValid (void) {
    update_valid (m_output && m_actuator);
}

void LinkDigitalOutputToHybridActuator::syncNormal (void) {
    m_actuator->set_value (m_output->get_value ());
}

void LinkDigitalOutputToHybridActuator::syncReverse (void) {
    m_output->set_value (m_actuator->get_value ());
}

LinkHybridActuatorToPhysicalValue::LinkHybridActuatorToPhysicalValue (QObject * parent)
    : AbstractLink (parent)
    , m_actuator (Q_NULLPTR)
    , m_value (Q_NULLPTR)
{
    Manager::instance ().registerObject (this);
}

LinkHybridActuatorToPhysicalValue::~LinkHybridActuatorToPhysicalValue (void) {
    Manager::instance ().unregisterObject (this);
}

void LinkHybridActuatorToPhysicalValue::onComponentCompleted (void) {
    AbstractLink::onComponentCompleted ();
    if (get_title ().isEmpty ()) {
        set_title ("Apply");
    }
    Manager::instance ().intializeObject (this);
}

void LinkHybridActuatorToPhysicalValue::initHandler (void) {
    update_actuator (get_source () ? get_source ()->as<HybridActuator> () : Q_NULLPTR);
    update_value    (get_target () ? get_target ()->as<PhysicalValue>  () : Q_NULLPTR);
}

void LinkHybridActuatorToPhysicalValue::ensureValid (void) {
    update_valid (m_actuator && m_value);
}

void LinkHybridActuatorToPhysicalValue::syncNormal (void) {
    if (m_actuator->get_valSpeed () != 0) {
        m_value->update_assignedSpeed (MathUtils::intToFloat (transformNormal (m_actuator->get_valSpeed ()), m_actuator->get_decimals ()));
    }
}

void LinkHybridActuatorToPhysicalValue::syncReverse (void) { }
