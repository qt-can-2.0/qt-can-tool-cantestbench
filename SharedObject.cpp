
#include "SharedObject.h"

#include "Actuator.h"
#include "CppUtils.h"
#include "IdentifiableObject.h"
#include "IO.h"
#include "Manager.h"
#include "CanOpen.h"
#include "CanBus.h"
#include "SerialBus.h"
#include "Memory.h"
#include "NetworkDefinition.h"
#include "Node.h"
#include "Physics.h"
#include "Renderer.h"
#include "Routine.h"
#include "Sensor.h"
#include "Transformer.h"
#include "Link.h"
#include "Dashboard.h"
#include "Grapher.h"
#include "SyntaxHighlighter.h"
#include "Help.h"

#include "CanDataQmlWrapper.h"
#include "CanDriverQmlWrapper.h"
#include "ByteArrayQmlWrapper.h"

#include "CanOpenObjDict.h"
#include "CanOpenProtocolManager.h"

#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QStringBuilder>
#include <QStringList>
#include <QQmlComponent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QtMath>

SharedObject::SharedObject (QObject * parent)
    : QObject (parent)
    , m_manager (&Manager::instance ())
    , m_showOnlyEditable (true)
    , m_showDescriptions (false)
    , m_currentNode (Q_NULLPTR)
    , m_currentDashboard (Q_NULLPTR)
    , m_highlightIO (Q_NULLPTR)
    , m_highlightSensor (Q_NULLPTR)
    , m_highlightActuator (Q_NULLPTR)
    , m_highlightPhyVal (Q_NULLPTR)
    , m_settings (QSettings::IniFormat, QSettings::UserScope, "QtCANv2", "TestBench")
{
    m_mruModel = new QQmlVariantListModel (this);
    m_mruModel->appendList (qListToVariant<QString> (m_settings.value ("mru").value<QStringList> ()));
    if (!m_settings.contains ("rememberTheme")) {
        m_settings.setValue ("rememberTheme", 0);
    }
    m_rememberTheme = (m_settings.value ("rememberTheme").toInt () == 1);

    if (!m_settings.contains ("lastFolderUsed") ||
        !QFile::exists (m_settings.value ("lastFolderUsed").toString ())) {
        m_settings.setValue ("lastFolderUsed", QDir::homePath ());
    }
    m_lastFolderUsed = m_settings.value ("lastFolderUsed").toString ();
    connect (this, &SharedObject::rememberThemeChanged,  this, &SharedObject::onRememberThemeChanged);
    connect (this, &SharedObject::lastFolderUsedChanged, this, &SharedObject::onLastFolderUsedChanged);
}

void SharedObject::registerQmlTypes (QQmlEngine * qmlEngine) {
    Q_UNUSED (qmlEngine)
    static const QString msg = "!!!";
    static const char *  uri = "QtCAN.CanTestBench"; // @uri QtCAN.CanTestBench
    static const int     maj = 2;
    static const int     min = 0;
    qmlEngine->addImportPath (":/import");
    // singletons
    Help::registerQmlModule (uri, maj, min, "Help");
    qmlRegisterSingletonType (QUrl ("qrc:///components/Components.qml"), uri, maj, min, "Components");
    qmlRegisterSingletonType<SharedObject>               (uri, maj, min, "Shared", &SharedObject::qmlSingletonProvider);
    // deprecated types
    qmlRegisterTypeNotAvailable (uri, maj, min, "NodesList",     "Type removed, put nodes anywhere in Network !");
    qmlRegisterTypeNotAvailable (uri, maj, min, "SensorsList",   "Type removed, put sensors anywhere in Network !");
    qmlRegisterTypeNotAvailable (uri, maj, min, "ActuatorsList", "Type removed, put actuators anywhere in Network !");
    // must not be instanciated in QML enum
    qmlRegisterUncreatableType<Tabs>                     (uri, maj, min, "Tabs",                     msg);
    qmlRegisterUncreatableType<Modes>                    (uri, maj, min, "Modes",                    msg);
    qmlRegisterUncreatableType<ObjectFamily>             (uri, maj, min, "ObjectFamily",             msg);
    qmlRegisterUncreatableType<ObjectType>               (uri, maj, min, "ObjectType",               msg);
    qmlRegisterUncreatableType<ObjectDirection>          (uri, maj, min, "ObjectDirection",          msg);
    qmlRegisterUncreatableType<Triggers>                 (uri, maj, min, "Triggers",                 msg);
    qmlRegisterUncreatableType<Axis>                     (uri, maj, min, "Axis",                     msg);
    qmlRegisterUncreatableType<Corners>                  (uri, maj, min, "Corners",                  msg);
    qmlRegisterUncreatableType<Sides>                    (uri, maj, min, "Sides",                    msg);
    qmlRegisterUncreatableType<Borders>                  (uri, maj, min, "Borders",                  msg);
    qmlRegisterUncreatableType<CanOpenNmtCmd>            (uri, maj, min, "CanOpenNmtCmd",            msg);
    qmlRegisterUncreatableType<CanOpenNmtState>          (uri, maj, min, "CanOpenNmtState",          msg);
    qmlRegisterUncreatableType<CanOpenLssCommands>       (uri, maj, min, "CanOpenLssCmd",            msg);
    qmlRegisterUncreatableType<VarTypes>                 (uri, maj, min, "VarTypes",                 msg);
    // must not be instanciated in QML helpers
    qmlRegisterUncreatableType<CanDataWrapperRx>         (uri, maj, min, "CanDataWrapperRx",         msg);
    qmlRegisterUncreatableType<CanDataWrapperTx>         (uri, maj, min, "CanDataWrapperTx",         msg);
    qmlRegisterUncreatableType<Manager>                  (uri, maj, min, "Manager",                  msg);
    qmlRegisterUncreatableType<Dictionary>               (uri, maj, min, "Dictionary",               msg);
    qmlRegisterUncreatableType<CanOpenObjDict>           (uri, maj, min, "CanOpenObjDict",           msg);
    qmlRegisterUncreatableType<CanOpenProtocolManager>   (uri, maj, min, "CanOpenProtocolManager",   msg);
    qmlRegisterUncreatableType<ByteArrayWrapper>         (uri, maj, min, "ByteArrayWrapper",         msg);
#if QT_VERSION >= 0x050800 // bug fixed from Qt 5.8.0 (ability to use uncreatable type in QML properties)
    // should not be instanciated
    qmlRegisterUncreatableType<HelpSection>              (uri, maj, min, "HelpSection",              msg);
    qmlRegisterUncreatableType<HelpPage>                 (uri, maj, min, "HelpPage",                 msg);
    qmlRegisterUncreatableType<MethodHelp>               (uri, maj, min, "MethodHelp",               msg);
    qmlRegisterUncreatableType<SignalHelp>               (uri, maj, min, "SignalHelp",               msg);
    qmlRegisterUncreatableType<QmlSyntaxTokenId>         (uri, maj, min, "SyntaxTokenId",            msg);
    qmlRegisterUncreatableType<ObjectHelp>               (uri, maj, min, "ObjectHelp",               msg);
    qmlRegisterUncreatableType<PropertyHelp>             (uri, maj, min, "PropertyHelp",             msg);
    qmlRegisterUncreatableType<ArgumentHelp>             (uri, maj, min, "ArgumentHelp",             msg);
    qmlRegisterUncreatableType<EnumKeyHelp>              (uri, maj, min, "EnumKeyHelp",              msg);
    qmlRegisterUncreatableType<MemoryModelItem>          (uri, maj, min, "MemoryModelItem",          msg);
    // should not be instanciated but are provied as base class or grouped properties in user API
    qmlRegisterUncreatableType<AbstractIO>               (uri, maj, min, "AbstractIO",               msg);
    qmlRegisterUncreatableType<AbstractAnalogIO>         (uri, maj, min, "AbstractAnalogIO",         msg);
    qmlRegisterUncreatableType<AbstractDigitalIO>        (uri, maj, min, "AbstractDigitalIO",        msg);
    qmlRegisterUncreatableType<AbstractSensor>           (uri, maj, min, "AbstractSensor",           msg);
    qmlRegisterUncreatableType<AbstractActuator>         (uri, maj, min, "AbstractActuator",         msg);
    qmlRegisterUncreatableType<AbstractRoutine>          (uri, maj, min, "AbstractRoutine",          msg);
    qmlRegisterUncreatableType<AbstractTransformer>      (uri, maj, min, "AbstractTransformer",      msg);
    qmlRegisterUncreatableType<AbstractLink>             (uri, maj, min, "AbstractLink",             msg);
    qmlRegisterUncreatableType<BasicObject>              (uri, maj, min, "BasicObject",              msg);
    qmlRegisterUncreatableType<Memory>                   (uri, maj, min, "Memory",                   msg);
    qmlRegisterUncreatableType<PhysicalWorld>            (uri, maj, min, "PhysicalWorld",            msg);
    qmlRegisterUncreatableType<AbstractObjectListModel>  (uri, maj, min, "ObjectRefListModel",       msg);
#else
    // should not be instanciated but due to QML engine bug, must be declared as "creatable" for properties
    qmlRegisterType<HelpSection>                         (uri, maj, min, "HelpSection");
    qmlRegisterType<HelpPage>                            (uri, maj, min, "HelpPage");
    qmlRegisterType<MethodHelp>                          (uri, maj, min, "MethodHelp");
    qmlRegisterType<SignalHelp>                          (uri, maj, min, "SignalHelp");
    qmlRegisterType<QmlSyntaxTokenId>                    (uri, maj, min, "SyntaxTokenId");
    qmlRegisterType<ObjectHelp>                          (uri, maj, min, "ObjectHelp");
    qmlRegisterType<PropertyHelp>                        (uri, maj, min, "PropertyHelp");
    qmlRegisterType<ArgumentHelp>                        (uri, maj, min, "ArgumentHelp");
    qmlRegisterType<EnumKeyHelp>                         (uri, maj, min, "EnumKeyHelp");
    qmlRegisterType<MemoryModelItem>                     (uri, maj, min, "MemoryModelItem");
    // should not be instanciated but are provied as base class or grouped properties in user API
    qmlRegisterType<AbstractIO>                          (uri, maj, min, "AbstractIO");
    qmlRegisterType<AbstractAnalogIO>                    (uri, maj, min, "AbstractAnalogIO");
    qmlRegisterType<AbstractDigitalIO>                   (uri, maj, min, "AbstractDigitalIO");
    qmlRegisterType<AbstractSensor>                      (uri, maj, min, "AbstractSensor");
    qmlRegisterType<AbstractActuator>                    (uri, maj, min, "AbstractActuator");
    qmlRegisterType<AbstractRoutine>                     (uri, maj, min, "AbstractRoutine");
    qmlRegisterType<AbstractTransformer>                 (uri, maj, min, "AbstractTransformer");
    qmlRegisterType<AbstractLink>                        (uri, maj, min, "AbstractLink");
    qmlRegisterType<BasicObject>                         (uri, maj, min, "BasicObject");
    qmlRegisterType<Memory>                              (uri, maj, min, "Memory");
    qmlRegisterType<PhysicalWorld>                       (uri, maj, min, "PhysicalWorld");
    qmlRegisterType<QObject>                             (uri, maj, min, "ObjectRefListModel");
#endif
    // can be created in QML in UI
    qmlRegisterType<RendererItem>                        (uri, maj, min, "RendererItem");
    qmlRegisterType<Grapher>                             (uri, maj, min, "Grapher");
    qmlRegisterType<SyntaxHighlighter>                   (uri, maj, min, "SyntaxHighlighter");
    // can be created in QML by user (API)
    qmlRegisterType<AnalogActuator>                      (uri, maj, min, "AnalogActuator");
    qmlRegisterType<AnalogInput>                         (uri, maj, min, "AnalogInput");
    qmlRegisterType<AnalogOutput>                        (uri, maj, min, "AnalogOutput");
    qmlRegisterType<AnalogSensor>                        (uri, maj, min, "AnalogSensor");
    qmlRegisterType<Board>                               (uri, maj, min, "Board");
    qmlRegisterType<Dashboard>                           (uri, maj, min, "Dashboard");
    qmlRegisterType<DigitalActuator>                     (uri, maj, min, "DigitalActuator");
    qmlRegisterType<DigitalInput>                        (uri, maj, min, "DigitalInput");
    qmlRegisterType<DigitalOutput>                       (uri, maj, min, "DigitalOutput");
    qmlRegisterType<DigitalSensor>                       (uri, maj, min, "DigitalSensor");
    qmlRegisterType<HybridActuator>                      (uri, maj, min, "HybridActuator");
    qmlRegisterType<HybridSensor>                        (uri, maj, min, "HybridSensor");
    qmlRegisterType<LinkAnalogActuatorToPhysicalValue>   (uri, maj, min, "LinkAnalogActuatorToPhysicalValue");
    qmlRegisterType<LinkAnalogOutputToAnalogActuator>    (uri, maj, min, "LinkAnalogOutputToAnalogActuator");
    qmlRegisterType<LinkAnalogSensorToAnalogInput>       (uri, maj, min, "LinkAnalogSensorToAnalogInput");
    qmlRegisterType<LinkDigitalOutputToDigitalActuator>  (uri, maj, min, "LinkDigitalOutputToDigitalActuator");
    qmlRegisterType<LinkDigitalSensorToDigitalInput>     (uri, maj, min, "LinkDigitalSensorToDigitalInput");
    qmlRegisterType<LinkPhysicalValueToAnalogSensor>     (uri, maj, min, "LinkPhysicalValueToAnalogSensor");
    qmlRegisterType<LinkAnalogOutputToAnalogInput>       (uri, maj, min, "LinkAnalogOutputToAnalogInput");
    qmlRegisterType<LinkDigitalOutputToDigitalInput>     (uri, maj, min, "LinkDigitalOutputToDigitalInput");
    qmlRegisterType<LinkPhysicalValueToHybridSensor>     (uri, maj, min, "LinkPhysicalValueToHybridSensor");
    qmlRegisterType<LinkHybridSensorToDigitalInput>      (uri, maj, min, "LinkHybridSensorToDigitalInput");
    qmlRegisterType<LinkDigitalOutputToHybridActuator>   (uri, maj, min, "LinkDigitalOutputToHybridActuator");
    qmlRegisterType<LinkHybridActuatorToPhysicalValue>   (uri, maj, min, "LinkHybridActuatorToPhysicalValue");
    qmlRegisterType<CanBus>                              (uri, maj, min, "CanBus");
    qmlRegisterType<CanOpen>                             (uri, maj, min, "CanOpen");
    qmlRegisterType<NetworkDefinition>                   (uri, maj, min, "NetworkDefinition");
    qmlRegisterType<Node>                                (uri, maj, min, "Node");
    qmlRegisterType<ObjectsGroup>                        (uri, maj, min, "ObjectsGroup");
    qmlRegisterType<PhysicalPoint>                       (uri, maj, min, "PhysicalPoint");
    qmlRegisterType<PhysicalSize>                        (uri, maj, min, "PhysicalSize");
    qmlRegisterType<PhysicalAngle>                       (uri, maj, min, "PhysicalAngle");
    qmlRegisterType<PhysicalBlock>                       (uri, maj, min, "PhysicalBlock");
    qmlRegisterType<PhysicalValue>                       (uri, maj, min, "PhysicalValue");
    qmlRegisterType<PhysicalMarker>                      (uri, maj, min, "PhysicalMarker");
    qmlRegisterType<RoutineOnCanFrame>                   (uri, maj, min, "RoutineOnCanFrame");
    qmlRegisterType<RoutineOnSerialFrame>                (uri, maj, min, "RoutineOnSerialFrame");
    qmlRegisterType<RoutineOnEvent>                      (uri, maj, min, "RoutineOnEvent");
    qmlRegisterType<RoutineOnTimer>                      (uri, maj, min, "RoutineOnTimer");
    qmlRegisterType<RoutineOnCanOpenStateChange>         (uri, maj, min, "RoutineOnCanOpenStateChange");
    qmlRegisterType<RoutineOnCanOpenObdValChange>        (uri, maj, min, "RoutineOnCanOpenObdValChange");
    qmlRegisterType<RoutineOnCanOpenSdoReadReply>        (uri, maj, min, "RoutineOnCanOpenSdoReadReply");
    qmlRegisterType<RoutineOnCanOpenSdoWriteRequest>     (uri, maj, min, "RoutineOnCanOpenSdoWriteRequest");
    qmlRegisterType<RoutineOnCanOpenBootUp>              (uri, maj, min, "RoutineOnCanOpenBootUp");
    qmlRegisterType<RoutineOnCanOpenHeartbeatConsumer>   (uri, maj, min, "RoutineOnCanOpenHeartbeatConsumer");
    qmlRegisterType<SerialBus>                           (uri, maj, min, "SerialBus");
    qmlRegisterType<AffineTransformer>                   (uri, maj, min, "AffineTransformer");
    qmlRegisterType<CustomTransformer>                   (uri, maj, min, "CustomTransformer");
}

QObject * SharedObject::qmlSingletonProvider (QQmlEngine * qmlEngine, QJSEngine * jsEngine) {
    Q_UNUSED (qmlEngine)
    Q_UNUSED (jsEngine)
    return new SharedObject;
}

qreal SharedObject::intToFloat (const int value, const int decimals) const {
    return qreal (qreal (value) / qreal (qPow (10, decimals)));
}

int SharedObject::floatToInt (const qreal value, const int decimals) const {
    return int (value * int (qPow (10, decimals)));
}

QString SharedObject::format (const int value, const int decimals, const QString unit) const {
    const QString tmp = QString::number (intToFloat (value, decimals), 'f', decimals);
    return (!unit.isEmpty () ? tmp % ' ' % unit : tmp);
}

void SharedObject::highlightLinkedObject (BasicObject * object) {
    if (object) {
        switch (object->get_family ()) {
            case ObjectFamily::IO: {
                set_highlightIO (Q_NULLPTR);
                set_highlightIO (object->as<AbstractIO> ());
                break;
            }
            case ObjectFamily::VALUE: {
                set_highlightPhyVal (Q_NULLPTR);
                set_highlightPhyVal (object->as<PhysicalValue> ());
                break;
            }
            case ObjectFamily::SENSOR: {
                set_highlightSensor (Q_NULLPTR);
                set_highlightSensor (object->as<AbstractSensor> ());
                break;
            }
            case ObjectFamily::ACTUATOR: {
                set_highlightActuator (Q_NULLPTR);
                set_highlightActuator (object->as<AbstractActuator> ());
                break;
            }
            default: break;
        }
    }
}

void SharedObject::addMru (const QString & filePath) {
    QStringList tmp = m_settings.value ("mru").toStringList ();
    tmp.removeAll (filePath);
    tmp.prepend (filePath);
    m_settings.setValue ("mru", QVariant::fromValue (tmp));
    m_settings.sync ();
    m_mruModel->clear ();
    m_mruModel->appendList (qListToVariant<QString> (tmp));
}

void SharedObject::removeMru (const QString & filePath) {
    QStringList tmp = m_settings.value ("mru").toStringList ();
    tmp.removeAll (filePath);
    m_settings.setValue ("mru", QVariant::fromValue (tmp));
    m_settings.sync ();
    m_mruModel->clear ();
    m_mruModel->appendList (qListToVariant<QString> (tmp));
}

void SharedObject::onRememberThemeChanged (void) {
    m_settings.setValue ("rememberTheme", (m_rememberTheme ? 1 : 0));
    m_settings.sync ();
}

void SharedObject::onLastFolderUsedChanged (void) {
    m_settings.setValue ("lastFolderUsed", m_lastFolderUsed);
    m_settings.sync ();
}
