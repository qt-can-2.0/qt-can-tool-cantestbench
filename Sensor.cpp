
#include "Sensor.h"

#include "IO.h"
#include "Link.h"
#include "Physics.h"
#include "MathUtils.h"
#include "Manager.h"

AbstractSensor::AbstractSensor (const ObjectType::Type type, QObject * parent)
    : BasicObject (ObjectFamily::SENSOR, parent)
    , m_type (type)
{ }

AbstractSensor::~AbstractSensor (void) { }

void AbstractSensor::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
}

AnalogSensor::AnalogSensor (QObject * parent)
    : AbstractSensor (ObjectType::ANALOG, parent)
    , m_inverted (false)
    , m_bypassScale (false)
    , m_valPhy (0)
    , m_minPhy (0)
    , m_maxPhy (1)
    , m_minRaw (0)
    , m_maxRaw (5000)
    , m_valRaw (0)
    , m_percents (0)
    , m_decimals (0)
    , m_useSplitPoint (false)
    , m_upperMargin (0)
    , m_middleMargin (0)
    , m_lowerMargin (0)
    , m_sourceLink (Q_NULLPTR)
    , m_targetLinks (this)
{
    connect (this, &AnalogSensor::valPhyChanged,        this, &AnalogSensor::refreshValRaw);
    connect (this, &AnalogSensor::minPhyChanged,        this, &AnalogSensor::refreshValRaw);
    connect (this, &AnalogSensor::maxPhyChanged,        this, &AnalogSensor::refreshValRaw);
    connect (this, &AnalogSensor::valRawChanged,        this, &AnalogSensor::refreshValPhy);
    connect (this, &AnalogSensor::minRawChanged,        this, &AnalogSensor::refreshValPhy);
    connect (this, &AnalogSensor::maxRawChanged,        this, &AnalogSensor::refreshValPhy);
    connect (this, &AnalogSensor::valRawChanged,        this, &AnalogSensor::refreshPercents);
    connect (this, &AnalogSensor::useSplitPointChanged, this, &AnalogSensor::refreshPercents);
    connect (this, &AnalogSensor::upperMarginChanged,   this, &AnalogSensor::refreshPercents);
    connect (this, &AnalogSensor::middleMarginChanged,  this, &AnalogSensor::refreshPercents);
    connect (this, &AnalogSensor::lowerMarginChanged,   this, &AnalogSensor::refreshPercents);
    Manager::instance ().registerObject (this);
}

AnalogSensor::~AnalogSensor (void) {
    Manager::instance ().unregisterObject (this);
}

void AnalogSensor::onComponentCompleted (void) {
    AbstractSensor::onComponentCompleted ();
    if (m_sourceLink) {
        m_sourceLink->set_target (this);
    }
    for (LinkAnalogSensorToAnalogInput * link : m_targetLinks) {
        link->set_source (this);
    }
    Manager::instance ().intializeObject (this);
}

void AnalogSensor::refreshValRaw (void) {
    if (!m_inverted && !m_bypassScale) {
        update_valRaw (MathUtils::convert (m_valPhy, m_minPhy, m_maxPhy, m_minRaw, m_maxRaw));
    }
}

void AnalogSensor::refreshValPhy (void) {
    if (m_inverted && !m_bypassScale) {
        set_valPhy (MathUtils::convert (m_valRaw, m_minRaw, m_maxRaw, m_minPhy, m_maxPhy));
    }
}

void AnalogSensor::refreshPercents (void) {
    if (m_useSplitPoint) {
        const int m_midRaw = (m_maxRaw + m_minRaw) / 2;
        if (m_valRaw <= m_minRaw + m_lowerMargin) { // lower limit
            update_percents (-100);
        }
        else if (m_valRaw >= m_maxRaw - m_upperMargin) { // upper limit
            update_percents (+100);
        }
        else if (m_valRaw > m_midRaw + m_middleMargin) { // positive value
            update_percents (MathUtils::convert (m_valRaw, m_midRaw + m_middleMargin, m_maxRaw - m_upperMargin, 0, +100));
        }
        else if (m_valRaw < m_midRaw - m_middleMargin) { // negative value
            update_percents (MathUtils::convert (m_valRaw, m_midRaw - m_middleMargin, m_minRaw + m_lowerMargin, 0, -100));
        }
        else { // dead band
            update_percents (0);
        }
    }
    else {
        update_percents (MathUtils::convert (m_valPhy, m_minPhy, m_maxPhy, 0, 100));
    }
}

QJsonObject AnalogSensor::exportState (void) const {
    return QJsonObject {
        { "valPhy", m_valPhy },
    };
}

DigitalSensor::DigitalSensor (QObject * parent)
    : AbstractSensor (ObjectType::DIGITAL, parent)
    , m_value (false)
    , m_trueLabel ("ON")
    , m_falseLabel ("OFF")
    , m_targetLinks (this)
{
    Manager::instance ().registerObject (this);
}

DigitalSensor::~DigitalSensor (void) {
    Manager::instance ().unregisterObject (this);
}

QJsonObject DigitalSensor::exportState (void) const {
    return QJsonObject {
        { "value", m_value },
    };
}

void DigitalSensor::onComponentCompleted (void) {
    AbstractSensor::onComponentCompleted ();
    for (LinkDigitalSensorToDigitalInput * link : m_targetLinks) {
        link->set_source (this);
    }
    Manager::instance ().intializeObject (this);
}

HybridSensor::HybridSensor (QObject * parent)
    : AbstractSensor (ObjectType::HYBRID, parent)
    , m_disabled (false)
    , m_valPhy (0)
    , m_lowThresholdPhy (0)
    , m_highThresholdPhy (1)
    , m_invertState (false)
    , m_valState (false)
    , m_decimals (0)
    , m_unit ("")
    , m_sourceLink (Q_NULLPTR)
    , m_targetLinks (this)
{
    connect (this, &HybridSensor::valPhyChanged,           this, &HybridSensor::refreshValState);
    connect (this, &HybridSensor::lowThresholdPhyChanged,  this, &HybridSensor::refreshValState);
    connect (this, &HybridSensor::highThresholdPhyChanged, this, &HybridSensor::refreshValState);
    connect (this, &HybridSensor::invertStateChanged,      this, &HybridSensor::refreshValState);
    Manager::instance ().registerObject (this);
}

HybridSensor::~HybridSensor (void) {
    Manager::instance ().unregisterObject (this);
}

void HybridSensor::onComponentCompleted (void) {
    AbstractSensor::onComponentCompleted ();
    if (m_sourceLink) {
        m_sourceLink->set_target (this);
    }
    for (LinkHybridSensorToDigitalInput * link : m_targetLinks) {
        link->set_source (this);
    }
    Manager::instance ().intializeObject (this);
}

void HybridSensor::refreshValState (void) {
    if (m_valPhy >= m_highThresholdPhy) {
        update_valState (m_invertState ? false : true);
    }
    else if (m_valPhy <= m_lowThresholdPhy) {
        update_valState (m_invertState ? true : false);
    }
    else { }
}
