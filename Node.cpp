
#include "Node.h"

#include "IO.h"
#include "Routine.h"
#include "CanOpen.h"
#include "Manager.h"
#include "Memory.h"
#include "CanDriverQmlWrapper.h"

Board::Board (QObject * parent)
    : BasicObject (ObjectFamily::BOARD, parent)
    , m_subObjects (this)
{
    Manager::instance ().registerObject (this);
}

Board::~Board (void) {
    Manager::instance ().unregisterObject (this);
}

void Board::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    for (QObject * object : m_subObjects) {
        if (AbstractIO * io = qobject_cast<AbstractIO *> (object)) {
            m_ios.append (io);
        }
        else { }
    }
    Manager::instance ().intializeObject (this);
}

Node::Node (QObject * parent)
    : BasicObject (ObjectFamily::NODE, parent)
    , m_subObjects (this)
    , m_mode (Modes::ACTIVE)
{
    m_memory = new Memory (this);
    Manager::instance ().registerObject (this);
}

Node::~Node (void) {
    Manager::instance ().unregisterObject (this);
}

void Node::onComponentCompleted (void) {
    BasicObject::onComponentCompleted ();
    for (QObject * object : m_subObjects) {
        if (Board * board = qobject_cast<Board *> (object)) {
            m_boards.append (board);
        }
        else if (AbstractRoutine * routine = qobject_cast<AbstractRoutine *> (object)) {
            m_routines.append (routine);
        }
        else { }
    }
    Manager::instance ().intializeObject (this);
}

void Node::init (void) {
    if (m_memory) {
        m_memory->initialize ();
    }
}

void Node::start (void) {
    emit started ();
}

void Node::stop (void) {
    emit stopped ();
}

void Node::reset (void) {
    m_memory->reset ();
}
