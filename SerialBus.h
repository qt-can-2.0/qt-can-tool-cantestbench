#ifndef SERIALBUS_H
#define SERIALBUS_H

#include <QObject>
#include <QPointer>
#include <QSerialPort>
#include <QVariantList>
#include <QQmlParserStatus>

#include "QQmlVarPropertyHelpers.h"
#include "QQmlPtrPropertyHelpers.h"
#include "QQmlConstRefPropertyHelpers.h"

#include "TypesQmlWrapper.h"
#include "IdentifiableObject.h"

class RoutineOnSerialFrame;

class SerialBus : public BasicObject {
    Q_OBJECT
    QML_READONLY_VAR_PROPERTY (bool, portLoaded)
    QML_READONLY_CSTREF_PROPERTY (QString, portName)
    QML_READONLY_CSTREF_PROPERTY (QVariantList, portsList)

public:
    explicit SerialBus (QObject * parent = Q_NULLPTR);
    virtual ~SerialBus (void);

    void onComponentCompleted (void) Q_DECL_FINAL;

    void subscribeRoutineToSerialFrame (RoutineOnSerialFrame * routine);

    Q_INVOKABLE bool init  (const QString & portName, const int baudrate, const int dataBits, const int stopBits, const int parity);
    Q_INVOKABLE bool stop  (void);

    Q_INVOKABLE int  count (void);

    Q_INVOKABLE void writeBool   (const bool          value);
    Q_INVOKABLE void writeInt8   (const QmlBiggestInt value);
    Q_INVOKABLE void writeInt16  (const QmlBiggestInt value);
    Q_INVOKABLE void writeInt32  (const QmlBiggestInt value);
    Q_INVOKABLE void writeUInt8  (const QmlBiggestInt value);
    Q_INVOKABLE void writeUInt16 (const QmlBiggestInt value);
    Q_INVOKABLE void writeUInt32 (const QmlBiggestInt value);
    Q_INVOKABLE void writeString (const QString &     value, const int len);

    Q_INVOKABLE bool          readBool   (void);
    Q_INVOKABLE QmlBiggestInt readInt8   (void);
    Q_INVOKABLE QmlBiggestInt readInt16  (void);
    Q_INVOKABLE QmlBiggestInt readInt32  (void);
    Q_INVOKABLE QmlBiggestInt readUInt8  (void);
    Q_INVOKABLE QmlBiggestInt readUInt16 (void);
    Q_INVOKABLE QmlBiggestInt readUInt32 (void);
    Q_INVOKABLE QString       readString (const int len);

    template<typename T> void writeAs (const T value) {
        const int size = sizeof (T);
        if (m_serialPort && m_serialPort->isWritable ()) {
            m_serialPort->write (reinterpret_cast<const char *> (&value), size);
        }
    }

    template<typename T> T readAs (void) {
        T ret = false;
        const int size = sizeof (T);
        if (m_serialPort && m_serialPort->bytesAvailable () >= size) {
            m_serialPort->read (reinterpret_cast<char *> (&ret), size);
        }
        return ret;
    }

signals:
    void started (void);
    void stopped (void);
    void error   (const QString & msg);

protected:
    void onDataReady (void);
    void onTimer     (void);

private:
    QSerialPort * m_serialPort;
    QPointer<RoutineOnSerialFrame> m_routineForSerialFrame;
};

#endif // SERIALBUS_H
